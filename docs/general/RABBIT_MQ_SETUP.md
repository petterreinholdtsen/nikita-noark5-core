# Setting up and using rabbitmq with nikita

In some cases you can use rabbitmq for additional functionality associated with nikita. These can be acticated using the
following profiles:

* mailintegration
* eventhandling

*mailintegration* allows you to send messages to a rabbitmq queue with information about an email that should be sent.
It creates a (nikita) email object that includes information about where documents are stored and submits that object to
a queue called `nikita-outgoing-email`. *eventhandling* is more generic and can be used to inform other services about
CRUD events that happen in nikita. It sends messages to a queue called `noark-crud-reporting`. It is currently
used to in a demonstrator showing how to expose recordkeeping information to a blockchain, but likely will have other
relevant use-cases.

If you do not require this functionality simply avoid starting nikita with the above profiles. The functionality
requires a working rabbitmq. At OsloMet we use docker compose to start and configure the rabbitmq service.

_Before continuing take a look at the application-*profile*.yml files and make sure you see what the properties these
files contain are called_.

## Setting up rabbitmq in docker

You need to create the following files:

* definitions.json
* docker-compose.yaml
* rabbitmq.conf
* Dockerfile

After you have created these files run the following docker commands:

* docker-compose build
* docker-compose up

### definitions.json

Here we create two users. 1. An administrator called *nikita-rabbitmq-admin* and a general user called *nikita-queues*.
The password for the nikita-queues user is *nikitaRabbitMQpass*, while the password for the admin user is
*adminPassword*. These should be changed to more secure values when setting up nikita
yourself.

```
{
  "rabbit_version": "3.12.13",
  "rabbitmq_version": "3.12.13",
  "product_name": "RabbitMQ",
  "product_version": "3.12.13",
  "users": [
    {
      "name": "nikita-queues",
      "password_hash": "1RAe9mPIrfjz06bnKccbm8In/c42FtbgKStgDolYd0+Ps27U",
      "hashing_algorithm": "rabbit_password_hashing_sha256",
      "tags": "administrator"
    },
    {
      "name": "nikita-rabbitmq-admin",
      "password_hash": "7nmGlK9gXtl+2pxQmnAVKkFrXiZqFdY1GN6aKz+yJclH/YsU",
      "hashing_algorithm": "rabbit_password_hashing_sha256",
      "tags": "administrator"
    }
  ],
  "vhosts": [
    {
      "name": "nikita-vhost"
    }
  ],
  "permissions": [
    {
      "user": "nikita-queues",
      "vhost": "nikita-vhost",
      "configure": ".*",
      "write": ".*",
      "read": ".*"
    }
  ],
  "topic_permissions": [],
  "parameters": [],
  "global_parameters": [
    {
      "name": "cluster_name",
      "value": "nikita-rabbit-cluster"
    },
    {
      "name": "internal_cluster_id",
      "value": "nikita-rabbit-cluster-id"
    }
  ],
  "policies": [],
  "queues": [
    {
      "name": "noark-crud-reporting",
      "vhost": "nikita-vhost",
      "durable": true,
      "auto_delete": false,
      "arguments": {
      }
    },
    {
      "name": "nikita-outgoing-email",
      "vhost": "nikita-vhost",
      "durable": true,
      "auto_delete": false,
      "arguments": {
      }
    }
  ],
  "exchanges": [],
  "bindings": [
    {
      "source": "amq.direct",
      "vhost": "nikita-vhost",
      "destination": "noark-crud-reporting",
      "destination_type": "queue",
      "routing_key": "noark-crud-reporting",
      "arguments": {}
    },
    {
      "source": "amq.direct",
      "vhost": "nikita-vhost",
      "destination": "nikita-outgoing-email",
      "destination_type": "queue",
      "routing_key": "nikita-outgoing-email",
      "arguments": {}
    }
  ]
}
```

### docker-compose.yaml

Create a docker-compose.yaml with the following contents:

```
version: '3'

services:
  nikita-rabbitmq-message-broker:
    build:
      context: .
      dockerfile: Dockerfile
    container_name: nikita-rabbitmq
    ports:
      - "5682:5682"
      - "15682:15682"
    networks:
      - rabbitq_mq_net

networks:
    rabbitq_mq_net:
      driver: bridge
```

### rabbitmq.conf

```
default_user = nikita-queues
default_pass = nikitaRabbitMQpass

listeners.tcp.default = 5682
management.tcp.port = 15682

management.load_definitions = /etc/rabbitmq/definitions.json
```

### Dockerfile

```
FROM rabbitmq:3.12.13-management

COPY rabbitmq.conf /etc/rabbitmq
COPY definitions.json /etc/rabbitmq

RUN cat /etc/rabbitmq/rabbitmq.conf
```

### Generating a password for rabbitmq users

To create a rabbitmq password you can use the following bash function after it has been defined. Remember to source your
.basrhc file. See \[1\] for source of the bach function.

```
#!/bin/bash

function encode_password()
{
    SALT=$(od -A n -t x -N 4 /dev/urandom)
    PASS=$SALT$(echo -n $1 | xxd -ps | tr -d '\n' | tr -d ' ')
    PASS=$(echo -n $PASS | xxd -r -p | sha256sum | head -c 128)
    PASS=$(echo -n $SALT$PASS | xxd -r -p | base64 | tr -d '\n')
    echo $PASS
}

```

You can then create a password using the following bach function

```
encode_password "your password"
```

The above bash function is taken from:

\[1\] https://stackoverflow.com/questions/41306350/how-to-generate-password-hash-for-rabbitmq-management-http-api

The above thread shows others methods for creating a password.
