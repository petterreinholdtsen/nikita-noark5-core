package app.controller.noark5.secondary;

import app.domain.noark5.secondary.Author;
import app.service.interfaces.secondary.IAuthorService;
import app.webapp.exceptions.NikitaException;
import app.webapp.payload.links.secondary.AuthorLinks;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

import static app.utils.constants.Constants.*;
import static app.utils.constants.HATEOASConstants.*;
import static app.utils.constants.N5ResourceMappings.*;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = HREF_BASE_FONDS_STRUCTURE + SLASH,
        produces = NOARK5_V5_CONTENT_TYPE_JSON)
public class AuthorController {

    private final IAuthorService authorService;

    public AuthorController(IAuthorService authorService) {
        this.authorService = authorService;
    }

    // API - All GET Requests (CRUD - READ)

    // GET [contextPath][api]/arkivstruktur/forfatter/{systemId}
    @Operation(summary = "Retrieves a single Author entity given a systemId")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "Author returned"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @GetMapping(value = AUTHOR + SLASH + SYSTEM_ID_PARAMETER)
    public ResponseEntity<AuthorLinks> findAuthorBySystemId(
            @Parameter(name = SYSTEM_ID,
                    description = "systemID of the Author to retrieve",
                    required = true)
            @PathVariable(SYSTEM_ID) final UUID systemId) {
        AuthorLinks authorLinks = authorService.findBySystemId(systemId);
        return ResponseEntity.status(OK)
                .body(authorLinks);
    }

    // PUT [contextPath][api]/arkivstruktur/forfatter/{systemId}
    @Operation(summary = "Updates a Author identified by a given systemId",
            description = "Returns the newly updated nationalIdentifierPerson")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "Author " +
                            API_MESSAGE_OBJECT_ALREADY_PERSISTED),
            @ApiResponse(
                    responseCode = CREATED_VAL,
                    description = "Author " +
                            API_MESSAGE_OBJECT_SUCCESSFULLY_CREATED),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = NOT_FOUND_VAL,
                    description = API_MESSAGE_PARENT_DOES_NOT_EXIST +
                            " of type Author"),
            @ApiResponse(
                    responseCode = CONFLICT_VAL,
                    description = API_MESSAGE_CONFLICT),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @PutMapping(value = AUTHOR + SLASH + SYSTEM_ID_PARAMETER,
            consumes = NOARK5_V5_CONTENT_TYPE_JSON)
    public ResponseEntity<AuthorLinks> updateAuthorBySystemId(
            @Parameter(name = SYSTEM_ID,
                    description = "systemId of Author to update",
                    required = true)
            @PathVariable(SYSTEM_ID) final UUID systemID,
            @Parameter(name = "Author",
                    description = "Incoming Author object",
                    required = true)
            @RequestBody Author author) throws NikitaException {
        AuthorLinks authorLinks =
                authorService.updateAuthorBySystemId
                        (systemID, author);
        return ResponseEntity.status(OK)
                .body(authorLinks);
    }

    // DELETE [contextPath][api]/arkivstruktur/forfatter/{systemID}/
    @Operation(
            summary = "Deletes a single Author entity identified by systemID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = NO_CONTENT_VAL,
                    description = "Author deleted"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @DeleteMapping(value = AUTHOR + SLASH + SYSTEM_ID_PARAMETER)
    public ResponseEntity<String> deleteAuthorBySystemId(
            @Parameter(name = SYSTEM_ID,
                    description = "systemID of the author to delete",
                    required = true)
            @PathVariable(SYSTEM_ID) final UUID systemID) {
        authorService.deleteAuthorBySystemId(systemID);
        return ResponseEntity.status(NO_CONTENT)
                .body(DELETE_RESPONSE);
    }
}
