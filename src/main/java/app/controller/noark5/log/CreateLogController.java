package app.controller.noark5.log;

import app.domain.noark5.admin.CreateLog;
import app.service.interfaces.admin.ICreateLogService;
import app.webapp.exceptions.NikitaException;
import app.webapp.payload.links.admin.CreateLogLinks;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

import static app.utils.constants.Constants.*;
import static app.utils.constants.HATEOASConstants.*;
import static app.utils.constants.N5ResourceMappings.*;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = HREF_BASE_LOGGING + SLASH,
        produces = NOARK5_V5_CONTENT_TYPE_JSON)
public class CreateLogController {

    private final ICreateLogService createLogService;

    public CreateLogController(ICreateLogService createLogService) {
        this.createLogService = createLogService;
    }

    // GET [contextPath][api]/loggingogsporing/leselogg/
    @Operation(
            summary = "Retrieves all CreateLog entities limited by ownership " +
                    " rights")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "CreateLog found"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)
    })
    @GetMapping(CREATE_LOG)
    public ResponseEntity<CreateLogLinks> findAllCreateLog() {
        CreateLogLinks createLogLinks = createLogService.
                findCreateLogByOwner();
        return ResponseEntity.status(OK)
                .body(createLogLinks);
    }

    // GET [contextPath][api]/loggingogsporing/leselogg/{systemId}/
    @Operation(summary = "Retrieves a single createLog entity given a systemId")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "CreateLog returned"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @GetMapping(value = CREATE_LOG + SLASH + SYSTEM_ID_PARAMETER)
    public ResponseEntity<CreateLogLinks> findOne(
            @Parameter(name = SYSTEM_ID,
                    description = "systemId of createLog to retrieve.",
                    required = true)
            @PathVariable(SYSTEM_ID) final UUID systemID) {
        CreateLogLinks createLogLinks =
                createLogService.findSingleCreateLog(systemID);
        return ResponseEntity.status(OK)
                .body(createLogLinks);
    }

    // PUT [contextPath][api]/loggingogsporing/leselogg/{systemId}/
    @Operation(
            summary = "Updates a CreateLog object",
            description = "Returns the newly updated CreateLog object after " +
                    "it is persisted to the database")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "CreateLog " +
                            API_MESSAGE_OBJECT_ALREADY_PERSISTED),
            @ApiResponse(
                    responseCode = CREATED_VAL,
                    description = "CreateLog " +
                            API_MESSAGE_OBJECT_SUCCESSFULLY_CREATED),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = NOT_FOUND_VAL,
                    description = API_MESSAGE_PARENT_DOES_NOT_EXIST +
                            " of type CreateLog"),
            @ApiResponse(
                    responseCode = CONFLICT_VAL,
                    description = API_MESSAGE_CONFLICT),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @PutMapping(value = CREATE_LOG + SLASH + SYSTEM_ID_PARAMETER,
            consumes = NOARK5_V5_CONTENT_TYPE_JSON)
    public ResponseEntity<CreateLogLinks> updateCreateLog(
            @Parameter(name = SYSTEM_ID,
                    description = "systemId of createLog to update.",
                    required = true)
            @PathVariable(SYSTEM_ID) UUID systemID,
            @Parameter(name = "createLog",
                    description = "Incoming createLog object",
                    required = true)
            @RequestBody CreateLog createLog) throws NikitaException {
        CreateLogLinks createLogLinks =
                createLogService.handleUpdate(systemID, createLog);
        return ResponseEntity.status(OK)
                .body(createLogLinks);
    }

    // DELETE [contextPath][api]/loggingogsporing/leselogg/{systemId}/
    @Operation(
            summary = "Deletes a single CreateLog entity identified by " +
                    "systemID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = NO_CONTENT_VAL,
                    description = "ok message"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @DeleteMapping(value = CREATE_LOG + SLASH + SYSTEM_ID_PARAMETER)
    public ResponseEntity<String> deleteCreateLogBySystemId(
            @Parameter(name = SYSTEM_ID,
                    description = "systemID of the createLog to delete",
                    required = true)
            @PathVariable(SYSTEM_ID) final UUID systemID) {
        createLogService.deleteEntity(systemID);
        return ResponseEntity.status(HttpStatus.NO_CONTENT)
                .body(DELETE_RESPONSE);
    }
}
