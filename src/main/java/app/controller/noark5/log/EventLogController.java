package app.controller.noark5.log;

import app.domain.noark5.EventLog;
import app.service.interfaces.IEventLogService;
import app.webapp.exceptions.NikitaException;
import app.webapp.payload.links.EventLogLinks;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

import static app.utils.constants.Constants.*;
import static app.utils.constants.HATEOASConstants.*;
import static app.utils.constants.N5ResourceMappings.*;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = HREF_BASE_LOGGING + SLASH,
        produces = NOARK5_V5_CONTENT_TYPE_JSON)
public class EventLogController {

    private final IEventLogService eventLogService;

    public EventLogController(
            IEventLogService eventLogService) {
        this.eventLogService = eventLogService;
    }

    // GET [contextPath][api]/loggingogsporing/hendelseslogg/
    @Operation(
            summary = "Retrieves all EventLog entities limited by ownership " +
                    "rights")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "EventLog found"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)
    })
    @GetMapping(EVENT_LOG)
    public ResponseEntity<EventLogLinks> findAllEventLog() {
        EventLogLinks eventLogLinks = eventLogService.
                findEventLogByOwner();
        return ResponseEntity.status(OK)
                .body(eventLogLinks);
    }

    // GET [contextPath][api]/loggingogsporing/hendelseslogg/{systemId}/
    @Operation(summary = "Retrieves a single eventLog entity given a systemId")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "EventLog returned"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @GetMapping(value = EVENT_LOG + SLASH + SYSTEM_ID_PARAMETER)
    public ResponseEntity<EventLogLinks> findOne(
            @Parameter(name = SYSTEM_ID,
                    description = "systemID of eventLog to retrieve.",
                    required = true)
            @PathVariable(SYSTEM_ID) final UUID systemID) {
        EventLogLinks eventLogLinks =
                eventLogService.findSingleEventLog(systemID);
        return ResponseEntity.status(OK)
                .body(eventLogLinks);
    }

    // PUT [contextPath][api]/loggingogsporing/hendelseslogg/{systemId}/
    @Operation(
            summary = "Updates a EventLog object",
            description = "Returns the newly updated EventLog object after it" +
                    " is persisted to the database")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "EventLog " +
                            API_MESSAGE_OBJECT_ALREADY_PERSISTED),
            @ApiResponse(
                    responseCode = CREATED_VAL,
                    description = "EventLog " +
                            API_MESSAGE_OBJECT_SUCCESSFULLY_CREATED),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = NOT_FOUND_VAL,
                    description = API_MESSAGE_PARENT_DOES_NOT_EXIST +
                            " of type EventLog"),
            @ApiResponse(
                    responseCode = CONFLICT_VAL,
                    description = API_MESSAGE_CONFLICT),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @PutMapping(value = EVENT_LOG + SLASH + SYSTEM_ID_PARAMETER,
            consumes = NOARK5_V5_CONTENT_TYPE_JSON)
    public ResponseEntity<EventLogLinks> updateEventLog(
            @Parameter(name = SYSTEM_ID,
                    description = "systemID of eventLog to update.",
                    required = true)
            @PathVariable(SYSTEM_ID) UUID systemID,
            @Parameter(name = "eventLog",
                    description = "Incoming eventLog object",
                    required = true)
            @RequestBody EventLog eventLog) throws NikitaException {
        EventLogLinks eventLogLinks =
                eventLogService.handleUpdate(systemID,
                        eventLog);
        return ResponseEntity.status(OK)
                .body(eventLogLinks);
    }

    // DELETE [contextPath][api]/loggingogsporing/hendelseslogg/{systemId}/
    @Operation(
            summary = "Deletes a single EventLog entity identified by systemID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = NO_CONTENT_VAL,
                    description = "ok message"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @DeleteMapping(value = EVENT_LOG + SLASH + SYSTEM_ID_PARAMETER)
    public ResponseEntity<String> deleteEventLogBySystemId(
            @Parameter(name = SYSTEM_ID,
                    description = "systemID of the eventLog to delete",
                    required = true)
            @PathVariable(SYSTEM_ID) final UUID systemID) {
        eventLogService.deleteEntity(systemID);
        return ResponseEntity.status(NO_CONTENT)
                .body(DELETE_RESPONSE);
    }
}
