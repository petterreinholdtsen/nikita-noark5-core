package app.controller.noark5.log;

import app.domain.noark5.admin.ReadLog;
import app.service.interfaces.admin.IReadLogService;
import app.webapp.exceptions.NikitaException;
import app.webapp.payload.links.admin.ReadLogLinks;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

import static app.utils.constants.Constants.*;
import static app.utils.constants.HATEOASConstants.*;
import static app.utils.constants.N5ResourceMappings.*;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = HREF_BASE_LOGGING + SLASH,
        produces = NOARK5_V5_CONTENT_TYPE_JSON)
public class ReadLogController {

    private final IReadLogService readLogService;

    public ReadLogController(IReadLogService readLogService) {
        this.readLogService = readLogService;
    }

    // GET [contextPath][api]/loggingogsporing/leselogg/
    @Operation(
            summary = "Retrieves all ReadLog entities limited by ownership " +
                    " rights")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "ReadLog found"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)
    })
    @GetMapping(READ_LOG)
    public ResponseEntity<ReadLogLinks> findAllReadLog() {
        ReadLogLinks readLogLinks = readLogService.
                findReadLogByOwner();
        return ResponseEntity.status(OK)
                .body(readLogLinks);
    }

    // GET [contextPath][api]/loggingogsporing/leselogg/{systemId}/
    @Operation(summary = "Retrieves a single readLog entity given a systemId")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "ReadLog returned"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @GetMapping(value = READ_LOG + SLASH + SYSTEM_ID_PARAMETER)
    public ResponseEntity<ReadLogLinks> findOne(
            @Parameter(name = SYSTEM_ID,
                    description = "systemId of readLog to retrieve.",
                    required = true)
            @PathVariable(SYSTEM_ID) final UUID systemID) {
        ReadLogLinks readLogLinks =
                readLogService.findSingleReadLog(systemID);
        return ResponseEntity.status(OK)
                .body(readLogLinks);
    }

    // PUT [contextPath][api]/loggingogsporing/leselogg/{systemId}/
    @Operation(
            summary = "Updates a ReadLog object",
            description = "Returns the newly updated ReadLog object after " +
                    "it is persisted to the database")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "ReadLog " +
                            API_MESSAGE_OBJECT_ALREADY_PERSISTED),
            @ApiResponse(
                    responseCode = CREATED_VAL,
                    description = "ReadLog " +
                            API_MESSAGE_OBJECT_SUCCESSFULLY_CREATED),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = NOT_FOUND_VAL,
                    description = API_MESSAGE_PARENT_DOES_NOT_EXIST +
                            " of type ReadLog"),
            @ApiResponse(
                    responseCode = CONFLICT_VAL,
                    description = API_MESSAGE_CONFLICT),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @PutMapping(value = READ_LOG + SLASH + SYSTEM_ID_PARAMETER,
            consumes = NOARK5_V5_CONTENT_TYPE_JSON)
    public ResponseEntity<ReadLogLinks> updateReadLog(
            @Parameter(name = SYSTEM_ID,
                    description = "systemId of readLog to update.",
                    required = true)
            @PathVariable(SYSTEM_ID) UUID systemID,
            @Parameter(name = "readLog",
                    description = "Incoming readLog object",
                    required = true)
            @RequestBody ReadLog readLog) throws NikitaException {
        ReadLogLinks readLogLinks =
                readLogService.handleUpdate(systemID, readLog);
        return ResponseEntity.status(OK)
                .body(readLogLinks);
    }

    // DELETE [contextPath][api]/loggingogsporing/leselogg/{systemId}/
    @Operation(
            summary = "Deletes a single ReadLog entity identified by " +
                    "systemID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = NO_CONTENT_VAL,
                    description = "ok message"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @DeleteMapping(value = READ_LOG + SLASH + SYSTEM_ID_PARAMETER)
    public ResponseEntity<String> deleteReadLogBySystemId(
            @Parameter(name = SYSTEM_ID,
                    description = "systemID of the readLog to delete",
                    required = true)
            @PathVariable(SYSTEM_ID) final UUID systemID) {
        readLogService.deleteEntity(systemID);
        return ResponseEntity.status(HttpStatus.NO_CONTENT)
                .body(DELETE_RESPONSE);
    }
}
