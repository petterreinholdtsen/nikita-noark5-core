package app.controller.noark5;


import app.service.application.VendorService;
import app.webapp.model.*;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static app.utils.CommonUtils.WebUtils.getMethodsForRequestOrThrow;
import static app.utils.constants.Constants.*;
import static org.springframework.http.HttpStatus.OK;

/**
 * REST controller that returns information about the Noark 5 cores
 * conformity to standards.
 */
@RestController
@RequestMapping(produces = NOARK5_V5_CONTENT_TYPE_JSON)
public class ApplicationController {

    private final VendorService vendorService;

    public ApplicationController(VendorService vendorService) {
        this.vendorService = vendorService;
    }

    /**
     * identify the interfaces the core supports
     *
     * @return the application details along with the correct response code
     * (200 OK, or 500 Internal Error)
     */
    // API - All GET Requests (CRUD - READ)
    @GetMapping
    public ResponseEntity<ApplicationDetails> identify(
            HttpServletRequest request) {
        return ResponseEntity.status(OK)
                .allow(getMethodsForRequestOrThrow(request.getServletPath()))
                .body(vendorService.getApplicationDetails());
    }

    @GetMapping(value = HREF_SYSTEM_INFORMATION)
    public ResponseEntity<SystemInformation> getSystemInformation(
            HttpServletRequest request) {
        return ResponseEntity.status(OK)
                .allow(getMethodsForRequestOrThrow(request.getServletPath()))
                .body(vendorService.getSystemInformation());
    }

    @GetMapping(value = HREF_BASE_FONDS_STRUCTURE)
    public ResponseEntity<FondsStructureDetails> getFondsStructure(
            HttpServletRequest request) {
        return ResponseEntity.status(OK)
                .allow(getMethodsForRequestOrThrow(request.getServletPath()))
                .body(vendorService.getFondsStructureDetails());
    }

    @GetMapping(value = HREF_BASE_METADATA)
    public ResponseEntity<MetadataDetails> getMetadataPath(
            HttpServletRequest request) {
        return ResponseEntity.status(OK)
                .allow(getMethodsForRequestOrThrow(request.getServletPath()))
                .body(vendorService.getMetadataDetails());
    }

    @GetMapping(value = HREF_BASE_ADMIN)
    public ResponseEntity<AdministrationDetails> getAdminPath(
            HttpServletRequest request) {
        return ResponseEntity.status(OK)
                .allow(getMethodsForRequestOrThrow(request.getServletPath()))
                .body(vendorService.getAdministrationDetails());
    }

    @GetMapping(value = HREF_BASE_CASE_HANDLING)
    public ResponseEntity<CaseHandlingDetails> getCaseHandling(
            HttpServletRequest request) {
        return ResponseEntity.status(OK)
                .allow(getMethodsForRequestOrThrow(request.getServletPath()))
                .body(vendorService.getCaseHandlingDetails());
    }

    @GetMapping(value = HREF_BASE_LOGGING)
    public ResponseEntity<LoggingDetails> getLogging(
            HttpServletRequest request) {
        return ResponseEntity.status(OK)
                .allow(getMethodsForRequestOrThrow(request.getServletPath()))
                .body(vendorService.getLoggingDetails());
    }
}
