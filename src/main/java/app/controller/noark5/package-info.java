/**
 * This package contains all the HTTP endpoints the application controls
 * <p>
 * .: Controllers for the primary entities (Fonds etc)
 * casehandling: Controllers for the case-handling entities (Precedence etc).
 * secondary: Controllers for the secondary entities (Precedence etc).
 * admin: Controllers for all the secondary entities (Users, Roles etc)
 * metadata: Controllers for the metadata entities (RegistryEntryStatus etc)
 * log: Controllers for the logging entities (Events, Changes etc)
 * odata: An internal endpoint used to handle internal redirects. Will be removed when a better approach is found.
 * </p>
 *
 * @author tsodring
 * @version 0.7
 * @since 0.7
 */
package app.controller.noark5;