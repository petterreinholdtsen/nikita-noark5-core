package app.domain.annotation;

import app.webapp.payload.links.LinksNoarkObject;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({ANNOTATION_TYPE, TYPE})
@Retention(RUNTIME)
public @interface LinksObject {
    Class<? extends LinksNoarkObject> using()
            default LinksNoarkObject.class;
}
