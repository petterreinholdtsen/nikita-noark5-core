package app.domain.interfaces;

import app.domain.noark5.secondary.Classified;

/**
 * Created by tsodring on 12/7/16.
 */
public interface IClassified {
    Classified getReferenceClassified();

    void setReferenceClassified(Classified classified);
}
