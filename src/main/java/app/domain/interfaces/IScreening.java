package app.domain.interfaces;

import app.domain.noark5.secondary.Screening;

/**
 * Created by tsodring on 12/7/16.
 */
public interface IScreening {
    Screening getReferenceScreening();

    void setReferenceScreening(Screening screening);
}
