package app.domain.interfaces;

import app.domain.noark5.secondary.SignOff;

import java.util.Set;

public interface ISignOff {
    Set<SignOff> getReferenceSignOff();

    void addSignOff(SignOff signOff);
}
