package app.domain.interfaces;

import app.domain.noark5.secondary.Disposal;

/**
 * Created by tsodring on 12/7/16.
 */

public interface IDisposal {
    Disposal getReferenceDisposal();

    void setReferenceDisposal(Disposal disposal);

}
