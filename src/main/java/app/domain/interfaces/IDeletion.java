package app.domain.interfaces;

import app.domain.noark5.secondary.Deletion;

/**
 * Created by tsodring on 12/7/16.
 */
public interface IDeletion {
    Deletion getReferenceDeletion();

    void setReferenceDeletion(Deletion deletion);
}
