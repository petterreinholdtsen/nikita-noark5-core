package app.domain.interfaces.entities.nationalidentifier;

import app.domain.interfaces.entities.ISystemId;

public interface IBuildingEntity
        extends ISystemId {
    Integer getBuildingNumber();

    void setBuildingNumber(Integer buildingNumber);

    Integer getRunningChangeNumber();

    void setRunningChangeNumber(Integer runningChangeNumber);
}
