package app.domain.interfaces.entities.metadata;

import app.domain.interfaces.entities.IMetadataEntity;

public interface ICaseStatus extends IMetadataEntity {

    Boolean getDefaultCaseStatus();

    void setDefaultCaseStatus(Boolean defaultCaseStatus);
}
