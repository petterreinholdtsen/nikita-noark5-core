package app.domain.interfaces.entities;


public interface ITitleDescription {

    String getTitle();

    void setTitle(String title);

    String getDescription();

    void setDescription(String description);
}
