package app.domain.interfaces.entities.secondary;


import app.domain.noark5.casehandling.secondary.BusinessAddress;

/**
 * Created by tsodring on 31/03/19.
 */
public interface IBusinessAddress {

    BusinessAddress getBusinessAddress();

    void setBusinessAddress(BusinessAddress businessAddress);
}
