package app.domain.interfaces.entities.secondary;

import app.domain.interfaces.entities.ISystemId;
import app.domain.noark5.DocumentObject;
import app.domain.noark5.metadata.Format;

import java.time.OffsetDateTime;

// TODO check if this inheritence is ok.
public interface IConversionEntity
        extends ISystemId {

    OffsetDateTime getConvertedDate();

    void setConvertedDate(OffsetDateTime convertedDate);

    String getConvertedBy();

    void setConvertedBy(String convertedBy);

    Format getConvertedFromFormat();

    void setConvertedFromFormat(Format convertedFromFormat);

    Format getConverteLinksFormat();

    void setConverteLinksFormat(Format converteLinksFormat);

    String getConversionTool();

    void setConversionTool(String conversionTool);

    String getConversionComment();

    void setConversionComment(String conversionComment);

    DocumentObject getReferenceDocumentObject();

    void setReferenceDocumentObject(DocumentObject referenceDocumentObject);
}
