package app.domain.interfaces.entities.secondary;

/**
 * Created by tsodring on 5/22/17.
 */
public interface ICorrespondencePartUnitEntity
        extends IGenericUnitEntity, ICorrespondencePartEntity {
}
