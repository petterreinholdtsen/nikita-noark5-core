package app.domain.interfaces.entities.secondary;


import app.domain.noark5.casehandling.secondary.ResidingAddress;

/**
 * Created by tsodring on 31/03/19.
 */
public interface IResidingAddress {

    ResidingAddress getResidingAddress();

    void setResidingAddress(ResidingAddress ResidingAddress);
}
