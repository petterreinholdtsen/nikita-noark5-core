package app.domain.interfaces.entities.secondary;

import app.domain.noark5.casehandling.secondary.CorrespondencePart;

import java.util.List;

public interface ICorrespondencePart {
    List<CorrespondencePart> getReferenceCorrespondencePart();
    void addCorrespondencePart(CorrespondencePart part);

    void removeCorrespondencePart(CorrespondencePart part);
}
