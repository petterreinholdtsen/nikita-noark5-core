package app.domain.interfaces.entities.secondary;

import app.domain.interfaces.entities.ISystemId;

public interface IStorageLocationEntity
        extends ISystemId {
    String getStorageLocation();

    void setStorageLocation(String keyword);
}
