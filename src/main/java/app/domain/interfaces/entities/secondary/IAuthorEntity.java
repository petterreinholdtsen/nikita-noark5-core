package app.domain.interfaces.entities.secondary;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.DocumentDescription;
import app.domain.noark5.RecordEntity;

public interface IAuthorEntity
        extends INoarkEntity {

    String getAuthor();

    void setAuthor(String author);

    RecordEntity getReferenceRecordEntity();

    void setReferenceRecord(RecordEntity referenceRecordEntity);

    DocumentDescription getReferenceDocumentDescription();

    void setReferenceDocumentDescription(
            DocumentDescription referenceDocumentDescription);

    Boolean getForDocumentDescription();

    Boolean getForRecord();
}
