package app.domain.interfaces.entities.secondary;

import app.domain.interfaces.IBSM;
import app.domain.interfaces.entities.ISystemId;
import app.domain.noark5.DocumentDescription;
import app.domain.noark5.RecordEntity;
import app.domain.noark5.metadata.PartRole;

import java.util.Set;

public interface IPartEntity
        extends ISystemId, IBSM {
    PartRole getPartRole();

    void setPartRole(PartRole partRole);

    Set<DocumentDescription> getReferenceDocumentDescription();

    void addDocumentDescription(DocumentDescription documentDescription);

    void removeDocumentDescription(DocumentDescription documentDescription);

    Set<RecordEntity> getReferenceRecordEntity();

    void addRecordEntity(RecordEntity record);

    void removeRecordEntity(RecordEntity record);
}
