package app.domain.interfaces.entities.secondary;

import app.domain.interfaces.IBSM;
import app.domain.interfaces.entities.ISystemId;
import app.domain.noark5.metadata.CorrespondencePartType;

/**
 * Created by tsodring on 1/16/17.
 */
public interface ICorrespondencePartEntity
        extends ISystemId, IBSM {

    CorrespondencePartType getCorrespondencePartType();

    void setCorrespondencePartType(
            CorrespondencePartType correspondencePartType);

}
