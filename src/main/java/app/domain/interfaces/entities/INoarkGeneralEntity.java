package app.domain.interfaces.entities;


public interface INoarkGeneralEntity
        extends ISystemId, IFinalise, ITitleDescription {
}
