package app.domain.interfaces.entities;

import app.domain.noark5.Class;
import app.domain.noark5.Series;
import app.domain.noark5.metadata.ClassificationType;

import java.util.Set;

public interface IClassificationSystemEntity
        extends INoarkGeneralEntity {
    ClassificationType getClassificationType();

    void setClassificationType(ClassificationType classificationType);

    Set<Series> getReferenceSeries();

    void addSeries(Series series);

    void removeSeries(Series series);

    Set<Class> getReferenceClass();

    void addClass(Class klass);

    void removeClass(Class klass);
}
