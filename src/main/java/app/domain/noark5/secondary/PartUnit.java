package app.domain.noark5.secondary;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.secondary.IPartUnitEntity;
import app.domain.noark5.casehandling.secondary.BusinessAddress;
import app.domain.noark5.casehandling.secondary.ContactInformation;
import app.domain.noark5.casehandling.secondary.PostalAddress;
import app.webapp.payload.builder.noark5.secondary.PartUnitLinksBuilder;
import app.webapp.payload.deserializers.noark5.secondary.PartUnitDeserializer;
import app.webapp.payload.links.secondary.PartUnitLinks;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import jakarta.persistence.*;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.*;
import static jakarta.persistence.CascadeType.ALL;
import static jakarta.persistence.FetchType.LAZY;

@Entity
@Table(name = TABLE_PART_UNIT)
@JsonDeserialize(using = PartUnitDeserializer.class)
@LinksPacker(using = PartUnitLinksBuilder.class)
@LinksObject(using = PartUnitLinks.class)

public class PartUnit
        extends Part
        implements IPartUnitEntity {

    /**
     * M??? - organisasjonsnummer (xs:string)
     */
    @Column(name = ORGANISATION_NUMBER_ENG)
    @JsonProperty(ORGANISATION_NUMBER)

    private String organisationNumber;

    /**
     * M412 - kontaktperson  (xs:string)
     */
    @Column(name = CONTACT_PERSON_ENG)
    @JsonProperty(CONTACT_PERSON)

    private String contactPerson;

    @OneToOne(mappedBy = "partUnit", fetch = LAZY, cascade = ALL)
    @JoinColumn(name = PRIMARY_KEY_SYSTEM_ID,
            referencedColumnName = PRIMARY_KEY_SYSTEM_ID)
    private PostalAddress postalAddress;

    @OneToOne(mappedBy = "partUnit", fetch = LAZY, cascade = ALL)
    @JoinColumn(name = PRIMARY_KEY_SYSTEM_ID,
            referencedColumnName = PRIMARY_KEY_SYSTEM_ID)
    private BusinessAddress businessAddress;

    @OneToOne(mappedBy = "partUnit", fetch = LAZY, cascade = ALL)
    @JoinColumn(name = PRIMARY_KEY_SYSTEM_ID,
            referencedColumnName = PRIMARY_KEY_SYSTEM_ID)
    private ContactInformation contactInformation;

    public String getUnitIdentifier() {
        return organisationNumber;
    }

    public void setUnitIdentifier(String organisationNumber) {
        this.organisationNumber = organisationNumber;
    }

    public ContactInformation getContactInformation() {
        return contactInformation;
    }

    public void setContactInformation(ContactInformation contactInformation) {
        this.contactInformation = contactInformation;
    }

    public BusinessAddress getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(BusinessAddress businessAddress) {
        this.businessAddress = businessAddress;
    }

    public PostalAddress getPostalAddress() {
        return postalAddress;
    }

    public void setPostalAddress(PostalAddress postalAddress) {
        this.postalAddress = postalAddress;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    @Override
    public String getBaseTypeName() {
        return PART_UNIT;
    }

    @Override
    public String getBaseRel() {
        return REL_FONDS_STRUCTURE_PART;
    }

    @Override
    public String toString() {
        return "PartUnit{" + super.toString() +
                ", organisationNumber='" + organisationNumber + '\'' +
                ", contactPerson='" + contactPerson + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != getClass()) {
            return false;
        }
        PartUnit rhs = (PartUnit) other;
        return new EqualsBuilder()
                .appendSuper(super.equals(other))
                .append(organisationNumber, rhs.organisationNumber)
                .append(contactPerson, rhs.contactPerson)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(organisationNumber)
                .append(contactPerson)
                .toHashCode();
    }
}
