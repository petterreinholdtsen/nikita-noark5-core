package app.domain.noark5.secondary;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.secondary.IConversionEntity;
import app.domain.noark5.DocumentObject;
import app.domain.noark5.SystemIdEntity;
import app.domain.noark5.metadata.Format;
import app.webapp.payload.builder.noark5.secondary.ConversionLinksBuilder;
import app.webapp.payload.deserializers.noark5.secondary.ConversionDeserializer;
import app.webapp.payload.links.secondary.ConversionLinks;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import jakarta.persistence.*;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.time.OffsetDateTime;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.*;
import static jakarta.persistence.FetchType.LAZY;

@Entity
@Table(name = TABLE_CONVERSION)
@JsonDeserialize(using = ConversionDeserializer.class)
@LinksPacker(using = ConversionLinksBuilder.class)
@LinksObject(using = ConversionLinks.class)
public class Conversion
        extends SystemIdEntity
        implements IConversionEntity {

    private static final long serialVersionUID = 1L;

    /**
     * M615 - konvertertDato (xs:dateTime)
     */
    @Column(name = CONVERTED_DATE_ENG)
    @JsonProperty(CONVERTED_DATE)
    private OffsetDateTime convertedDate;

    /**
     * M616 - konvertertAv (xs:string)
     */
    @Column(name = CONVERTED_BY_ENG)
    @JsonProperty(CONVERTED_BY)

    private String convertedBy;

    /**
     * M??? - konvertertFraFormat code (xs:string)
     */
    @Column(name = "converted_from_format_code")
    private String convertedFromFormatCode;

    /**
     * M712 - konvertertFraFormat code name (xs:string)
     */
    @Column(name = "converted_from_format_code_name")
    private String convertedFromFormatCodeName;

    /**
     * M??? - konvertertTilFormat code (xs:string)
     */
    @Column(name = "converted_to_format_code")
    private String converteLinksFormatCode;

    /**
     * M713 - konvertertTilFormat code name (xs:string)
     */
    @Column(name = "converted_to_format_code_name")
    private String converteLinksFormatCodeName;

    /**
     * M714 - konverteringsverktoey (xs:string)
     */
    @Column(name = CONVERSION_TOOL_ENG)
    @JsonProperty(CONVERSION_TOOL)
    private String conversionTool;

    /**
     * M715 - konverteringskommentar (xs:string)
     */
    @Column(name = CONVERSION_COMMENT_ENG)
    @JsonProperty(CONVERSION_COMMENT)
    private String conversionComment;

    // Link to DocumentObject
    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = CONVERSION_DOCUMENT_OBJECT_ID,
            referencedColumnName = PRIMARY_KEY_SYSTEM_ID)
    private DocumentObject referenceDocumentObject;

    @Override
    public OffsetDateTime getConvertedDate() {
        return convertedDate;
    }

    @Override
    public void setConvertedDate(OffsetDateTime convertedDate) {
        this.convertedDate = convertedDate;
    }

    @Override
    public String getConvertedBy() {
        return convertedBy;
    }

    @Override
    public void setConvertedBy(String convertedBy) {
        this.convertedBy = convertedBy;
    }

    @Override
    public Format getConvertedFromFormat() {
        if (null == convertedFromFormatCode)
            return null;
        Format convertedFromFormat = new Format();
        convertedFromFormat.setCode(convertedFromFormatCode);
        convertedFromFormat.setCodeName(convertedFromFormatCodeName);
        return convertedFromFormat;
    }

    @Override
    public void setConvertedFromFormat(Format convertedFromFormat) {
        if (null != convertedFromFormat) {
            this.convertedFromFormatCode = convertedFromFormat.getCode();
            this.convertedFromFormatCodeName = convertedFromFormat.getCodeName();
        } else {
            this.convertedFromFormatCode = null;
            this.convertedFromFormatCodeName = null;
        }
    }

    @Override
    public Format getConverteLinksFormat() {
        if (null == converteLinksFormatCode)
            return null;
        Format converteLinksFormat = new Format();
        converteLinksFormat.setCode(converteLinksFormatCode);
        converteLinksFormat.setCodeName(converteLinksFormatCodeName);
        return converteLinksFormat;
    }

    @Override
    public void setConverteLinksFormat(Format converteLinksFormat) {
        if (null != converteLinksFormat) {
            this.converteLinksFormatCode = converteLinksFormat.getCode();
            this.converteLinksFormatCodeName = converteLinksFormat.getCodeName();
        } else {
            this.converteLinksFormatCode = null;
            this.converteLinksFormatCodeName = null;
        }
    }

    @Override
    public String getConversionTool() {
        return conversionTool;
    }

    @Override
    public void setConversionTool(String conversionTool) {
        this.conversionTool = conversionTool;
    }

    @Override
    public String getConversionComment() {
        return conversionComment;
    }

    @Override
    public void setConversionComment(String conversionComment) {
        this.conversionComment = conversionComment;
    }

    @Override
    public String getBaseTypeName() {
        return CONVERSION;
    }

    @Override
    public String getBaseRel() {
        return REL_FONDS_STRUCTURE_CONVERSION;
    }

    @Override
    public DocumentObject getReferenceDocumentObject() {
        return referenceDocumentObject;
    }

    @Override
    public void setReferenceDocumentObject(
            DocumentObject referenceDocumentObject) {
        this.referenceDocumentObject = referenceDocumentObject;
    }

    @Override
    public String toString() {
        return "Conversion{" + super.toString() +
                ", convertedDate=" + convertedDate +
                ", convertedBy='" + convertedBy + '\'' +
                ", convertedFromFormatCode='" + convertedFromFormatCode + '\'' +
                ", convertedFromFormatCodeName='" + convertedFromFormatCodeName + '\'' +
                ", converteLinksFormatCode='" + converteLinksFormatCode + '\'' +
                ", converteLinksFormatCodeName='" + converteLinksFormatCodeName + '\'' +
                ", conversionTool='" + conversionTool + '\'' +
                ", conversionComment='" + conversionComment + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != getClass()) {
            return false;
        }
        Conversion rhs = (Conversion) other;
        return new EqualsBuilder()
                .appendSuper(super.equals(other))
                .append(convertedDate, rhs.convertedDate)
                .append(convertedBy, rhs.convertedBy)
                .append(convertedFromFormatCode, rhs.convertedFromFormatCode)
                .append(convertedFromFormatCodeName, rhs.convertedFromFormatCodeName)
                .append(converteLinksFormatCodeName, rhs.converteLinksFormatCodeName)
                .append(conversionTool, rhs.conversionTool)
                .append(conversionComment, rhs.conversionComment)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(convertedDate)
                .append(convertedBy)
                .append(convertedFromFormatCode)
                .append(convertedFromFormatCodeName)
                .append(converteLinksFormatCode)
                .append(converteLinksFormatCodeName)
                .append(conversionTool)
                .append(conversionComment)
                .toHashCode();
    }
}
