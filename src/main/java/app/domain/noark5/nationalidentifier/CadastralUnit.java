package app.domain.noark5.nationalidentifier;

import app.domain.annotation.ANationalIdentifier;
import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.nationalidentifier.ICadastralUnitEntity;
import app.webapp.payload.builder.noark5.nationalidentifier.CadastralUnitLinksBuilder;
import app.webapp.payload.deserializers.noark5.nationalidentifier.CadastralUnitDeserializer;
import app.webapp.payload.links.nationalidentifier.CadastralUnitLinks;
import app.webapp.payload.serializers.noark5.nationalidentifier.CadastralUnitSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import static app.utils.constants.Constants.REL_FONDS_STRUCTURE_CADASTRAL_UNIT;
import static app.utils.constants.Constants.TABLE_CADASTRAL;
import static app.utils.constants.N5ResourceMappings.*;

@Entity
@Table(name = TABLE_CADASTRAL)
@JsonSerialize(using = CadastralUnitSerializer.class)
@JsonDeserialize(using = CadastralUnitDeserializer.class)
@LinksPacker(using = CadastralUnitLinksBuilder.class)
@LinksObject(using = CadastralUnitLinks.class)
@ANationalIdentifier(name = CADASTRAL_UNIT)
public class CadastralUnit
        extends NationalIdentifier
        implements ICadastralUnitEntity {

    /**
     * M??? - kommunenummer (xs:string)
     */
    @Column(name = MUNICIPALITY_NUMBER_ENG, nullable = false)
    @JsonProperty(MUNICIPALITY_NUMBER)

    String municipalityNumber;

    /**
     * M??? gaardsnummer - (xs:integer)
     */
    @Column(name = HOLDING_NUMBER_ENG, nullable = false)
    @JsonProperty(HOLDING_NUMBER)
    Integer holdingNumber;

    /**
     * M??? bruksnummer - (xs:integer)
     */
    @Column(name = SUB_HOLDING_NUMBER_ENG, nullable = false)
    @JsonProperty(SUB_HOLDING_NUMBER)
    Integer subHoldingNumber;

    /**
     * M??? festenummer - (xs:integer)
     */
    @Column(name = LEASE_NUMBER_ENG)
    @JsonProperty(LEASE_NUMBER)
    Integer leaseNumber;

    /**
     * M??? seksjonsnummer - (xs:integer)
     */
    @Column(name = SECTION_NUMBER_ENG)
    @JsonProperty(SECTION_NUMBER)
    Integer sectionNumber;

    @Override
    public String getMunicipalityNumber() {
        return municipalityNumber;
    }

    @Override
    public void setMunicipalityNumber(String municipalityNumber) {
        this.municipalityNumber = municipalityNumber;
    }

    @Override
    public Integer getHoldingNumber() {
        return holdingNumber;
    }

    @Override
    public void setHoldingNumber(Integer holdingNumber) {
        this.holdingNumber = holdingNumber;
    }

    @Override
    public Integer getSubHoldingNumber() {
        return subHoldingNumber;
    }

    @Override
    public void setSubHoldingNumber(Integer subHoldingNumber) {
        this.subHoldingNumber = subHoldingNumber;
    }

    @Override
    public Integer getLeaseNumber() {
        return leaseNumber;
    }

    @Override
    public void setLeaseNumber(Integer leaseNumber) {
        this.leaseNumber = leaseNumber;
    }

    @Override
    public Integer getSectionNumber() {
        return sectionNumber;
    }

    @Override
    public void setSectionNumber(Integer sectionNumber) {
        this.sectionNumber = sectionNumber;
    }

    @Override
    public String getBaseTypeName() {
        return CADASTRAL_UNIT;
    }

    @Override
    public String getBaseRel() {
        return REL_FONDS_STRUCTURE_CADASTRAL_UNIT;
    }

    @Override
    public String toString() {
        return "CadastralUnit{" + super.toString() + '\'' +
                "municipalityNumber='" + municipalityNumber + '\'' +
                ", holdingNumber=" + holdingNumber +
                ", subHoldingNumber=" + subHoldingNumber +
                ", leaseNumber=" + leaseNumber +
                ", sectionNumber=" + sectionNumber +
                '}';
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != getClass()) {
            return false;
        }
        CadastralUnit rhs = (CadastralUnit) other;
        return new EqualsBuilder()
                .appendSuper(super.equals(other))
                .append(municipalityNumber, rhs.municipalityNumber)
                .append(holdingNumber, rhs.holdingNumber)
                .append(subHoldingNumber, rhs.subHoldingNumber)
                .append(leaseNumber, rhs.leaseNumber)
                .append(sectionNumber, rhs.sectionNumber)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(municipalityNumber)
                .append(holdingNumber)
                .append(subHoldingNumber)
                .append(leaseNumber)
                .append(sectionNumber)
                .toHashCode();
    }
}
