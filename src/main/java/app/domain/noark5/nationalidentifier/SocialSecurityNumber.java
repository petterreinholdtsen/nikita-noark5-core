package app.domain.noark5.nationalidentifier;

import app.domain.annotation.ANationalIdentifier;
import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.nationalidentifier.ISocialSecurityNumberEntity;
import app.webapp.payload.builder.noark5.nationalidentifier.SocialSecurityNumberLinksBuilder;
import app.webapp.payload.deserializers.noark5.nationalidentifier.SocialSecurityNumberDeserializer;
import app.webapp.payload.links.nationalidentifier.SocialSecurityNumberLinks;
import app.webapp.payload.serializers.noark5.nationalidentifier.SocialSecurityNumberSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import static app.utils.constants.Constants.REL_FONDS_STRUCTURE_SOCIAL_SECURITY_NUMBER;
import static app.utils.constants.Constants.TABLE_SOCIAL_SECURITY_NUMBER;
import static app.utils.constants.N5ResourceMappings.SOCIAL_SECURITY_NUMBER;
import static app.utils.constants.N5ResourceMappings.SOCIAL_SECURITY_NUMBER_ENG;
@Entity
@Table(name = TABLE_SOCIAL_SECURITY_NUMBER)
@JsonSerialize(using = SocialSecurityNumberSerializer.class)
@JsonDeserialize(using = SocialSecurityNumberDeserializer.class)
@LinksPacker(using = SocialSecurityNumberLinksBuilder.class)
@LinksObject(using = SocialSecurityNumberLinks.class)
@ANationalIdentifier(name = SOCIAL_SECURITY_NUMBER)
public class SocialSecurityNumber
        extends PersonIdentifier
        implements ISocialSecurityNumberEntity {
    /**
     * M??? - foedselsnummer (xs:string)
     */
    @Column(name = SOCIAL_SECURITY_NUMBER_ENG)
    @JsonProperty(SOCIAL_SECURITY_NUMBER)

    private String socialSecurityNumber;

    @Override
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    @Override
    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    @Override
    public String getBaseTypeName() {
        return SOCIAL_SECURITY_NUMBER;
    }

    @Override
    public String getBaseRel() {
        return REL_FONDS_STRUCTURE_SOCIAL_SECURITY_NUMBER;
    }

    @Override
    public String toString() {
        return "SocialSecurityNumber{" + super.toString() + '\'' +
                "socialSecurityNumber='" + socialSecurityNumber + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != getClass()) {
            return false;
        }
        SocialSecurityNumber rhs = (SocialSecurityNumber) other;
        return new EqualsBuilder()
                .appendSuper(super.equals(other))
                .append(socialSecurityNumber, rhs.socialSecurityNumber)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(socialSecurityNumber)
                .toHashCode();
    }
}
