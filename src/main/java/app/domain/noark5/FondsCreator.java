package app.domain.noark5;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.IFondsCreatorEntity;
import app.webapp.payload.builder.noark5.FondsCreatorLinksBuilder;
import app.webapp.payload.deserializers.noark5.FondsCreatorDeserializer;
import app.webapp.payload.links.FondsCreatorLinks;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.HashSet;
import java.util.Set;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.*;

@Entity
@Table(name = TABLE_FONDS_CREATOR)
@JsonDeserialize(using = FondsCreatorDeserializer.class)
@LinksPacker(using = FondsCreatorLinksBuilder.class)
@LinksObject(using = FondsCreatorLinks.class)
public class FondsCreator
        extends SystemIdEntity
        implements IFondsCreatorEntity {

    /**
     * M006 - arkivskaperID (xs:string)
     */
    @NotNull
    @Column(name = FONDS_CREATOR_ID_ENG, nullable = false)
    @JsonProperty(FONDS_CREATOR_ID)

    private String fondsCreatorId;

    /**
     * M023 - arkivskaperNavn (xs:string)
     */
    @NotNull
    @Column(name = FONDS_CREATOR_NAME_ENG, nullable = false)
    @JsonProperty(FONDS_CREATOR_NAME)
    private String fondsCreatorName;

    /**
     * M021 - beskrivelse (xs:string)
     */
    @Column(name = DESCRIPTION_ENG, length = DESCRIPTION_LENGTH)
    @JsonProperty(DESCRIPTION)
    private String description;

    // Links to Fonds
    @ManyToMany(mappedBy = "referenceFondsCreator")
    private Set<Fonds> referenceFonds = new HashSet<>();

    public String getFondsCreatorId() {
        return fondsCreatorId;
    }

    public void setFondsCreatorId(String fondsCreatorId) {
        this.fondsCreatorId = fondsCreatorId;
    }

    public String getFondsCreatorName() {
        return fondsCreatorName;
    }

    public void setFondsCreatorName(String fondsCreatorName) {
        this.fondsCreatorName = fondsCreatorName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String getBaseTypeName() {
        return FONDS_CREATOR;
    }

    @Override
    public String getBaseRel() {
        return REL_FONDS_STRUCTURE_FONDS_CREATOR;
    }

    public Set<Fonds> getReferenceFonds() {
        return referenceFonds;
    }

    public void addFonds(Fonds fonds) {
        this.referenceFonds.add(fonds);
        fonds.getReferenceFondsCreator().add(this);
    }

    public void removeFonds(Fonds fonds) {
        this.referenceFonds.remove(fonds);
        fonds.getReferenceFondsCreator().remove(this);
    }

    @Override
    public String toString() {
        return "FondsCreator{" + super.toString() +
                ", fondsCreatorId='" + fondsCreatorId + '\'' +
                ", fondsCreatorName='" + fondsCreatorName + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != getClass()) {
            return false;
        }
        FondsCreator rhs = (FondsCreator) other;
        return new EqualsBuilder()
                .appendSuper(super.equals(other))
                .append(fondsCreatorId, rhs.fondsCreatorId)
                .append(fondsCreatorName, rhs.fondsCreatorName)
                .append(description, rhs.description)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(fondsCreatorId)
                .append(fondsCreatorName)
                .append(description)
                .toHashCode();
    }
}
