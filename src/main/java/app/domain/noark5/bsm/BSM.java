package app.domain.noark5.bsm;

import app.domain.interfaces.IBSM;
import app.domain.noark5.NoarkEntity;
import app.webapp.payload.deserializers.noark5.BSMDeserializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.ArrayList;
import java.util.List;

@JsonDeserialize(using = BSMDeserializer.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BSM extends NoarkEntity
        implements IBSM {

    private final List<BSMBase> bsm = new ArrayList<>();

    @Override
    public List<BSMBase> getReferenceBSMBase() {
        return bsm;
    }

    @Override
    public void addReferenceBSMBase(List<BSMBase> bSMBases) {
        this.bsm.addAll(bSMBases);
    }

    @Override
    public void addBSMBase(BSMBase bSMBase) {
        this.bsm.add(bSMBase);
    }

    @Override
    public void removeBSMBase(BSMBase bSMBase) {
        this.bsm.remove(bSMBase);
    }
}
