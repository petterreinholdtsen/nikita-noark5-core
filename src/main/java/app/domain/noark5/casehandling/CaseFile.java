package app.domain.noark5.casehandling;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.ICaseFileEntity;
import app.domain.noark5.File;
import app.domain.noark5.admin.AdministrativeUnit;
import app.domain.noark5.metadata.CaseStatus;
import app.domain.noark5.secondary.Precedence;
import app.webapp.payload.builder.noark5.casehandling.CaseFileLinksBuilder;
import app.webapp.payload.deserializers.noark5.casehandling.CaseFileDeserializer;
import app.webapp.payload.links.casehandling.CaseFileLinks;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.OffsetDateTime;
import java.util.HashSet;
import java.util.Set;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.*;
import static jakarta.persistence.CascadeType.MERGE;
import static jakarta.persistence.CascadeType.PERSIST;
import static jakarta.persistence.FetchType.LAZY;
import static org.springframework.format.annotation.DateTimeFormat.ISO.DATE_TIME;

@Entity
@Table(name = TABLE_CASE_FILE)
@JsonDeserialize(using = CaseFileDeserializer.class)
@LinksPacker(using = CaseFileLinksBuilder.class)
@LinksObject(using = CaseFileLinks.class)
public class CaseFile
        extends File
        implements ICaseFileEntity {

    private static final long serialVersionUID = 1L;

    /**
     * M011 - saksaar (xs:integer)
     */
    @Column(name = CASE_YEAR_ENG)
    @JsonProperty(CASE_YEAR)
    private Integer caseYear;

    /**
     * M012 - sakssekvensnummer (xs:integer)
     */
    @Column(name = CASE_SEQUENCE_NUMBER_ENG)
    @JsonProperty(CASE_SEQUENCE_NUMBER)
    private Integer caseSequenceNumber;

    /**
     * M100 - saksdato (xs:date)
     */
    @NotNull
    @Column(name = CASE_DATE_ENG, nullable = false)
    @DateTimeFormat(iso = DATE_TIME)
    @JsonProperty(CASE_DATE)
    private OffsetDateTime caseDate;

    /**
     * M306 - saksansvarlig (xs:string)
     */
    @NotNull
    @Column(name = CASE_RESPONSIBLE_ENG, nullable = false)
    @JsonProperty(CASE_RESPONSIBLE)

    private String caseResponsible;

    /**
     * M308 - journalenhet (xs:string)
     */
    @Column(name = CASE_RECORDS_MANAGEMENT_UNIT_ENG)
    @JsonProperty(CASE_RECORDS_MANAGEMENT_UNIT)

    private String recordsManagementUnit;

    // Links to Precedence
    @ManyToMany(cascade = {PERSIST, MERGE})
    @JoinTable(name = TABLE_CASE_FILE_PRECEDENCE,
            joinColumns = @JoinColumn(
                    name = FOREIGN_KEY_CASE_FILE_PK,
                    referencedColumnName = PRIMARY_KEY_SYSTEM_ID),
            inverseJoinColumns = @JoinColumn(
                    name = FOREIGN_KEY_PRECEDENCE_PK,
                    referencedColumnName = PRIMARY_KEY_SYSTEM_ID))
    private final Set<Precedence> referencePrecedence = new HashSet<>();
    /**
     * M??? - saksstatus kode (xs:string)
     */
    @NotNull
    @Column(name = CASE_STATUS_CODE_ENG, nullable = false)
    @JsonProperty(CASE_STATUS_CODE)
    private String caseStatusCode;

    /**
     * M106 - utlaantDato (xs:date)
     */
    @Column(name = CASE_LOANED_DATE_ENG)
    @DateTimeFormat(iso = DATE_TIME)
    @JsonProperty(CASE_LOANED_DATE)
    private OffsetDateTime loanedDate;

    /**
     * M309 - utlaantTil (xs:string)
     */
    @Column(name = CASE_LOANED_TO_ENG)
    @JsonProperty(CASE_LOANED_TO)

    private String loaneLinks;
    /**
     * M??? - saksstatus name (xs:string)
     */
    @Column(name = CASE_STATUS_CODE_NAME_ENG)
    @JsonProperty(CASE_STATUS_CODE_NAME)
    private String caseStatusCodeName;

    // Link to AdministrativeUnit
    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = CASE_FILE_ADMINISTRATIVE_UNIT_ID,
            referencedColumnName = PRIMARY_KEY_SYSTEM_ID)
    @JsonIgnore
    private AdministrativeUnit referenceAdministrativeUnit;

    public CaseFile(File file) {
        super();
        setSystemId(file.getSystemId());
        setVersion(file.getVersion(), true);
        setTitle(file.getTitle());
        setPublicTitle(file.getPublicTitle());
        setDocumentMedium(file.getDocumentMedium());
        setFileId(file.getFileId());
        // Is this a potential problem if the person that expands the file to
        // a caseFile is not the same person? However the expanded portion
        // (CaseFile) should not have a different owner than File
        setCreatedBy(file.getCreatedBy());
        setCaseDate(file.getCreatedDate());
        setLastModifiedDate(file.getLastModifiedDate());
        setLastModifiedBy(file.getLastModifiedBy());
    }

    public CaseFile() {
    }

    public Integer getCaseYear() {
        return caseYear;
    }

    public void setCaseYear(Integer caseYear) {
        this.caseYear = caseYear;
    }

    public Integer getCaseSequenceNumber() {
        return caseSequenceNumber;
    }

    public void setCaseSequenceNumber(Integer caseSequenceNumber) {
        this.caseSequenceNumber = caseSequenceNumber;
    }

    public OffsetDateTime getCaseDate() {
        return caseDate;
    }

    public void setCaseDate(OffsetDateTime caseDate) {
        this.caseDate = caseDate;
    }

    public String getCaseResponsible() {
        return caseResponsible;
    }

    public void setCaseResponsible(String caseResponsible) {
        this.caseResponsible = caseResponsible;
    }

    public String getRecordsManagementUnit() {
        return recordsManagementUnit;
    }

    public void setRecordsManagementUnit(String recordsManagementUnit) {
        this.recordsManagementUnit = recordsManagementUnit;
    }

    @Override
    public CaseStatus getCaseStatus() {
        if (null == caseStatusCode)
            return null;
        return new CaseStatus(caseStatusCode,caseStatusCodeName);
    }

    @Override
    public void setCaseStatus(CaseStatus caseStatus) {
        if (null != caseStatus) {
            this.caseStatusCode = caseStatus.getCode();
            this.caseStatusCodeName = caseStatus.getCodeName();
        } else {
            this.caseStatusCode = null;
            this.caseStatusCodeName = null;
        }
    }

    public OffsetDateTime getLoanedDate() {
        return loanedDate;
    }

    public void setLoanedDate(OffsetDateTime loanedDate) {
        this.loanedDate = loanedDate;
    }

    public String getLoaneLinks() {
        return loaneLinks;
    }

    public void setLoaneLinks(String loaneLinks) {
        this.loaneLinks = loaneLinks;
    }

    @Override
    public String getBaseTypeName() {
        return CASE_FILE;
    }

    @Override
    public String getBaseRel() {
        return REL_CASE_HANDLING_CASE_FILE;
    }

    @Override
    public String getFunctionalTypeName() {
        return NOARK_CASE_HANDLING_PATH;
    }

    @Override
    public Set<Precedence> getReferencePrecedence() {
        return referencePrecedence;
    }

    @Override
    public void addPrecedence(Precedence precedence) {
        this.referencePrecedence.add(precedence);
        precedence.getReferenceCaseFile().add(this);
    }

    @Override
    public void removePrecedence(Precedence precedence) {
        this.referencePrecedence.remove(precedence);
        precedence.getReferenceCaseFile().remove(this);
    }

    public AdministrativeUnit getReferenceAdministrativeUnit() {
        return referenceAdministrativeUnit;
    }

    public void setReferenceAdministrativeUnit(
            AdministrativeUnit referenceAdministrativeUnit) {
        this.referenceAdministrativeUnit = referenceAdministrativeUnit;
    }

    @Override
    public String toString() {
        return super.toString() + " CaseFile{" +
                "loaneLinks='" + loaneLinks + '\'' +
                ", loanedDate=" + loanedDate +
                ", caseStatusCode='" + caseStatusCode + '\'' +
                ", caseStatusCodeName='" + caseStatusCodeName + '\'' +
                ", recordsManagementUnit='" + recordsManagementUnit + '\'' +
                ", caseResponsible='" + caseResponsible + '\'' +
                ", caseDate=" + caseDate +
                ", caseSequenceNumber=" + caseSequenceNumber +
                ", caseYear=" + caseYear +
                '}';
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != getClass()) {
            return false;
        }
        CaseFile rhs = (CaseFile) other;
        return new EqualsBuilder()
                .appendSuper(super.equals(other))
                .append(caseSequenceNumber, rhs.caseSequenceNumber)
                .append(caseYear, rhs.caseYear)
                .append(caseDate, rhs.caseDate)
                .append(caseResponsible, rhs.caseResponsible)
                .append(caseStatusCode, rhs.caseStatusCode)
                .append(caseStatusCodeName, rhs.caseStatusCodeName)
                .append(recordsManagementUnit, rhs.recordsManagementUnit)
                .append(loanedDate, rhs.loanedDate)
                .append(loaneLinks, rhs.loaneLinks)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(caseSequenceNumber)
                .append(caseYear)
                .append(caseDate)
                .append(caseResponsible)
                .append(caseStatusCode)
                .append(caseStatusCodeName)
                .append(recordsManagementUnit)
                .append(loanedDate)
                .append(loaneLinks)
                .toHashCode();
    }
}
