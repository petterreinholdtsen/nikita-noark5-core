package app.domain.noark5.casehandling.secondary;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.secondary.ICorrespondencePartUnitEntity;
import app.webapp.payload.builder.noark5.casehandling.CorrespondencePartUnitLinksBuilder;
import app.webapp.payload.deserializers.noark5.casehandling.CorrespondencePartUnitDeserializer;
import app.webapp.payload.links.casehandling.CorrespondencePartUnitLinks;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import jakarta.persistence.*;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.*;
import static jakarta.persistence.CascadeType.ALL;
import static jakarta.persistence.FetchType.LAZY;
@Entity
@Table(name = TABLE_CORRESPONDENCE_PART_UNIT)
@JsonDeserialize(using = CorrespondencePartUnitDeserializer.class)
@LinksPacker(using = CorrespondencePartUnitLinksBuilder.class)
@LinksObject(using = CorrespondencePartUnitLinks.class)
public class CorrespondencePartUnit
        extends CorrespondencePart
        implements ICorrespondencePartUnitEntity {

    /**
     * M??? - enhetsidentifikator (xs:string)
     */
    @Column(name = UNIT_IDENTIFIER_ENG)
    @JsonProperty(UNIT_IDENTIFIER)
    private String unitIdentifier;

    /**
     * M??? - navn (xs:string)
     */
    @Column(name = CORRESPONDENCE_PART_NAME_ENG)
    @JsonProperty(CORRESPONDENCE_PART_NAME)
    private String name;

    /**
     * M412 - kontaktperson  (xs:string)
     */
    @Column(name = CONTACT_PERSON_ENG)
    @JsonProperty(CONTACT_PERSON)
    private String contactPerson;

    @OneToOne(mappedBy = REFERENCE_CORRESPONDENCE_PART_UNIT, fetch = LAZY,
            cascade = ALL)
    @JoinColumn(name = PRIMARY_KEY_SYSTEM_ID,
            referencedColumnName = PRIMARY_KEY_SYSTEM_ID)
    private PostalAddress postalAddress;

    @OneToOne(mappedBy = REFERENCE_CORRESPONDENCE_PART_UNIT, fetch = LAZY,
            cascade = ALL)
    @JoinColumn(name = PRIMARY_KEY_SYSTEM_ID,
            referencedColumnName = PRIMARY_KEY_SYSTEM_ID)
    private BusinessAddress businessAddress;

    @OneToOne(mappedBy = REFERENCE_CORRESPONDENCE_PART_UNIT, fetch = LAZY,
            cascade = ALL)
    @JoinColumn(name = PRIMARY_KEY_SYSTEM_ID,
            referencedColumnName = PRIMARY_KEY_SYSTEM_ID)
    private ContactInformation contactInformation;

    public String getUnitIdentifier() {
        return unitIdentifier;
    }

    public void setUnitIdentifier(String unitIdentifier) {
        this.unitIdentifier = unitIdentifier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ContactInformation getContactInformation() {
        return contactInformation;
    }

    public void setContactInformation(ContactInformation contactInformation) {
        this.contactInformation = contactInformation;
        if (null != contactInformation) {
            contactInformation.setCorrespondencePartUnit(this);
        }
    }

    public BusinessAddress getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(BusinessAddress businessAddress) {
        this.businessAddress = businessAddress;
        if (null != businessAddress) {
            businessAddress.setReferenceCorrespondencePartUnit(this);
        }
    }

    public PostalAddress getPostalAddress() {
        return postalAddress;
    }

    public void setPostalAddress(PostalAddress postalAddress) {
        this.postalAddress = postalAddress;
        if (null != postalAddress) {
            postalAddress.setReferenceCorrespondencePartUnit(this);
        }
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    @Override
    public String getBaseTypeName() {
        return CORRESPONDENCE_PART_UNIT;
    }

    @Override
    public String getBaseRel() {
        return REL_FONDS_STRUCTURE_CORRESPONDENCE_PART_UNIT;
    }

    @Override
    public String toString() {
        return super.toString() +
                ", unitIdentifier='" + unitIdentifier + '\'' +
                ", name='" + name + '\'' +
                ", contactPerson='" + contactPerson + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != getClass()) {
            return false;
        }
        CorrespondencePartUnit rhs = (CorrespondencePartUnit) other;
        return new EqualsBuilder()
                .appendSuper(super.equals(other))
                .append(unitIdentifier, rhs.unitIdentifier)
                .append(name, rhs.name)
                .append(contactPerson, rhs.contactPerson)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(unitIdentifier)
                .append(name)
                .append(contactPerson)
                .toHashCode();
    }
}
