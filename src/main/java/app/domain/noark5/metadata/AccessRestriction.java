package app.domain.noark5.metadata;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.REL_METADATA_ACCESS_RESTRICTION;
import static app.utils.constants.Constants.TABLE_ACCESS_RESTRICTION;
import static app.utils.constants.N5ResourceMappings.ACCESS_RESTRICTION;

// Noark 5v5 Tilgangsrestriksjon
@Entity
@Table(name = TABLE_ACCESS_RESTRICTION)
public class AccessRestriction
        extends Metadata {

    private static final long serialVersionUID = 1L;

    public AccessRestriction() {
    }

    public AccessRestriction(String code, String codename) {
        super(code, codename);
    }

    public AccessRestriction(String code) {
        super(code, null);
    }

    @Override
    public String getBaseTypeName() {
        return ACCESS_RESTRICTION;
    }

    @Override
    public String getBaseRel() {
        return REL_METADATA_ACCESS_RESTRICTION;
    }
}
