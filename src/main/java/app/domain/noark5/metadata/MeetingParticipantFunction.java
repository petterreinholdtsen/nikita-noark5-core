package app.domain.noark5.metadata;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.TABLE_MEETING_PARTICIPANT_FUNCTION;
import static app.utils.constants.N5ResourceMappings.MEETING_PARTICIPANT_FUNCTION;

// Noark 5v5 Møtedeltakerfunksjon
@Entity
@Table(name = TABLE_MEETING_PARTICIPANT_FUNCTION)
public class MeetingParticipantFunction
        extends Metadata {

    private static final long serialVersionUID = 1L;

    public MeetingParticipantFunction() {
    }

    public MeetingParticipantFunction(String code, String codename) {
        super(code, codename);
    }

    public MeetingParticipantFunction(String code) {
        super(code, null);
    }

    @Override
    public String getBaseTypeName() {
        return MEETING_PARTICIPANT_FUNCTION;
    }
}
