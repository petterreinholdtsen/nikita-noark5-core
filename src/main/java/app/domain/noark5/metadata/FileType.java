package app.domain.noark5.metadata;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.REL_METADATA_FILE_TYPE;
import static app.utils.constants.Constants.TABLE_FILE_TYPE;
import static app.utils.constants.N5ResourceMappings.FILE_TYPE;

// Noark 5v5 mappetype
@Entity
@Table(name = TABLE_FILE_TYPE)
public class FileType
        extends Metadata {

    private static final long serialVersionUID = 1L;

    public FileType() {
    }

    public FileType(String code, String codename) {
        super(code, codename);
    }

    public FileType(String code) {
        super(code, null);
    }

    @Override
    public String getBaseTypeName() {
        return FILE_TYPE;
    }

    @Override
    public String getBaseRel() {
        return REL_METADATA_FILE_TYPE;
    }
}
