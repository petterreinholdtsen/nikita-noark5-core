package app.domain.noark5.metadata;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.REL_METADATA_EVENT_TYPE;
import static app.utils.constants.Constants.TABLE_EVENT_TYPE;
import static app.utils.constants.N5ResourceMappings.EVENT_TYPE;

// Noark 5v5 hendelsetype
@Entity
@Table(name = TABLE_EVENT_TYPE)
public class EventType
        extends Metadata {

    private static final long serialVersionUID = 1L;

    public EventType() {
    }

    public EventType(String code, String codename) {
        super(code, codename);
    }

    public EventType(String code) {
        super(code, null);
    }

    @Override
    public String getBaseTypeName() {
        return EVENT_TYPE;
    }

    @Override
    public String getBaseRel() {
        return REL_METADATA_EVENT_TYPE;
    }
}
