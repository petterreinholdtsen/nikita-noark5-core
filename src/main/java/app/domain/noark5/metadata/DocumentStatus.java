package app.domain.noark5.metadata;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.REL_METADATA_DOCUMENT_STATUS;
import static app.utils.constants.Constants.TABLE_DOCUMENT_STATUS;
import static app.utils.constants.N5ResourceMappings.DOCUMENT_STATUS;

// Noark 5v5 dokumentstatus
@Entity
@Table(name = TABLE_DOCUMENT_STATUS)
public class DocumentStatus
        extends Metadata {

    private static final long serialVersionUID = 1L;

    public DocumentStatus() {
    }

    public DocumentStatus(String code, String codename) {
        super(code, codename);
    }

    public DocumentStatus(String code) {
        super(code, null);
    }

    @Override
    public String getBaseTypeName() {
        return DOCUMENT_STATUS;
    }

    @Override
    public String getBaseRel() {
        return REL_METADATA_DOCUMENT_STATUS;
    }
}
