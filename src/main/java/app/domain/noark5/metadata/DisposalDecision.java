package app.domain.noark5.metadata;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.REL_METADATA_DISPOSAL_DECISION;
import static app.utils.constants.Constants.TABLE_DISPOSAL_DECISION;
import static app.utils.constants.N5ResourceMappings.DISPOSAL_DECISION;

// Noark 5v5 Kassasjonsvedtak
@Entity
@Table(name = TABLE_DISPOSAL_DECISION)
public class DisposalDecision
        extends Metadata {

    private static final long serialVersionUID = 1L;

    public DisposalDecision() {
    }

    public DisposalDecision(String code, String codename) {
        super(code, codename);
    }

    public DisposalDecision(String code) {
        super(code, null);
    }

    @Override
    public String getBaseTypeName() {
        return DISPOSAL_DECISION;
    }

    @Override
    public String getBaseRel() {
        return REL_METADATA_DISPOSAL_DECISION;
    }
}
