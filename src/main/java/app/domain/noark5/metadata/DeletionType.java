package app.domain.noark5.metadata;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.REL_METADATA_DELETION_TYPE;
import static app.utils.constants.Constants.TABLE_DELETION_TYPE;
import static app.utils.constants.N5ResourceMappings.DELETION_TYPE;

// Noark 5v5 Slettingstype
@Entity
@Table(name = TABLE_DELETION_TYPE)
public class DeletionType
        extends Metadata {

    private static final long serialVersionUID = 1L;

    public DeletionType() {
    }

    public DeletionType(String code, String codename) {
        super(code, codename);
    }

    public DeletionType(String code) {
        super(code, null);
    }

    @Override
    public String getBaseTypeName() {
        return DELETION_TYPE;
    }

    @Override
    public String getBaseRel() {
        return REL_METADATA_DELETION_TYPE;
    }

}
