package app.domain.noark5.metadata;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.REL_METADATA_DOCUMENT_TYPE;
import static app.utils.constants.Constants.TABLE_DOCUMENT_TYPE;
import static app.utils.constants.N5ResourceMappings.DOCUMENT_TYPE;

// Noark 5v5 dokumenttype
@Entity
@Table(name = TABLE_DOCUMENT_TYPE)
public class DocumentType
        extends Metadata {

    private static final long serialVersionUID = 1L;

    public DocumentType() {
    }

    public DocumentType(String code, String codename) {
        super(code, codename);
    }

    public DocumentType(String code) {
        super(code, null);
    }

    @Override
    public String getBaseTypeName() {
        return DOCUMENT_TYPE;
    }

    @Override
    public String getBaseRel() {
        return REL_METADATA_DOCUMENT_TYPE;
    }
}
