package app.domain.noark5.metadata;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.REL_METADATA_SERIES_STATUS;
import static app.utils.constants.Constants.TABLE_SERIES_STATUS;
import static app.utils.constants.N5ResourceMappings.SERIES_STATUS;

// Noark 5v5 arkvdelstatus
@Entity
@Table(name = TABLE_SERIES_STATUS)
public class SeriesStatus
        extends Metadata {

    private static final long serialVersionUID = 1L;

    public SeriesStatus() {
    }

    public SeriesStatus(String code, String codename) {
        super(code, codename);
    }

    public SeriesStatus(String code) {
        super(code, null);
    }

    @Override
    public String getBaseTypeName() {
        return SERIES_STATUS;
    }

    @Override
    public String getBaseRel() {
        return REL_METADATA_SERIES_STATUS;
    }
}
