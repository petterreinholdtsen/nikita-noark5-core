package app.domain.noark5.admin;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.admin.IReadLogEntity;
import app.domain.noark5.EventLog;
import app.webapp.payload.builder.noark5.admin.ReadLogLinksBuilder;
import app.webapp.payload.deserializers.noark5.admin.ReadLogDeserializer;
import app.webapp.payload.links.admin.ReadLogLinks;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.REL_LOGGING_READ_LOG;
import static app.utils.constants.Constants.TABLE_READ_LOG;
import static app.utils.constants.N5ResourceMappings.READ_LOG;

@Entity
@Table(name = TABLE_READ_LOG)

@JsonDeserialize(using = ReadLogDeserializer.class)
@LinksPacker(using = ReadLogLinksBuilder.class)
@LinksObject(using = ReadLogLinks.class)
public class ReadLog
        extends EventLog
        implements IReadLogEntity {


    @Override
    public String getBaseTypeName() {
        return READ_LOG;
    }

    @Override
    public String getBaseRel() {
        return REL_LOGGING_READ_LOG;
    }

}
