package app.domain.repository.noark5.v5.metadata;

import app.domain.noark5.metadata.ClassifiedCode;

public interface IClassifiedCodeRepository
        extends IMetadataRepository<ClassifiedCode, String> {

}
