package app.domain.repository.noark5.v5.admin;

import app.domain.noark5.admin.ReadLog;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface IReadLogRepository extends
        CrudRepository<ReadLog, UUID> {

    ReadLog findBySystemId(UUID systemId);
}
