package app.domain.repository.noark5.v5.metadata;

import app.domain.noark5.metadata.DeletionType;

public interface IDeletionTypeRepository
        extends IMetadataRepository<DeletionType, String> {

}
