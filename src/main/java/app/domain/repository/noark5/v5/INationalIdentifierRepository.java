package app.domain.repository.noark5.v5;

import app.domain.noark5.nationalidentifier.NationalIdentifier;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface INationalIdentifierRepository
    extends NoarkEntityRepository<NationalIdentifier, UUID> {
}
