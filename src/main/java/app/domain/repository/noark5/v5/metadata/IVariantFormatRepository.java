package app.domain.repository.noark5.v5.metadata;

import app.domain.noark5.metadata.VariantFormat;

public interface IVariantFormatRepository
        extends IMetadataRepository<VariantFormat, String> {

}
