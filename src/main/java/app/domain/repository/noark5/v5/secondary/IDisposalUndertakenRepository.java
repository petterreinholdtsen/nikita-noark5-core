package app.domain.repository.noark5.v5.secondary;

import app.domain.noark5.secondary.DisposalUndertaken;
import app.domain.repository.noark5.v5.NoarkEntityRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface IDisposalUndertakenRepository extends
        NoarkEntityRepository<DisposalUndertaken, UUID> {
}
