package app.domain.repository.noark5.v5.metadata;

import app.domain.noark5.metadata.ScreeningDocument;

public interface IScreeningDocumentRepository
        extends IMetadataRepository<ScreeningDocument, String> {

}
