package app.domain.repository.noark5.v5.secondary;

import app.domain.noark5.secondary.SignOff;
import app.domain.repository.noark5.v5.NoarkEntityRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ISignOffRepository extends
        NoarkEntityRepository<SignOff, UUID> {
}
