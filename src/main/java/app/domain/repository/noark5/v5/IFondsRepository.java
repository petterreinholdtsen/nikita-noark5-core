package app.domain.repository.noark5.v5;

import app.domain.noark5.Fonds;
import app.domain.noark5.admin.Organisation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface IFondsRepository extends
        CrudRepository<Fonds, UUID> {

    Fonds findBySystemId(UUID systemId);

    long deleteByOwnedBy(Organisation ownedBy);
}
