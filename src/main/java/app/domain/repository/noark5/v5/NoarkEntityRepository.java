package app.domain.repository.noark5.v5;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

/**
 * Created by tsodring on 4/2/17.
 */

@Repository
@NoRepositoryBean
public interface NoarkEntityRepository<INoarkEntity, ID extends Serializable> extends
        CrudRepository<INoarkEntity, UUID> {

    List<INoarkEntity> findAll();

    INoarkEntity findBySystemId(UUID systemId);
}
