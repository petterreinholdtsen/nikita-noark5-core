package app.domain.repository.noark5.v5.metadata;

import app.domain.noark5.metadata.FlowStatus;

public interface IFlowStatusRepository
        extends IMetadataRepository<FlowStatus, String> {

}
