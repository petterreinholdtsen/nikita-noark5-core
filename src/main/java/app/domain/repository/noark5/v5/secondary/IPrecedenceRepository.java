package app.domain.repository.noark5.v5.secondary;

import app.domain.noark5.secondary.Precedence;
import app.domain.repository.noark5.v5.NoarkEntityRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IPrecedenceRepository
        extends NoarkEntityRepository<Precedence, String> {

}
