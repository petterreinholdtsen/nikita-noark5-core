package app.domain.repository.noark5.v5.secondary;

import app.domain.noark5.secondary.DocumentFlow;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface IDocumentFlowRepository
        extends CrudRepository<DocumentFlow, UUID> {

    DocumentFlow findBySystemId(UUID systemId);
}
