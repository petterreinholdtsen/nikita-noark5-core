package app.domain.repository.noark5.v5.metadata;

import app.domain.noark5.metadata.CorrespondencePartType;

public interface ICorrespondencePartTypeRepository
        extends IMetadataRepository<CorrespondencePartType, String> {

}
