package app.domain.repository.noark5.v5.secondary;

import app.domain.noark5.secondary.CrossReference;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface ICrossReferenceRepository
        extends CrudRepository<CrossReference, UUID> {
    CrossReference findBySystemId(UUID systemId);

    Optional<CrossReference> findByFromSystemIdAndToSystemId(
            UUID fromSystemId, UUID toSystemId);
}
