package app.domain.repository.noark5.v5.metadata;

import app.domain.noark5.metadata.PostalCode;

public interface IPostalCodeRepository
        extends IMetadataRepository<PostalCode, String> {

}
