package app.domain.repository.noark5.v5;

import app.domain.interfaces.entities.ISystemId;
import app.domain.noark5.SystemIdEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ISystemIdEntityRepository extends
        CrudRepository<SystemIdEntity, UUID> {
    ISystemId findBySystemId(UUID systemId);
}
