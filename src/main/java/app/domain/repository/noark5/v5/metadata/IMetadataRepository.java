package app.domain.repository.noark5.v5.metadata;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

@Repository
@NoRepositoryBean
public interface IMetadataRepository
        <Metadata, ID extends Serializable> extends
        CrudRepository<Metadata, String> {
    List<Metadata> findAll();

    Metadata findByCode(String code);
}
