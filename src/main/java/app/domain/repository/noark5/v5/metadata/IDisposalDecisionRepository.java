package app.domain.repository.noark5.v5.metadata;

import app.domain.noark5.metadata.DisposalDecision;

public interface IDisposalDecisionRepository
        extends IMetadataRepository<DisposalDecision, String> {

}
