package app.domain.repository.noark5.v5;

import app.domain.noark5.admin.ChangeLog;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface IChangeLogRepository extends
        CrudRepository<ChangeLog, UUID> {

    ChangeLog findBySystemId(UUID systemId);
}
