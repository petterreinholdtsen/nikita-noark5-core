package app.domain.repository.noark5.v5.metadata;

import app.domain.noark5.metadata.MeetingRegistrationType;

public interface IMeetingRegistrationTypeRepository
        extends IMetadataRepository<MeetingRegistrationType, String> {

}
