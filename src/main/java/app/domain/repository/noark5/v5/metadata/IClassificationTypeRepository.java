package app.domain.repository.noark5.v5.metadata;

import app.domain.noark5.metadata.ClassificationType;

public interface IClassificationTypeRepository
        extends IMetadataRepository<ClassificationType, String> {

}
