package app.domain.repository.noark5.v5.metadata;

import app.domain.noark5.metadata.MeetingParticipantFunction;

public interface IMeetingParticipantFunctionRepository
        extends IMetadataRepository<MeetingParticipantFunction, String> {

}
