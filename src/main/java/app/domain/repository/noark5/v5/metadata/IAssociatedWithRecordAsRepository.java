package app.domain.repository.noark5.v5.metadata;

import app.domain.noark5.metadata.AssociatedWithRecordAs;

public interface IAssociatedWithRecordAsRepository
        extends IMetadataRepository<AssociatedWithRecordAs, String> {
}
