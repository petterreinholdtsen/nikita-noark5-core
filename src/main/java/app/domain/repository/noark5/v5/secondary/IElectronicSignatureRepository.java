package app.domain.repository.noark5.v5.secondary;

import app.domain.noark5.secondary.ElectronicSignature;
import app.domain.repository.noark5.v5.NoarkEntityRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface IElectronicSignatureRepository extends
        NoarkEntityRepository<ElectronicSignature, UUID> {
}
