// Generated from /home/tsodring/git/nikita-noark5-core/src/main/antlr4/ODataParser.g4 by ANTLR 4.10.1

package app.webapp.odata.base;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.ATN;
import org.antlr.v4.runtime.atn.ATNDeserializer;
import org.antlr.v4.runtime.atn.ParserATNSimulator;
import org.antlr.v4.runtime.atn.PredictionContextCache;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.tree.ParseTreeListener;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.List;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ODataParser extends Parser {
	public static final int
		SPACE=1, FILTER=2, TOP=3, SKIPRULE=4, SKIPTOKEN=5, ORDERBY=6, REF=7, EXPAND=8,
		COUNT=9, SELECT=10, DOLLARID=11, CONTAINS=12, STARTSWITH=13, ENDSWITH=14,
		SUBSTRINGOF=15, LENGTH=16, INDEXOF=17, REPLACE=18, SUBSTRING=19, TOLOWER=20,
		TOUPPER=21, TRIM=22, CONCAT=23, DAY=24, MONTH=25, YEAR=26, HOUR=27, MINUTE=28,
		SECOND=29, NOW=30, TIME=31, MAX_DATE_TIME=32, MIN_DATE_TIME=33, TOTAL_OFFSET_MINUTES=34,
		FRACTIONAL_SECONDS=35, TOTAL_SECONDS=36, GEO_INTERSECTS=37, GEO_DISTANCE=38,
		GEO_LENGTH=39, ROUND=40, FLOOR=41, CEILING=42, CAST=43, ISOF=44, EQUAL=45,
		EQ=46, GT=47, LT=48, GE=49, LE=50, NE=51, ADD=52, SUB=53, MUL=54, DIV=55,
		MOD=56, ORDER=57, BY=58, DESC=59, ASC=60, OR=61, AND=62, NOT=63, SEPERATOR=64,
		HTTP=65, HTTPS=66, OPEN=67, CLOSE=68, COMMA=69, QUESTION=70, DOLLAR=71,
		SEMI=72, AT_SIGN=73, BAR=74, SINGLE_QUOTE_SYMB=75, DOUBLE_QUOTE_SYMB=76,
		REVERSE_QUOTE_SYMB=77, COLON_SYMB=78, AMPERSAND=79, NULL_TOKEN=80, NULL_SPEC_LITERAL=81,
		DOT=82, SLASH=83, UNDERSCORE=84, EDM=85, COLLECTION=86, GEOGRAPHY=87,
		GEOMETRY=88, BINARY=89, BOOLEAN=90, BYTE=91, DATE=92, DATETIMEOFFSET=93,
		DECIMAL=94, DOUBLE=95, DURATION=96, GUID=97, INT16=98, INT32=99, INT64=100,
		SBYTE=101, SINGLE=102, STREAM=103, STRING=104, TIMEOFDAY=105, LINESTRING=106,
		MULTILINESTRING=107, MULTIPOINT=108, MULTIPOLYGON=109, POINT=110, POLYGON=111,
		BOOLEAN_VALUE=112, UUID=113, INTEGER=114, FLOAT=115, BSM_ID=116, ID=117,
			QUOTED_UUID = 118, QUOTED_STRING = 119, DQUOTED_STRING = 120, DECIMAL_LITERAL = 121,
		TIME_OF_DAY_VALUE=122, DURATION_VALUE=123, DATE_VALUE=124, DATE_TIME_OFFSET_VALUE=125,
		HOUR_DEF=126, MINUTE_DEF=127, SECOND_DEF=128, ZERO_TO_FIFTY_NINE=129,
		ONE_TO_NINE=130, YEAR_DEF=131, MONTH_DEF=132, DAY_DEF=133, HEX=134, SINGLE_CHAR_SMALL=135,
		SINGLE_CHAR=136, NEGATIVE=137, DEC_OCTECT=138, HEX_NUMBER=139, BIN_NUMBER=140,
		ERROR_RECONGNIGION=141;

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		RULE_odataRelativeUri = 0, RULE_resourcePath = 1, RULE_entity = 2, RULE_entityCast = 3,
		RULE_entityUUID = 4, RULE_embeddedEntitySet = 5, RULE_queryOptions = 6,
		RULE_queryOption = 7, RULE_filter = 8, RULE_expand = 9, RULE_filterExpression = 10,
		RULE_boolCommonExpr = 11, RULE_leftComparatorExpr = 12, RULE_rightComparatorExpr = 13,
		RULE_orderBy = 14, RULE_orderByItem = 15, RULE_topStatement = 16, RULE_skipStatement = 17,
		RULE_joinEntities = 18, RULE_commonExpr = 19, RULE_mathExpr = 20, RULE_methodExpr = 21,
		RULE_compareMethodExpr = 22, RULE_methodCallExpr = 23, RULE_calenderMethodExp = 24,
		RULE_concatMethodExpr = 25, RULE_singleMethodCallExpr = 26, RULE_substringMethodCallExpr = 27,
		RULE_comparisonOperator = 28, RULE_compareMethodName = 29, RULE_methodName = 30,
		RULE_calenderMethodName = 31, RULE_number = 32, RULE_primitiveLiteral = 33,
		RULE_countStatement = 34, RULE_attributeName = 35, RULE_openPar = 36,
		RULE_closePar = 37, RULE_logicalOperator = 38, RULE_sortOrder = 39, RULE_entityName = 40,
		RULE_orderAttributeName = 41, RULE_uuidIdValue = 42, RULE_quotedUUID = 43,
			RULE_quotedString = 44, RULE_dquotedString = 45, RULE_nullSpecLiteral = 46,
			RULE_nullToken = 47, RULE_booleanValue = 48, RULE_durationValue = 49,
			RULE_dateValue = 50, RULE_dateTimeOffsetValue = 51, RULE_timeOfDayValue = 52,
			RULE_decimalLiteral = 53, RULE_floatValue = 54, RULE_integerValue = 55;
	public static final String _serializedATN =
			"\u0004\u0001\u008d\u0191\u0002\u0000\u0007\u0000\u0002\u0001\u0007\u0001" +
		"\u0002\u0002\u0007\u0002\u0002\u0003\u0007\u0003\u0002\u0004\u0007\u0004"+
		"\u0002\u0005\u0007\u0005\u0002\u0006\u0007\u0006\u0002\u0007\u0007\u0007"+
		"\u0002\b\u0007\b\u0002\t\u0007\t\u0002\n\u0007\n\u0002\u000b\u0007\u000b"+
		"\u0002\f\u0007\f\u0002\r\u0007\r\u0002\u000e\u0007\u000e\u0002\u000f\u0007"+
		"\u000f\u0002\u0010\u0007\u0010\u0002\u0011\u0007\u0011\u0002\u0012\u0007"+
		"\u0012\u0002\u0013\u0007\u0013\u0002\u0014\u0007\u0014\u0002\u0015\u0007"+
		"\u0015\u0002\u0016\u0007\u0016\u0002\u0017\u0007\u0017\u0002\u0018\u0007"+
		"\u0018\u0002\u0019\u0007\u0019\u0002\u001a\u0007\u001a\u0002\u001b\u0007"+
		"\u001b\u0002\u001c\u0007\u001c\u0002\u001d\u0007\u001d\u0002\u001e\u0007"+
		"\u001e\u0002\u001f\u0007\u001f\u0002 \u0007 \u0002!\u0007!\u0002\"\u0007"+
		"\"\u0002#\u0007#\u0002$\u0007$\u0002%\u0007%\u0002&\u0007&\u0002\'\u0007"+
		"\'\u0002(\u0007(\u0002)\u0007)\u0002*\u0007*\u0002+\u0007+\u0002,\u0007"+
		",\u0002-\u0007-\u0002.\u0007.\u0002/\u0007/\u00020\u00070\u00021\u0007"+
		"1\u00022\u00072\u00023\u00073\u00024\u00074\u00025\u00075\u00026\u0007"+
					"6\u00027\u00077\u0001\u0000\u0001\u0000\u0001\u0000\u0003\u0000t\b\u0000" +
					"\u0001\u0001\u0001\u0001\u0001\u0001\u0003\u0001y\b\u0001\u0001\u0001" +
					"\u0003\u0001|\b\u0001\u0001\u0002\u0001\u0002\u0003\u0002\u0080\b\u0002" +
					"\u0001\u0003\u0001\u0003\u0001\u0003\u0001\u0003\u0001\u0004\u0001\u0004" +
					"\u0001\u0004\u0001\u0004\u0001\u0005\u0001\u0005\u0001\u0005\u0001\u0005" +
					"\u0001\u0005\u0001\u0005\u0003\u0005\u0090\b\u0005\u0005\u0005\u0092\b" +
					"\u0005\n\u0005\f\u0005\u0095\t\u0005\u0001\u0006\u0001\u0006\u0001\u0006" +
					"\u0005\u0006\u009a\b\u0006\n\u0006\f\u0006\u009d\t\u0006\u0001\u0007\u0001" +
					"\u0007\u0001\u0007\u0001\u0007\u0001\u0007\u0003\u0007\u00a4\b\u0007\u0001" +
					"\b\u0001\b\u0001\b\u0001\b\u0001\t\u0001\t\u0001\t\u0001\t\u0001\t\u0003" +
					"\t\u00af\b\t\u0001\t\u0001\t\u0003\t\u00b3\b\t\u0001\n\u0001\n\u0001\n" +
					"\u0001\n\u0001\n\u0001\n\u0003\n\u00bb\b\n\u0001\n\u0001\n\u0001\n\u0001" +
					"\n\u0005\n\u00c1\b\n\n\n\f\n\u00c4\t\n\u0001\u000b\u0001\u000b\u0001\u000b" +
					"\u0001\u000b\u0001\u000b\u0001\u000b\u0001\u000b\u0001\u000b\u0001\u000b" +
					"\u0003\u000b\u00cf\b\u000b\u0001\f\u0001\f\u0001\f\u0001\f\u0001\f\u0001" +
					"\f\u0003\f\u00d7\b\f\u0001\r\u0001\r\u0001\r\u0001\r\u0001\r\u0001\r\u0003" +
					"\r\u00df\b\r\u0001\u000e\u0001\u000e\u0001\u000e\u0001\u000e\u0001\u000e" +
					"\u0005\u000e\u00e6\b\u000e\n\u000e\f\u000e\u00e9\t\u000e\u0001\u000f\u0001" +
					"\u000f\u0003\u000f\u00ed\b\u000f\u0001\u0010\u0001\u0010\u0001\u0010\u0001" +
					"\u0011\u0001\u0011\u0001\u0011\u0001\u0012\u0001\u0012\u0001\u0012\u0004" +
					"\u0012\u00f8\b\u0012\u000b\u0012\f\u0012\u00f9\u0001\u0012\u0001\u0012" +
					"\u0001\u0013\u0001\u0013\u0001\u0013\u0003\u0013\u0101\b\u0013\u0001\u0014" +
					"\u0001\u0014\u0001\u0014\u0001\u0015\u0001\u0015\u0001\u0015\u0001\u0015" +
					"\u0001\u0015\u0001\u0015\u0003\u0015\u010c\b\u0015\u0001\u0016\u0001\u0016" +
					"\u0001\u0016\u0001\u0016\u0001\u0016\u0003\u0016\u0113\b\u0016\u0001\u0016" +
					"\u0001\u0016\u0001\u0016\u0001\u0016\u0001\u0017\u0001\u0017\u0001\u0017" +
					"\u0001\u0017\u0001\u0017\u0003\u0017\u011e\b\u0017\u0001\u0017\u0001\u0017" +
					"\u0001\u0018\u0001\u0018\u0001\u0018\u0001\u0018\u0001\u0018\u0003\u0018" +
					"\u0127\b\u0018\u0001\u0018\u0001\u0018\u0001\u0019\u0001\u0019\u0001\u0019" +
					"\u0001\u0019\u0001\u0019\u0003\u0019\u0130\b\u0019\u0001\u0019\u0001\u0019" +
					"\u0001\u0019\u0003\u0019\u0135\b\u0019\u0001\u0019\u0001\u0019\u0001\u001a" +
					"\u0001\u001a\u0001\u001a\u0001\u001a\u0001\u001b\u0001\u001b\u0001\u001b" +
					"\u0001\u001b\u0001\u001b\u0003\u001b\u0142\b\u001b\u0001\u001b\u0001\u001b" +
					"\u0001\u001b\u0001\u001b\u0001\u001c\u0001\u001c\u0001\u001d\u0001\u001d" +
					"\u0001\u001e\u0001\u001e\u0001\u001f\u0001\u001f\u0001 \u0001 \u0001!" +
					"\u0001!\u0001!\u0001!\u0001!\u0001!\u0001!\u0001!\u0001!\u0001!\u0001" +
					"!\u0001!\u0001!\u0003!\u015f\b!\u0001\"\u0001\"\u0001\"\u0001\"\u0003" +
					"\"\u0165\b\"\u0001#\u0001#\u0001$\u0001$\u0001%\u0001%\u0001&\u0001&\u0001" +
					"\'\u0001\'\u0001(\u0001(\u0001)\u0001)\u0001*\u0001*\u0001+\u0001+\u0001" +
					",\u0001,\u0001-\u0001-\u0001.\u0001.\u0001/\u0001/\u00010\u00010\u0001" +
					"1\u00011\u00012\u00012\u00013\u00013\u00014\u00014\u00015\u00015\u0001" +
					"6\u00016\u00017\u00017\u00017\u0000\u0001\u00148\u0000\u0002\u0004\u0006" +
					"\b\n\f\u000e\u0010\u0012\u0014\u0016\u0018\u001a\u001c\u001e \"$&(*,." +
					"02468:<>@BDFHJLNPRTVXZ\\^`bdfhjln\u0000\n\u0001\u000048\u0002\u0000\u001e" +
					"\u001e !\u0001\u0000.3\u0003\u0000\f\u000e\u0011\u0011%&\u0003\u0000\u0010" +
					"\u0010\u0014\u0016\'*\u0004\u0000\u0018\u001d\u001f\u001f\"$\\\\\u0001" +
					"\u0000rs\u0001\u0000tu\u0001\u0000=>\u0001\u0000;<\u0198\u0000p\u0001" +
					"\u0000\u0000\u0000\u0002x\u0001\u0000\u0000\u0000\u0004\u007f\u0001\u0000" +
					"\u0000\u0000\u0006\u0081\u0001\u0000\u0000\u0000\b\u0085\u0001\u0000\u0000" +
					"\u0000\n\u0089\u0001\u0000\u0000\u0000\f\u0096\u0001\u0000\u0000\u0000" +
					"\u000e\u00a3\u0001\u0000\u0000\u0000\u0010\u00a5\u0001\u0000\u0000\u0000" +
					"\u0012\u00a9\u0001\u0000\u0000\u0000\u0014\u00ba\u0001\u0000\u0000\u0000" +
					"\u0016\u00ce\u0001\u0000\u0000\u0000\u0018\u00d6\u0001\u0000\u0000\u0000" +
					"\u001a\u00de\u0001\u0000\u0000\u0000\u001c\u00e0\u0001\u0000\u0000\u0000" +
					"\u001e\u00ea\u0001\u0000\u0000\u0000 \u00ee\u0001\u0000\u0000\u0000\"" +
					"\u00f1\u0001\u0000\u0000\u0000$\u00f7\u0001\u0000\u0000\u0000&\u0100\u0001" +
					"\u0000\u0000\u0000(\u0102\u0001\u0000\u0000\u0000*\u010b\u0001\u0000\u0000" +
					"\u0000,\u010d\u0001\u0000\u0000\u0000.\u0118\u0001\u0000\u0000\u00000" +
					"\u0121\u0001\u0000\u0000\u00002\u012a\u0001\u0000\u0000\u00004\u0138\u0001" +
					"\u0000\u0000\u00006\u013c\u0001\u0000\u0000\u00008\u0147\u0001\u0000\u0000" +
					"\u0000:\u0149\u0001\u0000\u0000\u0000<\u014b\u0001\u0000\u0000\u0000>" +
					"\u014d\u0001\u0000\u0000\u0000@\u014f\u0001\u0000\u0000\u0000B\u015e\u0001" +
					"\u0000\u0000\u0000D\u0160\u0001\u0000\u0000\u0000F\u0166\u0001\u0000\u0000" +
					"\u0000H\u0168\u0001\u0000\u0000\u0000J\u016a\u0001\u0000\u0000\u0000L" +
					"\u016c\u0001\u0000\u0000\u0000N\u016e\u0001\u0000\u0000\u0000P\u0170\u0001" +
					"\u0000\u0000\u0000R\u0172\u0001\u0000\u0000\u0000T\u0174\u0001\u0000\u0000" +
					"\u0000V\u0176\u0001\u0000\u0000\u0000X\u0178\u0001\u0000\u0000\u0000Z" +
					"\u017a\u0001\u0000\u0000\u0000\\\u017c\u0001\u0000\u0000\u0000^\u017e" +
					"\u0001\u0000\u0000\u0000`\u0180\u0001\u0000\u0000\u0000b\u0182\u0001\u0000" +
					"\u0000\u0000d\u0184\u0001\u0000\u0000\u0000f\u0186\u0001\u0000\u0000\u0000" +
					"h\u0188\u0001\u0000\u0000\u0000j\u018a\u0001\u0000\u0000\u0000l\u018c" +
					"\u0001\u0000\u0000\u0000n\u018e\u0001\u0000\u0000\u0000ps\u0003\u0002" +
					"\u0001\u0000qr\u0005F\u0000\u0000rt\u0003\f\u0006\u0000sq\u0001\u0000" +
					"\u0000\u0000st\u0001\u0000\u0000\u0000t\u0001\u0001\u0000\u0000\u0000" +
					"uy\u0003\n\u0005\u0000vy\u0003\b\u0004\u0000wy\u0003\u0004\u0002\u0000" +
					"xu\u0001\u0000\u0000\u0000xv\u0001\u0000\u0000\u0000xw\u0001\u0000\u0000" +
					"\u0000y{\u0001\u0000\u0000\u0000z|\u0003D\"\u0000{z\u0001\u0000\u0000" +
					"\u0000{|\u0001\u0000\u0000\u0000|\u0003\u0001\u0000\u0000\u0000}\u0080" +
					"\u0003P(\u0000~\u0080\u0003\u0006\u0003\u0000\u007f}\u0001\u0000\u0000" +
					"\u0000\u007f~\u0001\u0000\u0000\u0000\u0080\u0005\u0001\u0000\u0000\u0000" +
					"\u0081\u0082\u0003P(\u0000\u0082\u0083\u0005S\u0000\u0000\u0083\u0084" +
					"\u0003P(\u0000\u0084\u0007\u0001\u0000\u0000\u0000\u0085\u0086\u0003P" +
					"(\u0000\u0086\u0087\u0005S\u0000\u0000\u0087\u0088\u0003T*\u0000\u0088" +
					"\t\u0001\u0000\u0000\u0000\u0089\u0093\u0003\b\u0004\u0000\u008a\u008b" +
					"\u0005S\u0000\u0000\u008b\u0092\u0003\b\u0004\u0000\u008c\u008f\u0005" +
					"S\u0000\u0000\u008d\u0090\u0003\u0006\u0003\u0000\u008e\u0090\u0003\u0004" +
					"\u0002\u0000\u008f\u008d\u0001\u0000\u0000\u0000\u008f\u008e\u0001\u0000" +
					"\u0000\u0000\u0090\u0092\u0001\u0000\u0000\u0000\u0091\u008a\u0001\u0000" +
					"\u0000\u0000\u0091\u008c\u0001\u0000\u0000\u0000\u0092\u0095\u0001\u0000" +
					"\u0000\u0000\u0093\u0091\u0001\u0000\u0000\u0000\u0093\u0094\u0001\u0000" +
					"\u0000\u0000\u0094\u000b\u0001\u0000\u0000\u0000\u0095\u0093\u0001\u0000" +
					"\u0000\u0000\u0096\u009b\u0003\u000e\u0007\u0000\u0097\u0098\u0005O\u0000" +
					"\u0000\u0098\u009a\u0003\u000e\u0007\u0000\u0099\u0097\u0001\u0000\u0000" +
					"\u0000\u009a\u009d\u0001\u0000\u0000\u0000\u009b\u0099\u0001\u0000\u0000" +
					"\u0000\u009b\u009c\u0001\u0000\u0000\u0000\u009c\r\u0001\u0000\u0000\u0000" +
					"\u009d\u009b\u0001\u0000\u0000\u0000\u009e\u00a4\u0003\u0010\b\u0000\u009f" +
					"\u00a4\u0003\u0012\t\u0000\u00a0\u00a4\u0003\u001c\u000e\u0000\u00a1\u00a4" +
					"\u0003\"\u0011\u0000\u00a2\u00a4\u0003 \u0010\u0000\u00a3\u009e\u0001" +
					"\u0000\u0000\u0000\u00a3\u009f\u0001\u0000\u0000\u0000\u00a3\u00a0\u0001" +
					"\u0000\u0000\u0000\u00a3\u00a1\u0001\u0000\u0000\u0000\u00a3\u00a2\u0001" +
					"\u0000\u0000\u0000\u00a4\u000f\u0001\u0000\u0000\u0000\u00a5\u00a6\u0005" +
					"\u0002\u0000\u0000\u00a6\u00a7\u0005-\u0000\u0000\u00a7\u00a8\u0003\u0014" +
					"\n\u0000\u00a8\u0011\u0001\u0000\u0000\u0000\u00a9\u00aa\u0005\b\u0000" +
					"\u0000\u00aa\u00ae\u0005-\u0000\u0000\u00ab\u00af\u0003$\u0012\u0000\u00ac" +
					"\u00af\u0003F#\u0000\u00ad\u00af\u0003\u0016\u000b\u0000\u00ae\u00ab\u0001" +
					"\u0000\u0000\u0000\u00ae\u00ac\u0001\u0000\u0000\u0000\u00ae\u00ad\u0001" +
					"\u0000\u0000\u0000\u00af\u00b2\u0001\u0000\u0000\u0000\u00b0\u00b1\u0005" +
					"S\u0000\u0000\u00b1\u00b3\u0003\u0010\b\u0000\u00b2\u00b0\u0001\u0000" +
					"\u0000\u0000\u00b2\u00b3\u0001\u0000\u0000\u0000\u00b3\u0013\u0001\u0000" +
					"\u0000\u0000\u00b4\u00b5\u0006\n\uffff\uffff\u0000\u00b5\u00b6\u0003H" +
					"$\u0000\u00b6\u00b7\u0003\u0014\n\u0000\u00b7\u00b8\u0003J%\u0000\u00b8" +
					"\u00bb\u0001\u0000\u0000\u0000\u00b9\u00bb\u0003\u0016\u000b\u0000\u00ba" +
					"\u00b4\u0001\u0000\u0000\u0000\u00ba\u00b9\u0001\u0000\u0000\u0000\u00bb" +
					"\u00c2\u0001\u0000\u0000\u0000\u00bc\u00bd\n\u0002\u0000\u0000\u00bd\u00be" +
					"\u0003L&\u0000\u00be\u00bf\u0003\u0014\n\u0003\u00bf\u00c1\u0001\u0000" +
					"\u0000\u0000\u00c0\u00bc\u0001\u0000\u0000\u0000\u00c1\u00c4\u0001\u0000" +
					"\u0000\u0000\u00c2\u00c0\u0001\u0000\u0000\u0000\u00c2\u00c3\u0001\u0000" +
					"\u0000\u0000\u00c3\u0015\u0001\u0000\u0000\u0000\u00c4\u00c2\u0001\u0000" +
					"\u0000\u0000\u00c5\u00c6\u0003\u0018\f\u0000\u00c6\u00c7\u00038\u001c" +
					"\u0000\u00c7\u00c8\u0003\u001a\r\u0000\u00c8\u00cf\u0001\u0000\u0000\u0000" +
					"\u00c9\u00ca\u0005\t\u0000\u0000\u00ca\u00cb\u00038\u001c\u0000\u00cb" +
					"\u00cc\u0003B!\u0000\u00cc\u00cf\u0001\u0000\u0000\u0000\u00cd\u00cf\u0003" +
					",\u0016\u0000\u00ce\u00c5\u0001\u0000\u0000\u0000\u00ce\u00c9\u0001\u0000" +
					"\u0000\u0000\u00ce\u00cd\u0001\u0000\u0000\u0000\u00cf\u0017\u0001\u0000" +
					"\u0000\u0000\u00d0\u00d7\u0003.\u0017\u0000\u00d1\u00d7\u00030\u0018\u0000" +
					"\u00d2\u00d7\u00032\u0019\u0000\u00d3\u00d7\u0003$\u0012\u0000\u00d4\u00d7" +
					"\u0003F#\u0000\u00d5\u00d7\u0003&\u0013\u0000\u00d6\u00d0\u0001\u0000" +
					"\u0000\u0000\u00d6\u00d1\u0001\u0000\u0000\u0000\u00d6\u00d2\u0001\u0000" +
					"\u0000\u0000\u00d6\u00d3\u0001\u0000\u0000\u0000\u00d6\u00d4\u0001\u0000" +
					"\u0000\u0000\u00d6\u00d5\u0001\u0000\u0000\u0000\u00d7\u0019\u0001\u0000" +
					"\u0000\u0000\u00d8\u00df\u0003.\u0017\u0000\u00d9\u00df\u00030\u0018\u0000" +
					"\u00da\u00df\u00032\u0019\u0000\u00db\u00df\u0003$\u0012\u0000\u00dc\u00df" +
					"\u0003F#\u0000\u00dd\u00df\u0003&\u0013\u0000\u00de\u00d8\u0001\u0000" +
					"\u0000\u0000\u00de\u00d9\u0001\u0000\u0000\u0000\u00de\u00da\u0001\u0000" +
					"\u0000\u0000\u00de\u00db\u0001\u0000\u0000\u0000\u00de\u00dc\u0001\u0000" +
					"\u0000\u0000\u00de\u00dd\u0001\u0000\u0000\u0000\u00df\u001b\u0001\u0000" +
					"\u0000\u0000\u00e0\u00e1\u0005\u0006\u0000\u0000\u00e1\u00e2\u0005-\u0000" +
					"\u0000\u00e2\u00e7\u0003\u001e\u000f\u0000\u00e3\u00e4\u0005E\u0000\u0000" +
					"\u00e4\u00e6\u0003\u001e\u000f\u0000\u00e5\u00e3\u0001\u0000\u0000\u0000" +
					"\u00e6\u00e9\u0001\u0000\u0000\u0000\u00e7\u00e5\u0001\u0000\u0000\u0000" +
					"\u00e7\u00e8\u0001\u0000\u0000\u0000\u00e8\u001d\u0001\u0000\u0000\u0000" +
					"\u00e9\u00e7\u0001\u0000\u0000\u0000\u00ea\u00ec\u0003R)\u0000\u00eb\u00ed" +
					"\u0003N\'\u0000\u00ec\u00eb\u0001\u0000\u0000\u0000\u00ec\u00ed\u0001" +
					"\u0000\u0000\u0000\u00ed\u001f\u0001\u0000\u0000\u0000\u00ee\u00ef\u0005" +
					"\u0003\u0000\u0000\u00ef\u00f0\u0003n7\u0000\u00f0!\u0001\u0000\u0000" +
					"\u0000\u00f1\u00f2\u0005\u0004\u0000\u0000\u00f2\u00f3\u0003n7\u0000\u00f3" +
					"#\u0001\u0000\u0000\u0000\u00f4\u00f5\u0003P(\u0000\u00f5\u00f6\u0005" +
					"S\u0000\u0000\u00f6\u00f8\u0001\u0000\u0000\u0000\u00f7\u00f4\u0001\u0000" +
					"\u0000\u0000\u00f8\u00f9\u0001\u0000\u0000\u0000\u00f9\u00f7\u0001\u0000" +
					"\u0000\u0000\u00f9\u00fa\u0001\u0000\u0000\u0000\u00fa\u00fb\u0001\u0000" +
					"\u0000\u0000\u00fb\u00fc\u0003F#\u0000\u00fc%\u0001\u0000\u0000\u0000" +
					"\u00fd\u0101\u0003B!\u0000\u00fe\u0101\u0003*\u0015\u0000\u00ff\u0101" +
					"\u0003(\u0014\u0000\u0100\u00fd\u0001\u0000\u0000\u0000\u0100\u00fe\u0001" +
					"\u0000\u0000\u0000\u0100\u00ff\u0001\u0000\u0000\u0000\u0101\'\u0001\u0000" +
					"\u0000\u0000\u0102\u0103\u0007\u0000\u0000\u0000\u0103\u0104\u0003@ \u0000" +
					"\u0104)\u0001\u0000\u0000\u0000\u0105\u010c\u0003.\u0017\u0000\u0106\u010c" +
					"\u00030\u0018\u0000\u0107\u010c\u00034\u001a\u0000\u0108\u010c\u00032" +
					"\u0019\u0000\u0109\u010c\u00036\u001b\u0000\u010a\u010c\u0003,\u0016\u0000" +
					"\u010b\u0105\u0001\u0000\u0000\u0000\u010b\u0106\u0001\u0000\u0000\u0000" +
					"\u010b\u0107\u0001\u0000\u0000\u0000\u010b\u0108\u0001\u0000\u0000\u0000" +
					"\u010b\u0109\u0001\u0000\u0000\u0000\u010b\u010a\u0001\u0000\u0000\u0000" +
					"\u010c+\u0001\u0000\u0000\u0000\u010d\u010e\u0003:\u001d\u0000\u010e\u0112" +
					"\u0005C\u0000\u0000\u010f\u0113\u0003$\u0012\u0000\u0110\u0113\u0003F" +
					"#\u0000\u0111\u0113\u0003&\u0013\u0000\u0112\u010f\u0001\u0000\u0000\u0000" +
					"\u0112\u0110\u0001\u0000\u0000\u0000\u0112\u0111\u0001\u0000\u0000\u0000" +
					"\u0113\u0114\u0001\u0000\u0000\u0000\u0114\u0115\u0005E\u0000\u0000\u0115" +
					"\u0116\u0003&\u0013\u0000\u0116\u0117\u0005D\u0000\u0000\u0117-\u0001" +
					"\u0000\u0000\u0000\u0118\u0119\u0003<\u001e\u0000\u0119\u011d\u0005C\u0000" +
					"\u0000\u011a\u011e\u0003.\u0017\u0000\u011b\u011e\u0003$\u0012\u0000\u011c" +
					"\u011e\u0003F#\u0000\u011d\u011a\u0001\u0000\u0000\u0000\u011d\u011b\u0001" +
					"\u0000\u0000\u0000\u011d\u011c\u0001\u0000\u0000\u0000\u011e\u011f\u0001" +
					"\u0000\u0000\u0000\u011f\u0120\u0005D\u0000\u0000\u0120/\u0001\u0000\u0000" +
					"\u0000\u0121\u0122\u0003>\u001f\u0000\u0122\u0126\u0005C\u0000\u0000\u0123" +
					"\u0127\u0003$\u0012\u0000\u0124\u0127\u0003F#\u0000\u0125\u0127\u0003" +
					"B!\u0000\u0126\u0123\u0001\u0000\u0000\u0000\u0126\u0124\u0001\u0000\u0000" +
					"\u0000\u0126\u0125\u0001\u0000\u0000\u0000\u0127\u0128\u0001\u0000\u0000" +
					"\u0000\u0128\u0129\u0005D\u0000\u0000\u01291\u0001\u0000\u0000\u0000\u012a" +
					"\u012b\u0005\u0017\u0000\u0000\u012b\u012f\u0005C\u0000\u0000\u012c\u0130" +
					"\u00032\u0019\u0000\u012d\u0130\u0003B!\u0000\u012e\u0130\u0003F#\u0000" +
					"\u012f\u012c\u0001\u0000\u0000\u0000\u012f\u012d\u0001\u0000\u0000\u0000" +
					"\u012f\u012e\u0001\u0000\u0000\u0000\u0130\u0131\u0001\u0000\u0000\u0000" +
					"\u0131\u0134\u0005E\u0000\u0000\u0132\u0135\u0003B!\u0000\u0133\u0135" +
					"\u0003F#\u0000\u0134\u0132\u0001\u0000\u0000\u0000\u0134\u0133\u0001\u0000" +
					"\u0000\u0000\u0135\u0136\u0001\u0000\u0000\u0000\u0136\u0137\u0005D\u0000" +
					"\u0000\u01373\u0001\u0000\u0000\u0000\u0138\u0139\u0007\u0001\u0000\u0000" +
					"\u0139\u013a\u0005C\u0000\u0000\u013a\u013b\u0005D\u0000\u0000\u013b5" +
					"\u0001\u0000\u0000\u0000\u013c\u013d\u0005\u0013\u0000\u0000\u013d\u0141" +
					"\u0005C\u0000\u0000\u013e\u0142\u0003$\u0012\u0000\u013f\u0142\u0003F" +
					"#\u0000\u0140\u0142\u0003&\u0013\u0000\u0141\u013e\u0001\u0000\u0000\u0000" +
					"\u0141\u013f\u0001\u0000\u0000\u0000\u0141\u0140\u0001\u0000\u0000\u0000" +
					"\u0142\u0143\u0001\u0000\u0000\u0000\u0143\u0144\u0005E\u0000\u0000\u0144" +
					"\u0145\u0003&\u0013\u0000\u0145\u0146\u0005D\u0000\u0000\u01467\u0001" +
					"\u0000\u0000\u0000\u0147\u0148\u0007\u0002\u0000\u0000\u01489\u0001\u0000" +
					"\u0000\u0000\u0149\u014a\u0007\u0003\u0000\u0000\u014a;\u0001\u0000\u0000" +
					"\u0000\u014b\u014c\u0007\u0004\u0000\u0000\u014c=\u0001\u0000\u0000\u0000" +
					"\u014d\u014e\u0007\u0005\u0000\u0000\u014e?\u0001\u0000\u0000\u0000\u014f" +
					"\u0150\u0007\u0006\u0000\u0000\u0150A\u0001\u0000\u0000\u0000\u0151\u015f" +
					"\u0003V+\u0000\u0152\u015f\u0003Z-\u0000\u0153\u015f\u0003X,\u0000\u0154" +
					"\u015f\u0003\\.\u0000\u0155\u015f\u0003^/\u0000\u0156\u015f\u0003`0\u0000" +
					"\u0157\u015f\u0003b1\u0000\u0158\u015f\u0003d2\u0000\u0159\u015f\u0003" +
					"f3\u0000\u015a\u015f\u0003h4\u0000\u015b\u015f\u0003j5\u0000\u015c\u015f" +
					"\u0003l6\u0000\u015d\u015f\u0003n7\u0000\u015e\u0151\u0001\u0000\u0000" +
					"\u0000\u015e\u0152\u0001\u0000\u0000\u0000\u015e\u0153\u0001\u0000\u0000" +
					"\u0000\u015e\u0154\u0001\u0000\u0000\u0000\u015e\u0155\u0001\u0000\u0000" +
					"\u0000\u015e\u0156\u0001\u0000\u0000\u0000\u015e\u0157\u0001\u0000\u0000" +
					"\u0000\u015e\u0158\u0001\u0000\u0000\u0000\u015e\u0159\u0001\u0000\u0000" +
					"\u0000\u015e\u015a\u0001\u0000\u0000\u0000\u015e\u015b\u0001\u0000\u0000" +
					"\u0000\u015e\u015c\u0001\u0000\u0000\u0000\u015e\u015d\u0001\u0000\u0000" +
					"\u0000\u015fC\u0001\u0000\u0000\u0000\u0160\u0161\u0005S\u0000\u0000\u0161" +
					"\u0164\u0005\t\u0000\u0000\u0162\u0163\u0005-\u0000\u0000\u0163\u0165" +
					"\u0003`0\u0000\u0164\u0162\u0001\u0000\u0000\u0000\u0164\u0165\u0001\u0000" +
					"\u0000\u0000\u0165E\u0001\u0000\u0000\u0000\u0166\u0167\u0007\u0007\u0000" +
					"\u0000\u0167G\u0001\u0000\u0000\u0000\u0168\u0169\u0005C\u0000\u0000\u0169" +
					"I\u0001\u0000\u0000\u0000\u016a\u016b\u0005D\u0000\u0000\u016bK\u0001" +
					"\u0000\u0000\u0000\u016c\u016d\u0007\b\u0000\u0000\u016dM\u0001\u0000" +
					"\u0000\u0000\u016e\u016f\u0007\t\u0000\u0000\u016fO\u0001\u0000\u0000" +
					"\u0000\u0170\u0171\u0005u\u0000\u0000\u0171Q\u0001\u0000\u0000\u0000\u0172" +
					"\u0173\u0005u\u0000\u0000\u0173S\u0001\u0000\u0000\u0000\u0174\u0175\u0005" +
					"q\u0000\u0000\u0175U\u0001\u0000\u0000\u0000\u0176\u0177\u0005v\u0000" +
					"\u0000\u0177W\u0001\u0000\u0000\u0000\u0178\u0179\u0005w\u0000\u0000\u0179" +
					"Y\u0001\u0000\u0000\u0000\u017a\u017b\u0005x\u0000\u0000\u017b[\u0001" +
					"\u0000\u0000\u0000\u017c\u017d\u0005Q\u0000\u0000\u017d]\u0001\u0000\u0000" +
					"\u0000\u017e\u017f\u0005P\u0000\u0000\u017f_\u0001\u0000\u0000\u0000\u0180" +
					"\u0181\u0005p\u0000\u0000\u0181a\u0001\u0000\u0000\u0000\u0182\u0183\u0005" +
					"{\u0000\u0000\u0183c\u0001\u0000\u0000\u0000\u0184\u0185\u0005|\u0000" +
					"\u0000\u0185e\u0001\u0000\u0000\u0000\u0186\u0187\u0005}\u0000\u0000\u0187" +
					"g\u0001\u0000\u0000\u0000\u0188\u0189\u0005z\u0000\u0000\u0189i\u0001" +
					"\u0000\u0000\u0000\u018a\u018b\u0005y\u0000\u0000\u018bk\u0001\u0000\u0000" +
					"\u0000\u018c\u018d\u0005s\u0000\u0000\u018dm\u0001\u0000\u0000\u0000\u018e" +
					"\u018f\u0005r\u0000\u0000\u018fo\u0001\u0000\u0000\u0000\u001dsx{\u007f" +
					"\u008f\u0091\u0093\u009b\u00a3\u00ae\u00b2\u00ba\u00c2\u00ce\u00d6\u00de" +
					"\u00e7\u00ec\u00f9\u0100\u010b\u0112\u011d\u0126\u012f\u0134\u0141\u015e" +
					"\u0164";

	static {
		RuntimeMetaData.checkVersion("4.10.1", RuntimeMetaData.VERSION);
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, null, null, null, null, "'$skiptoken'", "'$orderby'", "'$ref?$id='", 
			"'$expand'", "'$count'", "'$select='", "'$id='", null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, "'='", null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			"'://'", null, null, "'('", "')'", "','", "'?'", "'$'", "';'", "'@'", 
			"'|'", "'''", "'\"'", "'`'", "':'", "'&'", null, null, "'.'", "'/'", 
			"'_'", "'Edm'", "'Collection'", "'Geography'", "'Geometry'", "'Binary'", 
			"'Boolean'", "'Byte'", "'Date'", "'DateTimeOffset'", "'Decimal'", "'Double'", 
			"'Duration'", "'Guid'", "'Int16'", "'Int32'", "'Int64'", "'SByte'", "'Single'", 
			"'Stream'", "'String'", "'TimeOfDay'", null, "'MultiLineString'", "'MultiPoint'", 
			"'MultiPolygon'", "'Point'", "'Polygon'", null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, "'-'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();

	private static String[] makeRuleNames() {
		return new String[] {
			"odataRelativeUri", "resourcePath", "entity", "entityCast", "entityUUID",
			"embeddedEntitySet", "queryOptions", "queryOption", "filter", "expand",
			"filterExpression", "boolCommonExpr", "leftComparatorExpr", "rightComparatorExpr",
			"orderBy", "orderByItem", "topStatement", "skipStatement", "joinEntities",
			"commonExpr", "mathExpr", "methodExpr", "compareMethodExpr", "methodCallExpr",
			"calenderMethodExp", "concatMethodExpr", "singleMethodCallExpr", "substringMethodCallExpr",
			"comparisonOperator", "compareMethodName", "methodName", "calenderMethodName",
			"number", "primitiveLiteral", "countStatement", "attributeName", "openPar",
			"closePar", "logicalOperator", "sortOrder", "entityName", "orderAttributeName",
				"uuidIdValue", "quotedUUID", "quotedString", "dquotedString", "nullSpecLiteral",
				"nullToken", "booleanValue", "durationValue", "dateValue", "dateTimeOffsetValue",
			"timeOfDayValue", "decimalLiteral", "floatValue", "integerValue"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() {
		return "ODataParser.g4";
	}

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public ODataParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class OdataRelativeUriContext extends ParserRuleContext {
		public ResourcePathContext resourcePath() {
			return getRuleContext(ResourcePathContext.class,0);
		}
		public TerminalNode QUESTION() { return getToken(ODataParser.QUESTION, 0); }
		public QueryOptionsContext queryOptions() {
			return getRuleContext(QueryOptionsContext.class,0);
		}
		public OdataRelativeUriContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_odataRelativeUri; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterOdataRelativeUri(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitOdataRelativeUri(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitOdataRelativeUri(this);
			else return visitor.visitChildren(this);
		}
	}

	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "SPACE", "FILTER", "TOP", "SKIPRULE", "SKIPTOKEN", "ORDERBY", "REF",
			"EXPAND", "COUNT", "SELECT", "DOLLARID", "CONTAINS", "STARTSWITH", "ENDSWITH",
			"SUBSTRINGOF", "LENGTH", "INDEXOF", "REPLACE", "SUBSTRING", "TOLOWER",
			"TOUPPER", "TRIM", "CONCAT", "DAY", "MONTH", "YEAR", "HOUR", "MINUTE",
			"SECOND", "NOW", "TIME", "MAX_DATE_TIME", "MIN_DATE_TIME", "TOTAL_OFFSET_MINUTES",
			"FRACTIONAL_SECONDS", "TOTAL_SECONDS", "GEO_INTERSECTS", "GEO_DISTANCE",
			"GEO_LENGTH", "ROUND", "FLOOR", "CEILING", "CAST", "ISOF", "EQUAL", "EQ",
			"GT", "LT", "GE", "LE", "NE", "ADD", "SUB", "MUL", "DIV", "MOD", "ORDER",
			"BY", "DESC", "ASC", "OR", "AND", "NOT", "SEPERATOR", "HTTP", "HTTPS",
			"OPEN", "CLOSE", "COMMA", "QUESTION", "DOLLAR", "SEMI", "AT_SIGN", "BAR",
			"SINGLE_QUOTE_SYMB", "DOUBLE_QUOTE_SYMB", "REVERSE_QUOTE_SYMB", "COLON_SYMB",
			"AMPERSAND", "NULL_TOKEN", "NULL_SPEC_LITERAL", "DOT", "SLASH", "UNDERSCORE",
			"EDM", "COLLECTION", "GEOGRAPHY", "GEOMETRY", "BINARY", "BOOLEAN", "BYTE",
			"DATE", "DATETIMEOFFSET", "DECIMAL", "DOUBLE", "DURATION", "GUID", "INT16",
			"INT32", "INT64", "SBYTE", "SINGLE", "STREAM", "STRING", "TIMEOFDAY",
			"LINESTRING", "MULTILINESTRING", "MULTIPOINT", "MULTIPOLYGON", "POINT",
			"POLYGON", "BOOLEAN_VALUE", "UUID", "INTEGER", "FLOAT", "BSM_ID", "ID",
				"QUOTED_UUID", "QUOTED_STRING", "DQUOTED_STRING", "DECIMAL_LITERAL",
			"TIME_OF_DAY_VALUE", "DURATION_VALUE", "DATE_VALUE", "DATE_TIME_OFFSET_VALUE",
			"HOUR_DEF", "MINUTE_DEF", "SECOND_DEF", "ZERO_TO_FIFTY_NINE", "ONE_TO_NINE",
			"YEAR_DEF", "MONTH_DEF", "DAY_DEF", "HEX", "SINGLE_CHAR_SMALL", "SINGLE_CHAR",
			"NEGATIVE", "DEC_OCTECT", "HEX_NUMBER", "BIN_NUMBER", "ERROR_RECONGNIGION"
		};
	}

	public static class ResourcePathContext extends ParserRuleContext {
		public EmbeddedEntitySetContext embeddedEntitySet() {
			return getRuleContext(EmbeddedEntitySetContext.class,0);
		}
		public EntityUUIDContext entityUUID() {
			return getRuleContext(EntityUUIDContext.class,0);
		}
		public EntityContext entity() {
			return getRuleContext(EntityContext.class,0);
		}
		public CountStatementContext countStatement() {
			return getRuleContext(CountStatementContext.class,0);
		}
		public ResourcePathContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_resourcePath; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterResourcePath(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitResourcePath(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitResourcePath(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OdataRelativeUriContext odataRelativeUri() throws RecognitionException {
		OdataRelativeUriContext _localctx = new OdataRelativeUriContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_odataRelativeUri);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(112);
			resourcePath();
				setState(115);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==QUESTION) {
				{
					setState(113);
				match(QUESTION);
					setState(114);
				queryOptions();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EntityContext extends ParserRuleContext {
		public EntityNameContext entityName() {
			return getRuleContext(EntityNameContext.class,0);
		}
		public EntityCastContext entityCast() {
			return getRuleContext(EntityCastContext.class,0);
		}
		public EntityContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_entity; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterEntity(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitEntity(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitEntity(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ResourcePathContext resourcePath() throws RecognitionException {
		ResourcePathContext _localctx = new ResourcePathContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_resourcePath);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(120);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				{
					setState(117);
				embeddedEntitySet();
				}
				break;
			case 2:
				{
					setState(118);
				entityUUID();
				}
				break;
			case 3:
				{
					setState(119);
				entity();
				}
				break;
			}
				setState(123);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SLASH) {
				{
					setState(122);
				countStatement();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EntityCastContext extends ParserRuleContext {
		public List<EntityNameContext> entityName() {
			return getRuleContexts(EntityNameContext.class);
		}
		public EntityNameContext entityName(int i) {
			return getRuleContext(EntityNameContext.class,i);
		}
		public TerminalNode SLASH() { return getToken(ODataParser.SLASH, 0); }
		public EntityCastContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_entityCast; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterEntityCast(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitEntityCast(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitEntityCast(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EntityContext entity() throws RecognitionException {
		EntityContext _localctx = new EntityContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_entity);
		try {
			setState(127);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
					setState(125);
				entityName();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
					setState(126);
				entityCast();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EntityUUIDContext extends ParserRuleContext {
		public EntityNameContext entityName() {
			return getRuleContext(EntityNameContext.class,0);
		}
		public TerminalNode SLASH() { return getToken(ODataParser.SLASH, 0); }
		public UuidIdValueContext uuidIdValue() {
			return getRuleContext(UuidIdValueContext.class,0);
		}
		public EntityUUIDContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_entityUUID; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterEntityUUID(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitEntityUUID(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitEntityUUID(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EntityCastContext entityCast() throws RecognitionException {
		EntityCastContext _localctx = new EntityCastContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_entityCast);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(129);
			entityName();
				setState(130);
			match(SLASH);
				setState(131);
			entityName();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EmbeddedEntitySetContext extends ParserRuleContext {
		public List<EntityUUIDContext> entityUUID() {
			return getRuleContexts(EntityUUIDContext.class);
		}
		public EntityUUIDContext entityUUID(int i) {
			return getRuleContext(EntityUUIDContext.class,i);
		}
		public List<TerminalNode> SLASH() { return getTokens(ODataParser.SLASH); }
		public TerminalNode SLASH(int i) {
			return getToken(ODataParser.SLASH, i);
		}
		public List<EntityCastContext> entityCast() {
			return getRuleContexts(EntityCastContext.class);
		}
		public EntityCastContext entityCast(int i) {
			return getRuleContext(EntityCastContext.class,i);
		}
		public List<EntityContext> entity() {
			return getRuleContexts(EntityContext.class);
		}
		public EntityContext entity(int i) {
			return getRuleContext(EntityContext.class,i);
		}
		public EmbeddedEntitySetContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_embeddedEntitySet; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterEmbeddedEntitySet(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitEmbeddedEntitySet(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitEmbeddedEntitySet(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EntityUUIDContext entityUUID() throws RecognitionException {
		EntityUUIDContext _localctx = new EntityUUIDContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_entityUUID);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(133);
			entityName();
				setState(134);
			match(SLASH);
				setState(135);
			uuidIdValue();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class QueryOptionsContext extends ParserRuleContext {
		public List<QueryOptionContext> queryOption() {
			return getRuleContexts(QueryOptionContext.class);
		}
		public QueryOptionContext queryOption(int i) {
			return getRuleContext(QueryOptionContext.class,i);
		}
		public List<TerminalNode> AMPERSAND() { return getTokens(ODataParser.AMPERSAND); }
		public TerminalNode AMPERSAND(int i) {
			return getToken(ODataParser.AMPERSAND, i);
		}
		public QueryOptionsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_queryOptions; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterQueryOptions(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitQueryOptions(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitQueryOptions(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EmbeddedEntitySetContext embeddedEntitySet() throws RecognitionException {
		EmbeddedEntitySetContext _localctx = new EmbeddedEntitySetContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_embeddedEntitySet);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
				setState(137);
			entityUUID();
				setState(147);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,6,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
						setState(145);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,5,_ctx) ) {
					case 1:
						{
							setState(138);
						match(SLASH);
							setState(139);
						entityUUID();
						}
						break;
					case 2:
						{
						{
							setState(140);
						match(SLASH);
							setState(143);
						_errHandler.sync(this);
						switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
						case 1:
							{
								setState(141);
							entityCast();
							}
							break;
						case 2:
							{
								setState(142);
							entity();
							}
							break;
						}
						}
						}
						break;
					}
					}
				}
				setState(149);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,6,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class QueryOptionContext extends ParserRuleContext {
		public FilterContext filter() {
			return getRuleContext(FilterContext.class,0);
		}
		public ExpandContext expand() {
			return getRuleContext(ExpandContext.class,0);
		}
		public OrderByContext orderBy() {
			return getRuleContext(OrderByContext.class,0);
		}
		public SkipStatementContext skipStatement() {
			return getRuleContext(SkipStatementContext.class,0);
		}
		public TopStatementContext topStatement() {
			return getRuleContext(TopStatementContext.class,0);
		}
		public QueryOptionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_queryOption; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterQueryOption(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitQueryOption(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitQueryOption(this);
			else return visitor.visitChildren(this);
		}
	}

	public final QueryOptionsContext queryOptions() throws RecognitionException {
		QueryOptionsContext _localctx = new QueryOptionsContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_queryOptions);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(150);
			queryOption();
				setState(155);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AMPERSAND) {
				{
				{
					setState(151);
				match(AMPERSAND);
					setState(152);
				queryOption();
				}
				}
				setState(157);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FilterContext extends ParserRuleContext {
		public TerminalNode FILTER() { return getToken(ODataParser.FILTER, 0); }
		public TerminalNode EQUAL() { return getToken(ODataParser.EQUAL, 0); }
		public FilterExpressionContext filterExpression() {
			return getRuleContext(FilterExpressionContext.class,0);
		}
		public FilterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_filter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterFilter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitFilter(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitFilter(this);
			else return visitor.visitChildren(this);
		}
	}

	public final QueryOptionContext queryOption() throws RecognitionException {
		QueryOptionContext _localctx = new QueryOptionContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_queryOption);
		try {
			setState(163);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case FILTER:
				enterOuterAlt(_localctx, 1);
				{
					setState(158);
				filter();
				}
				break;
			case EXPAND:
				enterOuterAlt(_localctx, 2);
				{
					setState(159);
				expand();
				}
				break;
			case ORDERBY:
				enterOuterAlt(_localctx, 3);
				{
					setState(160);
				orderBy();
				}
				break;
			case SKIPRULE:
				enterOuterAlt(_localctx, 4);
				{
					setState(161);
				skipStatement();
				}
				break;
			case TOP:
				enterOuterAlt(_localctx, 5);
				{
					setState(162);
				topStatement();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpandContext extends ParserRuleContext {
		public TerminalNode EXPAND() { return getToken(ODataParser.EXPAND, 0); }
		public TerminalNode EQUAL() { return getToken(ODataParser.EQUAL, 0); }
		public JoinEntitiesContext joinEntities() {
			return getRuleContext(JoinEntitiesContext.class,0);
		}
		public AttributeNameContext attributeName() {
			return getRuleContext(AttributeNameContext.class,0);
		}
		public BoolCommonExprContext boolCommonExpr() {
			return getRuleContext(BoolCommonExprContext.class,0);
		}
		public TerminalNode SLASH() { return getToken(ODataParser.SLASH, 0); }
		public FilterContext filter() {
			return getRuleContext(FilterContext.class,0);
		}
		public ExpandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expand; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterExpand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitExpand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitExpand(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FilterContext filter() throws RecognitionException {
		FilterContext _localctx = new FilterContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_filter);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(165);
			match(FILTER);
				setState(166);
			match(EQUAL);
				setState(167);
			filterExpression(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FilterExpressionContext extends ParserRuleContext {
		public FilterExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_filterExpression; }
	 
		public FilterExpressionContext() { }
		public void copyFrom(FilterExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class BinaryExpressionContext extends FilterExpressionContext {
		public FilterExpressionContext left;
		public LogicalOperatorContext op;
		public FilterExpressionContext right;
		public List<FilterExpressionContext> filterExpression() {
			return getRuleContexts(FilterExpressionContext.class);
		}
		public FilterExpressionContext filterExpression(int i) {
			return getRuleContext(FilterExpressionContext.class,i);
		}
		public LogicalOperatorContext logicalOperator() {
			return getRuleContext(LogicalOperatorContext.class,0);
		}
		public BinaryExpressionContext(FilterExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterBinaryExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitBinaryExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitBinaryExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BoolExpressionContext extends FilterExpressionContext {
		public BoolCommonExprContext boolCommonExpr() {
			return getRuleContext(BoolCommonExprContext.class,0);
		}
		public BoolExpressionContext(FilterExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterBoolExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitBoolExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitBoolExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ParenExpressionContext extends FilterExpressionContext {
		public OpenParContext openPar() {
			return getRuleContext(OpenParContext.class,0);
		}
		public FilterExpressionContext filterExpression() {
			return getRuleContext(FilterExpressionContext.class,0);
		}
		public CloseParContext closePar() {
			return getRuleContext(CloseParContext.class,0);
		}
		public ParenExpressionContext(FilterExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterParenExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitParenExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitParenExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FilterExpressionContext filterExpression() throws RecognitionException {
		return filterExpression(0);
	}

	public final ExpandContext expand() throws RecognitionException {
		ExpandContext _localctx = new ExpandContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_expand);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(169);
			match(EXPAND);
				setState(170);
			match(EQUAL);
				setState(174);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
			case 1:
				{
					setState(171);
				joinEntities();
				}
				break;
			case 2:
				{
					setState(172);
				attributeName();
				}
				break;
			case 3:
				{
					setState(173);
				boolCommonExpr();
				}
				break;
			}
				setState(178);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SLASH) {
				{
					setState(176);
				match(SLASH);
					setState(177);
				filter();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BoolCommonExprContext extends ParserRuleContext {
		public BoolCommonExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_boolCommonExpr; }
	 
		public BoolCommonExprContext() { }
		public void copyFrom(BoolCommonExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class CompareMethodExpressionContext extends BoolCommonExprContext {
		public CompareMethodExprContext compareMethodExpr() {
			return getRuleContext(CompareMethodExprContext.class,0);
		}
		public CompareMethodExpressionContext(BoolCommonExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterCompareMethodExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitCompareMethodExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitCompareMethodExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ComparatorExpressionContext extends BoolCommonExprContext {
		public LeftComparatorExprContext left;
		public ComparisonOperatorContext op;
		public RightComparatorExprContext right;
		public LeftComparatorExprContext leftComparatorExpr() {
			return getRuleContext(LeftComparatorExprContext.class,0);
		}
		public ComparisonOperatorContext comparisonOperator() {
			return getRuleContext(ComparisonOperatorContext.class,0);
		}
		public RightComparatorExprContext rightComparatorExpr() {
			return getRuleContext(RightComparatorExprContext.class,0);
		}
		public ComparatorExpressionContext(BoolCommonExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterComparatorExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitComparatorExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitComparatorExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class CountComparatorExpressionContext extends BoolCommonExprContext {
		public Token left;
		public ComparisonOperatorContext op;
		public PrimitiveLiteralContext right;
		public TerminalNode COUNT() { return getToken(ODataParser.COUNT, 0); }
		public ComparisonOperatorContext comparisonOperator() {
			return getRuleContext(ComparisonOperatorContext.class,0);
		}
		public PrimitiveLiteralContext primitiveLiteral() {
			return getRuleContext(PrimitiveLiteralContext.class,0);
		}
		public CountComparatorExpressionContext(BoolCommonExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterCountComparatorExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitCountComparatorExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitCountComparatorExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	private FilterExpressionContext filterExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		FilterExpressionContext _localctx = new FilterExpressionContext(_ctx, _parentState);
		FilterExpressionContext _prevctx = _localctx;
		int _startState = 20;
		enterRecursionRule(_localctx, 20, RULE_filterExpression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
				setState(186);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case OPEN:
				{
				_localctx = new ParenExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

					setState(181);
				openPar();
					setState(182);
				filterExpression(0);
					setState(183);
				closePar();
				}
				break;
			case COUNT:
			case CONTAINS:
			case STARTSWITH:
			case ENDSWITH:
			case LENGTH:
			case INDEXOF:
			case SUBSTRING:
			case TOLOWER:
			case TOUPPER:
			case TRIM:
			case CONCAT:
			case DAY:
			case MONTH:
			case YEAR:
			case HOUR:
			case MINUTE:
			case SECOND:
			case NOW:
			case TIME:
			case MAX_DATE_TIME:
			case MIN_DATE_TIME:
			case TOTAL_OFFSET_MINUTES:
			case FRACTIONAL_SECONDS:
			case TOTAL_SECONDS:
			case GEO_INTERSECTS:
			case GEO_DISTANCE:
			case GEO_LENGTH:
			case ROUND:
			case FLOOR:
			case CEILING:
			case ADD:
			case SUB:
			case MUL:
			case DIV:
			case MOD:
			case NULL_TOKEN:
			case NULL_SPEC_LITERAL:
			case DATE:
			case BOOLEAN_VALUE:
			case INTEGER:
			case FLOAT:
			case BSM_ID:
			case ID:
			case QUOTED_UUID:
			case QUOTED_STRING:
				case DQUOTED_STRING:
			case DECIMAL_LITERAL:
			case TIME_OF_DAY_VALUE:
			case DURATION_VALUE:
			case DATE_VALUE:
			case DATE_TIME_OFFSET_VALUE:
				{
				_localctx = new BoolExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
					setState(185);
				boolCommonExpr();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
				setState(194);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,12,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new BinaryExpressionContext(new FilterExpressionContext(_parentctx, _parentState));
					((BinaryExpressionContext)_localctx).left = _prevctx;
					pushNewRecursionContext(_localctx, _startState, RULE_filterExpression);
						setState(188);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(189);
					((BinaryExpressionContext)_localctx).op = logicalOperator();
						setState(190);
					((BinaryExpressionContext)_localctx).right = filterExpression(3);
					}
					}
				}
				setState(196);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,12,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class LeftComparatorExprContext extends ParserRuleContext {
		public MethodCallExprContext methodCallExpr() {
			return getRuleContext(MethodCallExprContext.class,0);
		}
		public CalenderMethodExpContext calenderMethodExp() {
			return getRuleContext(CalenderMethodExpContext.class,0);
		}
		public ConcatMethodExprContext concatMethodExpr() {
			return getRuleContext(ConcatMethodExprContext.class,0);
		}
		public JoinEntitiesContext joinEntities() {
			return getRuleContext(JoinEntitiesContext.class,0);
		}
		public AttributeNameContext attributeName() {
			return getRuleContext(AttributeNameContext.class,0);
		}
		public CommonExprContext commonExpr() {
			return getRuleContext(CommonExprContext.class,0);
		}
		public LeftComparatorExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_leftComparatorExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterLeftComparatorExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitLeftComparatorExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitLeftComparatorExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BoolCommonExprContext boolCommonExpr() throws RecognitionException {
		BoolCommonExprContext _localctx = new BoolCommonExprContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_boolCommonExpr);
		try {
			setState(206);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,13,_ctx) ) {
			case 1:
				_localctx = new ComparatorExpressionContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
					setState(197);
				((ComparatorExpressionContext)_localctx).left = leftComparatorExpr();
					setState(198);
				((ComparatorExpressionContext)_localctx).op = comparisonOperator();
					setState(199);
				((ComparatorExpressionContext)_localctx).right = rightComparatorExpr();
				}
				break;
			case 2:
				_localctx = new CountComparatorExpressionContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
					setState(201);
				((CountComparatorExpressionContext)_localctx).left = match(COUNT);
					setState(202);
				((CountComparatorExpressionContext)_localctx).op = comparisonOperator();
					setState(203);
				((CountComparatorExpressionContext)_localctx).right = primitiveLiteral();
				}
				break;
			case 3:
				_localctx = new CompareMethodExpressionContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
					setState(205);
				compareMethodExpr();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RightComparatorExprContext extends ParserRuleContext {
		public MethodCallExprContext methodCallExpr() {
			return getRuleContext(MethodCallExprContext.class,0);
		}
		public CalenderMethodExpContext calenderMethodExp() {
			return getRuleContext(CalenderMethodExpContext.class,0);
		}
		public ConcatMethodExprContext concatMethodExpr() {
			return getRuleContext(ConcatMethodExprContext.class,0);
		}
		public JoinEntitiesContext joinEntities() {
			return getRuleContext(JoinEntitiesContext.class,0);
		}
		public AttributeNameContext attributeName() {
			return getRuleContext(AttributeNameContext.class,0);
		}
		public CommonExprContext commonExpr() {
			return getRuleContext(CommonExprContext.class,0);
		}
		public RightComparatorExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_rightComparatorExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterRightComparatorExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitRightComparatorExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitRightComparatorExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LeftComparatorExprContext leftComparatorExpr() throws RecognitionException {
		LeftComparatorExprContext _localctx = new LeftComparatorExprContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_leftComparatorExpr);
		try {
			setState(214);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,14,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
					setState(208);
				methodCallExpr();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
					setState(209);
				calenderMethodExp();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
					setState(210);
				concatMethodExpr();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
					setState(211);
				joinEntities();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
					setState(212);
				attributeName();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
					setState(213);
				commonExpr();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OrderByContext extends ParserRuleContext {
		public TerminalNode ORDERBY() { return getToken(ODataParser.ORDERBY, 0); }
		public TerminalNode EQUAL() { return getToken(ODataParser.EQUAL, 0); }
		public List<OrderByItemContext> orderByItem() {
			return getRuleContexts(OrderByItemContext.class);
		}
		public OrderByItemContext orderByItem(int i) {
			return getRuleContext(OrderByItemContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(ODataParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(ODataParser.COMMA, i);
		}
		public OrderByContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_orderBy; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterOrderBy(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitOrderBy(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitOrderBy(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RightComparatorExprContext rightComparatorExpr() throws RecognitionException {
		RightComparatorExprContext _localctx = new RightComparatorExprContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_rightComparatorExpr);
		try {
			setState(222);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,15,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
					setState(216);
				methodCallExpr();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
					setState(217);
				calenderMethodExp();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
					setState(218);
				concatMethodExpr();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
					setState(219);
				joinEntities();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
					setState(220);
				attributeName();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
					setState(221);
				commonExpr();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OrderByItemContext extends ParserRuleContext {
		public OrderAttributeNameContext orderAttributeName() {
			return getRuleContext(OrderAttributeNameContext.class,0);
		}
		public SortOrderContext sortOrder() {
			return getRuleContext(SortOrderContext.class,0);
		}
		public OrderByItemContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_orderByItem; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterOrderByItem(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitOrderByItem(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitOrderByItem(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OrderByContext orderBy() throws RecognitionException {
		OrderByContext _localctx = new OrderByContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_orderBy);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(224);
			match(ORDERBY);
				setState(225);
			match(EQUAL);
				setState(226);
			orderByItem();
				setState(231);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
					setState(227);
				match(COMMA);
					setState(228);
				orderByItem();
				}
				}
				setState(233);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TopStatementContext extends ParserRuleContext {
		public TerminalNode TOP() { return getToken(ODataParser.TOP, 0); }
		public IntegerValueContext integerValue() {
			return getRuleContext(IntegerValueContext.class,0);
		}
		public TopStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_topStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterTopStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitTopStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitTopStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OrderByItemContext orderByItem() throws RecognitionException {
		OrderByItemContext _localctx = new OrderByItemContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_orderByItem);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(234);
			orderAttributeName();
				setState(236);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==DESC || _la==ASC) {
				{
					setState(235);
				sortOrder();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SkipStatementContext extends ParserRuleContext {
		public TerminalNode SKIPRULE() { return getToken(ODataParser.SKIPRULE, 0); }
		public IntegerValueContext integerValue() {
			return getRuleContext(IntegerValueContext.class,0);
		}
		public SkipStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_skipStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterSkipStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitSkipStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitSkipStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TopStatementContext topStatement() throws RecognitionException {
		TopStatementContext _localctx = new TopStatementContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_topStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(238);
			match(TOP);
				setState(239);
			integerValue();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class JoinEntitiesContext extends ParserRuleContext {
		public AttributeNameContext attributeName() {
			return getRuleContext(AttributeNameContext.class,0);
		}
		public List<EntityNameContext> entityName() {
			return getRuleContexts(EntityNameContext.class);
		}
		public EntityNameContext entityName(int i) {
			return getRuleContext(EntityNameContext.class,i);
		}
		public List<TerminalNode> SLASH() { return getTokens(ODataParser.SLASH); }
		public TerminalNode SLASH(int i) {
			return getToken(ODataParser.SLASH, i);
		}
		public JoinEntitiesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_joinEntities; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterJoinEntities(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitJoinEntities(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitJoinEntities(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SkipStatementContext skipStatement() throws RecognitionException {
		SkipStatementContext _localctx = new SkipStatementContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_skipStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(241);
			match(SKIPRULE);
				setState(242);
			integerValue();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CommonExprContext extends ParserRuleContext {
		public PrimitiveLiteralContext primitiveLiteral() {
			return getRuleContext(PrimitiveLiteralContext.class,0);
		}
		public MethodExprContext methodExpr() {
			return getRuleContext(MethodExprContext.class,0);
		}
		public MathExprContext mathExpr() {
			return getRuleContext(MathExprContext.class,0);
		}
		public CommonExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_commonExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterCommonExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitCommonExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitCommonExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final JoinEntitiesContext joinEntities() throws RecognitionException {
		JoinEntitiesContext _localctx = new JoinEntitiesContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_joinEntities);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
				setState(247);
			_errHandler.sync(this);
			_alt = 1;
			do {
				switch (_alt) {
				case 1:
					{
					{
						setState(244);
					entityName();
						setState(245);
					match(SLASH);
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(249);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,18,_ctx);
			} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				setState(251);
			attributeName();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MathExprContext extends ParserRuleContext {
		public NumberContext number() {
			return getRuleContext(NumberContext.class,0);
		}
		public TerminalNode ADD() { return getToken(ODataParser.ADD, 0); }
		public TerminalNode SUB() { return getToken(ODataParser.SUB, 0); }
		public TerminalNode MUL() { return getToken(ODataParser.MUL, 0); }
		public TerminalNode DIV() { return getToken(ODataParser.DIV, 0); }
		public TerminalNode MOD() { return getToken(ODataParser.MOD, 0); }
		public MathExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_mathExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterMathExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitMathExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitMathExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CommonExprContext commonExpr() throws RecognitionException {
		CommonExprContext _localctx = new CommonExprContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_commonExpr);
		try {
			setState(256);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case NULL_TOKEN:
			case NULL_SPEC_LITERAL:
			case BOOLEAN_VALUE:
			case INTEGER:
			case FLOAT:
			case QUOTED_UUID:
			case QUOTED_STRING:
				case DQUOTED_STRING:
			case DECIMAL_LITERAL:
			case TIME_OF_DAY_VALUE:
			case DURATION_VALUE:
			case DATE_VALUE:
			case DATE_TIME_OFFSET_VALUE:
				enterOuterAlt(_localctx, 1);
				{
					setState(253);
				primitiveLiteral();
				}
				break;
			case CONTAINS:
			case STARTSWITH:
			case ENDSWITH:
			case LENGTH:
			case INDEXOF:
			case SUBSTRING:
			case TOLOWER:
			case TOUPPER:
			case TRIM:
			case CONCAT:
			case DAY:
			case MONTH:
			case YEAR:
			case HOUR:
			case MINUTE:
			case SECOND:
			case NOW:
			case TIME:
			case MAX_DATE_TIME:
			case MIN_DATE_TIME:
			case TOTAL_OFFSET_MINUTES:
			case FRACTIONAL_SECONDS:
			case TOTAL_SECONDS:
			case GEO_INTERSECTS:
			case GEO_DISTANCE:
			case GEO_LENGTH:
			case ROUND:
			case FLOOR:
			case CEILING:
			case DATE:
				enterOuterAlt(_localctx, 2);
				{
					setState(254);
				methodExpr();
				}
				break;
			case ADD:
			case SUB:
			case MUL:
			case DIV:
			case MOD:
				enterOuterAlt(_localctx, 3);
				{
					setState(255);
				mathExpr();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodExprContext extends ParserRuleContext {
		public MethodCallExprContext methodCallExpr() {
			return getRuleContext(MethodCallExprContext.class,0);
		}
		public CalenderMethodExpContext calenderMethodExp() {
			return getRuleContext(CalenderMethodExpContext.class,0);
		}
		public SingleMethodCallExprContext singleMethodCallExpr() {
			return getRuleContext(SingleMethodCallExprContext.class,0);
		}
		public ConcatMethodExprContext concatMethodExpr() {
			return getRuleContext(ConcatMethodExprContext.class,0);
		}
		public SubstringMethodCallExprContext substringMethodCallExpr() {
			return getRuleContext(SubstringMethodCallExprContext.class,0);
		}
		public CompareMethodExprContext compareMethodExpr() {
			return getRuleContext(CompareMethodExprContext.class,0);
		}
		public MethodExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_methodExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterMethodExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitMethodExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitMethodExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MathExprContext mathExpr() throws RecognitionException {
		MathExprContext _localctx = new MathExprContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_mathExpr);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(258);
			_la = _input.LA(1);
				if (!((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ADD) | (1L << SUB) | (1L << MUL) | (1L << DIV) | (1L << MOD))) != 0))) {
					_errHandler.recoverInline(this);
				} else {
					if (_input.LA(1) == Token.EOF) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(259);
			number();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CompareMethodExprContext extends ParserRuleContext {
		public CompareMethodNameContext compareMethodName() {
			return getRuleContext(CompareMethodNameContext.class,0);
		}
		public TerminalNode OPEN() { return getToken(ODataParser.OPEN, 0); }
		public TerminalNode COMMA() { return getToken(ODataParser.COMMA, 0); }
		public List<CommonExprContext> commonExpr() {
			return getRuleContexts(CommonExprContext.class);
		}
		public CommonExprContext commonExpr(int i) {
			return getRuleContext(CommonExprContext.class,i);
		}
		public TerminalNode CLOSE() { return getToken(ODataParser.CLOSE, 0); }
		public JoinEntitiesContext joinEntities() {
			return getRuleContext(JoinEntitiesContext.class,0);
		}
		public AttributeNameContext attributeName() {
			return getRuleContext(AttributeNameContext.class,0);
		}
		public CompareMethodExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_compareMethodExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterCompareMethodExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitCompareMethodExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitCompareMethodExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MethodExprContext methodExpr() throws RecognitionException {
		MethodExprContext _localctx = new MethodExprContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_methodExpr);
		try {
			setState(267);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case LENGTH:
			case TOLOWER:
			case TOUPPER:
			case TRIM:
			case GEO_LENGTH:
			case ROUND:
			case FLOOR:
			case CEILING:
				enterOuterAlt(_localctx, 1);
				{
					setState(261);
				methodCallExpr();
				}
				break;
			case DAY:
			case MONTH:
			case YEAR:
			case HOUR:
			case MINUTE:
			case SECOND:
			case TIME:
			case TOTAL_OFFSET_MINUTES:
			case FRACTIONAL_SECONDS:
			case TOTAL_SECONDS:
			case DATE:
				enterOuterAlt(_localctx, 2);
				{
					setState(262);
				calenderMethodExp();
				}
				break;
			case NOW:
			case MAX_DATE_TIME:
			case MIN_DATE_TIME:
				enterOuterAlt(_localctx, 3);
				{
					setState(263);
				singleMethodCallExpr();
				}
				break;
			case CONCAT:
				enterOuterAlt(_localctx, 4);
				{
					setState(264);
				concatMethodExpr();
				}
				break;
			case SUBSTRING:
				enterOuterAlt(_localctx, 5);
				{
					setState(265);
				substringMethodCallExpr();
				}
				break;
			case CONTAINS:
			case STARTSWITH:
			case ENDSWITH:
			case INDEXOF:
			case GEO_INTERSECTS:
			case GEO_DISTANCE:
				enterOuterAlt(_localctx, 6);
				{
					setState(266);
				compareMethodExpr();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodCallExprContext extends ParserRuleContext {
		public MethodNameContext methodName() {
			return getRuleContext(MethodNameContext.class,0);
		}
		public TerminalNode OPEN() { return getToken(ODataParser.OPEN, 0); }
		public TerminalNode CLOSE() { return getToken(ODataParser.CLOSE, 0); }
		public MethodCallExprContext methodCallExpr() {
			return getRuleContext(MethodCallExprContext.class,0);
		}
		public JoinEntitiesContext joinEntities() {
			return getRuleContext(JoinEntitiesContext.class,0);
		}
		public AttributeNameContext attributeName() {
			return getRuleContext(AttributeNameContext.class,0);
		}
		public MethodCallExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_methodCallExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterMethodCallExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitMethodCallExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitMethodCallExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CompareMethodExprContext compareMethodExpr() throws RecognitionException {
		CompareMethodExprContext _localctx = new CompareMethodExprContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_compareMethodExpr);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(269);
			compareMethodName();
				setState(270);
			match(OPEN);
				setState(274);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,21,_ctx) ) {
			case 1:
				{
					setState(271);
				joinEntities();
				}
				break;
			case 2:
				{
					setState(272);
				attributeName();
				}
				break;
			case 3:
				{
					setState(273);
				commonExpr();
				}
				break;
			}
				setState(276);
			match(COMMA);
				setState(277);
			commonExpr();
				setState(278);
			match(CLOSE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CalenderMethodExpContext extends ParserRuleContext {
		public CalenderMethodNameContext calenderMethodName() {
			return getRuleContext(CalenderMethodNameContext.class,0);
		}
		public TerminalNode OPEN() { return getToken(ODataParser.OPEN, 0); }
		public TerminalNode CLOSE() { return getToken(ODataParser.CLOSE, 0); }
		public JoinEntitiesContext joinEntities() {
			return getRuleContext(JoinEntitiesContext.class,0);
		}
		public AttributeNameContext attributeName() {
			return getRuleContext(AttributeNameContext.class,0);
		}
		public PrimitiveLiteralContext primitiveLiteral() {
			return getRuleContext(PrimitiveLiteralContext.class,0);
		}
		public CalenderMethodExpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_calenderMethodExp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterCalenderMethodExp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitCalenderMethodExp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitCalenderMethodExp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MethodCallExprContext methodCallExpr() throws RecognitionException {
		MethodCallExprContext _localctx = new MethodCallExprContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_methodCallExpr);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(280);
			methodName();
				setState(281);
			match(OPEN);
				setState(285);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,22,_ctx) ) {
			case 1:
				{
					setState(282);
				methodCallExpr();
				}
				break;
			case 2:
				{
					setState(283);
				joinEntities();
				}
				break;
			case 3:
				{
					setState(284);
				attributeName();
				}
				break;
			}
				setState(287);
			match(CLOSE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConcatMethodExprContext extends ParserRuleContext {
		public TerminalNode CONCAT() { return getToken(ODataParser.CONCAT, 0); }
		public TerminalNode OPEN() { return getToken(ODataParser.OPEN, 0); }
		public TerminalNode COMMA() { return getToken(ODataParser.COMMA, 0); }
		public TerminalNode CLOSE() { return getToken(ODataParser.CLOSE, 0); }
		public ConcatMethodExprContext concatMethodExpr() {
			return getRuleContext(ConcatMethodExprContext.class,0);
		}
		public List<PrimitiveLiteralContext> primitiveLiteral() {
			return getRuleContexts(PrimitiveLiteralContext.class);
		}
		public PrimitiveLiteralContext primitiveLiteral(int i) {
			return getRuleContext(PrimitiveLiteralContext.class,i);
		}
		public List<AttributeNameContext> attributeName() {
			return getRuleContexts(AttributeNameContext.class);
		}
		public AttributeNameContext attributeName(int i) {
			return getRuleContext(AttributeNameContext.class,i);
		}
		public ConcatMethodExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_concatMethodExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterConcatMethodExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitConcatMethodExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitConcatMethodExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CalenderMethodExpContext calenderMethodExp() throws RecognitionException {
		CalenderMethodExpContext _localctx = new CalenderMethodExpContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_calenderMethodExp);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(289);
			calenderMethodName();
				setState(290);
			match(OPEN);
				setState(294);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,23,_ctx) ) {
			case 1:
				{
					setState(291);
				joinEntities();
				}
				break;
			case 2:
				{
					setState(292);
				attributeName();
				}
				break;
			case 3:
				{
					setState(293);
				primitiveLiteral();
				}
				break;
			}
				setState(296);
			match(CLOSE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SingleMethodCallExprContext extends ParserRuleContext {
		public TerminalNode OPEN() { return getToken(ODataParser.OPEN, 0); }
		public TerminalNode CLOSE() { return getToken(ODataParser.CLOSE, 0); }
		public TerminalNode MIN_DATE_TIME() { return getToken(ODataParser.MIN_DATE_TIME, 0); }
		public TerminalNode MAX_DATE_TIME() { return getToken(ODataParser.MAX_DATE_TIME, 0); }
		public TerminalNode NOW() { return getToken(ODataParser.NOW, 0); }
		public SingleMethodCallExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_singleMethodCallExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterSingleMethodCallExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitSingleMethodCallExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitSingleMethodCallExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConcatMethodExprContext concatMethodExpr() throws RecognitionException {
		ConcatMethodExprContext _localctx = new ConcatMethodExprContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_concatMethodExpr);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(298);
			match(CONCAT);
				setState(299);
			match(OPEN);
				setState(303);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case CONCAT:
				{
					setState(300);
				concatMethodExpr();
				}
				break;
			case NULL_TOKEN:
			case NULL_SPEC_LITERAL:
			case BOOLEAN_VALUE:
			case INTEGER:
			case FLOAT:
			case QUOTED_UUID:
			case QUOTED_STRING:
				case DQUOTED_STRING:
			case DECIMAL_LITERAL:
			case TIME_OF_DAY_VALUE:
			case DURATION_VALUE:
			case DATE_VALUE:
			case DATE_TIME_OFFSET_VALUE:
				{
					setState(301);
				primitiveLiteral();
				}
				break;
			case BSM_ID:
			case ID:
				{
					setState(302);
				attributeName();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
				setState(305);
			match(COMMA);
				setState(308);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case NULL_TOKEN:
			case NULL_SPEC_LITERAL:
			case BOOLEAN_VALUE:
			case INTEGER:
			case FLOAT:
			case QUOTED_UUID:
			case QUOTED_STRING:
				case DQUOTED_STRING:
			case DECIMAL_LITERAL:
			case TIME_OF_DAY_VALUE:
			case DURATION_VALUE:
			case DATE_VALUE:
			case DATE_TIME_OFFSET_VALUE:
				{
					setState(306);
				primitiveLiteral();
				}
				break;
			case BSM_ID:
			case ID:
				{
					setState(307);
				attributeName();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
				setState(310);
			match(CLOSE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SubstringMethodCallExprContext extends ParserRuleContext {
		public TerminalNode SUBSTRING() { return getToken(ODataParser.SUBSTRING, 0); }
		public TerminalNode OPEN() { return getToken(ODataParser.OPEN, 0); }
		public TerminalNode COMMA() { return getToken(ODataParser.COMMA, 0); }
		public List<CommonExprContext> commonExpr() {
			return getRuleContexts(CommonExprContext.class);
		}
		public CommonExprContext commonExpr(int i) {
			return getRuleContext(CommonExprContext.class,i);
		}
		public TerminalNode CLOSE() { return getToken(ODataParser.CLOSE, 0); }
		public JoinEntitiesContext joinEntities() {
			return getRuleContext(JoinEntitiesContext.class,0);
		}
		public AttributeNameContext attributeName() {
			return getRuleContext(AttributeNameContext.class,0);
		}
		public SubstringMethodCallExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_substringMethodCallExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterSubstringMethodCallExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitSubstringMethodCallExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitSubstringMethodCallExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SingleMethodCallExprContext singleMethodCallExpr() throws RecognitionException {
		SingleMethodCallExprContext _localctx = new SingleMethodCallExprContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_singleMethodCallExpr);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(312);
			_la = _input.LA(1);
				if (!((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NOW) | (1L << MAX_DATE_TIME) | (1L << MIN_DATE_TIME))) != 0))) {
					_errHandler.recoverInline(this);
				} else {
					if (_input.LA(1) == Token.EOF) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(313);
			match(OPEN);
				setState(314);
			match(CLOSE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ComparisonOperatorContext extends ParserRuleContext {
		public TerminalNode EQ() { return getToken(ODataParser.EQ, 0); }
		public TerminalNode GT() { return getToken(ODataParser.GT, 0); }
		public TerminalNode LT() { return getToken(ODataParser.LT, 0); }
		public TerminalNode LE() { return getToken(ODataParser.LE, 0); }
		public TerminalNode GE() { return getToken(ODataParser.GE, 0); }
		public TerminalNode NE() { return getToken(ODataParser.NE, 0); }
		public ComparisonOperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_comparisonOperator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterComparisonOperator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitComparisonOperator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitComparisonOperator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SubstringMethodCallExprContext substringMethodCallExpr() throws RecognitionException {
		SubstringMethodCallExprContext _localctx = new SubstringMethodCallExprContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_substringMethodCallExpr);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(316);
			match(SUBSTRING);
				setState(317);
			match(OPEN);
				setState(321);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,26,_ctx) ) {
			case 1:
				{
					setState(318);
				joinEntities();
				}
				break;
			case 2:
				{
					setState(319);
				attributeName();
				}
				break;
			case 3:
				{
					setState(320);
				commonExpr();
				}
				break;
			}
				setState(323);
			match(COMMA);
				setState(324);
			commonExpr();
				setState(325);
			match(CLOSE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CompareMethodNameContext extends ParserRuleContext {
		public TerminalNode CONTAINS() { return getToken(ODataParser.CONTAINS, 0); }
		public TerminalNode STARTSWITH() { return getToken(ODataParser.STARTSWITH, 0); }
		public TerminalNode ENDSWITH() { return getToken(ODataParser.ENDSWITH, 0); }
		public TerminalNode INDEXOF() { return getToken(ODataParser.INDEXOF, 0); }
		public TerminalNode GEO_INTERSECTS() { return getToken(ODataParser.GEO_INTERSECTS, 0); }
		public TerminalNode GEO_DISTANCE() { return getToken(ODataParser.GEO_DISTANCE, 0); }
		public CompareMethodNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_compareMethodName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterCompareMethodName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitCompareMethodName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitCompareMethodName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ComparisonOperatorContext comparisonOperator() throws RecognitionException {
		ComparisonOperatorContext _localctx = new ComparisonOperatorContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_comparisonOperator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(327);
			_la = _input.LA(1);
				if (!((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EQ) | (1L << GT) | (1L << LT) | (1L << GE) | (1L << LE) | (1L << NE))) != 0))) {
					_errHandler.recoverInline(this);
				} else {
					if (_input.LA(1) == Token.EOF) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodNameContext extends ParserRuleContext {
		public TerminalNode LENGTH() { return getToken(ODataParser.LENGTH, 0); }
		public TerminalNode TOLOWER() { return getToken(ODataParser.TOLOWER, 0); }
		public TerminalNode TOUPPER() { return getToken(ODataParser.TOUPPER, 0); }
		public TerminalNode TRIM() { return getToken(ODataParser.TRIM, 0); }
		public TerminalNode ROUND() { return getToken(ODataParser.ROUND, 0); }
		public TerminalNode FLOOR() { return getToken(ODataParser.FLOOR, 0); }
		public TerminalNode CEILING() { return getToken(ODataParser.CEILING, 0); }
		public TerminalNode GEO_LENGTH() { return getToken(ODataParser.GEO_LENGTH, 0); }
		public MethodNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_methodName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterMethodName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitMethodName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitMethodName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CompareMethodNameContext compareMethodName() throws RecognitionException {
		CompareMethodNameContext _localctx = new CompareMethodNameContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_compareMethodName);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(329);
			_la = _input.LA(1);
				if (!((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << CONTAINS) | (1L << STARTSWITH) | (1L << ENDSWITH) | (1L << INDEXOF) | (1L << GEO_INTERSECTS) | (1L << GEO_DISTANCE))) != 0))) {
					_errHandler.recoverInline(this);
				} else {
					if (_input.LA(1) == Token.EOF) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CalenderMethodNameContext extends ParserRuleContext {
		public TerminalNode YEAR() { return getToken(ODataParser.YEAR, 0); }
		public TerminalNode MONTH() { return getToken(ODataParser.MONTH, 0); }
		public TerminalNode DAY() { return getToken(ODataParser.DAY, 0); }
		public TerminalNode HOUR() { return getToken(ODataParser.HOUR, 0); }
		public TerminalNode MINUTE() { return getToken(ODataParser.MINUTE, 0); }
		public TerminalNode SECOND() { return getToken(ODataParser.SECOND, 0); }
		public TerminalNode FRACTIONAL_SECONDS() { return getToken(ODataParser.FRACTIONAL_SECONDS, 0); }
		public TerminalNode TOTAL_SECONDS() { return getToken(ODataParser.TOTAL_SECONDS, 0); }
		public TerminalNode DATE() { return getToken(ODataParser.DATE, 0); }
		public TerminalNode TIME() { return getToken(ODataParser.TIME, 0); }
		public TerminalNode TOTAL_OFFSET_MINUTES() { return getToken(ODataParser.TOTAL_OFFSET_MINUTES, 0); }
		public CalenderMethodNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_calenderMethodName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterCalenderMethodName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitCalenderMethodName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitCalenderMethodName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MethodNameContext methodName() throws RecognitionException {
		MethodNameContext _localctx = new MethodNameContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_methodName);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(331);
			_la = _input.LA(1);
				if (!((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LENGTH) | (1L << TOLOWER) | (1L << TOUPPER) | (1L << TRIM) | (1L << GEO_LENGTH) | (1L << ROUND) | (1L << FLOOR) | (1L << CEILING))) != 0))) {
					_errHandler.recoverInline(this);
				} else {
					if (_input.LA(1) == Token.EOF) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NumberContext extends ParserRuleContext {
		public TerminalNode INTEGER() { return getToken(ODataParser.INTEGER, 0); }
		public TerminalNode FLOAT() { return getToken(ODataParser.FLOAT, 0); }
		public NumberContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_number; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterNumber(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitNumber(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitNumber(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CalenderMethodNameContext calenderMethodName() throws RecognitionException {
		CalenderMethodNameContext _localctx = new CalenderMethodNameContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_calenderMethodName);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(333);
			_la = _input.LA(1);
				if (!((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << DAY) | (1L << MONTH) | (1L << YEAR) | (1L << HOUR) | (1L << MINUTE) | (1L << SECOND) | (1L << TIME) | (1L << TOTAL_OFFSET_MINUTES) | (1L << FRACTIONAL_SECONDS) | (1L << TOTAL_SECONDS))) != 0) || _la == DATE)) {
					_errHandler.recoverInline(this);
				} else {
					if (_input.LA(1) == Token.EOF) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public final NumberContext number() throws RecognitionException {
		NumberContext _localctx = new NumberContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_number);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(335);
			_la = _input.LA(1);
			if ( !(_la==INTEGER || _la==FLOAT) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public final PrimitiveLiteralContext primitiveLiteral() throws RecognitionException {
		PrimitiveLiteralContext _localctx = new PrimitiveLiteralContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_primitiveLiteral);
		try {
			setState(350);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case QUOTED_UUID:
				enterOuterAlt(_localctx, 1);
				{
					setState(337);
				quotedUUID();
				}
				break;
				case DQUOTED_STRING:
				enterOuterAlt(_localctx, 2);
				{
					setState(338);
					dquotedString();
				}
				break;
				case QUOTED_STRING:
					enterOuterAlt(_localctx, 3);
				{
					setState(339);
				quotedString();
				}
				break;
			case NULL_SPEC_LITERAL:
				enterOuterAlt(_localctx, 4);
				{
					setState(340);
				nullSpecLiteral();
				}
				break;
			case NULL_TOKEN:
				enterOuterAlt(_localctx, 5);
				{
					setState(341);
				nullToken();
				}
				break;
			case BOOLEAN_VALUE:
				enterOuterAlt(_localctx, 6);
				{
					setState(342);
				booleanValue();
				}
				break;
			case DURATION_VALUE:
				enterOuterAlt(_localctx, 7);
				{
					setState(343);
				durationValue();
				}
				break;
			case DATE_VALUE:
				enterOuterAlt(_localctx, 8);
				{
					setState(344);
				dateValue();
				}
				break;
			case DATE_TIME_OFFSET_VALUE:
				enterOuterAlt(_localctx, 9);
				{
					setState(345);
				dateTimeOffsetValue();
				}
				break;
			case TIME_OF_DAY_VALUE:
				enterOuterAlt(_localctx, 10);
				{
					setState(346);
				timeOfDayValue();
				}
				break;
			case DECIMAL_LITERAL:
				enterOuterAlt(_localctx, 11);
				{
					setState(347);
				decimalLiteral();
				}
				break;
			case FLOAT:
				enterOuterAlt(_localctx, 12);
				{
					setState(348);
				floatValue();
				}
				break;
			case INTEGER:
				enterOuterAlt(_localctx, 13);
				{
					setState(349);
				integerValue();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CountStatementContext extends ParserRuleContext {
		public TerminalNode SLASH() { return getToken(ODataParser.SLASH, 0); }
		public TerminalNode COUNT() { return getToken(ODataParser.COUNT, 0); }
		public TerminalNode EQUAL() { return getToken(ODataParser.EQUAL, 0); }
		public BooleanValueContext booleanValue() {
			return getRuleContext(BooleanValueContext.class,0);
		}
		public CountStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_countStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterCountStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitCountStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitCountStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CountStatementContext countStatement() throws RecognitionException {
		CountStatementContext _localctx = new CountStatementContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_countStatement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(352);
			match(SLASH);
				setState(353);
			match(COUNT);
				setState(356);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==EQUAL) {
				{
					setState(354);
				match(EQUAL);
					setState(355);
				booleanValue();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AttributeNameContext extends ParserRuleContext {
		public TerminalNode BSM_ID() { return getToken(ODataParser.BSM_ID, 0); }
		public TerminalNode ID() { return getToken(ODataParser.ID, 0); }
		public AttributeNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_attributeName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterAttributeName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitAttributeName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitAttributeName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AttributeNameContext attributeName() throws RecognitionException {
		AttributeNameContext _localctx = new AttributeNameContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_attributeName);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(358);
			_la = _input.LA(1);
			if ( !(_la==BSM_ID || _la==ID) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OpenParContext extends ParserRuleContext {
		public TerminalNode OPEN() { return getToken(ODataParser.OPEN, 0); }
		public OpenParContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_openPar; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterOpenPar(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitOpenPar(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitOpenPar(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OpenParContext openPar() throws RecognitionException {
		OpenParContext _localctx = new OpenParContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_openPar);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(360);
			match(OPEN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CloseParContext extends ParserRuleContext {
		public TerminalNode CLOSE() { return getToken(ODataParser.CLOSE, 0); }
		public CloseParContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_closePar; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterClosePar(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitClosePar(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitClosePar(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CloseParContext closePar() throws RecognitionException {
		CloseParContext _localctx = new CloseParContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_closePar);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(362);
			match(CLOSE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LogicalOperatorContext extends ParserRuleContext {
		public TerminalNode AND() { return getToken(ODataParser.AND, 0); }
		public TerminalNode OR() { return getToken(ODataParser.OR, 0); }
		public LogicalOperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logicalOperator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterLogicalOperator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitLogicalOperator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitLogicalOperator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LogicalOperatorContext logicalOperator() throws RecognitionException {
		LogicalOperatorContext _localctx = new LogicalOperatorContext(_ctx, getState());
		enterRule(_localctx, 76, RULE_logicalOperator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(364);
			_la = _input.LA(1);
			if ( !(_la==OR || _la==AND) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SortOrderContext extends ParserRuleContext {
		public TerminalNode ASC() { return getToken(ODataParser.ASC, 0); }
		public TerminalNode DESC() { return getToken(ODataParser.DESC, 0); }
		public SortOrderContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sortOrder; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterSortOrder(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitSortOrder(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitSortOrder(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SortOrderContext sortOrder() throws RecognitionException {
		SortOrderContext _localctx = new SortOrderContext(_ctx, getState());
		enterRule(_localctx, 78, RULE_sortOrder);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(366);
			_la = _input.LA(1);
			if ( !(_la==DESC || _la==ASC) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EntityNameContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(ODataParser.ID, 0); }
		public EntityNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_entityName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterEntityName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitEntityName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitEntityName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EntityNameContext entityName() throws RecognitionException {
		EntityNameContext _localctx = new EntityNameContext(_ctx, getState());
		enterRule(_localctx, 80, RULE_entityName);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(368);
			match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OrderAttributeNameContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(ODataParser.ID, 0); }
		public OrderAttributeNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_orderAttributeName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterOrderAttributeName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitOrderAttributeName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitOrderAttributeName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OrderAttributeNameContext orderAttributeName() throws RecognitionException {
		OrderAttributeNameContext _localctx = new OrderAttributeNameContext(_ctx, getState());
		enterRule(_localctx, 82, RULE_orderAttributeName);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(370);
			match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UuidIdValueContext extends ParserRuleContext {
		public TerminalNode UUID() { return getToken(ODataParser.UUID, 0); }
		public UuidIdValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_uuidIdValue; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterUuidIdValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitUuidIdValue(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitUuidIdValue(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UuidIdValueContext uuidIdValue() throws RecognitionException {
		UuidIdValueContext _localctx = new UuidIdValueContext(_ctx, getState());
		enterRule(_localctx, 84, RULE_uuidIdValue);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(372);
			match(UUID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class QuotedUUIDContext extends ParserRuleContext {
		public TerminalNode QUOTED_UUID() { return getToken(ODataParser.QUOTED_UUID, 0); }
		public QuotedUUIDContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_quotedUUID; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterQuotedUUID(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitQuotedUUID(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitQuotedUUID(this);
			else return visitor.visitChildren(this);
		}
	}

	public final QuotedUUIDContext quotedUUID() throws RecognitionException {
		QuotedUUIDContext _localctx = new QuotedUUIDContext(_ctx, getState());
		enterRule(_localctx, 86, RULE_quotedUUID);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(374);
			match(QUOTED_UUID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class QuotedStringContext extends ParserRuleContext {
		public TerminalNode QUOTED_STRING() { return getToken(ODataParser.QUOTED_STRING, 0); }
		public QuotedStringContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_quotedString; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterQuotedString(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitQuotedString(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitQuotedString(this);
			else return visitor.visitChildren(this);
		}
	}

	public final QuotedStringContext quotedString() throws RecognitionException {
		QuotedStringContext _localctx = new QuotedStringContext(_ctx, getState());
		enterRule(_localctx, 88, RULE_quotedString);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(376);
			match(QUOTED_STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public final DquotedStringContext dquotedString() throws RecognitionException {
		DquotedStringContext _localctx = new DquotedStringContext(_ctx, getState());
		enterRule(_localctx, 90, RULE_dquotedString);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(378);
				match(DQUOTED_STRING);
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final NullSpecLiteralContext nullSpecLiteral() throws RecognitionException {
		NullSpecLiteralContext _localctx = new NullSpecLiteralContext(_ctx, getState());
		enterRule(_localctx, 92, RULE_nullSpecLiteral);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(380);
			match(NULL_SPEC_LITERAL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NullSpecLiteralContext extends ParserRuleContext {
		public TerminalNode NULL_SPEC_LITERAL() { return getToken(ODataParser.NULL_SPEC_LITERAL, 0); }
		public NullSpecLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_nullSpecLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterNullSpecLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitNullSpecLiteral(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitNullSpecLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NullTokenContext nullToken() throws RecognitionException {
		NullTokenContext _localctx = new NullTokenContext(_ctx, getState());
		enterRule(_localctx, 94, RULE_nullToken);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(382);
			match(NULL_TOKEN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NullTokenContext extends ParserRuleContext {
		public TerminalNode NULL_TOKEN() { return getToken(ODataParser.NULL_TOKEN, 0); }
		public NullTokenContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_nullToken; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterNullToken(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitNullToken(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitNullToken(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BooleanValueContext booleanValue() throws RecognitionException {
		BooleanValueContext _localctx = new BooleanValueContext(_ctx, getState());
		enterRule(_localctx, 96, RULE_booleanValue);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(384);
			match(BOOLEAN_VALUE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BooleanValueContext extends ParserRuleContext {
		public TerminalNode BOOLEAN_VALUE() { return getToken(ODataParser.BOOLEAN_VALUE, 0); }
		public BooleanValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_booleanValue; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterBooleanValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitBooleanValue(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitBooleanValue(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DurationValueContext durationValue() throws RecognitionException {
		DurationValueContext _localctx = new DurationValueContext(_ctx, getState());
		enterRule(_localctx, 98, RULE_durationValue);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(386);
			match(DURATION_VALUE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DurationValueContext extends ParserRuleContext {
		public TerminalNode DURATION_VALUE() { return getToken(ODataParser.DURATION_VALUE, 0); }
		public DurationValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_durationValue; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterDurationValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitDurationValue(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitDurationValue(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DateValueContext dateValue() throws RecognitionException {
		DateValueContext _localctx = new DateValueContext(_ctx, getState());
		enterRule(_localctx, 100, RULE_dateValue);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(388);
			match(DATE_VALUE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DateValueContext extends ParserRuleContext {
		public TerminalNode DATE_VALUE() { return getToken(ODataParser.DATE_VALUE, 0); }
		public DateValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dateValue; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterDateValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitDateValue(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitDateValue(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DateTimeOffsetValueContext dateTimeOffsetValue() throws RecognitionException {
		DateTimeOffsetValueContext _localctx = new DateTimeOffsetValueContext(_ctx, getState());
		enterRule(_localctx, 102, RULE_dateTimeOffsetValue);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(390);
			match(DATE_TIME_OFFSET_VALUE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DateTimeOffsetValueContext extends ParserRuleContext {
		public TerminalNode DATE_TIME_OFFSET_VALUE() { return getToken(ODataParser.DATE_TIME_OFFSET_VALUE, 0); }
		public DateTimeOffsetValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dateTimeOffsetValue; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterDateTimeOffsetValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitDateTimeOffsetValue(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitDateTimeOffsetValue(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TimeOfDayValueContext timeOfDayValue() throws RecognitionException {
		TimeOfDayValueContext _localctx = new TimeOfDayValueContext(_ctx, getState());
		enterRule(_localctx, 104, RULE_timeOfDayValue);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(392);
			match(TIME_OF_DAY_VALUE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TimeOfDayValueContext extends ParserRuleContext {
		public TerminalNode TIME_OF_DAY_VALUE() { return getToken(ODataParser.TIME_OF_DAY_VALUE, 0); }
		public TimeOfDayValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_timeOfDayValue; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterTimeOfDayValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitTimeOfDayValue(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitTimeOfDayValue(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DecimalLiteralContext decimalLiteral() throws RecognitionException {
		DecimalLiteralContext _localctx = new DecimalLiteralContext(_ctx, getState());
		enterRule(_localctx, 106, RULE_decimalLiteral);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(394);
			match(DECIMAL_LITERAL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DecimalLiteralContext extends ParserRuleContext {
		public TerminalNode DECIMAL_LITERAL() { return getToken(ODataParser.DECIMAL_LITERAL, 0); }
		public DecimalLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_decimalLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterDecimalLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitDecimalLiteral(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitDecimalLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FloatValueContext floatValue() throws RecognitionException {
		FloatValueContext _localctx = new FloatValueContext(_ctx, getState());
		enterRule(_localctx, 108, RULE_floatValue);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(396);
			match(FLOAT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FloatValueContext extends ParserRuleContext {
		public TerminalNode FLOAT() { return getToken(ODataParser.FLOAT, 0); }
		public FloatValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_floatValue; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterFloatValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitFloatValue(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitFloatValue(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IntegerValueContext integerValue() throws RecognitionException {
		IntegerValueContext _localctx = new IntegerValueContext(_ctx, getState());
		enterRule(_localctx, 110, RULE_integerValue);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(398);
			match(INTEGER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntegerValueContext extends ParserRuleContext {
		public TerminalNode INTEGER() { return getToken(ODataParser.INTEGER, 0); }
		public IntegerValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_integerValue; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterIntegerValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitIntegerValue(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitIntegerValue(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class PrimitiveLiteralContext extends ParserRuleContext {
		public QuotedUUIDContext quotedUUID() {
			return getRuleContext(QuotedUUIDContext.class,0);
		}

		public DquotedStringContext dquotedString() {
			return getRuleContext(DquotedStringContext.class, 0);
		}
		public QuotedStringContext quotedString() {
			return getRuleContext(QuotedStringContext.class,0);
		}
		public NullSpecLiteralContext nullSpecLiteral() {
			return getRuleContext(NullSpecLiteralContext.class,0);
		}
		public NullTokenContext nullToken() {
			return getRuleContext(NullTokenContext.class,0);
		}
		public BooleanValueContext booleanValue() {
			return getRuleContext(BooleanValueContext.class,0);
		}
		public DurationValueContext durationValue() {
			return getRuleContext(DurationValueContext.class,0);
		}
		public DateValueContext dateValue() {
			return getRuleContext(DateValueContext.class,0);
		}
		public DateTimeOffsetValueContext dateTimeOffsetValue() {
			return getRuleContext(DateTimeOffsetValueContext.class,0);
		}
		public TimeOfDayValueContext timeOfDayValue() {
			return getRuleContext(TimeOfDayValueContext.class,0);
		}
		public DecimalLiteralContext decimalLiteral() {
			return getRuleContext(DecimalLiteralContext.class,0);
		}
		public FloatValueContext floatValue() {
			return getRuleContext(FloatValueContext.class,0);
		}
		public IntegerValueContext integerValue() {
			return getRuleContext(IntegerValueContext.class,0);
		}
		public PrimitiveLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_primitiveLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).enterPrimitiveLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ODataParserListener ) ((ODataParserListener)listener).exitPrimitiveLiteral(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ODataParserVisitor ) return ((ODataParserVisitor<? extends T>)visitor).visitPrimitiveLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 10:
			return filterExpression_sempred((FilterExpressionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean filterExpression_sempred(FilterExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 2);
		}
		return true;
	}

	public static class DquotedStringContext extends ParserRuleContext {
		public DquotedStringContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public TerminalNode DQUOTED_STRING() {
			return getToken(ODataParser.DQUOTED_STRING, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_dquotedString;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof ODataParserListener) ((ODataParserListener) listener).enterDquotedString(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof ODataParserListener) ((ODataParserListener) listener).exitDquotedString(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof ODataParserVisitor)
				return ((ODataParserVisitor<? extends T>) visitor).visitDquotedString(this);
			else return visitor.visitChildren(this);
		}
	}
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}