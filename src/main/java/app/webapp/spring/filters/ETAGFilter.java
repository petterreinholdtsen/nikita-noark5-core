package app.webapp.spring.filters;

import app.webapp.exceptions.NikitaMalformedHeaderException;
import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Component;

import java.io.IOException;

import static app.utils.constants.ErrorMessagesConstants.*;
import static java.lang.String.format;
import static org.springframework.http.HttpHeaders.ETAG;

/**
 * The filter has three purposes:
 * 1. Make sure that each incoming PUT/PATCH request has en ETAG
 * 2. Make sure that the ETAG is numeric as that is what we use in the nikita service layer
 * Throwing early means nikita service layer can assume that the ETAG is not null and numeric
 */
@Component
public class ETAGFilter
        implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String method = request.getMethod().toLowerCase();
        if ("put".equals(method) || "patch".equals(method)) {
            String quotedETAG = request.getHeader(ETAG);
            if (null != quotedETAG && !quotedETAG.isBlank()) {
                long etagVal;
                try {
                    etagVal = Long.parseLong(quotedETAG.replaceAll("^\"|\"$", ""));
                } catch (NumberFormatException nfe) {
                    throw new NikitaMalformedHeaderException(format(ETAG_MISSING_NON_NUMERIC, quotedETAG));
                }
                if (etagVal < 0) {
                    throw new NikitaMalformedHeaderException(format(ETAG_MISSING_NEGATIVE, quotedETAG));
                }
            } else {
                throw new NikitaMalformedHeaderException(format(REQUIRED_ETAG_MISSING, method));
            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        Filter.super.init(filterConfig);
    }

    @Override
    public void destroy() {
        Filter.super.destroy();
    }
}
