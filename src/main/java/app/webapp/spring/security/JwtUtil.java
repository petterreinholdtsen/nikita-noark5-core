package app.webapp.spring.security;

import com.nimbusds.jose.shaded.gson.internal.LinkedTreeMap;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;

import java.util.List;
import java.util.stream.Collectors;

public class JwtUtil {
    private JwtUtil() {
    }

    public static JwtUser createJwtUser(Jwt jwt) {
        LinkedTreeMap map = (LinkedTreeMap) jwt.getClaims().get("realm_access");
        List<GrantedAuthority> authoritiesLocal = ((List<String>)map.get("roles")).stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
        return new JwtUser(jwt, authoritiesLocal, 1);
    }
}

