package app.webapp.exceptions;

public class UsernameExistsException extends NikitaException {
    public UsernameExistsException() {
        super();
    }

    public UsernameExistsException(String message) {
        super(message);
    }
}
