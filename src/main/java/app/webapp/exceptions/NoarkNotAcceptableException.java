package app.webapp.exceptions;

public class NoarkNotAcceptableException extends NikitaException {

    public NoarkNotAcceptableException(final String message) {
        super(message);
    }
}
