package app.webapp.exceptions;

public class NoarkConcurrencyException extends NikitaException {

    public NoarkConcurrencyException(final String message) {
        super(message);
    }
}
