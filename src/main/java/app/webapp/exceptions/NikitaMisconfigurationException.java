package app.webapp.exceptions;

public class NikitaMisconfigurationException extends NikitaException {

    public NikitaMisconfigurationException(final String message) {
        super(message);
    }
}
