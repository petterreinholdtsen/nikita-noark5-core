package app.webapp.exceptions;

/**
 * Created by tsodring on 3/6/17.
 */
public class StorageFileNotFoundException extends NikitaException {
    public StorageFileNotFoundException(String message) {
        super(message);
    }
}
