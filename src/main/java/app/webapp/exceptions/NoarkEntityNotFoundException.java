package app.webapp.exceptions;

public class NoarkEntityNotFoundException
        extends NikitaException {

    public NoarkEntityNotFoundException(String message) {
        super(message);
    }
}
