package app.webapp.events;

import app.domain.interfaces.entities.ISystemId;
import app.domain.noark5.admin.DeleteLog;
import jakarta.validation.constraints.NotNull;

/**
 * Base class that can be used to identify DELETE events that can occur in
 * nikita.
 */
public class AfterNoarkEntityDeletedEvent
        extends AfterNoarkEntityEvent {

    private final DeleteLog deleteLog;

    public AfterNoarkEntityDeletedEvent(@NotNull DeleteLog deleteLog,
                                        @NotNull ISystemId entity) {
        super(entity);
        this.deleteLog = deleteLog;
    }

    public DeleteLog getDeleteLog() {
        return deleteLog;
    }
}
