package app.webapp.events;

import app.domain.interfaces.entities.ISystemId;
import app.domain.noark5.admin.CreateLog;
import app.domain.repository.noark5.v5.IChangeLogRepository;
import app.domain.repository.noark5.v5.admin.ICreateLogRepository;
import app.domain.repository.noark5.v5.admin.IDeleteLogRepository;
import app.domain.repository.noark5.v5.admin.IReadLogRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

import static app.utils.constants.Constants.HATEOAS_API_PATH;
import static app.utils.constants.Constants.SLASH;
import static org.springframework.transaction.event.TransactionPhase.BEFORE_COMMIT;

/**
 * Basic Event listeners to implement a Noark EventLog.
 */
@Component
public class NikitaEventListener {

    protected static final Logger logger =
            LoggerFactory.getLogger(NikitaEventListener.class);
    private final IChangeLogRepository changeLogRepository;
    private final IReadLogRepository readLogRepository;
    private final IDeleteLogRepository deleteLogRepository;
    private final ICreateLogRepository createLogRepository;

    public NikitaEventListener(IChangeLogRepository changeLogRepository,
                               IReadLogRepository readLogRepository,
                               IDeleteLogRepository deleteLogRepository,
                               ICreateLogRepository createLogRepository) {
        this.changeLogRepository = changeLogRepository;
        this.readLogRepository = readLogRepository;
        this.deleteLogRepository = deleteLogRepository;
        this.createLogRepository = createLogRepository;
    }

    @TransactionalEventListener(phase = BEFORE_COMMIT)
    public void handleCreationEvent(AfterNoarkEntityCreatedEvent event) {
        CreateLog createLog = event.getCreateLog();
        ISystemId systemIdEntity = event.getEntity();
        // The following fields will have relevant values that can be used at this stage (BEFORE_COMMIT)
        // in the hibernate lifecycle phase.
        // Copy creation time value into the changelog so the changelog value matches the value in the archiveUnit
        createLog.setEventDate(systemIdEntity.getCreatedDate());
        createLog.setEventInitiator(event.getEntity().getCreatedBy());
        createLog.setHrefReferenceArchiveUnit(HATEOAS_API_PATH + SLASH + systemIdEntity.getFunctionalTypeName() +
                SLASH + systemIdEntity.getBaseTypeName() + SLASH + systemIdEntity.getSystemIdAsString());
        createLog.setRelReferenceArchiveUnit(systemIdEntity.getBaseRel());
        createLog.setReferenceArchiveUnitSystemId(systemIdEntity.getSystemId());

        createLogRepository.save(createLog);
        logger.info("Nikita created an object of type [{}], {}", event.getEntity().getClass().getSimpleName(), event);
    }

    @TransactionalEventListener(phase = BEFORE_COMMIT)
    public void handleUpdateEvent(AfterNoarkEntityUpdatedEvent event) {
        changeLogRepository.save(event.getChangeLog());
        logger.info("Nikita updated an object of type [{}], {}", event.getEntity().getClass().getSimpleName(), event);
    }

    @TransactionalEventListener(phase = BEFORE_COMMIT)
    public void handleDeletionEvent(AfterNoarkEntityDeletedEvent event) {
        deleteLogRepository.save(event.getDeleteLog());
        logger.info("Nikita deleted an object of type [{}], {}", event.getEntity().getClass().getSimpleName(), event);
    }

    @TransactionalEventListener(phase = BEFORE_COMMIT)
    public void handleReadEvent(AfterNoarkEntityReadEvent event) {
        readLogRepository.save(event.getReadLog());
        logger.info("Nikita read an object of type [{}], {}", event.getEntity().getClass().getSimpleName(), event);
    }
}
