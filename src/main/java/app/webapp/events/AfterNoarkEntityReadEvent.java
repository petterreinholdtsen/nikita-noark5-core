package app.webapp.events;

import app.domain.interfaces.entities.ISystemId;
import app.domain.noark5.admin.ReadLog;
import jakarta.validation.constraints.NotNull;

/**
 * Base class that can be used to identify READ events that can occur in
 * nikita.
 */
public class AfterNoarkEntityReadEvent
        extends AfterNoarkEntityEvent {

    private final ReadLog readLog;

    public AfterNoarkEntityReadEvent(@NotNull ReadLog readLog,
                                     @NotNull ISystemId entity) {
        super(entity);
        this.readLog = readLog;
    }

    public ReadLog getReadLog() {
        return readLog;
    }
}
