package app.webapp.events;

import app.domain.interfaces.entities.ISystemId;
import app.domain.repository.noark5.v5.IChangeLogRepository;
import app.domain.repository.noark5.v5.admin.ICreateLogRepository;
import app.domain.repository.noark5.v5.admin.IDeleteLogRepository;
import app.domain.repository.noark5.v5.admin.IReadLogRepository;
import app.integration.crud.CRUDMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

import static java.time.LocalTime.now;
import static org.springframework.transaction.event.TransactionPhase.BEFORE_COMMIT;

/**
 * Event listeners to be able to respond when CRUD that operations have
 * occurred.
 */

@Component
@Profile("crudreporting")
public class NikitaCRUDEventListener
        extends NikitaEventListener {

    protected static final Logger logger =
            LoggerFactory.getLogger(NikitaCRUDEventListener.class);
    private final RabbitTemplate rabbitTemplate;

    @Value("${nikita.rabbitmq.queue_name_crud_reporting:noark-crud-reporting}")
    private String reportingQueue;

    public NikitaCRUDEventListener(IChangeLogRepository changeLogRepository,
                                   IReadLogRepository readLogRepository,
                                   IDeleteLogRepository deleteLogRepository,
                                   ICreateLogRepository createLogRepository,
                                   RabbitTemplate rabbitTemplate) {
        super(changeLogRepository, readLogRepository, deleteLogRepository, createLogRepository);
        this.rabbitTemplate = rabbitTemplate;
    }

    @TransactionalEventListener
    public void handleCreationEvent(AfterNoarkEntityCreatedEvent event) {
        super.handleCreationEvent(event);
        sendMessage(event, "create");
        logger.info("CRUD Reporting (CREATE): Nikita queued an object of type [" +
                event.getEntity().getClass().getSimpleName() + "], " +
                event);
    }

    public void handleReadEvent(AfterNoarkEntityReadEvent event) {
        super.handleReadEvent(event);
        logger.info("CRUD Reporting (READ): Nikita queued anobject of type [" +
                event.getEntity().getClass().getSimpleName() + "], " +
                event);
    }

    @TransactionalEventListener(phase = BEFORE_COMMIT)
    public void handleUpdateEvent(AfterNoarkEntityUpdatedEvent event) {
        super.handleUpdateEvent(event);
        sendMessage(event, "update");
        logger.info("CRUD Reporting (UPDATE): Nikita queued an object of type [" +
                event.getEntity().getClass().getSimpleName() + "], " +
                event);
    }

    @TransactionalEventListener(phase = BEFORE_COMMIT)
    public void handleDeletionEvent(AfterNoarkEntityDeletedEvent event) {
        super.handleDeletionEvent(event);
        sendMessage(event, "delete");
        logger.info("CRUD Reporting (DELETE): Nikita queued an object of type [" +
                event.getEntity().getClass().getSimpleName() + "], " +
                event);
    }

    private void sendMessage(AfterNoarkEntityEvent event, String eventType) {
        String timeNow = now().toString();
        ISystemId systemIdEntity = event.getEntity();
        CRUDMessage crudMessage = new CRUDMessage(
                systemIdEntity.getSystemIdAsString(),
                systemIdEntity.getCreatedBy(),
                timeNow,
                systemIdEntity.getBaseTypeName(),
                eventType);
        String payload = crudMessage.toString();
        rabbitTemplate.convertAndSend(reportingQueue, payload);
    }
}
