package app.webapp.startup;

import app.domain.noark5.admin.*;
import app.domain.repository.admin.AuthorityRepository;
import app.domain.repository.noark5.v5.admin.IOrganisationRepository;
import app.service.IUserService;
import app.service.noark5.admin.AdministrativeUnitService;
import app.service.noark5.admin.UserService;
import app.webapp.exceptions.NikitaMisconfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import static app.domain.noark5.admin.AuthorityName.*;
import static app.utils.constants.Constants.TEST_ADMINISTRATIVE_UNIT;

/**
 * Create built in users.
 * TODO: Replace with data in JSON files so that it can be handled in the same
 * way metadata is imported
 */
@Component
public class UserStartupImport {

    private final IUserService userService;
    private final AuthorityRepository authorityRepository;
    private final IOrganisationRepository organisationRepository;
    private final AdministrativeUnitService administrativeUnitService;

    private final Logger logger =
            LoggerFactory.getLogger(UserStartupImport.class);

    public UserStartupImport(
            UserService userService,
            AuthorityRepository authorityRepository,
            IOrganisationRepository organisationRepository,
            AdministrativeUnitService administrativeUnitService) {
        this.userService = userService;
        this.authorityRepository = authorityRepository;
        this.organisationRepository = organisationRepository;
        this.administrativeUnitService = administrativeUnitService;
    }

    @Transactional
    public void addAdminUnit() {
        // Create an administrative unit
        AdministrativeUnit administrativeUnit = new AdministrativeUnit();
        administrativeUnit.setAdministrativeUnitName(TEST_ADMINISTRATIVE_UNIT);
        administrativeUnit.setShortName("test");
        administrativeUnit.setDefaultAdministrativeUnit(true);
        administrativeUnitService.
                createNewAdministrativeUnitBySystemNoDuplicate(
                        administrativeUnit);
    }

    @Transactional
    public void addAuthorities() {
        addAuthority(RECORDS_MANAGER);
        addAuthority(RECORDS_KEEPER);
        addAuthority(CASE_HANDLER);
        addAuthority(LEADER);
        addAuthority(GUEST);
    }

    @Transactional
    public void addOrganisation() {
        Organisation organisation = new Organisation();
        organisation.setOrganisationName("Demo Organisation");
        organisation.setTitle("Demo Organisation");
        organisationRepository.save(organisation);
    }

    @Transactional
    public void addUserAdmin() {
        if (userService.userExists("admin@example.com")) {
            logger.info("During startup, user admin@example.com is already " +
                    "registered in the database");
            return;
        }
        AdministrativeUnit administrativeUnit = getAdministrativeUnitOrThrow();
        User admin = new User();
        admin.setOrganisation(organisationRepository.findAll().iterator().next());
        admin.setPassword("password");
        admin.setFirstname("Frank");
        admin.setLastname("Grimes");
        admin.setUsername("admin@example.com");
        admin.addAuthority(authorityRepository
                .findByAuthorityName(RECORDS_MANAGER));
        administrativeUnit.addUser(admin);
        userService.createNewUserDuringStartup(admin);
    }

    @Transactional
    public void addUserRecordKeeper() {
        if (userService.userExists("recordkeeper@example.com")) {
            logger.info("During startup, user recordkeeper@example.com is " +
                    "already registered in the database");
            return;
        }
        AdministrativeUnit administrativeUnit = getAdministrativeUnitOrThrow();
        User recordKeeper = new User();
        recordKeeper.setOrganisation(organisationRepository.findAll().iterator().next());
        recordKeeper.setPassword("password");
        recordKeeper.setFirstname("Moe");
        recordKeeper.setLastname("Szyslak");
        recordKeeper.setUsername("recordkeeper@example.com");
        recordKeeper.addAuthority(authorityRepository
                .findByAuthorityName(RECORDS_KEEPER));
        administrativeUnit.addUser(recordKeeper);
        userService.createNewUserDuringStartup(recordKeeper);
    }

    private AdministrativeUnit getAdministrativeUnitOrThrow() {
        AdministrativeUnit administrativeUnit =
                administrativeUnitService.findFirst();
        if (null != administrativeUnit) {
            return administrativeUnit;
        } else {
            throw new NikitaMisconfigurationException("Could not find default" +
                    " administrativeUnit for demo users");
        }
    }

    private void addAuthority(AuthorityName authorityName) {
        Authority authority = new Authority();
        if (!userService.authorityExists(authorityName)) {
            authority.setAuthorityName(authorityName);
            authorityRepository.save(authority);
        }
    }
}
