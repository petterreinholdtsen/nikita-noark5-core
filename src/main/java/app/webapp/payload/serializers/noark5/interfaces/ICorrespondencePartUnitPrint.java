package app.webapp.payload.serializers.noark5.interfaces;

import app.domain.interfaces.entities.secondary.IGenericUnitEntity;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

public interface ICorrespondencePartUnitPrint
        extends ICorrespondencePartPrint {
    default void printCorrespondencePartUnit(JsonGenerator jgen, IGenericUnitEntity unit) throws IOException {
        printGenericUnit(jgen, unit);
    }
}
