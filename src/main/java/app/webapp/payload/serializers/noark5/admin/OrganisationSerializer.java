package app.webapp.payload.serializers.noark5.admin;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.admin.Organisation;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.NoarkGeneralEntitySerializer;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.ORGANISATION_NAME;
import static app.utils.constants.N5ResourceMappings.ORGANISATION_STATUS;

/**
 * Serialize an outgoing Organisation object as JSON.
 */
public class OrganisationSerializer
        extends NoarkGeneralEntitySerializer {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject organisationLinks,
                                     JsonGenerator jgen) throws IOException {
        Organisation organisation = (Organisation) noarkEntity;
        jgen.writeStartObject();
        printSystemIdEntity(jgen, organisation);
        printCreateEntity(jgen, organisation);
        printFinaliseEntity(jgen, organisation);
        print(jgen, ORGANISATION_NAME,
                organisation.getOrganisationName());
        printNullable(jgen, ORGANISATION_STATUS,
                organisation.getOrganisationStatus());
        printLinks(jgen, organisationLinks
                .getLinks(organisation));
        jgen.writeEndObject();
    }
}
