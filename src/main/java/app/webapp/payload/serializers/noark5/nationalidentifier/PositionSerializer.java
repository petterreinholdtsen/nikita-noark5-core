package app.webapp.payload.serializers.noark5.nationalidentifier;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.nationalidentifier.Position;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.SystemIdEntitySerializer;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

public class PositionSerializer
        extends SystemIdEntitySerializer {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject positionLinks,
                                     JsonGenerator jgen) throws IOException {
        Position position = (Position) noarkEntity;
        jgen.writeStartObject();
        printSystemIdEntity(jgen, position);
        printNullableMetadata(jgen, COORDINATE_SYSTEM, position.getCoordinateSystem());
        printNullable(jgen, X, position.getX());
        printNullable(jgen, Y, position.getY());
        printNullable(jgen, Z, position.getZ());
        printLinks(jgen, positionLinks.getLinks(position));
        jgen.writeEndObject();
    }
}
