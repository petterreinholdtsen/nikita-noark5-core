package app.webapp.payload.serializers.noark5.metadata;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.md_other.BSMMetadata;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.SystemIdEntitySerializer;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

public class BSMMetadataSerializer
        extends SystemIdEntitySerializer {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity,
                                     LinksNoarkObject bSMMetadataLinks, JsonGenerator jgen)
            throws IOException {
        BSMMetadata bsmMetadata = (BSMMetadata) noarkEntity;
        jgen.writeStartObject();
        printSystemIdEntity(jgen, bsmMetadata);
        print(jgen, NAME, bsmMetadata.getName());
        print(jgen, TYPE, bsmMetadata.getType());
        Boolean outdated = bsmMetadata.getOutdated();
        if (null != outdated && outdated) {
            jgen.writeBooleanField(OUTDATED, outdated);
        }
        printNullable(jgen, DESCRIPTION, bsmMetadata.getDescription());
        printNullable(jgen, SOURCE, bsmMetadata.getSource());
        printLinks(jgen, bSMMetadataLinks.getLinks(bsmMetadata));
        jgen.writeEndObject();
    }
}
