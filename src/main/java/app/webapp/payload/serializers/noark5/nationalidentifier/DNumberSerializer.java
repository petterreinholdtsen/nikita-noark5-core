package app.webapp.payload.serializers.noark5.nationalidentifier;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.nationalidentifier.DNumber;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.SystemIdEntitySerializer;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.D_NUMBER_FIELD;

public class DNumberSerializer
        extends SystemIdEntitySerializer {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject dNumberLinks,
                                     JsonGenerator jgen) throws IOException {
        DNumber dNumber = (DNumber) noarkEntity;
        jgen.writeStartObject();
        printSystemIdEntity(jgen, dNumber);
        printNullable(jgen, D_NUMBER_FIELD, dNumber.getdNumber());
        printLinks(jgen, dNumberLinks.getLinks(dNumber));
        jgen.writeEndObject();
    }
}
