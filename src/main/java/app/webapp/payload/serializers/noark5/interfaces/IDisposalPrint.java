package app.webapp.payload.serializers.noark5.interfaces;

import app.domain.interfaces.IDisposal;
import app.domain.noark5.secondary.Disposal;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

public interface IDisposalPrint
        extends IPrint {
    default void printDisposal(JsonGenerator jgen, IDisposal disposalEntity)
            throws IOException {
        if (disposalEntity != null) {
            Disposal disposal = disposalEntity.getReferenceDisposal();
            if (disposal != null) {
                jgen.writeObjectFieldStart(DISPOSAL);
                printNullable(jgen, DISPOSAL_DECISION, disposal.getDisposalDecision());
                printNullable(jgen, DISPOSAL_AUTHORITY, disposal.getDisposalAuthority());
                printNullable(jgen, DISPOSAL_PRESERVATION_TIME, disposal.getPreservationTime());
                printNullableDateTime(jgen, DISPOSAL_DATE, disposal.getDisposalDate());
                jgen.writeEndObject();
            }
        }
    }
}
