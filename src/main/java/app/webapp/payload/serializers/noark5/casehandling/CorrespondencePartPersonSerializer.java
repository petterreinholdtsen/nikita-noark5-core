package app.webapp.payload.serializers.noark5.casehandling;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.casehandling.secondary.CorrespondencePartPerson;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.interfaces.ICorrespondencePartPersonPrint;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

/**
 * Serialize an outgoing CorrespondencePartPerson object as JSON.
 */
public class CorrespondencePartPersonSerializer
        extends CorrespondencePartSerializer
        implements ICorrespondencePartPersonPrint {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject correspondencePartPersonLinks,
                                     JsonGenerator jgen) throws IOException {
        CorrespondencePartPerson correspondencePartPerson = (CorrespondencePartPerson) noarkEntity;
        jgen.writeStartObject();
        printCorrespondencePart(jgen, correspondencePartPerson);
        printCorrespondencePartPerson(jgen, correspondencePartPerson);
        printBSM(jgen, correspondencePartPerson);
        printLinks(jgen, correspondencePartPersonLinks.getLinks(correspondencePartPerson));
        jgen.writeEndObject();
    }
}
