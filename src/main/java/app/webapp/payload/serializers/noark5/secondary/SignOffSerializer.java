package app.webapp.payload.serializers.noark5.secondary;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.INoarkEntity;
import app.domain.interfaces.entities.secondary.ISignOffEntity;
import app.webapp.payload.builder.noark5.secondary.SignOffLinksBuilder;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.links.secondary.SignOffLinks;
import app.webapp.payload.serializers.noark5.SystemIdEntitySerializer;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

/**
 * Serialize an outgoing SignOff object as JSON.
 */
@LinksPacker(using = SignOffLinksBuilder.class)
@LinksObject(using = SignOffLinks.class)
public class SignOffSerializer
        extends SystemIdEntitySerializer {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject signOffLinks, JsonGenerator jgen)
            throws IOException {
        ISignOffEntity signOffEntity = (ISignOffEntity) noarkEntity;
        jgen.writeStartObject();

        if (signOffEntity != null) {
            printSystemIdEntity(jgen, signOffEntity);
            printDateTime(jgen, SIGN_OFF_DATE, signOffEntity.getSignOffDate());
            printNullable(jgen, SIGN_OFF_BY, signOffEntity.getSignOffBy());
            printNullableMetadata(jgen, SIGN_OFF_METHOD, signOffEntity.getSignOffMethod());
            // TODO handle referanseAvskrevetAv
            if (null != signOffEntity.getReferenceSignedOffRecordSystemID()) {
                print(jgen, SIGN_OFF_REFERENCE_RECORD, signOffEntity.getReferenceSignedOffRecordSystemID());
            }
            if (null != signOffEntity.getReferenceSignedOffCorrespondencePartSystemID()) {
                print(jgen, SIGN_OFF_REFERENCE_CORRESPONDENCE_PART,
                        signOffEntity.getReferenceSignedOffCorrespondencePartSystemID());
            }
        }
        printLinks(jgen, signOffLinks.getLinks(signOffEntity));
        jgen.writeEndObject();
    }
}
