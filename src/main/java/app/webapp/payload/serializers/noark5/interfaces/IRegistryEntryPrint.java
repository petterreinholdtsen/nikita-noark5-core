package app.webapp.payload.serializers.noark5.interfaces;

import app.domain.interfaces.entities.IRegistryEntryEntity;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

/**
 * A RegistryEntry is very similar to RecordNote. The main difference is that in a RecordNote,
 * the fields are voluntary, while in a RegistryEntry most fields are obligatory.
 */
public interface IRegistryEntryPrint
        extends IRecordNotePrint {

    default void printRegistryEntryEntity(JsonGenerator jgen, IRegistryEntryEntity registryEntry)
            throws IOException {
        printRecordNoteEntity(jgen, registryEntry);
        if (null != registryEntry) {
            print(jgen, REGISTRY_ENTRY_YEAR, registryEntry.getRecordYear());
            print(jgen, REGISTRY_ENTRY_SEQUENCE_NUMBER, registryEntry.getRecordSequenceNumber());
            print(jgen, REGISTRY_ENTRY_NUMBER, registryEntry.getRegistryEntryNumber());
            printMetadata(jgen, REGISTRY_ENTRY_TYPE, registryEntry.getRegistryEntryType());
            printMetadata(jgen, REGISTRY_ENTRY_STATUS, registryEntry.getRegistryEntryStatus());
            printDateTime(jgen, REGISTRY_ENTRY_DATE, registryEntry.getRecordDate());
        }
    }
}
