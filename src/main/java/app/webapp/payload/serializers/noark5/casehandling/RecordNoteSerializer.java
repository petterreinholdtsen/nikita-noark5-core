package app.webapp.payload.serializers.noark5.casehandling;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.casehandling.RecordNote;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.RecordSerializer;
import app.webapp.payload.serializers.noark5.interfaces.IRecordNotePrint;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

/**
 * Serialize an outgoing RecordNote object as JSON.
 */
public class RecordNoteSerializer
        extends RecordSerializer
        implements IRecordNotePrint {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject recordNoteLinks,
                                     JsonGenerator jgen) throws IOException {
        RecordNote recordNote = (RecordNote) noarkEntity;
        jgen.writeStartObject();
        printRecordEntity(jgen, recordNote);
        printRecordNoteEntity(jgen, recordNote);
        printLinks(jgen, recordNoteLinks.getLinks(recordNote));
        jgen.writeEndObject();
    }
}
