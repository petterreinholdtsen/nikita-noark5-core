package app.webapp.payload.serializers.noark5.nationalidentifier;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.interfaces.entities.nationalidentifier.*;
import app.domain.noark5.nationalidentifier.*;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.SystemIdEntitySerializer;
import com.fasterxml.jackson.core.JsonGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

/**
 * Serialize an outgoing NationalIdentifier object as JSON.
 */
public class NationalIdentifierSerializer
        extends SystemIdEntitySerializer {

    private static final Logger logger =
            LoggerFactory.getLogger(NationalIdentifierSerializer.class);

    // TODO figure out how to avoid duplicating code with BuildingSerializer
    public void printBuilding(IBuildingEntity building, LinksNoarkObject buildingLinks, JsonGenerator jgen)
            throws IOException {
        jgen.writeStartObject();
        printSystemIdEntity(jgen, building);
        print(jgen, BUILDING_NUMBER, building.getBuildingNumber());
        printNullable(jgen, BUILDING_CHANGE_NUMBER, building.getRunningChangeNumber());
        printLinks(jgen, buildingLinks.getLinks(building));
        jgen.writeEndObject();
    }

    // TODO figure out how to avoid duplicating code with CadastralUnitSerializer
    public void printCadastralUnit(ICadastralUnitEntity cadastralUnit, LinksNoarkObject cadastralUnitLinks,
                                   JsonGenerator jgen) throws IOException {
        jgen.writeStartObject();
        printSystemIdEntity(jgen, cadastralUnit);
        printNullable(jgen, MUNICIPALITY_NUMBER, cadastralUnit.getMunicipalityNumber());
        printNullable(jgen, HOLDING_NUMBER, cadastralUnit.getHoldingNumber());
        printNullable(jgen, SUB_HOLDING_NUMBER, cadastralUnit.getSubHoldingNumber());
        printNullable(jgen, LEASE_NUMBER, cadastralUnit.getLeaseNumber());
        printNullable(jgen, SECTION_NUMBER, cadastralUnit.getSectionNumber());
        printLinks(jgen, cadastralUnitLinks.getLinks(cadastralUnit));
        jgen.writeEndObject();
    }

    // TODO figure out how to avoid duplicating code with DNumberSerializer
    public void printDNumber(IDNumberEntity dNumber, LinksNoarkObject dNumberLinks, JsonGenerator jgen)
            throws IOException {
        jgen.writeStartObject();
        printSystemIdEntity(jgen, dNumber);
        printNullable(jgen, D_NUMBER_FIELD, dNumber.getdNumber());
        printLinks(jgen, dNumberLinks.getLinks(dNumber));
        jgen.writeEndObject();
    }

    // TODO figure out how to avoid duplicating code with PlanSerializer
    public void printPlan(IPlanEntity plan, LinksNoarkObject planLinks, JsonGenerator jgen)
            throws IOException {
        jgen.writeStartObject();
        printSystemIdEntity(jgen, plan);
        printNullable(jgen, MUNICIPALITY_NUMBER, plan.getMunicipalityNumber());
        printNullable(jgen, COUNTY_NUMBER, plan.getCountyNumber());
        printNullableMetadata(jgen, COUNTRY_CODE, plan.getCountry());
        printNullable(jgen, PLAN_IDENTIFICATION, plan.getPlanIdentification());
        printLinks(jgen, planLinks.getLinks(plan));
        jgen.writeEndObject();
    }

    // TODO figure out how to avoid duplicating code with PositionSerializer
    private void printPosition(IPositionEntity position, LinksNoarkObject positionLinks, JsonGenerator jgen)
            throws IOException {
        jgen.writeStartObject();
        printSystemIdEntity(jgen, position);
        printNullableMetadata(jgen, COORDINATE_SYSTEM, position.getCoordinateSystem());
        printNullable(jgen, X, position.getX());
        printNullable(jgen, Y, position.getY());
        printNullable(jgen, Z, position.getZ());
        printLinks(jgen, positionLinks.getLinks(position));
        jgen.writeEndObject();
    }

    // TODO figure out how to avoid duplicating code with SocialSecurityNumberSerializer
    public void printSocialSecurityNumber
    (ISocialSecurityNumberEntity socialSecurityNumber,
     LinksNoarkObject socialSecurityNumberLinks,
     JsonGenerator jgen)
            throws IOException {
        jgen.writeStartObject();
        printSystemIdEntity(jgen, socialSecurityNumber);
        printNullable(jgen, SOCIAL_SECURITY_NUMBER, socialSecurityNumber.getSocialSecurityNumber());
        printLinks(jgen, socialSecurityNumberLinks.getLinks(socialSecurityNumber));
        jgen.writeEndObject();
    }

    // TODO figure out how to avoid duplicating code with UnitSerializer
    private void printUnit(IUnitEntity unit, LinksNoarkObject unitLinks, JsonGenerator jgen)
            throws IOException {
        jgen.writeStartObject();
        printSystemIdEntity(jgen, unit);
        printNullable(jgen, ORGANISATION_NUMBER, unit.getUnitIdentifier());
        printLinks(jgen, unitLinks.getLinks(unit));
        jgen.writeEndObject();
    }

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject nationalIdentifierLinks,
                                     JsonGenerator jgen)
            throws IOException {

        NationalIdentifier id = (NationalIdentifier) noarkEntity;

        if (id instanceof Building) {
            printBuilding((IBuildingEntity) id, nationalIdentifierLinks, jgen);
        } else if (id instanceof CadastralUnit) {
            printCadastralUnit((ICadastralUnitEntity) id, nationalIdentifierLinks, jgen);
        } else if (id instanceof DNumber) {
            printDNumber((IDNumberEntity) id, nationalIdentifierLinks, jgen);
        } else if (id instanceof Plan) {
            printPlan((IPlanEntity) id, nationalIdentifierLinks, jgen);
        } else if (id instanceof Position) {
            printPosition((IPositionEntity) id, nationalIdentifierLinks, jgen);
        } else if (id instanceof SocialSecurityNumber) {
            printSocialSecurityNumber
                    ((ISocialSecurityNumberEntity) id, nationalIdentifierLinks, jgen);
        } else if (id instanceof Unit) {
            printUnit((IUnitEntity) id, nationalIdentifierLinks, jgen);
        } else {
            logger.warn("Unhandled national identifier "
                    + noarkEntity.getBaseTypeName()
                    + " not serialized");
        }
    }
}
