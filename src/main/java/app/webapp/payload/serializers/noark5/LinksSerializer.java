package app.webapp.payload.serializers.noark5;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.IMetadataEntity;
import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.SystemIdEntity;
import app.webapp.exceptions.NikitaMisconfigurationException;
import app.webapp.payload.builder.noark5.LinksBuilder;
import app.webapp.payload.links.Link;
import app.webapp.payload.links.LinksNoarkObject;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import jakarta.servlet.http.HttpServletRequest;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.io.IOException;
import java.util.List;

import static app.utils.constants.Constants.ENTITY_ROOT_NAME_LIST;
import static app.utils.constants.Constants.ENTITY_ROOT_NAME_LIST_COUNT;
import static app.utils.constants.HATEOASConstants.*;

/**
 * Created by tsodring on 2/9/17.
 */
public class LinksSerializer
        extends StdSerializer<LinksNoarkObject> {

    private static final Logger logger =
            LoggerFactory.getLogger(LinksSerializer.class);

    public LinksSerializer() {
        super(LinksNoarkObject.class);
    }

    @Override
    public void serialize(
            LinksNoarkObject linksObject, JsonGenerator jgen,
            SerializerProvider provider)
            throws IOException {
        // For lists the output should be
        //  { "count": N, "results": [], "_links": [] }
        // An empty list should produce
        // { "count": 0, "_links": [] }
        // An entity should produce
        // { "field" : "value", "_links": [] }
        // No such thing as an empty entity
        List<INoarkEntity> list = linksObject.getList();
        if (list.size() > 0) {
            if (!linksObject.isSingleEntity()) {
                jgen.writeStartObject();
                jgen.writeNumberField(ENTITY_ROOT_NAME_LIST_COUNT,
                        linksObject.getCount());
                jgen.writeFieldName(ENTITY_ROOT_NAME_LIST);
                jgen.writeStartArray();
            }

            for (INoarkEntity entity : list) {
                if (!linksObject.isSingleEntity()) {
                    /*
                     * Use LinksBuilder for the leaf class, not the
                     * base class, to ensure all _links entries for
                     * the leaf class show up in the list
                     */
                    LinksNoarkObject noarkObject;
                    try {

                        Class<? extends INoarkEntity> cls =
                                (Class<? extends INoarkEntity>)
                                        Hibernate.unproxy(entity.getClass());

                        LinksPacker packer = cls.getAnnotation(
                                LinksPacker.class);
                        LinksObject individualLinksObject =
                                cls.getAnnotation(LinksObject.class);

                        // If the class we require is missing a LinksPacker
                        // annotation, it is not possible to continue. This
                        // should never happen, as all classes have a annotation
                        // but new classes introduced into the domain model over
                        // time may be missing the annotation. However Java
                        // will not retrieve annotations belonging to the
                        // parent class. Metadata objects will need a lookup
                        // to the super class to retrieve the annotation
                        // Note: ScreeningMetadata is a special case of an
                        // entity that is both a IMetadataEntity and a
                        // SystemIdEntity. It is primarily a SystemIdEntity
                        if (null == packer || null == individualLinksObject) {
                            if (entity instanceof IMetadataEntity &&
                                    !(entity instanceof SystemIdEntity)) {
                                cls = (Class<? extends IMetadataEntity>)
                                        entity.getClass().getSuperclass();
                                packer = cls.getAnnotation(LinksPacker.class);
                                individualLinksObject =
                                        cls.getAnnotation(LinksObject.class);
                            }
                        }
                        // If they are still null, then the annotation is
                        // actually missing
                        if (null == packer || null == individualLinksObject) {
                            String errorMessage = "Internal misconfiguration" +
                                    ": Missing annotations for " +
                                    entity.getClass().getSimpleName();
                            logger.error(errorMessage);
                            throw new NikitaMisconfigurationException(
                                    errorMessage);
                        }
                        if (entity instanceof IMetadataEntity &&
                                !(entity instanceof SystemIdEntity)) {
                            // Create an instance of the LinksObject
                            noarkObject = individualLinksObject
                                    .using()
                                    .getDeclaredConstructor(
                                            IMetadataEntity.class)
                                    .newInstance(entity);
                        } else {
                            noarkObject = individualLinksObject
                                    .using()
                                    .getDeclaredConstructor(
                                            INoarkEntity.class)
                                    .newInstance(entity);
                        }

                        // Create an instance of the LinksBuilder (
                        // links generator)
                        LinksBuilder handler =
                                packer.using().getConstructor().newInstance();

                        // Set the values for outgoing address (localhost, or
                        // from X-Forwarded-*)
                        handler.setAddress(getAddress());
                        handler.setContextPath(getContextPath());

                        // Add the links
                        handler.addLinks(noarkObject);
                    } catch (Exception e) {
                        String errorMessage = "Introspection failed while " +
                                "serialising list, using base LinksBuilder." +
                                " (" + e.getMessage() + ")";
                        logger.error(errorMessage);
                        throw new NikitaMisconfigurationException(
                                errorMessage);
                    }
                    serializeNoarkEntity(entity, noarkObject, jgen);
                } else {
                    serializeNoarkEntity(entity, linksObject, jgen);
                }
            }
            if (!linksObject.isSingleEntity()) {
                jgen.writeEndArray();
                printLinks(jgen, linksObject.getSelfLinks());
                jgen.writeEndObject();
            }
        }
        // It's an empty object, so just returning Hateoas self links
        else {
            jgen.writeStartObject();
            jgen.writeNumberField(ENTITY_ROOT_NAME_LIST_COUNT, 0);
            printLinks(jgen, linksObject.getSelfLinks());
            jgen.writeEndObject();
        }
    }

    protected void serializeNoarkEntity(INoarkEntity entity, LinksNoarkObject linksObject,
                                        JsonGenerator jgen) throws IOException {
    }

    /**
     * Get the context path.
     *
     * @return the context path
     */
    protected String getContextPath() {
        HttpServletRequest request = getRequest();
        return request.getContextPath();
    }

    /**
     * Get the outgoing URL to use. If we are running from localhost, it will
     * return http://localhost:8292. If we are running with forwarding from a
     * web server it will return e.g. https://nikita.oslomet.no
     * <p>
     * First attempt to see if we are running behind a web server, retrieving
     * X-Forwarded-* values. If these are null just use the values from the
     * incoming request.
     *
     * @return the URL to use for outgoing links
     */
    protected String getAddress() {
        HttpServletRequest request = getRequest();
        String address = request.getHeader("X-Forwarded-Host");
        String scheme = request.getHeader("X-Forwarded-Proto");
        String port = request.getHeader("X-Forwarded-Port");

        if (address == null && scheme == null) {
            scheme = request.getScheme();
            address = request.getServerName();
            port = Integer.toString(request.getServerPort());
        }

        if (port != null) {
            return scheme + "://" + address + ":" + port;
        } else {
            return scheme + "://" + address;
        }
    }

    private HttpServletRequest getRequest() {
        return ((ServletRequestAttributes)
                RequestContextHolder.currentRequestAttributes())
                .getRequest();
    }

    /**
     * Note: This method assumes that the startObject has already been
     * written
     * <p>
     * {
     * "_links": {
     * "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/": {
     * "href": "https://n5.example.com/api/arkivstruktur"
     * },
     * "https://rel.arkivverket.no/noark5/v5/api/sakarkiv/": {
     * "href": "https://n5.example.com/api/sakarkiv"
     * },
     * "https://rel.arkivverket.no/noark5/v5/api/admin/system/": {
     * "href": "https://n5.example.com/api/admin/system/",
     * }
     * }
     * }
     *
     * @param jgen
     * @param links
     * @throws IOException
     */
    public void printLinks(JsonGenerator jgen, List<Link> links) throws IOException {
        if (links != null && links.size() > 0) {
            jgen.writeObjectFieldStart(LINKS);
            for (Link link : links) {
                jgen.writeObjectFieldStart(link.getRel());
                jgen.writeStringField(HREF, link.getHref());
                if (link.getTemplated()) {
                    jgen.writeBooleanField(TEMPLATED,
                            link.getTemplated());
                }
                jgen.writeEndObject();
            }
            jgen.writeEndObject();
        }
    }
}
