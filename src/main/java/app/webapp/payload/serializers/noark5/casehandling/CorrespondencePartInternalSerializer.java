package app.webapp.payload.serializers.noark5.casehandling;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.casehandling.secondary.CorrespondencePartInternal;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.interfaces.ICorrespondencePartInternalPrint;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

/**
 * Serialize an outgoing CorrespondencePartInternal object as JSON.
 */
public class CorrespondencePartInternalSerializer
        extends CorrespondencePartSerializer
        implements ICorrespondencePartInternalPrint {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity,
                                     LinksNoarkObject correspondencePartLinks, JsonGenerator jgen) throws IOException {
        CorrespondencePartInternal correspondencePartInternal = (CorrespondencePartInternal) noarkEntity;
        jgen.writeStartObject();
        printCorrespondencePart(jgen, correspondencePartInternal);
        printCorrespondencePartInternal(jgen, correspondencePartInternal);
        printLinks(jgen, correspondencePartLinks.getLinks(correspondencePartInternal));
        jgen.writeEndObject();
    }
}
