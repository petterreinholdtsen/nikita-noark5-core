package app.webapp.payload.serializers.noark5;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.Class;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.interfaces.IClassifiedPrint;
import app.webapp.payload.serializers.noark5.interfaces.ICrossReferencePrint;
import app.webapp.payload.serializers.noark5.interfaces.IDisposalPrint;
import app.webapp.payload.serializers.noark5.interfaces.IScreeningPrint;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.CLASS_ID;

/**
 * Serialize an outgoing Class object as JSON.
 */
public class ClassSerializer
        extends NoarkGeneralEntitySerializer
        implements IClassifiedPrint, IDisposalPrint, IScreeningPrint, ICrossReferencePrint {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject classLinks,
                                     JsonGenerator jgen) throws IOException {
        Class klass = (Class) noarkEntity;
        jgen.writeStartObject();
        printSystemIdEntity(jgen, klass);
        printNullable(jgen, CLASS_ID, klass.getClassId());
        printTitleAndDescription(jgen, klass);
        printFinaliseEntity(jgen, klass);
        printCreateEntity(jgen, klass);
        printModifiedEntity(jgen, klass);
        // TODO Fix CrossReference
        // printCrossReference(jgen, klass);
        printDisposal(jgen, klass);
        printScreening(jgen, klass);
        printClassified(jgen, klass);
        printLinks(jgen, classLinks.getLinks(klass));
        jgen.writeEndObject();
    }
}
