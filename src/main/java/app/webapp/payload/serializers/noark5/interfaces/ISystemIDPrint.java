package app.webapp.payload.serializers.noark5.interfaces;

import app.domain.interfaces.entities.ISystemId;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.SYSTEM_ID;

public interface ISystemIDPrint {
    default void printSystemIdEntity(JsonGenerator jgen, ISystemId systemIdEntity)
            throws IOException {
        if (systemIdEntity != null && systemIdEntity.getSystemId() != null) {
            jgen.writeStringField(SYSTEM_ID, systemIdEntity.getSystemIdAsString());
        }
    }
}
