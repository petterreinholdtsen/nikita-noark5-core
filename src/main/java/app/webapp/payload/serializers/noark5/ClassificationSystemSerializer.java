package app.webapp.payload.serializers.noark5;

import app.domain.interfaces.entities.IClassificationSystemEntity;
import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.ClassificationSystem;
import app.webapp.payload.links.LinksNoarkObject;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.CLASSIFICATION_SYSTEM_TYPE;

/**
 * Serialize an outgoing ClassificationSystem object as JSON.
 */
public class ClassificationSystemSerializer
        extends NoarkGeneralEntitySerializer {

    @Override
    public void serializeNoarkEntity(INoarkEntity classificationSystemEntity,
                                     LinksNoarkObject classificationSystemLinks, JsonGenerator jgen)
            throws IOException {
        ClassificationSystem classificationSystem = (ClassificationSystem) classificationSystemEntity;
        jgen.writeStartObject();
        printClassificationSystemEntity(jgen, classificationSystem);
        printFinaliseEntity(jgen, classificationSystem);
        printCreateEntity(jgen, classificationSystem);
        printModifiedEntity(jgen, classificationSystem);
        printLinks(jgen, classificationSystemLinks.getLinks(classificationSystem));
        jgen.writeEndObject();
    }

    public void printClassificationSystemEntity(JsonGenerator jgen, IClassificationSystemEntity classificationSystem)
            throws IOException {
        printSystemIdEntity(jgen, classificationSystem);
        printTitleAndDescription(jgen, classificationSystem);
        printNullableMetadata(jgen, CLASSIFICATION_SYSTEM_TYPE, classificationSystem.getClassificationType());
    }
}
