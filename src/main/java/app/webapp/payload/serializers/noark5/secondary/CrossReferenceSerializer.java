package app.webapp.payload.serializers.noark5.secondary;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.secondary.CrossReference;
import app.webapp.payload.builder.noark5.secondary.CrossReferenceLinksBuilder;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.links.secondary.CrossReferenceLinks;
import app.webapp.payload.serializers.noark5.SystemIdEntitySerializer;
import app.webapp.payload.serializers.noark5.interfaces.ICrossReferencePrint;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

/**
 * Serialize an outgoing CrossReference object as JSON.
 */
@LinksPacker(using = CrossReferenceLinksBuilder.class)
@LinksObject(using = CrossReferenceLinks.class)
public class CrossReferenceSerializer
        extends SystemIdEntitySerializer
        implements ICrossReferencePrint {
    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject crossReferenceLinks,
                                     JsonGenerator jgen) throws IOException {
        CrossReference crossReference = (CrossReference) noarkEntity;
        jgen.writeStartObject();
        printCrossReference(jgen, crossReference);
        printLinks(jgen, crossReferenceLinks.getLinks(crossReference));
        jgen.writeEndObject();
    }
}
