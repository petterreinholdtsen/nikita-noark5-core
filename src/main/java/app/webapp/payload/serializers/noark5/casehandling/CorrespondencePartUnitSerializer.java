package app.webapp.payload.serializers.noark5.casehandling;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.casehandling.secondary.CorrespondencePartUnit;
import app.webapp.payload.links.LinksNoarkObject;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

/**
 * Serialize an outgoing CorrespondencePartUnit object as JSON.
 */
public class CorrespondencePartUnitSerializer
        extends CorrespondencePartSerializer {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject correspondencePartUnitLinks,
                                     JsonGenerator jgen) throws IOException {
        CorrespondencePartUnit correspondencePartUnit = (CorrespondencePartUnit) noarkEntity;
        jgen.writeStartObject();
        printCorrespondencePart(jgen, correspondencePartUnit);
        printCorrespondencePartUnit(jgen, correspondencePartUnit);
        printLinks(jgen, correspondencePartUnitLinks.getLinks(correspondencePartUnit));
        jgen.writeEndObject();
    }
}
