package app.webapp.payload.serializers.application;

import app.webapp.model.APIDetail;
import app.webapp.model.APIDetails;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

import static app.utils.constants.HATEOASConstants.HREF;
import static app.utils.constants.HATEOASConstants.LINKS;

public class APIDetailsSerializer extends StdSerializer<APIDetails> {

    public APIDetailsSerializer() {
        super(APIDetails.class);
    }

    @Override
    public void serialize(APIDetails apiDetails,
                          JsonGenerator jgen, SerializerProvider provider)
            throws IOException {
        jgen.writeStartObject();
        jgen.writeObjectFieldStart(LINKS);

        for (APIDetail apiDetail : apiDetails.getApiDetails()) {
            jgen.writeObjectFieldStart(apiDetail.getRel());
            jgen.writeStringField(HREF, apiDetail.getHref());
            if (apiDetail.getTemplated()) {
                jgen.writeBooleanField("templated",
                        apiDetail.getTemplated());
            }
            jgen.writeEndObject();
        }
        jgen.writeEndObject();
        jgen.writeEndObject();
    }
}
