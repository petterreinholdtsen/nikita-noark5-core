package app.webapp.payload.serializers.interfaces;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.payload.links.LinksNoarkObject;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;
/**
 * Base interface for serialising
 */
public interface ILinksSerializer {
    void serializeNoarkEntity(INoarkEntity entity, LinksNoarkObject linksObject, JsonGenerator jgen)
            throws IOException;
}
