package app.webapp.payload.links.admin;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.admin.DeleteLogSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.DELETE_LOG;

@JsonSerialize(using = DeleteLogSerializer.class)
public class DeleteLogLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public DeleteLogLinks(INoarkEntity entity) {
        super(entity);
    }

    public DeleteLogLinks(SearchResultsPage page) {
        super(page, DELETE_LOG);
    }
}
