package app.webapp.payload.links.admin;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.admin.CreateLogSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.CREATE_LOG;

@JsonSerialize(using = CreateLogSerializer.class)
public class CreateLogLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public CreateLogLinks(INoarkEntity entity) {
        super(entity);
    }

    public CreateLogLinks(SearchResultsPage page) {
        super(page, CREATE_LOG);
    }
}
