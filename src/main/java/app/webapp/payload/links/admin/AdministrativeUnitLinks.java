package app.webapp.payload.links.admin;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.admin.AdministrativeUnitSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.ADMINISTRATIVE_UNIT;

@JsonSerialize(using = AdministrativeUnitSerializer.class)
public class AdministrativeUnitLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public AdministrativeUnitLinks(INoarkEntity entity) {
        super(entity);
    }

    public AdministrativeUnitLinks(SearchResultsPage page) {
        super(page, ADMINISTRATIVE_UNIT);
    }
}
