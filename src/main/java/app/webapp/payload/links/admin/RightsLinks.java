package app.webapp.payload.links.admin;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.admin.AdministrativeUnitSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.RIGHT;


@JsonSerialize(using = AdministrativeUnitSerializer.class)
public class RightsLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public RightsLinks(INoarkEntity entity) {
        super(entity);
    }

    public RightsLinks(SearchResultsPage page) {
        super(page, RIGHT);
    }
}
