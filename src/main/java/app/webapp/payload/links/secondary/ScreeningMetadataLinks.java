package app.webapp.payload.links.secondary;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.secondary.ScreeningMetadataSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.SCREENING_METADATA;

@JsonSerialize(using = ScreeningMetadataSerializer.class)
public class ScreeningMetadataLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public ScreeningMetadataLinks(INoarkEntity entity) {
        super(entity);
    }

    public ScreeningMetadataLinks(SearchResultsPage page) {
        super(page, SCREENING_METADATA);
    }
}
