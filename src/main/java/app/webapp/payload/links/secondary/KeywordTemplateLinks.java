package app.webapp.payload.links.secondary;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.secondary.KeywordTemplateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(using = KeywordTemplateSerializer.class)
public class KeywordTemplateLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public KeywordTemplateLinks() {
    }

    public KeywordTemplateLinks(INoarkEntity entity) {
        super(entity);
    }
}
