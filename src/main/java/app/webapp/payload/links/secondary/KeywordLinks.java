package app.webapp.payload.links.secondary;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.secondary.KeywordSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.KEYWORD;

@JsonSerialize(using = KeywordSerializer.class)
public class KeywordLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public KeywordLinks() {
    }

    public KeywordLinks(INoarkEntity entity) {
        super(entity);
    }

    public KeywordLinks(SearchResultsPage page) {
        super(page, KEYWORD);
    }
}
