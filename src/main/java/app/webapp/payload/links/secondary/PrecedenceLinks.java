package app.webapp.payload.links.secondary;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.secondary.PrecedenceSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.PRECEDENCE;

@JsonSerialize(using = PrecedenceSerializer.class)
public class PrecedenceLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public PrecedenceLinks(INoarkEntity entity) {
        super(entity);
    }

    public PrecedenceLinks(SearchResultsPage page) {
        super(page, PRECEDENCE);
    }
}
