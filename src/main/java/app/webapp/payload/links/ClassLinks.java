package app.webapp.payload.links;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.serializers.noark5.ClassSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.CLASS;

@JsonSerialize(using = ClassSerializer.class)
public class ClassLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public ClassLinks(INoarkEntity entity) {
        super(entity);
    }

    public ClassLinks(SearchResultsPage page) {
        super(page, CLASS);
    }
}
