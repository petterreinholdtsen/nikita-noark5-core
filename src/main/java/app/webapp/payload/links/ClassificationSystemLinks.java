package app.webapp.payload.links;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.serializers.noark5.ClassificationSystemSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.CLASSIFICATION_SYSTEM;

@JsonSerialize(using = ClassificationSystemSerializer.class)
public class ClassificationSystemLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public ClassificationSystemLinks(INoarkEntity entity) {
        super(entity);
    }

    public ClassificationSystemLinks(SearchResultsPage page) {
        super(page, CLASSIFICATION_SYSTEM);
    }
}
