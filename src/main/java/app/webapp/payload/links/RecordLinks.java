package app.webapp.payload.links;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.serializers.noark5.RecordSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.RECORD;

@JsonSerialize(using = RecordSerializer.class)
public class RecordLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public RecordLinks(INoarkEntity entity) {
        super(entity);
    }

    public RecordLinks(SearchResultsPage page) {
        super(page, RECORD);
    }
}
