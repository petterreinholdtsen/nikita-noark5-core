package app.webapp.payload.links.nationalidentifier;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.nationalidentifier.UnitSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.Constants.UNIT;

@JsonSerialize(using = UnitSerializer.class)
public class UnitLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public UnitLinks(INoarkEntity entity) {
        super(entity);
    }

    public UnitLinks(SearchResultsPage page) {
        super(page, UNIT);
    }
}
