package app.webapp.payload.links.nationalidentifier;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.nationalidentifier.PositionSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.POSITION;

@JsonSerialize(using = PositionSerializer.class)
public class PositionLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public PositionLinks(INoarkEntity entity) {
        super(entity);
    }

    public PositionLinks(SearchResultsPage page) {
        super(page, POSITION);
    }
}
