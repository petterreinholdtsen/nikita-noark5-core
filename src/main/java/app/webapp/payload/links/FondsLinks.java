package app.webapp.payload.links;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.serializers.noark5.FondsSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.FONDS;

@JsonSerialize(using = FondsSerializer.class)
public class FondsLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public FondsLinks(INoarkEntity entity) {
        super(entity);
    }

    public FondsLinks(SearchResultsPage page) {
        super(page, FONDS);
    }
}
