package app.webapp.payload.links.md_other;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.metadata.BSMMetadataSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.Constants.REL_METADATA_BSM;

@JsonSerialize(using = BSMMetadataSerializer.class)
public class BSMMetadataLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public BSMMetadataLinks(INoarkEntity entity) {
        super(entity);
    }

    public BSMMetadataLinks(SearchResultsPage page) {
        super(page, REL_METADATA_BSM);
    }
}
