package app.webapp.payload.links;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.serializers.noark5.EventLogSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.EVENT_LOG;

@JsonSerialize(using = EventLogSerializer.class)
public class EventLogLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public EventLogLinks(INoarkEntity entity) {
        super(entity);
    }

    public EventLogLinks(SearchResultsPage page) {
        super(page, EVENT_LOG);
    }
}
