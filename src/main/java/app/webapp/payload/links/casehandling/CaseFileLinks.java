package app.webapp.payload.links.casehandling;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.casehandling.CaseFileSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.CASE_FILE;

@JsonSerialize(using = CaseFileSerializer.class)
public class CaseFileLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public CaseFileLinks(INoarkEntity entity) {
        super(entity);
    }

    public CaseFileLinks(SearchResultsPage page) {
        super(page, CASE_FILE);
    }
}
