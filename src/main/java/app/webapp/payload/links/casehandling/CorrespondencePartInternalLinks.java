package app.webapp.payload.links.casehandling;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.casehandling.CorrespondencePartInternalSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.CORRESPONDENCE_PART_INTERNAL;

@JsonSerialize(using = CorrespondencePartInternalSerializer.class)
public class CorrespondencePartInternalLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public CorrespondencePartInternalLinks(INoarkEntity entity) {
        super(entity);
    }

    public CorrespondencePartInternalLinks(SearchResultsPage page) {
        super(page, CORRESPONDENCE_PART_INTERNAL);
    }
}
