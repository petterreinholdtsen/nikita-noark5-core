package app.webapp.payload.links.casehandling;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.casehandling.RecordNoteExpansionSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(using = RecordNoteExpansionSerializer.class)
public class RecordNoteExpansionLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {
    public RecordNoteExpansionLinks(INoarkEntity entity) {
        super(entity);
    }
}
