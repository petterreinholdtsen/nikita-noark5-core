package app.webapp.payload.links.casehandling;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.casehandling.CorrespondencePartPersonSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.CORRESPONDENCE_PART_PERSON;

@JsonSerialize(using = CorrespondencePartPersonSerializer.class)
public class CorrespondencePartPersonLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public CorrespondencePartPersonLinks(INoarkEntity entity) {
        super(entity);
    }

    public CorrespondencePartPersonLinks(SearchResultsPage page) {
        super(page, CORRESPONDENCE_PART_PERSON);
    }
}
