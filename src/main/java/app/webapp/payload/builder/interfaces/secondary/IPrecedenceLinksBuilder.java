package app.webapp.payload.builder.interfaces.secondary;

import app.domain.interfaces.entities.secondary.IPrecedenceEntity;
import app.webapp.payload.builder.interfaces.ILinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;

/**
 * Created by tsodring on 2/6/17.
 * <p>
 * Describe Hateoas links handler
 */
public interface IPrecedenceLinksBuilder extends ILinksBuilder {

    void addPrecedenceStatus(IPrecedenceEntity entity, ILinksNoarkObject linksNoarkObject);

    void addCaseFile(IPrecedenceEntity entity, ILinksNoarkObject linksNoarkObject);

    void addRegistryEntry(IPrecedenceEntity entity, ILinksNoarkObject linksNoarkObject);

}
