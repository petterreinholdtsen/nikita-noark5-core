package app.webapp.payload.builder.interfaces;

import app.domain.interfaces.entities.ISystemId;
import app.webapp.payload.links.ILinksNoarkObject;

/**
 * Created by tsodring on 2/6/17.
 * <p>
 * Describe Hateoas links handler for ClassificationSystem
 */
public interface IClassificationSystemLinksBuilder
        extends ILinksBuilder {

    void addSeries(ISystemId entity, ILinksNoarkObject linksNoarkObject);

    void addClass(ISystemId entity, ILinksNoarkObject linksNoarkObject);

    void addNewClass(ISystemId entity, ILinksNoarkObject linksNoarkObject);

    void addSecondaryClassificationSystem(
            ISystemId entity, ILinksNoarkObject linksNoarkObject);

    void addNewSecondaryClassificationSystem(
            ISystemId entity, ILinksNoarkObject linksNoarkObject);

    void addClassificationType(
            ISystemId entity, ILinksNoarkObject linksNoarkObject);
}
