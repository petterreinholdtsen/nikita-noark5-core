package app.webapp.payload.builder.interfaces;

import app.webapp.payload.links.ILinksNoarkObject;

/**
 * Created by tsodring on 2/6/17.
 * <p>
 * Base class for Hateoas links handler
 */
public interface ILinksBuilder {

    void addLinks(ILinksNoarkObject linksNoarkObject);

    void addLinksOnCreate(ILinksNoarkObject linksNoarkObject);

    void addLinksOnTemplate(ILinksNoarkObject linksNoarkObject);

    void addLinksOnRead(ILinksNoarkObject linksNoarkObject);

    void addLinksOnUpdate(ILinksNoarkObject linksNoarkObject);

    void addLinksOnDelete(ILinksNoarkObject linksNoarkObject);

    // The following are required to give @Value during reflection
    void setAddress(String address);

    void setContextPath(String contextPath);
}
