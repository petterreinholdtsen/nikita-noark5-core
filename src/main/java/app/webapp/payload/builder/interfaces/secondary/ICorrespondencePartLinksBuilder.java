package app.webapp.payload.builder.interfaces.secondary;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.payload.builder.interfaces.ILinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;

/**
 * Created by tsodring on 2/6/17.
 * <p>
 * Describe Hateoas links handler
 */
public interface ICorrespondencePartLinksBuilder
        extends ILinksBuilder {

    void addRecordEntity(INoarkEntity entity, ILinksNoarkObject linksNoarkObject);

    void addCorrespondencePartType(INoarkEntity entity,
                                   ILinksNoarkObject linksNoarkObject);
}
