package app.webapp.payload.builder.interfaces;

import app.domain.interfaces.entities.ISystemId;
import app.webapp.payload.links.ILinksNoarkObject;

/**
 * Created by tsodring on 2/6/17.
 * <p>
 * Describe Hateoas links handler
 */
public interface IRegistryEntryLinksBuilder
        extends IRecordLinksBuilder {

    void addPrecedence(ISystemId entity,
                       ILinksNoarkObject linksNoarkObject);

    void addNewPrecedence(ISystemId entity,
                          ILinksNoarkObject linksNoarkObject);

    void addSignOff(ISystemId entity,
                    ILinksNoarkObject linksNoarkObject);

    void addNewSignOff(ISystemId entity,
                       ILinksNoarkObject linksNoarkObject);

    void addDocumentFlow(ISystemId entity,
                         ILinksNoarkObject linksNoarkObject);

    void addNewDocumentFlow(ISystemId entity,
                            ILinksNoarkObject linksNoarkObject);

    // Metadata entries
    void addRegistryEntryStatus(ISystemId entity,
                                ILinksNoarkObject linksNoarkObject);

    void addRegistryEntryType(ISystemId entity,
                              ILinksNoarkObject linksNoarkObject);
}
