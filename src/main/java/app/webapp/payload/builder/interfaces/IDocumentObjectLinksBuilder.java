package app.webapp.payload.builder.interfaces;

import app.domain.interfaces.entities.ISystemId;
import app.webapp.payload.links.ILinksNoarkObject;

/**
 * Created by tsodring on 2/6/17.
 * <p>
 * Describe Hateoas links handler
 */
public interface IDocumentObjectLinksBuilder extends ILinksBuilder {

    void addDocumentDescription(ISystemId entity, ILinksNoarkObject linksNoarkObject);

    void addConversion(ISystemId entity, ILinksNoarkObject linksNoarkObject);

    void addNewConversion(ISystemId entity, ILinksNoarkObject linksNoarkObject);

    void addReferenceDocumentFile(ISystemId entity, ILinksNoarkObject linksNoarkObject);

    void addVariantFormat(ISystemId entity, ILinksNoarkObject linksNoarkObject);

    void addFormat(ISystemId entity, ILinksNoarkObject linksNoarkObject);
}

