package app.webapp.payload.builder.interfaces;

import app.domain.interfaces.entities.ISystemId;
import app.webapp.payload.links.ILinksNoarkObject;

/**
 * Describe Hateoas links handler
 */
public interface IRecordLinksBuilder
        extends ILinksBuilder {

    void addReferenceSeries(ISystemId entity,
                            ILinksNoarkObject linksNoarkObject);

    void addReferenceFile(ISystemId entity,
                          ILinksNoarkObject linksNoarkObject);

    void addReferenceClass(ISystemId entity,
                           ILinksNoarkObject linksNoarkObject);

    void addNewDocumentDescription(ISystemId entity,
                                   ILinksNoarkObject linksNoarkObject);

    void addDocumentDescription(ISystemId entity,
                                ILinksNoarkObject linksNoarkObject);

    void addNewReferenceSeries(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject);

    void addStorageLocation(ISystemId entity,
                            ILinksNoarkObject linksNoarkObject);

    void addNewStorageLocation(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject);

    void addComment(ISystemId entity,
                    ILinksNoarkObject linksNoarkObject);

    void addNewComment(ISystemId entity,
                       ILinksNoarkObject linksNoarkObject);

    void addKeyword(ISystemId entity,
                    ILinksNoarkObject linksNoarkObject);

    void addNewKeyword(ISystemId entity,
                       ILinksNoarkObject linksNoarkObject);

    void addCrossReference(ISystemId entity,
                           ILinksNoarkObject linksNoarkObject);

    void addNewCrossReference(ISystemId entity,
                              ILinksNoarkObject linksNoarkObject);

    void addPart(ISystemId entity,
                 ILinksNoarkObject linksNoarkObject);

    void addNewPartPerson(ISystemId entity,
                          ILinksNoarkObject linksNoarkObject);

    void addNewPartUnit(ISystemId entity,
                        ILinksNoarkObject linksNoarkObject);

    void addCorrespondencePart(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject);

    void addNewCorrespondencePartPerson(ISystemId entity,
                                        ILinksNoarkObject linksNoarkObject);

    void addNewCorrespondencePartUnit(ISystemId entity,
                                      ILinksNoarkObject linksNoarkObject);

    void addNewCorrespondencePartInternal(ISystemId entity,
                                          ILinksNoarkObject linksNoarkObject);

    void addAuthor(ISystemId entity,
                   ILinksNoarkObject linksNoarkObject);

    void addNewAuthor(ISystemId entity,
                      ILinksNoarkObject linksNoarkObject);

    void addClassifiedCodeMetadata(ISystemId entity,
                                   ILinksNoarkObject linksNoarkObject);

    void addAccessRestriction(ISystemId entity,
                              ILinksNoarkObject linksNoarkObject);

    void addScreeningDocument(ISystemId entity,
                              ILinksNoarkObject linksNoarkObject);

    void addScreeningMetadata(ISystemId entity,
                              ILinksNoarkObject linksNoarkObject);

    void addScreeningMetadataLocal(ISystemId entity,
                                   ILinksNoarkObject linksNoarkObject);

    void addNewScreeningMetadataLocal(ISystemId entity,
                                      ILinksNoarkObject linksNoarkObject);

    // Add national identifiers

    void addNewBuilding(ISystemId entity,
                        ILinksNoarkObject linksNoarkObject);

    void addNewCadastralUnit(ISystemId entity,
                             ILinksNoarkObject linksNoarkObject);

    void addNewDNumber(ISystemId entity,
                       ILinksNoarkObject linksNoarkObject);

    void addNewPlan(ISystemId entity,
                    ILinksNoarkObject linksNoarkObject);

    void addNewPosition(ISystemId entity,
                        ILinksNoarkObject linksNoarkObject);

    void addNewSocialSecurityNumber(ISystemId entity,
                                    ILinksNoarkObject linksNoarkObject);

    void addNewUnit(ISystemId entity,
                    ILinksNoarkObject linksNoarkObject);

    void addNationalIdentifier(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject);

    void addExpanLinksRegistryEntry(ISystemId entity,
                                    ILinksNoarkObject linksNoarkObject);
}
