package app.webapp.payload.builder.interfaces.metadata;

import app.webapp.payload.builder.interfaces.ILinksBuilder;

public interface IBSMMetadataLinksBuilder
        extends ILinksBuilder {
}
