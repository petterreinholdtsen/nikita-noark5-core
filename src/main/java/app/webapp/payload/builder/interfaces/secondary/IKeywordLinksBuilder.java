package app.webapp.payload.builder.interfaces.secondary;

import app.domain.interfaces.entities.secondary.IKeywordEntity;
import app.webapp.payload.builder.interfaces.ILinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;

public interface IKeywordLinksBuilder
        extends ILinksBuilder {

    void addRecordEntity(IKeywordEntity entity,
                         ILinksNoarkObject linksNoarkObject);

    void addFile(IKeywordEntity entity,
                 ILinksNoarkObject linksNoarkObject);

    void addClass(IKeywordEntity entity,
                  ILinksNoarkObject linksNoarkObject);
}
