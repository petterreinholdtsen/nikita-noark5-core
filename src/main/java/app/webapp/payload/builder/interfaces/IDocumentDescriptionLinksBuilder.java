package app.webapp.payload.builder.interfaces;

import app.domain.interfaces.entities.ISystemId;
import app.webapp.payload.links.ILinksNoarkObject;

/**
 * Created by tsodring on 2/6/17.
 * <p>
 * Describe Hateoas links handler
 */
public interface IDocumentDescriptionLinksBuilder
        extends ILinksBuilder {

    void addRecordEntity(ISystemId entity,
                         ILinksNoarkObject linksNoarkObject);

    void addDocumentObject(ISystemId entity,
                           ILinksNoarkObject linksNoarkObject);

    void addNewDocumentObject(ISystemId entity,
                              ILinksNoarkObject linksNoarkObject);

    void addStorageLocation(ISystemId entity,
                            ILinksNoarkObject linksNoarkObject);

    void addNewStorageLocation(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject);

    void addComment(ISystemId entity,
                    ILinksNoarkObject linksNoarkObject);

    void addNewComment(ISystemId entity,
                       ILinksNoarkObject linksNoarkObject);

    void addClassifiedCodeMetadata(ISystemId entity,
                                   ILinksNoarkObject linksNoarkObject);

    void addDeletionType(ISystemId entity,
                         ILinksNoarkObject linksNoarkObject);

    void addDocumentType(ISystemId entity,
                         ILinksNoarkObject linksNoarkObject);

    void addDocumentStatus(ISystemId entity,
                           ILinksNoarkObject linksNoarkObject);

    void addPart(ISystemId entity,
                 ILinksNoarkObject linksNoarkObject);

    void addNewPartPerson(ISystemId entity,
                          ILinksNoarkObject linksNoarkObject);

    void addNewPartUnit(ISystemId entity,
                        ILinksNoarkObject linksNoarkObject);

    void addAuthor(ISystemId entity,
                   ILinksNoarkObject linksNoarkObject);

    void addNewAuthor(ISystemId entity,
                      ILinksNoarkObject linksNoarkObject);

    void addAccessRestriction(ISystemId entity,
                              ILinksNoarkObject linksNoarkObject);

    void addScreeningDocument(ISystemId entity,
                              ILinksNoarkObject linksNoarkObject);

    void addScreeningMetadata(ISystemId entity,
                              ILinksNoarkObject linksNoarkObject);

    void addScreeningMetadataLocal(ISystemId entity,
                                   ILinksNoarkObject linksNoarkObject);

    void addNewScreeningMetadataLocal(ISystemId entity,
                                      ILinksNoarkObject linksNoarkObject);

    void addAssociatedWithRecordAs(ISystemId entity,
                                   ILinksNoarkObject linksNoarkObject);
}

