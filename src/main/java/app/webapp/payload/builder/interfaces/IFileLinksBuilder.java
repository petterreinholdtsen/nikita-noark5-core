package app.webapp.payload.builder.interfaces;

import app.domain.interfaces.entities.ISystemId;
import app.webapp.payload.links.ILinksNoarkObject;

/**
 * Describe Hateoas links handler
 */
public interface IFileLinksBuilder
        extends ILinksBuilder {

    void addSeries(ISystemId entity,
                   ILinksNoarkObject linksNoarkObject);

    void addEndFile(ISystemId entity,
                    ILinksNoarkObject linksNoarkObject);

    void addExpanLinksCaseFile(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject);

    void addExpanLinksMeetingFile(ISystemId entity,
                                  ILinksNoarkObject linksNoarkObject);

    void addRecordEntity(ISystemId entity,
                         ILinksNoarkObject linksNoarkObject);

    void addNewRecord(ISystemId entity,
                      ILinksNoarkObject linksNoarkObject);

    void addComment(ISystemId entity,
                    ILinksNoarkObject linksNoarkObject);

    void addNewComment(ISystemId entity,
                       ILinksNoarkObject linksNoarkObject);

    void addKeyword(ISystemId entity,
                    ILinksNoarkObject linksNoarkObject);

    void addNewKeyword(ISystemId entity,
                       ILinksNoarkObject linksNoarkObject);

    void addStorageLocation(ISystemId entity,
                            ILinksNoarkObject linksNoarkObject);

    void addNewStorageLocation(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject);

    void addParentFile(ISystemId entity,
                       ILinksNoarkObject linksNoarkObject);

    void addSubFile(ISystemId entity,
                    ILinksNoarkObject linksNoarkObject);

    void addNewSubFile(ISystemId entity,
                       ILinksNoarkObject linksNoarkObject);

    void addCrossReference(ISystemId entity,
                           ILinksNoarkObject linksNoarkObject);

    void addNewCrossReference(ISystemId entity,
                              ILinksNoarkObject linksNoarkObject);

    void addClass(ISystemId entity,
                  ILinksNoarkObject linksNoarkObject);

    void addNewClass(ISystemId entity,
                     ILinksNoarkObject linksNoarkObject);

    void addReferenceSeries(ISystemId entity,
                            ILinksNoarkObject linksNoarkObject);

    void addNewReferenceSeries(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject);

    void addReferenceSecondaryClassification(
            ISystemId entity,
            ILinksNoarkObject linksNoarkObject);

    void addNewReferenceSecondaryClassification(
            ISystemId entity,
            ILinksNoarkObject linksNoarkObject);

    void addPart(ISystemId entity,
                 ILinksNoarkObject linksNoarkObject);

    void addNewPartPerson(ISystemId entity,
                          ILinksNoarkObject linksNoarkObject);

    void addNewPartUnit(ISystemId entity,
                        ILinksNoarkObject linksNoarkObject);

    void addClassifiedCodeMetadata(ISystemId entity,
                                   ILinksNoarkObject linksNoarkObject);

    void addAccessRestriction(ISystemId entity,
                              ILinksNoarkObject linksNoarkObject);

    void addScreeningDocument(ISystemId entity,
                              ILinksNoarkObject linksNoarkObject);

    void addScreeningMetadata(ISystemId entity,
                              ILinksNoarkObject linksNoarkObject);

    void addScreeningMetadataLocal(ISystemId entity,
                                   ILinksNoarkObject linksNoarkObject);

    void addNewScreeningMetadataLocal(ISystemId entity,
                                      ILinksNoarkObject linksNoarkObject);

    // Add national identifiers

    void addNewBuilding(ISystemId entity,
                        ILinksNoarkObject linksNoarkObject);

    void addNewCadastralUnit(ISystemId entity,
                             ILinksNoarkObject linksNoarkObject);

    void addNewDNumber(ISystemId entity,
                       ILinksNoarkObject linksNoarkObject);

    void addNewPlan(ISystemId entity,
                    ILinksNoarkObject linksNoarkObject);

    void addNewPosition(ISystemId entity,
                        ILinksNoarkObject linksNoarkObject);

    void addNewSocialSecurityNumber(ISystemId entity,
                                    ILinksNoarkObject linksNoarkObject);

    void addNewUnit(ISystemId entity,
                    ILinksNoarkObject linksNoarkObject);

    void addNationalIdentifier(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject);
}
