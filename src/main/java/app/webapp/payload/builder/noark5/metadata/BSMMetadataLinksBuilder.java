package app.webapp.payload.builder.noark5.metadata;

import app.domain.interfaces.entities.ISystemId;
import app.webapp.payload.builder.interfaces.metadata.IBSMMetadataLinksBuilder;
import app.webapp.payload.builder.noark5.SystemIdLinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.Link;
import org.springframework.stereotype.Component;

import static app.utils.constants.Constants.HREF_BASE_METADATA;
import static app.utils.constants.Constants.SLASH;
import static app.utils.constants.N5ResourceMappings.BSM_DEF;

@Component()
public class BSMMetadataLinksBuilder
        extends SystemIdLinksBuilder
        implements IBSMMetadataLinksBuilder {

    @Override
    public void addSelfLink(ISystemId entity,
                            ILinksNoarkObject linksNoarkObject) {
        String selfhref = getOutgoingAddress() +
                HREF_BASE_METADATA + SLASH + BSM_DEF + SLASH + entity.getSystemId();
        linksNoarkObject.addLink(entity,
                new Link(selfhref, getRelSelfLink()));
        linksNoarkObject.addLink(entity,
                new Link(selfhref, entity.getBaseRel()));
    }
}
