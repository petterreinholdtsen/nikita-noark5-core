package app.webapp.payload.builder.noark5.admin;

import app.domain.interfaces.entities.ISystemId;
import app.domain.noark5.EventLog;
import app.webapp.payload.builder.interfaces.admin.IReadLogLinksBuilder;
import app.webapp.payload.builder.noark5.EventLogLinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.Link;
import org.springframework.stereotype.Component;

import static app.utils.constants.Constants.HREF_BASE_LOGGING;
import static app.utils.constants.Constants.SLASH;
import static app.utils.constants.N5ResourceMappings.READ_LOG;

/*
 * Used to add ReadLogLinks links with EventLog specific information
 */
@Component("readLogLinksBuilder")
public class ReadLogLinksBuilder
        extends EventLogLinksBuilder
        implements IReadLogLinksBuilder {

    public ReadLogLinksBuilder() {
    }

    @Override
    public void addSelfLink(ISystemId entity,
                            ILinksNoarkObject linksNoarkObject) {
        String selfHref = getOutgoingAddress() +
                HREF_BASE_LOGGING + SLASH + READ_LOG + SLASH + entity.getSystemId();
        linksNoarkObject.addLink(entity,
                new Link(selfHref, getRelSelfLink()));
        linksNoarkObject.addLink(entity,
                new Link(selfHref, entity.getBaseRel()));
    }

    @Override
    public void addEntityLinks(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject) {
        EventLog eventLog = (EventLog) entity;
        addReferenceArchiveUnitLink(eventLog, linksNoarkObject);
    }

    @Override
    public void addEntityLinksOnTemplate
            (ISystemId entity, ILinksNoarkObject linksNoarkObject) {
    }
}
