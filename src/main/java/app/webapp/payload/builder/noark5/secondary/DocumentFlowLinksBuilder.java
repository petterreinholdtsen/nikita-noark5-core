package app.webapp.payload.builder.noark5.secondary;

import app.domain.interfaces.entities.ISystemId;
import app.domain.interfaces.entities.secondary.IDocumentFlowEntity;
import app.domain.noark5.secondary.DocumentFlow;
import app.webapp.payload.builder.interfaces.secondary.IDocumentFlowLinksBuilder;
import app.webapp.payload.builder.noark5.SystemIdLinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.Link;
import org.springframework.stereotype.Component;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.FLOW_STATUS;

@Component("documentFlowLinksBuilder")
public class DocumentFlowLinksBuilder
        extends SystemIdLinksBuilder
        implements IDocumentFlowLinksBuilder {

    public DocumentFlowLinksBuilder() {
    }

    @Override
    public void addSelfLink(ISystemId entity,
                            ILinksNoarkObject linksNoarkObject) {
        String selfhref = getOutgoingAddress() +
                HREF_BASE_DOCUMENT_FLOW + SLASH + entity.getSystemIdAsString();
        linksNoarkObject.addLink(entity,
                new Link(selfhref, getRelSelfLink()));
        linksNoarkObject.addLink(entity,
                new Link(selfhref, entity.getBaseRel()));
    }

    @Override
    public void addEntityLinks(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject) {
        DocumentFlow documentFlow = (DocumentFlow) entity;
        addRecordEntityNote(documentFlow, linksNoarkObject);
        addRegistryEntry(documentFlow, linksNoarkObject);
        addFlowStatus(documentFlow, linksNoarkObject);
    }

    @Override
    public void addEntityLinksOnTemplate
            (ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        DocumentFlow documentFlow = (DocumentFlow) entity;
        addFlowStatus(documentFlow, linksNoarkObject);
    }

    @Override
    public void addRecordEntityNote(IDocumentFlowEntity documentFlow,
                                    ILinksNoarkObject linksNoarkObject) {
        if (null != documentFlow.getReferenceRecordNote()) {
            linksNoarkObject.addLink(documentFlow,
                    new Link(getOutgoingAddress() + HREF_BASE_RECORD_NOTE + SLASH +
                            documentFlow.getReferenceRecordNote().getSystemIdAsString(),
                            REL_CASE_HANDLING_RECORD_NOTE));
        }
    }

    @Override
    public void addRegistryEntry(IDocumentFlowEntity documentFlow,
                                 ILinksNoarkObject linksNoarkObject) {
        if (null != documentFlow.getReferenceRegistryEntry()) {
            linksNoarkObject.addLink(documentFlow,
                    new Link(getOutgoingAddress() +
                            HREF_BASE_REGISTRY_ENTRY + SLASH +
                            documentFlow.getReferenceRegistryEntry().getSystemIdAsString(),
                            REL_CASE_HANDLING_REGISTRY_ENTRY));
        }
    }

    @Override
    public void addFlowStatus(IDocumentFlowEntity documentFlow,
                              ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(documentFlow,
                new Link(getOutgoingAddress() + HREF_BASE_METADATA + SLASH + FLOW_STATUS,
                        REL_METADATA_FLOW_STATUS, true));
    }
}
