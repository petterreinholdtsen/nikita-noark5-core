package app.webapp.payload.builder.noark5;

import app.domain.interfaces.entities.ISystemId;
import app.domain.noark5.DocumentDescription;
import app.domain.noark5.DocumentObject;
import app.webapp.payload.builder.interfaces.IDocumentDescriptionLinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.Link;
import org.springframework.stereotype.Component;

import java.util.List;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.*;

/**
 * Used to add DocumentDescriptionLinks links with DocumentDescription
 * specific information
 */
@Component("documentDescriptionLinksBuilder")
public class DocumentDescriptionLinksBuilder
        extends SystemIdLinksBuilder
        implements IDocumentDescriptionLinksBuilder {

    public DocumentDescriptionLinksBuilder() {
    }

    @Override
    public void addEntityLinks(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject) {

        // links for primary entities
        addRecordEntity(entity, linksNoarkObject);
        addDocumentObject(entity, linksNoarkObject);
        addNewDocumentObject(entity, linksNoarkObject);
        // links for secondary entities M:1
        addPart(entity, linksNoarkObject);
        addNewPartPerson(entity, linksNoarkObject);
        addNewPartUnit(entity, linksNoarkObject);
        // links for secondary entities 1:M
        addComment(entity, linksNoarkObject);
        addNewComment(entity, linksNoarkObject);
        addAuthor(entity, linksNoarkObject);
        addNewAuthor(entity, linksNoarkObject);
        // links for metadata entities
        addClassifiedCodeMetadata(entity, linksNoarkObject);
        addDeletionType(entity, linksNoarkObject);
        addDocumentMedium(entity, linksNoarkObject);
        addDocumentType(entity, linksNoarkObject);
        addDocumentStatus(entity, linksNoarkObject);
        addAccessRestriction(entity, linksNoarkObject);
        addScreeningDocument(entity, linksNoarkObject);
        addScreeningMetadata(entity, linksNoarkObject);
        addScreeningMetadataLocal(entity, linksNoarkObject);
        addNewScreeningMetadataLocal(entity, linksNoarkObject);
    }

    @Override
    public void addEntityLinksOnTemplate(ISystemId entity,
                                         ILinksNoarkObject linksNoarkObject) {
        addClassifiedCodeMetadata(entity, linksNoarkObject);
        addDeletionType(entity, linksNoarkObject);
        addDocumentMedium(entity, linksNoarkObject);
        addDocumentType(entity, linksNoarkObject);
        addDocumentStatus(entity, linksNoarkObject);
        addAccessRestriction(entity, linksNoarkObject);
        addScreeningDocument(entity, linksNoarkObject);
        addScreeningMetadata(entity, linksNoarkObject);
        addAssociatedWithRecordAs(entity, linksNoarkObject);
    }

    @Override
    public void addAssociatedWithRecordAs(ISystemId entity,
                                          ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_METADATA + SLASH + ASSOCIATED_WITH_RECORD_AS,
                REL_METADATA_ASSOCIATED_WITH_RECORD_AS, false));
    }

    /**
     * Create a REL/HREF pair for the parent Record associated
     * with the given DocumentDescription
     * <p>
     * "../api/arkivstruktur/dokumentbeskrivelse/1234/registrering"
     * "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/registrering/"
     *
     * @param entity           documentDescription
     * @param linksNoarkObject linksDocumentObject
     */
    @Override
    public void addRecordEntity(ISystemId entity,
                                ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_DOCUMENT_DESCRIPTION + SLASH + entity.getSystemIdAsString() + SLASH +
                RECORD, REL_FONDS_STRUCTURE_RECORD, false));
    }

    @Override
    public void addComment(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_DOCUMENT_DESCRIPTION + SLASH + entity.getSystemIdAsString() + SLASH + COMMENT,
                REL_FONDS_STRUCTURE_COMMENT, false));
    }

    @Override
    public void addNewComment(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_DOCUMENT_DESCRIPTION + SLASH + entity.getSystemIdAsString() + SLASH + NEW_COMMENT,
                REL_FONDS_STRUCTURE_NEW_COMMENT, false));
    }

    @Override
    public void addStorageLocation(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_DOCUMENT_DESCRIPTION + SLASH + entity.getSystemIdAsString() + SLASH + STORAGE_LOCATION,
                REL_FONDS_STRUCTURE_STORAGE_LOCATION, false));
    }

    @Override
    public void addNewStorageLocation(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_DOCUMENT_DESCRIPTION + SLASH + entity.getSystemIdAsString() + SLASH + NEW_STORAGE_LOCATION,
                REL_FONDS_STRUCTURE_NEW_STORAGE_LOCATION, false));
    }

    /**
     * add rel/href links for document object iff there is at least one document object
     * where the originalfilename is not null. No value assigned to originalfilename
     * is used to imply that a documentobject rel/href link should not be in the returned
     * _links list
     *
     * @param entity           the DocumentDescription entity
     * @param linksNoarkObject the DocumentDescription entity with all links
     */
    @Override
    public void addDocumentObject(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        List<DocumentObject> documentObjects = ((DocumentDescription) entity).getReferenceDocumentObject();
	if (! documentObjects.isEmpty()) {
	    linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                    HREF_BASE_DOCUMENT_DESCRIPTION + SLASH + entity.getSystemIdAsString() + SLASH + DOCUMENT_OBJECT,
                    REL_FONDS_STRUCTURE_DOCUMENT_OBJECT, false));
	}
    }

    @Override
    public void addNewDocumentObject(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_DOCUMENT_DESCRIPTION + SLASH + entity.getSystemIdAsString() + SLASH + NEW_DOCUMENT_OBJECT,
                REL_FONDS_STRUCTURE_NEW_DOCUMENT_OBJECT, false));
    }

    @Override
    public void addClassifiedCodeMetadata(ISystemId entity,
                                          ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_METADATA + SLASH + CLASSIFIED_CODE,
                REL_METADATA_CLASSIFIED_CODE));
    }

    @Override
    public void addDocumentType(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_METADATA + SLASH + DOCUMENT_STATUS,
                REL_METADATA_DOCUMENT_STATUS, false));
    }

    @Override
    public void addDeletionType(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_METADATA + SLASH + DELETION_TYPE,
                REL_METADATA_DELETION_TYPE, false));
    }

    @Override
    public void addDocumentStatus(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_METADATA + SLASH + DOCUMENT_TYPE,
                REL_METADATA_DOCUMENT_TYPE, false));
    }

    /**
     * Create a REL/HREF pair for the list of Part objects associated with the
     * given DocumentDescription.
     * <p>
     * "../api/arkivstruktur/dokumentbeskrivelse/1234/part"
     * "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/part/"
     *
     * @param entity           documentDescription
     * @param linksNoarkObject linksDocumentDescription
     */
    @Override
    public void addPart(
            ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_DOCUMENT_DESCRIPTION + SLASH + entity.getSystemIdAsString() + SLASH + PART,
                REL_FONDS_STRUCTURE_PART, true));
    }

    @Override
    public void addNewPartPerson(
            ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_DOCUMENT_DESCRIPTION + SLASH + entity.getSystemIdAsString() + SLASH + NEW_PART_PERSON,
                REL_FONDS_STRUCTURE_NEW_PART_PERSON));
    }

    @Override
    public void addNewPartUnit(
            ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_DOCUMENT_DESCRIPTION + SLASH + entity.getSystemIdAsString() + SLASH + NEW_PART_UNIT,
                REL_FONDS_STRUCTURE_NEW_PART_UNIT));
    }

    /**
     * Create a REL/HREF pair to get the list of Author objects associated with
     * the given DocumentDescription.
     * <p>
     * "../api/arkivstruktur/dokumentbeskrivelse/1234/forfatter"
     * "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/forfatter/"
     *
     * @param entity           documentDescription
     * @param linksNoarkObject linksDocumentDescription
     */
    @Override
    public void addAuthor(ISystemId entity,
                          ILinksNoarkObject linksNoarkObject) {
        if (((DocumentDescription) entity).getReferenceAuthor().size() > 0) {
            linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                    HREF_BASE_DOCUMENT_DESCRIPTION + SLASH + entity.getSystemIdAsString() +
                    SLASH + AUTHOR, REL_FONDS_STRUCTURE_AUTHOR, true));
        }
    }

    /**
     * Create a REL/HREF pair to create a new Author object associated with
     * the given DocumentDescription.
     * <p>
     * "../api/arkivstruktur/dokumentbeskrivelse/1234/ny-forfatter"
     * "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/ny-forfatter/"
     *
     * @param entity           documentDescription
     * @param linksNoarkObject linksDocumentDescription
     */
    @Override
    public void addNewAuthor(ISystemId entity,
                             ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_DOCUMENT_DESCRIPTION + SLASH + entity.getSystemIdAsString() +
                SLASH + NEW_AUTHOR, REL_FONDS_STRUCTURE_NEW_AUTHOR));
    }

    @Override
    public void addAccessRestriction(ISystemId entity,
                                     ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_METADATA + SLASH + ACCESS_RESTRICTION,
                REL_METADATA_ACCESS_RESTRICTION, false));
    }

    @Override
    public void addScreeningDocument(ISystemId entity,
                                     ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_METADATA + SLASH + SCREENING_DOCUMENT,
                REL_METADATA_SCREENING_DOCUMENT, false));
    }

    @Override
    public void addScreeningMetadata(ISystemId entity,
                                     ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_METADATA + SLASH + SCREENING_METADATA,
                REL_METADATA_SCREENING_METADATA));
    }

    @Override
    public void addScreeningMetadataLocal(ISystemId entity,
                                          ILinksNoarkObject linksNoarkObject) {
        if (null != ((DocumentDescription) entity).getReferenceScreening()) {
            linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                    HREF_BASE_FONDS_STRUCTURE + SLASH + DOCUMENT_DESCRIPTION +
                    SLASH + entity.getSystemId() + SLASH + SCREENING_METADATA,
                    REL_FONDS_STRUCTURE_SCREENING_METADATA));
        }
    }

    @Override
    public void addNewScreeningMetadataLocal(ISystemId entity,
                                             ILinksNoarkObject linksNoarkObject) {
        if (null != ((DocumentDescription) entity).getReferenceScreening()) {
            linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                    HREF_BASE_FONDS_STRUCTURE + SLASH + DOCUMENT_DESCRIPTION +
                    SLASH + entity.getSystemId() +
                    SLASH + NEW_SCREENING_METADATA,
                    REL_FONDS_STRUCTURE_NEW_SCREENING_METADATA));
        }
    }
}
