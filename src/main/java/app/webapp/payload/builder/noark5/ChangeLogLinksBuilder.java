package app.webapp.payload.builder.noark5;

import app.domain.interfaces.entities.ISystemId;
import app.domain.noark5.EventLog;
import app.webapp.payload.builder.interfaces.IChangeLogLinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.Link;
import org.springframework.stereotype.Component;

import static app.utils.constants.Constants.HREF_BASE_LOGGING;
import static app.utils.constants.Constants.SLASH;
import static app.utils.constants.N5ResourceMappings.CHANGE_LOG;

/*
 * Used to add ChangeLogLinks links with EventLog specific information
 */
@Component("changeLogLinksBuilder")
public class ChangeLogLinksBuilder
        extends EventLogLinksBuilder
        implements IChangeLogLinksBuilder {

    public ChangeLogLinksBuilder() {
    }

    @Override
    public void addSelfLink(ISystemId entity,
                            ILinksNoarkObject linksNoarkObject) {
        String selfHref = getOutgoingAddress() +
                HREF_BASE_LOGGING + SLASH + CHANGE_LOG + SLASH + entity.getSystemId();
        linksNoarkObject.addLink(entity,
                new Link(selfHref, getRelSelfLink()));
        linksNoarkObject.addLink(entity,
                new Link(selfHref, entity.getBaseRel()));
    }

    @Override
    public void addEntityLinks(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject) {
        EventLog eventLog = (EventLog) entity;
        addReferenceArchiveUnitLink(eventLog, linksNoarkObject);
    }

    @Override
    public void addEntityLinksOnTemplate
            (ISystemId entity, ILinksNoarkObject linksNoarkObject) {
    }
}
