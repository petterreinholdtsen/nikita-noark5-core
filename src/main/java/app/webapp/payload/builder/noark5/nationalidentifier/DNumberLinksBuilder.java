package app.webapp.payload.builder.noark5.nationalidentifier;

import app.domain.interfaces.entities.ISystemId;
import app.webapp.payload.builder.interfaces.nationalidentifier.IDNumberLinksBuilder;
import app.webapp.payload.builder.noark5.SystemIdLinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;
import org.springframework.stereotype.Component;

/**
 * Created by tsodring
 */
@Component
public class DNumberLinksBuilder
        extends SystemIdLinksBuilder
        implements IDNumberLinksBuilder {

    public DNumberLinksBuilder() {
    }

    @Override
    public void addEntityLinks(
            ISystemId entity, ILinksNoarkObject linksNoarkObject) {
    }
}
