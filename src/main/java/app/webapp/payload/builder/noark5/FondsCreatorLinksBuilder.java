package app.webapp.payload.builder.noark5;

import app.domain.interfaces.entities.ISystemId;
import app.webapp.payload.builder.interfaces.IFondsCreatorLinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.Link;
import org.springframework.stereotype.Component;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.FONDS;
import static app.utils.constants.N5ResourceMappings.FONDS_CREATOR;

/**
 * Created by tsodring on 2/6/17.
 * <p>
 * Used to add FondsCreatorLinks links with FondsCreator specific information
 */
@Component("fondsCreatorLinksBuilder")
public class FondsCreatorLinksBuilder
        extends SystemIdLinksBuilder
        implements IFondsCreatorLinksBuilder {

    @Override
    public void addEntityLinks(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        addFonds(entity, linksNoarkObject);
        addNewFonds(entity, linksNoarkObject);
    }

    @Override
    public void addFonds(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_FONDS_STRUCTURE + SLASH + FONDS_CREATOR + SLASH + entity.getSystemId() + SLASH + FONDS,
                REL_FONDS_STRUCTURE_FONDS, false));
    }

    @Override
    public void addNewFonds(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_FONDS_STRUCTURE + SLASH + FONDS_CREATOR + SLASH + entity.getSystemId() + SLASH + NEW_FONDS,
                REL_FONDS_STRUCTURE_NEW_FONDS, false));
    }
}
