package app.webapp.payload.builder.noark5.casehandling;

import app.domain.interfaces.entities.ISystemId;
import app.domain.noark5.casehandling.RegistryEntry;
import app.webapp.payload.builder.interfaces.IRegistryEntryLinksBuilder;
import app.webapp.payload.builder.noark5.RecordLinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.Link;
import org.springframework.stereotype.Component;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.*;

/**
 * Created by tsodring on 2/6/17.
 * <p>
 * Used to add RegistryEntryLinks links with RegistryEntry specific
 * information
 */
@Component("registryEntryLinksBuilder")
public class RegistryEntryLinksBuilder
        extends RecordLinksBuilder
        implements IRegistryEntryLinksBuilder {

    @Override
    public void addEntityLinks(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject) {
        super.addEntityLinks(entity, linksNoarkObject);


        addPrecedence(entity, linksNoarkObject);
        addNewPrecedence(entity, linksNoarkObject);
        addSignOff(entity, linksNoarkObject);
        addNewSignOff(entity, linksNoarkObject);
        addDocumentFlow(entity, linksNoarkObject);
        addNewDocumentFlow(entity, linksNoarkObject);
        addRegistryEntryStatus(entity, linksNoarkObject);
        addRegistryEntryType(entity, linksNoarkObject);
    }

    @Override
    public void addEntityLinksOnTemplate(
            ISystemId entity,
            ILinksNoarkObject linksNoarkObject) {
        super.addEntityLinksOnTemplate(entity, linksNoarkObject);
        addRegistryEntryStatus(entity, linksNoarkObject);
        addRegistryEntryType(entity, linksNoarkObject);
    }

    @Override
    public void addPrecedence(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        if (((RegistryEntry) entity).getReferencePrecedence().size() > 0) {
            linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                    HREF_BASE_CASE_HANDLING + SLASH + REGISTRY_ENTRY + SLASH +
                    entity.getSystemIdAsString() + SLASH + PRECEDENCE,
                    REL_CASE_HANDLING_PRECEDENCE, false));
        }
    }

    @Override
    public void addNewPrecedence(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_CASE_HANDLING + SLASH + REGISTRY_ENTRY + SLASH + entity.getSystemId() + SLASH + NEW_PRECEDENCE,
                REL_CASE_HANDLING_NEW_PRECEDENCE, false));
    }

    @Override
    public void addSignOff(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_CASE_HANDLING + SLASH + REGISTRY_ENTRY + SLASH + entity.getSystemId() + SLASH + SIGN_OFF,
                REL_CASE_HANDLING_SIGN_OFF, false));
    }

    @Override
    public void addNewSignOff(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_CASE_HANDLING + SLASH + REGISTRY_ENTRY + SLASH + entity.getSystemId() + SLASH + NEW_SIGN_OFF,
                REL_CASE_HANDLING_NEW_SIGN_OFF, false));
    }

    @Override
    public void addDocumentFlow(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_CASE_HANDLING + SLASH + REGISTRY_ENTRY + SLASH + entity.getSystemId() + SLASH + DOCUMENT_FLOW,
                REL_CASE_HANDLING_DOCUMENT_FLOW, false));
    }

    @Override
    public void addNewDocumentFlow(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_CASE_HANDLING + SLASH + REGISTRY_ENTRY + SLASH + entity.getSystemId() + SLASH + NEW_DOCUMENT_FLOW,
                REL_CASE_HANDLING_NEW_DOCUMENT_FLOW, false));
    }

    // Metadata entries

    /**
     * Create a REL/HREF pair for the list of possible registryEntryStatus
     * (journalstatus) values
     * <p>
     * "../api/arkivstruktur/metadata/journalstatus"
     * "http://rel.kxml.no/noark5/v5/api/metadata/journalstatus/"
     *
     * @param entity           registryEntry
     * @param linksNoarkObject linksRegistryEntry
     */
    @Override
    public void addRegistryEntryStatus(ISystemId entity,
                                       ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity,
                new Link(getOutgoingAddress() + HREF_BASE_METADATA + SLASH +
                        REGISTRY_ENTRY_STATUS,
                        REL_METADATA_REGISTRY_ENTRY_STATUS, true));
    }

    /**
     * Create a REL/HREF pair for the list of possible registryEntryType
     * (journalposttype) values
     * <p>
     * "../api/arkivstruktur/metadata/journalposttype"
     * "http://rel.kxml.no/noark5/v5/api/metadata/journalposttype/"
     *
     * @param entity           registryEntry
     * @param linksNoarkObject linksRegistryEntry
     */
    @Override
    public void addRegistryEntryType(ISystemId entity,
                                     ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity,
                new Link(getOutgoingAddress() + HREF_BASE_METADATA + SLASH +
                        REGISTRY_ENTRY_TYPE,
                        REL_METADATA_REGISTRY_ENTRY_TYPE, true));
    }
}
