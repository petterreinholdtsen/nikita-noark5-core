package app.webapp.payload.builder.noark5.secondary;

import app.domain.interfaces.entities.ISystemId;
import app.domain.noark5.secondary.CrossReference;
import app.webapp.payload.builder.interfaces.secondary.ICrossReferenceLinksBuilder;
import app.webapp.payload.builder.noark5.SystemIdLinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.Link;
import org.springframework.stereotype.Component;

import static app.utils.constants.Constants.*;

/**
 * Used to add CrossReferenceLinks links with CrossReference specific
 * information
 */
@Component("crossReferenceLinksBuilder")
public class CrossReferenceLinksBuilder
        extends SystemIdLinksBuilder
        implements ICrossReferenceLinksBuilder {

    public CrossReferenceLinksBuilder() {
    }

    @Override
    public void addSelfLink(ISystemId entity,
                            ILinksNoarkObject linksNoarkObject) {
        String selfhref = getOutgoingAddress() + HREF_BASE_CROSS_REFERENCE +
                "/" + entity.getSystemId();
        linksNoarkObject.addLink(entity,
                new Link(selfhref, getRelSelfLink()));
        linksNoarkObject.addLink(entity,
                new Link(selfhref, entity.getBaseRel()));
    }

    @Override
    public void addEntityLinks(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject) {
        addRecordEntity(entity, linksNoarkObject);
        addFile(entity, linksNoarkObject);
        addClass(entity, linksNoarkObject);
    }

    /**
     * Create a REL/HREF pair for the Record associated with the given
     * CrossReference
     * https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/registrering/
     *
     * @param crossReference   CrossReference
     * @param linksNoarkObject linksDocumentObject
     */
    @Override
    public void addRecordEntity(ISystemId crossReference,
                                ILinksNoarkObject linksNoarkObject) {
        if (null != ((CrossReference) crossReference).getReferenceRecordEntity()) {
            linksNoarkObject.addLink(crossReference,
                    new Link(getOutgoingAddress() +
                            HREF_BASE_RECORD + "/" +
                            ((CrossReference) crossReference)
                                    .getReferenceRecordEntity().getSystemId(),
                            REL_FONDS_STRUCTURE_RECORD));
        }
    }

    /**
     * Create a REL/HREF pair for the Class associated with the given
     * CrossReference
     * https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/klasse/
     *
     * @param crossReference   The CrossReference object
     * @param linksNoarkObject linksDocumentObject
     */
    @Override
    public void addClass(ISystemId crossReference,
                         ILinksNoarkObject linksNoarkObject) {
        if (null != ((CrossReference) crossReference).getReferenceClass()) {
            linksNoarkObject.addLink(crossReference,
                    new Link(getOutgoingAddress() +
                            HREF_BASE_CLASS + "/" +
                            ((CrossReference) crossReference)
                                    .getReferenceClass().getSystemId(),
                            REL_FONDS_STRUCTURE_CLASS));
        }
    }

    /**
     * Create a REL/HREF pair for the File associated with the given
     * CrossReference
     * https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/mappe/
     *
     * @param crossReference     The CrossReference object
     * @param linksNoarkObject linksDocumentObject
     */
    @Override
    public void addFile(ISystemId crossReference,
                        ILinksNoarkObject linksNoarkObject) {
        if (null != ((CrossReference) crossReference).getReferenceFile()) {
            linksNoarkObject.addLink(crossReference,
                    new Link(getOutgoingAddress() +
                            HREF_BASE_FILE +
                            ((CrossReference) crossReference)
                                    .getReferenceFile().getSystemId(),
                            REL_FONDS_STRUCTURE_FILE));
        }
    }
}
