package app.webapp.payload.builder.noark5.casehandling;

import app.domain.interfaces.entities.ISystemId;
import app.domain.noark5.casehandling.CaseFile;
import app.webapp.payload.builder.interfaces.ICaseFileLinksBuilder;
import app.webapp.payload.builder.noark5.FileLinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.Link;
import org.springframework.stereotype.Component;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.*;

/**
 * Used to add CaseFileLinks links with CaseFile specific information
 */

@Component("caseFileLinksBuilder")
public class CaseFileLinksBuilder
        extends FileLinksBuilder
        implements ICaseFileLinksBuilder {

    @Override
    public void addEntityLinks(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject) {
        // Not calling super.addEntityLinks(entity,
        // linksNoarkObject); because addExpanLinksCaseFile,
        // addExpanLinksMeetingFile, addNewSubFile are not
        // applicable. The links /inherited from File are fetched
        // using AddFileLinks() instead.
        addFileLinks(entity, linksNoarkObject);

        // Methods from this class
        addNewPrecedence(entity, linksNoarkObject);
        addPrecedence(entity, linksNoarkObject);
        addNewRegistryEntry(entity, linksNoarkObject);
        addRegistryEntry(entity, linksNoarkObject);
        addNewRecordNote(entity, linksNoarkObject);
        addRecordEntityNote(entity, linksNoarkObject);
        //addSecondaryClassification(entity, linksNoarkObject);
        addNewSubCaseFile(entity, linksNoarkObject);
        addMetadataCaseStatus(entity, linksNoarkObject);
    }

    @Override
    public void addEntityLinksOnTemplate(
            ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        addMetadataCaseStatus(entity, linksNoarkObject);
    }

    @Override
    public void addNewClass(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_CASE_FILE + SLASH + entity.getSystemId() + SLASH + NEW_CLASS,
                REL_CASE_HANDLING_NEW_CLASS, false));
    }

    @Override
    public void addClass(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_CASE_FILE + SLASH + entity.getSystemId() + SLASH + CLASS,
                REL_CASE_HANDLING_CLASS, false));
    }

    @Override
    public void addNewPrecedence(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_CASE_FILE + SLASH + entity.getSystemId() + SLASH + NEW_PRECEDENCE,
                REL_CASE_HANDLING_NEW_PRECEDENCE, false));
    }

    @Override
    public void addPrecedence(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        if (((CaseFile) entity).getReferencePrecedence().size() > 0) {
            linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                    HREF_BASE_CASE_FILE + SLASH + entity.getSystemId() + SLASH
                    + PRECEDENCE, REL_CASE_HANDLING_PRECEDENCE, false));
        }
    }

    @Override
    public void addSecondaryClassification(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_CASE_FILE + SLASH + entity.getSystemId() + SLASH + SECONDARY_CLASSIFICATION,
                REL_CASE_HANDLING_SECONDARY_CLASSIFICATION, false));
    }

    @Override
    public void addNewSecondaryClassification(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_CASE_FILE + SLASH + entity.getSystemId() + SLASH + NEW_SECONDARY_CLASSIFICATION,
                REL_CASE_HANDLING_NEW_SECONDARY_CLASSIFICATION, false));
    }

    @Override
    public void addRegistryEntry(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_CASE_FILE + SLASH + entity.getSystemId() + SLASH + REGISTRY_ENTRY,
                REL_CASE_HANDLING_REGISTRY_ENTRY, false));
    }

    @Override
    public void addNewRegistryEntry(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_CASE_FILE + SLASH + entity.getSystemId() + SLASH + NEW_REGISTRY_ENTRY,
                REL_CASE_HANDLING_NEW_REGISTRY_ENTRY, false));
    }

    /**
     * Create a REL/HREF pair for the RecordNote associated with the
     * given CaseFile.
     * <p>
     * "../api/arkivstruktur/sakarkiv/1234/arkivnotat"
     * "https://rel.arkivverket.no/noark5/v5/api/sakarkiv/arkivnotat/"
     *
     * @param entity           caseFile
     * @param linksNoarkObject linksCaseFile
     */
    @Override
    public void addRecordEntityNote(ISystemId entity,
                                    ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_CASE_FILE + SLASH + entity.getSystemId() + SLASH + RECORD_NOTE,
                REL_CASE_HANDLING_RECORD_NOTE));
    }

    /**
     * Create a REL/HREF pair for the RecordNote associated with the
     * given CaseFile.
     * <p>
     * "../api/arkivstruktur/sakarkiv/1234/ny-arkivnotat"
     * "https://rel.arkivverket.no/noark5/v5/api/sakarkiv/ny-arkivnotat/"
     *
     * @param entity           caseFile
     * @param linksNoarkObject linksCaseFile
     */
    @Override
    public void addNewRecordNote(ISystemId entity,
                                 ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_CASE_FILE + SLASH + entity.getSystemId() + SLASH + NEW_RECORD_NOTE,
                REL_CASE_HANDLING_NEW_RECORD_NOTE));
    }

    @Override
    public void addNewSubCaseFile(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_CASE_FILE + SLASH + entity.getSystemId() + SLASH + NEW_CASE_FILE,
                REL_CASE_HANDLING_NEW_CASE_FILE, false));
    }

    /**
     * Create a REL/HREF pair for the CaseStatus metadata endpoint
     * <p>
     * "../api/metadata/saksstatus/"
     * https://rel.arkivverket.no/noark5/v5/api/metadata/saksstatus/
     *
     * @param entity             caseFile
     * @param linksNoarkObject linksCaseFile
     */
    @Override
    public void addMetadataCaseStatus(ISystemId entity,
                                      ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_METADATA + SLASH + CASE_STATUS,
                REL_METADATA_CASE_STATUS));
    }
}
