package app.webapp.payload.builder.noark5.secondary;

import app.domain.interfaces.entities.ISystemId;
import app.domain.interfaces.entities.secondary.IPrecedenceEntity;
import app.webapp.payload.builder.interfaces.secondary.IPrecedenceLinksBuilder;
import app.webapp.payload.builder.noark5.SystemIdLinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.Link;
import org.springframework.stereotype.Component;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.PRECEDENCE_STATUS;

/**
 * Created by tsodring on 2/6/17.
 * <p>
 * Used to add PrecedenceLinks links with Precedence specific information
 * <p>
 * Not sure if there is a difference in what should be returned of links for various CRUD operations so keeping them
 * separate calls at the moment.
 */
@Component("precedenceLinksBuilder")
public class PrecedenceLinksBuilder
        extends SystemIdLinksBuilder
        implements IPrecedenceLinksBuilder {

    @Override
    public void addEntityLinks
            (ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        IPrecedenceEntity precedence = (IPrecedenceEntity) entity;
        addPrecedenceStatus(precedence, linksNoarkObject);
        // TODO figure out relations to add more case files and record notes
        // TODO add relation to list case files and record notes
        //addCaseFile(precedence, linksNoarkObject);
        //addRegistryEntry(precedence, linksNoarkObject);
    }

    @Override
    public void addEntityLinksOnTemplate
            (ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        IPrecedenceEntity precedence = (IPrecedenceEntity) entity;
        addPrecedenceStatus(precedence, linksNoarkObject);
    }

    @Override
    public void addPrecedenceStatus
            (IPrecedenceEntity entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity,
                new Link(getOutgoingAddress() + HREF_BASE_METADATA + SLASH + PRECEDENCE_STATUS,
                        REL_METADATA_PRECEDENCE_STATUS, true));
    }

    @Override
    public void addCaseFile(IPrecedenceEntity precedence,
                            ILinksNoarkObject linksNoarkObject) {
        /*
        if (null != precedence.getReferenceCaseFile()) {
            linksNoarkObject.addLink(precedence,
                new Link(getOutgoingAddress() +
                         HREF_BASE_CASE_FILE + SLASH +
                         precedence.getReferenceCaseFile().getSystemIdAsString(),
                         REL_CASE_HANDLING_CASE_FILE));
        }
        */
    }

    @Override
    public void addRegistryEntry(IPrecedenceEntity precedence,
                                 ILinksNoarkObject linksNoarkObject) {
        /*
        if (null != precedence.getReferenceRegistryEntry()) {
            linksNoarkObject.addLink(precedence,
                new Link(getOutgoingAddress() +
                         HREF_BASE_REGISTRY_ENTRY + SLASH +
                         precedence.getReferenceRegistryEntry().getSystemIdAsString(),
                         REL_CASE_HANDLING_REGISTRY_ENTRY));
        }
        */
    }
}
