package app.webapp.payload.builder.noark5.casehandling;

import app.domain.interfaces.entities.ISystemId;
import app.webapp.payload.links.ILinksNoarkObject;
import org.springframework.stereotype.Component;

/**
 * Created by tsodring on 2/6/17.
 * <p>
 * Used to add CorrespondencePartLinks links with CorrespondencePart
 * specific information
 **/
@Component("correspondencePartInternalLinksBuilder")
public class CorrespondencePartInternalLinksBuilder
        extends CorrespondencePartLinksBuilder {

    @Override
    public void addEntityLinksOnTemplate(ISystemId entity,
                                         ILinksNoarkObject
                                                 linksNoarkObject) {
        addCorrespondencePartType(entity, linksNoarkObject);
    }
}
