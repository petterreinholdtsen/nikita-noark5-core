package app.webapp.payload.builder.noark5.secondary;

import app.domain.interfaces.entities.ISystemId;
import app.domain.interfaces.entities.secondary.IConversionEntity;
import app.domain.noark5.secondary.Conversion;
import app.webapp.payload.builder.interfaces.secondary.IConversionLinksBuilder;
import app.webapp.payload.builder.noark5.SystemIdLinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.Link;
import org.springframework.stereotype.Component;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.CONVERSION;
import static app.utils.constants.N5ResourceMappings.FORMAT;

/*
 * Used to add ConversionLinks links with Conversion specific information
 */
@Component("conversionLinksBuilder")
public class ConversionLinksBuilder
        extends SystemIdLinksBuilder
        implements IConversionLinksBuilder {

    public ConversionLinksBuilder() {
    }

    @Override
    public void addSelfLink(ISystemId entity,
                            ILinksNoarkObject linksNoarkObject) {

        Conversion conversion = (Conversion) entity;
        String parentEntity =
                conversion.getReferenceDocumentObject().getBaseTypeName();
        String parentSystemId =
                conversion.getReferenceDocumentObject().getSystemIdAsString();
        String selfhref = getOutgoingAddress() +
                HREF_BASE_FONDS_STRUCTURE + SLASH + parentEntity + SLASH +
                parentSystemId + SLASH + CONVERSION + SLASH + entity.getSystemId();
        linksNoarkObject.addLink(entity,
                new Link(selfhref, getRelSelfLink()));
        linksNoarkObject.addLink(entity,
                new Link(selfhref, entity.getBaseRel()));
    }

    @Override
    public void addEntityLinks(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject) {
        Conversion conversion = (Conversion) entity;
        addDocumentObject(conversion, linksNoarkObject);
        addFormat(conversion, linksNoarkObject);
    }

    @Override
    public void addEntityLinksOnTemplate
            (ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        Conversion conversion = (Conversion) entity;
        addFormat(conversion, linksNoarkObject);
    }

    /**
     * Create a REL/HREF pair for the parent DocumentObject associated
     * with the given Conversion
     * <p>
     * "../api/arkivstruktur/dokumentbeskrivelse/{systemId}"
     * "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/dokumentobject/"
     *
     * @param conversion       The Conversion object
     * @param linksNoarkObject linksDocumentObject
     */
    @Override
    public void addDocumentObject(IConversionEntity conversion,
                                  ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(conversion,
                new Link(getOutgoingAddress() +
                        HREF_BASE_DOCUMENT_OBJECT + SLASH +
                        conversion.getReferenceDocumentObject().getSystemId(),
                        REL_FONDS_STRUCTURE_DOCUMENT_OBJECT));
    }

    @Override
    public void addFormat(IConversionEntity conversion,
                          ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(conversion,
                new Link(getOutgoingAddress() + HREF_BASE_METADATA + SLASH + FORMAT,
                        REL_METADATA_FORMAT, true));
    }
}
