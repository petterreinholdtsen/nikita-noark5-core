package app.webapp.payload.builder.noark5.secondary;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.interfaces.entities.ISystemId;
import app.webapp.payload.builder.interfaces.secondary.IPartLinksBuilder;
import app.webapp.payload.builder.noark5.SystemIdLinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.Link;
import org.springframework.stereotype.Component;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.PART_ROLE;

/**
 * Created by tsodring on 11/07/19.
 * <p>
 * Used to add PartLinks links with Part
 * specific information
 **/
@Component("partLinksBuilder")
public class PartLinksBuilder
        extends SystemIdLinksBuilder
        implements IPartLinksBuilder {

    @Override
    public void addEntityLinksOnTemplate(ISystemId entity,
                                         ILinksNoarkObject
                                                 linksNoarkObject) {
        addPartRole(entity, linksNoarkObject);
    }

    @Override
    public void addPartRole(INoarkEntity entity,
                            ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_METADATA + SLASH + PART_ROLE, REL_METADATA_PART_ROLE,
                false));
    }
}
