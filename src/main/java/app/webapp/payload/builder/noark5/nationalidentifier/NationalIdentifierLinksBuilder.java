package app.webapp.payload.builder.noark5.nationalidentifier;

import app.webapp.payload.builder.interfaces.nationalidentifier.INationalIdentifierLinksBuilder;
import app.webapp.payload.builder.noark5.SystemIdLinksBuilder;
import org.springframework.stereotype.Component;

/**
 * Used to add NationalIdentifierLinks links with NationalIdentifier
 * specific information
 **/
@Component("nationalIdentifierLinksBuilder")
public class NationalIdentifierLinksBuilder
        extends SystemIdLinksBuilder
        implements INationalIdentifierLinksBuilder {
}
