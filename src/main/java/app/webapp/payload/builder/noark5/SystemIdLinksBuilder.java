package app.webapp.payload.builder.noark5;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.interfaces.entities.ISystemId;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.builder.interfaces.ISystemIdLinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.Link;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.regex.Pattern;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.*;
import static app.utils.constants.ODataConstants.*;

@Component
public class SystemIdLinksBuilder
        extends LinksBuilder
        implements ISystemIdLinksBuilder {

    private final Pattern patternDollarTop = Pattern.compile("\\$top=[0-9]+");
    private final Pattern patternDollarSkip = Pattern.compile("\\$skip=[0-9]+");

    /**
     * Create a self link and self pointing entity link. Allows a client to be
     * able to identify the entity type. Described in:
     * <p>
     * https://github.com/arkivverket/noark5-tjenestegrensesnitt-standard/blob/master/kapitler/06-konsepter_og_prinsipper.md#identifisere-entitetstype
     *
     * @param entity           The Noark entity
     * @param linksNoarkObject The Hateoas Noark Object
     */
    @Override
    public void addSelfLink(ISystemId entity,
                            ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HATEOAS_API_PATH + SLASH + entity.getFunctionalTypeName() +
                SLASH + entity.getBaseTypeName() + SLASH + entity.getSystemIdAsString(),
                getRelSelfLink()));

        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HATEOAS_API_PATH + SLASH + entity.getFunctionalTypeName() +
                SLASH + entity.getBaseTypeName() + SLASH + entity.getSystemIdAsString(),
                entity.getBaseRel()));
    }

    @Override
    public void addLinks(ILinksNoarkObject linksNoarkObject) {
        if (!linksNoarkObject.isSingleEntity()) {
            List<INoarkEntity> entities = linksNoarkObject.getList();

            for (INoarkEntity entity : entities) {
                addSelfLink((ISystemId) entity, linksNoarkObject);
                addEntityLinks((ISystemId) entity, linksNoarkObject);
            }
            String url = getRequestPathAndQueryString();
            Link selfLink = new Link(url, getRelSelfLink(), false);
            linksNoarkObject.addSelfLink(selfLink);
            SearchResultsPage page = linksNoarkObject.getPage();
            int top = page.getTop();
            int skip = page.getSkip();
            long count = page.getCount();
            addPaginationPrevious(linksNoarkObject, top, skip);
            addPaginationNext(linksNoarkObject, top, skip, count);
        } else {
            Iterable<ISystemId> entities = (List<ISystemId>) (List)
                    linksNoarkObject.getList();
            for (ISystemId entity : entities) {
                addSelfLink(entity, linksNoarkObject);
                addEntityLinks(entity, linksNoarkObject);
                addEventLogLinks(entity, linksNoarkObject);
            }
            // If linksNoarkObject is a list add a self link.
            // { "entity": [], "_links": [] }
            if (!linksNoarkObject.isSingleEntity()) {
                String url = getRequestPathAndQueryString();
                Link selfLink = new Link(url, getRelSelfLink(), false);
                linksNoarkObject.addSelfLink(selfLink);
            }
        }
    }

    @Override
    public void addDocumentMedium(ISystemId entity,
                                  ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_METADATA + SLASH + DOCUMENT_MEDIUM,
                REL_METADATA_DOCUMENT_MEDIUM, false));
    }


    // Sub class should handle this, empty links otherwise!
    @Override
    public void addEntityLinks(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject) {
    }

    public void addPaginationPrevious(ILinksNoarkObject linksNoarkObject,
                                      long top, long skip) {
        // Do not add PREVIOUS link if nothing to add. This is determined by
        // subtracting skip from top and checking that it is greater than 0
        if (skip != 0 && top - skip >= 0) {
            String urlRequest = getRequestPath();
            String urlQueryString = getRequestQueryString();
            long actualSkip = top - skip > 0 ? top - skip : 0;
            urlQueryString = replaceOrAddURLForTop(urlQueryString, top);
            urlQueryString = replaceOrAddURLForSkip(urlQueryString, actualSkip);
            Link previous;
            if (urlQueryString.isBlank()) {
                previous = new Link(urlRequest, PREVIOUS);
            } else {
                previous = new Link(urlRequest + "?" + urlQueryString, PREVIOUS);
            }

            linksNoarkObject.addLink(previous);
        }
    }

    public void addPaginationNext(ILinksNoarkObject linksNoarkObject,
                                  int top, int skip, long count) {
        // Do not add NEXT link if nothing to add. This is determined by
        // adding skip + top and checking that it is less than the row count
        if (skip + top < count) {
            String urlRequest = getRequestPath();
            String urlQueryString = getRequestQueryString();
            urlQueryString = replaceOrAddURLForTop(urlQueryString, top);
            urlQueryString = replaceOrAddURLForSkip(urlQueryString, skip + top);
            Link next = new Link(urlRequest + "?" + urlQueryString, NEXT);
            linksNoarkObject.addLink(next);
        }
    }

    private String replaceOrAddURLForTop(String url, long top) {
        // Do not print $top=10 as 10 is default value
        if (top == 10) {
            // replace &%24top=[0-9]+ with ""
            url = url.replaceAll("&\\$top=[0-9]+", "");
            // but & might not be there so also replace %24top=[0-9]+ if it is
            // there
            return url.replaceAll("%\\$top=[0-9]+", "");
        }
        if (patternDollarTop.matcher(url).find()) {
            return url.replaceAll("\\$top=[0-9]+", "\\$top=" + top);
        } else {
            return url + "&$top=" + top;
        }
    }

    private String replaceOrAddURLForSkip(String url, long skip) {
        // Do not print $skip=0 as 0 is default value

        if (skip == 0) {
            // replace &%24skip=[0-9]+ with ""
            url = url.replaceAll("&\\$skip=[0-9]+", "");
            // but & might not be there so also replace %24skip=[0-9]+ if it is
            // there
            return url.replaceAll("\\$skip=[0-9]+", "");
        }
        if (patternDollarSkip.matcher(url).find()) {
            url = url.replaceAll("\\$skip=[0-9]+", "\\$skip=" + skip);
        } else {
            url = url + "&$skip=" + skip;
        }
        return url;
    }

    // Sub class should handle this, empty links otherwise!
    @Override
    public void addEntityLinksOnCreate(ISystemId entity,
                                       ILinksNoarkObject linksNoarkObject) {
        addEntityLinks(entity, linksNoarkObject);
    }

    // Sub class should handle this, empty links otherwise!
    @Override
    public void addEntityLinksOnTemplate(ISystemId entity,
                                         ILinksNoarkObject linksNoarkObject) {
    }

    @Override
    public void addLinksOnTemplate(ILinksNoarkObject linksNoarkObject) {
        Iterable<ISystemId> entities = (List<ISystemId>) (List)
                linksNoarkObject.getList();
        for (ISystemId entity : entities) {
            addEntityLinksOnTemplate(entity, linksNoarkObject);
        }
    }

    // Sub class should handle this, empty links otherwise!
    @Override
    public void addEntityLinksOnRead(ISystemId entity,
                                     ILinksNoarkObject linksNoarkObject) {
        addEntityLinks(entity, linksNoarkObject);
    }

    @Override
    public void addEventLogLinks(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_LOGGING + SLASH + EVENT_LOG + "?" + DOLLAR_FILTER_EQ +
                REFERENCE_ARCHIVE_UNIT + " eq '" + entity.getSystemIdAsString() + "'",
                REL_LOGGING_EVENT_LOG, true));
    }
}
