package app.webapp.payload.deserializers;

import app.domain.noark5.NoarkEntity;
import app.webapp.payload.deserializers.noark5.interfaces.IDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class NikitaDeserializer<F extends NoarkEntity>
        extends JsonDeserializer<Object> implements IDeserializer {
    protected static final ObjectMapper mapper = new ObjectMapper();

    @Override
    public Object deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        return null;
    }
}
