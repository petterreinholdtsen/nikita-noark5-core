package app.webapp.payload.deserializers;

import app.domain.interfaces.entities.INoarkGeneralEntity;
import app.domain.noark5.NoarkGeneralEntity;
import app.domain.noark5.SystemIdEntity;
import app.webapp.payload.deserializers.noark5.interfaces.IFinalisedDeserializer;
import app.webapp.payload.deserializers.noark5.interfaces.ITitleAndDescriptionDeserializer;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class NoarkGeneralEntityDeserializer<T extends NoarkGeneralEntity>
        extends SystemIdEntityDeserializer<SystemIdEntity>
        implements ITitleAndDescriptionDeserializer, IFinalisedDeserializer {

    public void deserializeNoarkGeneralEntity(INoarkGeneralEntity noarkGeneralEntity, ObjectNode objectNode, StringBuilder errors) {
        deserializeSystemIdEntity(noarkGeneralEntity, objectNode, errors);
        deserializeNoarkFinaliseEntity(noarkGeneralEntity, objectNode, errors);
        deserializeNoarkTitleDescriptionEntity(noarkGeneralEntity, objectNode, errors);
    }
}
