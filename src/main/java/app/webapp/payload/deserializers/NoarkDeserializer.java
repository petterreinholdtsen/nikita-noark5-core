package app.webapp.payload.deserializers;

import app.domain.noark5.NoarkEntity;
import app.webapp.exceptions.NikitaMalformedInputDataException;
import app.webapp.exceptions.NikitaMisconfigurationException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

import static app.utils.constants.ErrorMessagesConstants.CALL_TO_PARENT_DESERIALIZER_NOT_ALLOWED;
import static app.utils.constants.ErrorMessagesConstants.MALFORMED_PAYLOAD;
import static app.utils.constants.HATEOASConstants.LINKS;

public class NoarkDeserializer<F extends NoarkEntity>
        extends NikitaDeserializer<NoarkEntity> {

    protected static final Logger logger = LoggerFactory.getLogger(NoarkDeserializer.class);
    protected StringBuilder errors = new StringBuilder();
    @Override
    public Object deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        throw new NikitaMisconfigurationException(CALL_TO_PARENT_DESERIALIZER_NOT_ALLOWED);
    }

    protected void check_payload_at_end(StringBuilder errors, ObjectNode node) {
        check_links(node);
        check_empty(errors, node);
        check_errors(errors);
    }

    protected void check_links(ObjectNode node) {
        JsonNode linksNode = node.get(LINKS);
        if (null != linksNode) {
            logger.debug("Payload contains " + LINKS + ". This value is being ignored.");
            node.remove(LINKS);
        }
    }

    /**
     * Check that there are no additional values left after processing the tree.
     *
     * @param errors StringBuilder object to append any errors that occur
     * @param node   Incoming payload as an ObjectNode
     */
    protected void check_empty(StringBuilder errors, ObjectNode node) {
        if (node.size() != 0) {
            errors.append(String.format(MALFORMED_PAYLOAD, getType(), checkNodeObjectEmpty(node)));
        }
    }

    /**
     * If any errors occurred during processing throw a malformed input exception
     *
     * @param errors StringBuilder object containing any deserializing errors that occurred
     */

    protected void check_errors(StringBuilder errors) {
        if (0 < errors.length()) {
            throw new NikitaMalformedInputDataException(errors.toString());
        }
    }

    /**
     * Each subclass must implement the getType() method
     *
     * @return The type of object the subclass is deserializing
     */
    protected String getType() {
        throw new NikitaMisconfigurationException(CALL_TO_PARENT_DESERIALIZER_NOT_ALLOWED);
    }
}
