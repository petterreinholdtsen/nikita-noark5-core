package app.webapp.payload.deserializers.noark5.interfaces;

import app.domain.interfaces.entities.IClassifiedEntity;
import app.domain.noark5.metadata.ClassifiedCode;
import app.domain.noark5.secondary.Classified;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import static app.utils.constants.N5ResourceMappings.*;

public interface IClassifiedDeserializer
        extends IDeserializer {

    default Classified deserializeClassified(ObjectNode objectNode, StringBuilder errors) {
        Classified classified = null;
        JsonNode classifiedNode = objectNode.get(CLASSIFIED);
        if (null != classifiedNode
                && !classifiedNode.equals(NullNode.getInstance())) {
            classified = new Classified();
            ObjectNode classifiedObjectNode = classifiedNode.deepCopy();
            deserializeClassifiedEntity(classified,
                    classifiedObjectNode, errors);
            if (0 == classifiedObjectNode.size()) {
                objectNode.remove(CLASSIFIED);
            }
        } else if (null != classifiedNode) { // Remove NullNode
            objectNode.remove(CLASSIFIED);
        }
        return classified;
    }

    default void deserializeClassifiedEntity(IClassifiedEntity classifiedEntity, ObjectNode objectNode, StringBuilder errors) {
        // Deserialize classification
        ClassifiedCode classifiedCode = (ClassifiedCode)
                deserializeMetadataValue(objectNode,
                        CLASSIFICATION,
                        new ClassifiedCode(),
                        errors, true);
        classifiedEntity.setClassification(classifiedCode);
        // Deserialize classificationDate
        classifiedEntity.setClassificationDate(deserializeDateTime(CLASSIFICATION_DATE, objectNode, errors));
        // Deserialize classificationBy
        JsonNode currentNode = objectNode.get(CLASSIFICATION_BY);
        if (null != currentNode) {
            classifiedEntity.setClassificationBy(currentNode.textValue());
            objectNode.remove(CLASSIFICATION_BY);
        }
        // Deserialize classificationDowngradedDate
        classifiedEntity.setClassificationDowngradedDate(deserializeDateTime(CLASSIFICATION_DOWNGRADED_DATE,
                objectNode, errors));
        // Deserialize
        currentNode = objectNode.get(CLASSIFICATION_DOWNGRADED_BY);
        if (null != currentNode) {
            classifiedEntity.setClassificationDowngradedBy(currentNode.textValue());
            objectNode.remove(CLASSIFICATION_DOWNGRADED_BY);
        }
    }
}