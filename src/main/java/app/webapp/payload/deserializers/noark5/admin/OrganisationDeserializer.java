package app.webapp.payload.deserializers.noark5.admin;

import app.domain.noark5.admin.Organisation;
import app.webapp.payload.deserializers.NoarkGeneralEntityDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

/**
 * Deserialize an incoming Organisation JSON object.
 */
public class OrganisationDeserializer
        extends NoarkGeneralEntityDeserializer<Organisation> {
    @Override
    public Organisation deserialize(JsonParser jsonParser,
                                    DeserializationContext dc)
            throws IOException {

        Organisation organisation = new Organisation();
        ObjectNode objectNode = mapper.readTree(jsonParser);

        // Deserialize general properties
        deserializeNoarkSystemIdEntity(organisation, objectNode);
        deserializeNoarkCreateEntity(organisation, objectNode, errors);
        deserializeNoarkFinaliseEntity(organisation, objectNode, errors);

        // Deserialize organisationStatus
        JsonNode currentNode = objectNode.get(ORGANISATION_STATUS);
        if (currentNode != null) {
            organisation.setOrganisationStatus(currentNode.textValue());
            objectNode.remove(ORGANISATION_STATUS);
        }

        // Deserialize organisationName
        currentNode = objectNode.get(ORGANISATION_NAME);
        if (currentNode != null) {
            organisation.setOrganisationName(currentNode.textValue());
            objectNode.remove(ORGANISATION_NAME);
        }

        check_payload_at_end(errors, objectNode);
        return organisation;
    }

    @Override
    protected String getType() {
        return ORGANISATION;
    }
}
