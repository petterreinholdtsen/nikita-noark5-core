package app.webapp.payload.deserializers.noark5.interfaces;

import app.domain.interfaces.entities.IDisposalEntity;
import app.domain.noark5.secondary.Disposal;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import static app.utils.constants.N5ResourceMappings.*;

public interface IDisposalDeserializer
        extends IDeserializer {

    default Disposal deserializeDisposal(ObjectNode objectNode, StringBuilder errors) {
        Disposal disposal = null;
        JsonNode disposalNode = objectNode.get(DISPOSAL);
        if (disposalNode != null) {
            disposal = new Disposal();
            deserializeDisposalEntity(disposal, objectNode, errors);
            objectNode.remove(DISPOSAL);
        }
        return disposal;
    }

    default void deserializeDisposalEntity(IDisposalEntity disposalEntity, ObjectNode objectNode, StringBuilder errors) {
        // Deserialize disposalDecision
        JsonNode currentNode = objectNode.get(DISPOSAL_DECISION);
        if (null != currentNode) {
            disposalEntity.setDisposalDecision(currentNode.textValue());
            objectNode.remove(DISPOSAL_DECISION);
        }
        // Deserialize disposalAuthority(
        currentNode = objectNode.get(DISPOSAL_AUTHORITY);
        if (null != currentNode) {
            disposalEntity.setDisposalAuthority(currentNode.textValue());
            objectNode.remove(DISPOSAL_AUTHORITY);
        }
        // Deserialize preservationTime
        disposalEntity.setPreservationTime
                (deserializeInteger(DISPOSAL_PRESERVATION_TIME,
                        objectNode, errors, false));
        // Deserialize disposalDate
        disposalEntity.setDisposalDate(
                deserializeDateTime(DISPOSAL_DATE, objectNode, errors));
    }

}
