package app.webapp.payload.deserializers.noark5.interfaces;

import app.domain.interfaces.entities.IScreeningEntity;
import app.domain.noark5.metadata.AccessRestriction;
import app.domain.noark5.metadata.ScreeningDocument;
import app.domain.noark5.secondary.Screening;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import static app.utils.constants.ErrorMessagesConstants.DESERIALIZE_LEFTOVER;
import static app.utils.constants.N5ResourceMappings.*;

public interface IScreeningDeserializer
        extends IDeserializer {

    default Screening deserializeScreening(ObjectNode objectNode, StringBuilder errors) {
        Screening screening = null;
        JsonNode screeningNode = objectNode.get(SCREENING);
        if (null != screeningNode
                && !screeningNode.equals(NullNode.getInstance())) {
            screening = new Screening();
            ObjectNode screeningObjectNode = screeningNode.deepCopy();
            deserializeScreeningEntity(screening, screeningObjectNode,
                    errors);
            if (0 == screeningObjectNode.size()) {
                objectNode.remove(SCREENING);
            } else {
                errors.append(String.format(DESERIALIZE_LEFTOVER,
                        screeningObjectNode));
            }
        } else { // Remove NullNode
            objectNode.remove(SCREENING);
        }
        return screening;
    }

    default void deserializeScreeningEntity(
            IScreeningEntity screeningEntity, ObjectNode objectNode,
            StringBuilder errors) {
        // Deserialize accessRestriction
        AccessRestriction accessRestriction = (AccessRestriction)
                deserializeMetadataValue(objectNode,
                        SCREENING_ACCESS_RESTRICTION,
                        new AccessRestriction(),
                        errors, true);
        screeningEntity.setAccessRestriction(accessRestriction);
        // Deserialize screeningAuthority
        JsonNode currentNode = objectNode.get(SCREENING_AUTHORITY);
        if (null != currentNode) {
            screeningEntity.setScreeningAuthority(currentNode.textValue());
            objectNode.remove(SCREENING_AUTHORITY);
        } else {
            errors.append(SCREENING_AUTHORITY + " is missing. ");
        }
        // Deserialize screeningDocument
        ScreeningDocument screeningDocument = (ScreeningDocument)
                deserializeMetadataValue(objectNode,
                        SCREENING_SCREENING_DOCUMENT,
                        new ScreeningDocument(),
                        errors, false);
        screeningEntity.setScreeningDocument(screeningDocument);
        // Deserialize screeningExpiresDate
        screeningEntity.setScreeningExpiresDate(
                deserializeDateTime(SCREENING_EXPIRES_DATE, objectNode, errors));
        // Deserialize screeningDuration
        screeningEntity.setScreeningDuration
                (deserializeInteger(SCREENING_DURATION,
                        objectNode, errors, false));
    }

}
