package app.webapp.payload.deserializers.noark5;

/**
 * <p>
 * Having an own deserializer is done to have more fine-grained control over the
 * input. The incoming payloads do not adhere to any format that a library supports
 * so we have to provide our own implementation.
 * - Unknown property values in the JSON will trigger an exception
 * - Missing obligatory property values in the JSON will trigger an exception
 *
 * @author tsodring
 * @version 0.7
 * @since 0.7
 */
