package app.webapp.payload.deserializers.noark5.secondary;

import app.domain.noark5.secondary.Part;
import app.domain.noark5.secondary.PartPerson;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.PART_PERSON;

/**
 * Deserialize an incoming PartPerson JSON object.
 */
public class PartPersonDeserializer<P extends Part>
        extends PartDeserializer<Part> {
    @Override
    public PartPerson deserialize(JsonParser jsonParser, DeserializationContext dc) throws IOException {
               PartPerson part = new PartPerson();
        ObjectNode objectNode = mapper.readTree(jsonParser);
        deserializeNoarkSystemIdEntity(part, objectNode);
        deserializePartPersonEntity(part, objectNode, errors);
        deserializeBSM(objectNode, part);
        check_payload_at_end(errors, objectNode);
        return part;
    }

    @Override
    protected String getType() {
        return PART_PERSON;
    }
}