package app.webapp.payload.deserializers.noark5.secondary;

import app.domain.interfaces.entities.admin.IAdministrativeUnitEntity;
import app.domain.noark5.admin.AdministrativeUnit;
import app.domain.noark5.casehandling.secondary.CorrespondencePart;
import app.webapp.payload.deserializers.SystemIdEntityDeserializer;
import app.webapp.payload.deserializers.noark5.interfaces.ICorrespondencePartDeserializer;
import app.webapp.payload.deserializers.noark5.interfaces.IFinalisedDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import static app.utils.constants.N5ResourceMappings.*;
import static java.util.UUID.fromString;

/**
 * This is just a base class for the others to use
 */
public class CorrespondencePartDeserializer<P extends CorrespondencePart>
        extends SystemIdEntityDeserializer<CorrespondencePart>
        implements ICorrespondencePartDeserializer, IFinalisedDeserializer {

    protected void deserializeAdministrativeUnitEntity(IAdministrativeUnitEntity administrativeUnit,
                                                       ObjectNode objectNode, StringBuilder errors) {
        if (null != administrativeUnit) {
            deserializeNoarkSystemIdEntity(administrativeUnit, objectNode);
            deserializeNoarkCreateEntity(administrativeUnit, objectNode, errors);
            deserializeNoarkFinaliseEntity(administrativeUnit, objectNode, errors);
            // Deserialize kortnavn
            JsonNode currentNode = objectNode.get(SHORT_NAME);
            if (null != currentNode) {
                administrativeUnit.setShortName(currentNode.textValue());
                objectNode.remove(SHORT_NAME);
            }
            // Deserialize administrativEnhetNavn
            currentNode = objectNode.get(ADMINISTRATIVE_UNIT_NAME);
            if (null != currentNode) {
                administrativeUnit.setAdministrativeUnitName(currentNode.textValue());
                objectNode.remove(ADMINISTRATIVE_UNIT_NAME);
            }
            // It is unclear if status values are to be set by special endpoint calls
            // e.g. adminunit->avsluttAdministrativeEnhet
            // Deserialize administrativEnhetsstatus
            currentNode = objectNode.get(ADMINISTRATIVE_UNIT_STATUS);
            if (null != currentNode) {
                administrativeUnit.setAdministrativeUnitStatus(currentNode.textValue());
                objectNode.remove(ADMINISTRATIVE_UNIT_STATUS);
            }
            // Deserialize referanseOverordnetEnhet
            currentNode = objectNode.get(ADMINISTRATIVE_UNIT_PARENT_REFERENCE);
            if (null != currentNode) {
                AdministrativeUnit parent = new AdministrativeUnit();
                parent.setSystemId(
                        fromString(currentNode.textValue()));
                parent.getReferenceChildAdministrativeUnit().add((AdministrativeUnit) administrativeUnit);
                administrativeUnit.setParentAdministrativeUnit(parent);
                objectNode.remove(ADMINISTRATIVE_UNIT_PARENT_REFERENCE);
            }
        }
    }
}