package app.webapp.payload.deserializers.noark5.nationalidentifier;

import app.domain.noark5.metadata.Country;
import app.domain.noark5.nationalidentifier.Plan;
import app.webapp.payload.deserializers.SystemIdEntityDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

public class PlanDeserializer
        extends SystemIdEntityDeserializer<Plan> {
    @Override
    public Plan deserialize(JsonParser jsonParser, DeserializationContext dc) throws IOException {
               Plan plan = new Plan();
        ObjectNode objectNode = mapper.readTree(jsonParser);
        // Deserialize systemID
        deserializeNoarkSystemIdEntity(plan, objectNode);
        // Deserialize kommunenummer
        JsonNode currentNode = objectNode.get(MUNICIPALITY_NUMBER);
        if (null != currentNode) {
            plan.setMunicipalityNumber(currentNode.textValue());
            objectNode.remove(MUNICIPALITY_NUMBER);
        }
        // Deserialize fylkesnummer
        currentNode = objectNode.get(COUNTY_NUMBER);
        if (null != currentNode) {
            plan.setCountyNumber(currentNode.textValue());
            objectNode.remove(COUNTY_NUMBER);
        }
        // Deserialize landkode
        Country country = (Country) deserializeMetadataValue(objectNode, COUNTRY_CODE, new Country(), errors, false);
        plan.setCountry(country);
        // Deserialize planidentifikasjon
        currentNode = objectNode.get(PLAN_IDENTIFICATION);
        if (null != currentNode) {
            plan.setPlanIdentification(currentNode.textValue());
            objectNode.remove(PLAN_IDENTIFICATION);
        }
        check_payload_at_end(errors, objectNode);
        return plan;
    }

    @Override
    protected String getType() {
        return PLAN;
    }
}