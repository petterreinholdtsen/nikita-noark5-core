package app.webapp.payload.deserializers.noark5;

import app.domain.interfaces.entities.IFondsCreatorEntity;
import app.domain.noark5.FondsCreator;
import app.webapp.payload.deserializers.SystemIdEntityDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

/**
 * Deserialize an incoming FondsCreator JSON object.
 */
public class FondsCreatorDeserializer
        extends SystemIdEntityDeserializer<FondsCreator> {
    @Override
    public FondsCreator deserialize(JsonParser jsonParser, DeserializationContext dc) throws IOException {
               FondsCreator fondsCreator = new FondsCreator();
        ObjectNode objectNode = mapper.readTree(jsonParser);
        // Deserialize general properties
        deserializeSystemIdEntity(fondsCreator, objectNode, errors);
        deserializeFondsCreator(fondsCreator, objectNode, errors);
        check_payload_at_end(errors, objectNode);
        return fondsCreator;
    }

    protected void deserializeFondsCreator(IFondsCreatorEntity fondsCreatorEntity, ObjectNode objectNode, StringBuilder errors) {
        // Deserialize fondsCreatorId
        JsonNode currentNode = objectNode.get(FONDS_CREATOR_ID);
        if (null != currentNode) {
            fondsCreatorEntity.setFondsCreatorId(currentNode.textValue());
            objectNode.remove(FONDS_CREATOR_ID);
        } else {
            errors.append(FONDS_CREATOR_ID + " is missing. ");
        }
        // Deserialize fondsCreatorName
        currentNode = objectNode.get(FONDS_CREATOR_NAME);
        if (null != currentNode) {
            fondsCreatorEntity.setFondsCreatorName(currentNode.textValue());
            objectNode.remove(FONDS_CREATOR_NAME);
        } else {
            errors.append(FONDS_CREATOR_NAME + " is missing. ");
        }
        // Deserialize description
        currentNode = objectNode.get(DESCRIPTION);
        if (null != currentNode) {
            fondsCreatorEntity.setDescription(currentNode.textValue());
            objectNode.remove(DESCRIPTION);
        }
    }

    @Override
    protected String getType() {
        return FONDS_CREATOR;
    }
}