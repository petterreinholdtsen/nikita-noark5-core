package app.webapp.payload.deserializers.noark5;

import app.domain.interfaces.entities.IMetadataEntity;
import app.domain.noark5.DocumentObject;
import app.domain.noark5.metadata.Format;
import app.domain.noark5.metadata.VariantFormat;
import app.webapp.payload.deserializers.SystemIdEntityDeserializer;
import app.webapp.payload.deserializers.noark5.interfaces.IElectronicSignatureDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

/**
 * Deserialize an incoming DocumentObject JSON object.
 */
public class DocumentObjectDeserializer
        extends SystemIdEntityDeserializer<DocumentObject>
        implements IElectronicSignatureDeserializer {

    @Override
    public DocumentObject deserialize(JsonParser jsonParser, DeserializationContext dc) throws IOException {
               DocumentObject documentObject = new DocumentObject();
        ObjectNode objectNode = mapper.readTree(jsonParser);
        // Deserialize general DocumentObject properties
        deserializeSystemIdEntity(documentObject, objectNode, errors);
        // Deserialize versionNumber
        documentObject.setVersionNumber
                (deserializeInteger(DOCUMENT_OBJECT_VERSION_NUMBER, objectNode,
                        errors, false));
        // Deserialize variantFormat
        IMetadataEntity entity =
                deserializeMetadataValue(objectNode,
                        DOCUMENT_OBJECT_VARIANT_FORMAT,
                        new VariantFormat(),
                        errors, true);
        documentObject.setVariantFormat((VariantFormat) entity);
        // Deserialize format
        entity = deserializeMetadataValue(objectNode, DOCUMENT_OBJECT_FORMAT,
                new Format(), errors, false);
        documentObject.setFormat((Format) entity);
        // Deserialize formatDetails
        JsonNode currentNode = objectNode.get(DOCUMENT_OBJECT_FORMAT_DETAILS);
        if (null != currentNode) {
            documentObject.setFormatDetails(currentNode.textValue());
            objectNode.remove(DOCUMENT_OBJECT_FORMAT_DETAILS);
        }
        // Deserialize checksum
        currentNode = objectNode.get(DOCUMENT_OBJECT_CHECKSUM);
        if (null != currentNode) {
            documentObject.setChecksum(currentNode.textValue());
            objectNode.remove(DOCUMENT_OBJECT_CHECKSUM);
        }
        // Deserialize checksumAlgorithm
        currentNode = objectNode.get(DOCUMENT_OBJECT_CHECKSUM_ALGORITHM);
        if (null != currentNode) {
            documentObject.setChecksumAlgorithm(currentNode.textValue());
            objectNode.remove(DOCUMENT_OBJECT_CHECKSUM_ALGORITHM);
        }
        // Deserialize fileSize
        documentObject.setFileSize
                (deserializeLong(DOCUMENT_OBJECT_FILE_SIZE, objectNode,
                        errors, false));
        // Deserialize filename
        currentNode = objectNode.get(DOCUMENT_OBJECT_FILE_NAME);
        if (null != currentNode) {
            documentObject.setOriginalFilename(currentNode.textValue());
            objectNode.remove(DOCUMENT_OBJECT_FILE_NAME);
        }
        // Deserialize mimeType
        currentNode = objectNode.get(DOCUMENT_OBJECT_MIME_TYPE);
        if (null != currentNode) {
            documentObject.setMimeType(currentNode.textValue());
            objectNode.remove(DOCUMENT_OBJECT_MIME_TYPE);
        }
        documentObject.setReferenceElectronicSignature(
                deserializeElectronicSignature(objectNode, errors));

        check_payload_at_end(errors, objectNode);
        return documentObject;
    }

    @Override
    protected String getType() {
        return DOCUMENT_OBJECT;
    }
}