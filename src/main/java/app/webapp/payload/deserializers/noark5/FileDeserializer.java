package app.webapp.payload.deserializers.noark5;

import app.domain.noark5.File;
import app.domain.noark5.Series;
import app.webapp.payload.deserializers.NoarkGeneralEntityDeserializer;
import app.webapp.payload.deserializers.noark5.interfaces.*;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.util.UUID;

import static app.utils.constants.N5ResourceMappings.*;

/**
 * Deserialize an incoming File JSON object.
 */
public class FileDeserializer
        extends NoarkGeneralEntityDeserializer<File>
        implements IClassifiedDeserializer, IDisposalDeserializer, IDisposalUndertakenDeserializer,
        IDocumentMediumDeserializer, IKeywordDeserializer, IScreeningDeserializer, IStorageLocationDeserializer {

    @Override
    public File deserialize(JsonParser jsonParser, DeserializationContext dc) throws IOException {
               File file = new File();
        ObjectNode objectNode = mapper.readTree(jsonParser);
        // Deserialize general properties
        deserializeNoarkGeneralEntity(file, objectNode, errors);
        deserializeDocumentMedium(file, objectNode, errors);
        deserializeStorageLocation(file, objectNode);
        deserializeKeyword(file, objectNode);

        // Deserialize fileId
        JsonNode currentNode = objectNode.get(FILE_ID);
        if (null != currentNode) {
            file.setFileId(currentNode.textValue());
            objectNode.remove(FILE_ID);
        }
        // Deserialize publicTitle
        currentNode = objectNode.get(FILE_PUBLIC_TITLE);
        if (null != currentNode) {
            file.setPublicTitle(currentNode.textValue());
            objectNode.remove(FILE_PUBLIC_TITLE);
        }
        // TODO: FIX THIS CommondeserializeCrossReference(file, objectNode);
        file.setReferenceDisposal(deserializeDisposal(objectNode, errors));
        file.setReferenceScreening(deserializeScreening(objectNode, errors));
        file.setReferenceClassified(deserializeClassified(objectNode, errors));
        // Deserialize referenceSeries
        currentNode = objectNode.get(REFERENCE_SERIES);
        if (null != currentNode) {
            Series series = new Series();
            String systemID = currentNode.textValue();
            if (systemID != null) {
                series.setSystemId(UUID.fromString(systemID));
            }
            file.setReferenceSeries(series);
            objectNode.remove(REFERENCE_SERIES);
        }
        deserializeBSM(objectNode, file);
        check_payload_at_end(errors, objectNode);
        return file;
    }

    @Override
    protected String getType() {
        return FILE;
    }
}
