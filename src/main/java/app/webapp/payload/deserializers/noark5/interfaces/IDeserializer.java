package app.webapp.payload.deserializers.noark5.interfaces;

import app.domain.interfaces.entities.IMetadataEntity;
import app.webapp.exceptions.NikitaMalformedInputDataException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.time.OffsetDateTime;
import java.time.format.DateTimeParseException;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

import static app.utils.constants.Constants.*;
import static app.utils.constants.ErrorMessagesConstants.STRING_IS_BLANK;
import static app.utils.constants.N5ResourceMappings.CODE;
import static app.utils.constants.N5ResourceMappings.CODE_NAME;
import static java.lang.String.format;
import static java.time.OffsetDateTime.parse;
import static java.time.format.DateTimeFormatter.ISO_OFFSET_DATE_TIME;
import static java.util.UUID.fromString;

public interface IDeserializer
        extends IParseDateTime {

    default Integer deserializeInteger(String fieldname,
                                       ObjectNode objectNode,
                                       StringBuilder errors,
                                       boolean required) {
        JsonNode currentNode = objectNode.get(fieldname);
        if (null != currentNode) {
            objectNode.remove(fieldname);
            if (currentNode.isNumber()) {
                return currentNode.intValue();
            } else {
                errors.append(fieldname);
                errors.append(" (\"");
                errors.append(currentNode.textValue());
                errors.append("\") is not numeric. ");
                return null;
            }
        } else if (required) {
            errors.append(fieldname);
            errors.append(" is missing. ");
        }
        return null;
    }

    default Long deserializeLong(String fieldname,
                                 ObjectNode objectNode,
                                 StringBuilder errors,
                                 boolean required) {
        JsonNode currentNode = objectNode.get(fieldname);
        if (null != currentNode) {
            objectNode.remove(fieldname);
            if (currentNode.isNumber()) {
                return currentNode.longValue();
            } else {
                errors.append(fieldname);
                errors.append(" (\"");
                errors.append(currentNode.textValue());
                errors.append("\") is not numeric. ");
                return null;
            }
        } else if (required) {
            errors.append(fieldname);
            errors.append(" is missing. ");
        }
        return null;
    }

    default UUID deserializeUUID(String fieldname,
                                 ObjectNode objectNode,
                                 StringBuilder errors,
                                 boolean required) {
        JsonNode currentNode = objectNode.get(fieldname);
        if (null != currentNode) {
            objectNode.remove(fieldname);
            if (currentNode.isTextual()) {
                try {
                    return fromString(currentNode.textValue());
                } catch (IllegalArgumentException e) {
                    errors.append(fieldname);
                    errors.append(" (\"");
                    errors.append(currentNode.textValue());
                    errors.append("\") is not valid UUID.");
                    return null;
                }
            } else {
                errors.append(fieldname);
                errors.append(" (\"");
                errors.append(currentNode.textValue());
                errors.append("\") is not numeric. ");
                return null;
            }
        } else if (required) {
            errors.append(fieldname);
            errors.append(" is missing. ");
        }
        return null;
    }

    default OffsetDateTime deserializeDate(String fieldname,
                                           ObjectNode objectNode,
                                           StringBuilder errors) {
        return deserializeDate(fieldname, objectNode, errors, false);
    }

    default OffsetDateTime deserializeDate(String fieldname,
                                           ObjectNode objectNode,
                                           StringBuilder errors,
                                           boolean required) {
        OffsetDateTime d = null;
        JsonNode currentNode = objectNode.get(fieldname);
        if (null != currentNode) {
            try {
                d = deserializeDate(currentNode.textValue());
            } catch (DateTimeParseException e) {
                errors.append("Malformed ");
                errors.append(fieldname);
                errors.append(". Make sure format is either ");
                errors.append(NOARK_DATE_FORMAT_PATTERN + " or ");
                errors.append(NOARK_ZONED_DATE_FORMAT_PATTERN + ". ");
                errors.append("Message is '");
                errors.append(e.getMessage());
                errors.append("'. ");
            }
            objectNode.remove(fieldname);
        } else if (required) {
            errors.append(fieldname);
            errors.append(" is missing. ");
        }
        return d;
    }

    default OffsetDateTime deserializeDateTime(String dateTime) {
        return parse(dateTime, ISO_OFFSET_DATE_TIME);
    }


    default OffsetDateTime deserializeDateTime(String fieldname,
                                               ObjectNode objectNode,
                                               StringBuilder errors,
                                               boolean required) {
        OffsetDateTime d = null;
        JsonNode currentNode = objectNode.get(fieldname);
        if (null != currentNode) {
            try {
                d = deserializeDateTime(currentNode.textValue());
            } catch (DateTimeParseException e) {
                errors.append("Malformed ");
                errors.append(fieldname);
                errors.append(". Make sure format is ");
                errors.append(NOARK_DATE_TIME_FORMAT_PATTERN + " or ");
                errors.append(NOARK_ZONED_DATE_TIME_FORMAT_PATTERN + ". ");
                errors.append("Message is '");
                errors.append(e.getMessage());
                errors.append("'. ");
            }
            objectNode.remove(fieldname);
        } else if (required) {
            errors.append(fieldname);
            errors.append(" is missing. ");
        }
        return d;
    }

    default OffsetDateTime deserializeDateTime(String fieldname,
                                               ObjectNode objectNode,
                                               StringBuilder errors) {
        return deserializeDateTime(fieldname, objectNode, errors, false);
    }


    default String checkNodeObjectEmpty(JsonNode objectNode) {
        StringBuilder result = new StringBuilder();
        if (objectNode.size() != 0) {
            Iterator<Map.Entry<String, JsonNode>> nodes = objectNode.fields();
            while (nodes.hasNext()) {
                Map.Entry<String, JsonNode> entry = nodes.next();
                String keyField = entry.getKey();
                result.append(keyField);
                if (nodes.hasNext()) {
                    result.append(", ");
                }
            }
        }
        return result.toString();
    }

    default IMetadataEntity
    deserializeMetadataValue(ObjectNode objectNode,
                             String parentname,
                             IMetadataEntity entity,
                             StringBuilder errors,
                             Boolean required) {
        JsonNode metadataNode = objectNode.get(parentname);
        if (null != metadataNode) {
            deserializeMetadataValue(
                    metadataNode, parentname, entity, errors);
            if (null != entity.getCode()) {
                objectNode.remove(parentname);
            }
        } else if (required) {
            errors.append(parentname);
            errors.append(" is missing. ");
        }
        return entity;
    }

    default IMetadataEntity deserializeMetadataValue(
            JsonNode metadataNode, String parentname,
            IMetadataEntity entity, StringBuilder errors) {
        JsonNode node = metadataNode.get(CODE);
        if (null != node) {
            entity.setCode(node.textValue());
        } else {
            errors.append(parentname);
            errors.append(".");
            errors.append(CODE);
            errors.append(" is missing. ");
        }
        node = metadataNode.get(CODE_NAME);
        if (null != node) {
            entity.setCodeName(node.textValue());
        }
        return entity;
    }

    default void rejectIfEmptyOrWhitespace(String stringToCheck, String attribute) {
        if (stringToCheck.isBlank()) {
            throw new NikitaMalformedInputDataException(format(STRING_IS_BLANK, attribute));
        }
    }
}
