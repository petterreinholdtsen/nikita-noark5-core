package app.webapp.payload.deserializers.noark5;

import app.domain.noark5.Series;
import app.domain.noark5.metadata.SeriesStatus;
import app.webapp.payload.deserializers.NoarkGeneralEntityDeserializer;
import app.webapp.payload.deserializers.noark5.interfaces.*;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.util.UUID;

import static app.utils.constants.N5ResourceMappings.*;

/**
 * Deserialize an incoming Series JSON object.
 */
public class SeriesDeserializer
        extends NoarkGeneralEntityDeserializer<Series>
        implements IClassifiedDeserializer, IDeletionDeserializer, IDisposalDeserializer, IDisposalUndertakenDeserializer,
        IDocumentMediumDeserializer, IScreeningDeserializer, IStorageLocationDeserializer {
    @Override
    public Series deserialize(JsonParser jsonParser, DeserializationContext dc)
            throws IOException {
               Series series = new Series();
        ObjectNode objectNode = mapper.readTree(jsonParser);
        // Deserialize general properties
        deserializeNoarkGeneralEntity(series, objectNode, errors);
        deserializeDocumentMedium(series, objectNode, errors);
        deserializeStorageLocation(series, objectNode);
        // Deserialize seriesStatus
        SeriesStatus seriesStatus = (SeriesStatus)
                deserializeMetadataValue(objectNode, SERIES_STATUS,
                        new SeriesStatus(), errors, false);
        series.setSeriesStatus(seriesStatus);
        // Deserialize seriesStartDate
        series.setSeriesStartDate(deserializeDateTime(SERIES_START_DATE,
                objectNode, errors));
        // Deserialize seriesEndDate
        series.setSeriesEndDate(deserializeDateTime(SERIES_END_DATE,
                objectNode, errors));
        // Deserialize referencePrecursor
        JsonNode currentNode = objectNode.get(SERIES_ASSOCIATE_AS_PRECURSOR);
        if (null != currentNode) {
            series.setReferencePrecursorSystemID
                    (UUID.fromString(currentNode.textValue()));
            objectNode.remove(SERIES_ASSOCIATE_AS_PRECURSOR);
        }
        // Deserialize referenceSuccessor
        currentNode = objectNode.get(SERIES_ASSOCIATE_AS_SUCCESSOR);
        if (null != currentNode) {
            series.setReferenceSuccessorSystemID
                    (UUID.fromString(currentNode.textValue()));
            objectNode.remove(SERIES_ASSOCIATE_AS_SUCCESSOR);
        }
        series.setReferenceDisposal(deserializeDisposal(objectNode, errors));
        series.setDisposalUndertaken(
                deserializeDisposalUndertaken(objectNode, errors));
        series.setReferenceDeletion(deserializeDeletion(objectNode, errors));
        series.setReferenceScreening(deserializeScreening(objectNode, errors));
        series.setReferenceClassified(deserializeClassified(objectNode, errors));
        check_payload_at_end(errors, objectNode);
        return series;
    }

    @Override
    protected String getType() {
        return SERIES;
    }
}