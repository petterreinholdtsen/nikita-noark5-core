package app.webapp.payload.deserializers.noark5.secondary;

import app.domain.noark5.secondary.CrossReference;
import app.webapp.payload.deserializers.SystemIdEntityDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;
import static java.util.UUID.fromString;

public class CrossReferenceDeserializer
        extends SystemIdEntityDeserializer<CrossReference> {
    @Override
    public CrossReference deserialize(JsonParser jsonParser, DeserializationContext dc) throws IOException {
               CrossReference crossReference = new CrossReference();
        ObjectNode objectNode = mapper.readTree(jsonParser);
        // deserialize systemID
        deserializeNoarkSystemIdEntity(crossReference, objectNode);
        deserializeNoarkCreateEntity(crossReference, objectNode, errors);
        // deserialize fraSystemID
        JsonNode currentNode = objectNode.get(FROM_SYSTEM_ID);
        if (null != currentNode) {
            crossReference.setFromSystemId(fromString(currentNode.textValue()));
            objectNode.remove(FROM_SYSTEM_ID);
        }
        // deserialize fraSystemID
        currentNode = objectNode.get(TO_SYSTEM_ID);
        if (null != currentNode) {
            crossReference.setToSystemId(fromString(currentNode.textValue()));
            objectNode.remove(TO_SYSTEM_ID);
        }
        // deserialize referanseType
        currentNode = objectNode.get(REFERENCE_TYPE);
        if (null != currentNode) {
            crossReference.setReferenceType(currentNode.textValue());
            objectNode.remove(REFERENCE_TYPE);
        }
        check_payload_at_end(errors, objectNode);
        return crossReference;
    }

    @Override
    protected String getType() {
        return CROSS_REFERENCE;
    }
}