package app.webapp.payload.deserializers.noark5.interfaces;

import app.domain.interfaces.entities.admin.IUserEntity;
import app.domain.interfaces.entities.secondary.*;
import app.domain.noark5.casehandling.secondary.*;
import app.domain.noark5.metadata.PartRole;
import app.domain.noark5.secondary.PartPerson;
import app.utils.constants.N5ResourceMappings;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import static app.utils.constants.N5ResourceMappings.*;

public interface IPartDeserializer
        extends IDeserializer {


    default void deserializeContactInformationEntity(
            IContactInformationEntity contactInformation,
            ObjectNode objectNode) {
        if (contactInformation != null) {
            // Deserialize telefonnummer
            JsonNode currentNode = objectNode.get(TELEPHONE_NUMBER);
            if (null != currentNode) {
                contactInformation.setTelephoneNumber(currentNode.textValue());
                objectNode.remove(TELEPHONE_NUMBER);
            }
            // Deserialize epostadresse
            currentNode = objectNode.get(EMAIL_ADDRESS);
            if (null != currentNode) {
                contactInformation.setEmailAddress(currentNode.textValue());
                objectNode.remove(EMAIL_ADDRESS);
            }
            // Deserialize mobiltelefon
            currentNode = objectNode.get(MOBILE_TELEPHONE_NUMBER);
            if (null != currentNode) {
                contactInformation.setMobileTelephoneNumber(currentNode.textValue());
                objectNode.remove(MOBILE_TELEPHONE_NUMBER);
            }
        }
    }

    default void deserializeSimpleAddressEntity(
            String addressType,
            ISimpleAddressEntity simpleAddress, ObjectNode objectNode,
            StringBuilder errors) {
        if (simpleAddress != null) {
            // Deserialize adresselinje1
            JsonNode currentNode = objectNode.get(ADDRESS_LINE_1);
            if (null != currentNode) {
                simpleAddress.setAddressLine1(currentNode.textValue());
                objectNode.remove(ADDRESS_LINE_1);
            }
            // Deserialize adresselinje2
            currentNode = objectNode.get(ADDRESS_LINE_2);
            if (null != currentNode) {
                simpleAddress.setAddressLine2(currentNode.textValue());
                objectNode.remove(ADDRESS_LINE_2);
            }
            // Deserialize adresselinje3
            currentNode = objectNode.get(ADDRESS_LINE_3);
            if (null != currentNode) {
                simpleAddress.setAddressLine3(currentNode.textValue());
                objectNode.remove(ADDRESS_LINE_3);
            }
            // Deserialize postnummer
            currentNode = objectNode.get(POSTAL_NUMBER);
            if (null != currentNode) {
                simpleAddress.setPostalNumber(
                        new PostalNumber(currentNode.textValue()));
                objectNode.remove(POSTAL_NUMBER);
            }
            // Deserialize poststed
            currentNode = objectNode.get(POSTAL_TOWN);
            if (null != currentNode) {
                simpleAddress.setPostalTown(currentNode.textValue());
                objectNode.remove(POSTAL_TOWN);
            } else {
                errors.append(addressType);
                errors.append(".");
                errors.append(POSTAL_TOWN);
                errors.append(" is missing. ");
            }
            // Deserialize landkode
            currentNode = objectNode.get(COUNTRY_CODE);
            if (null != currentNode) {
                simpleAddress.setCountryCode(currentNode.textValue());
                objectNode.remove(COUNTRY_CODE);
            }
        }
    }

    default void deserializePartPersonEntity(
            IPartPersonEntity partPersonEntity,
            ObjectNode objectNode, StringBuilder errors) {
        deserializePartRole(
                partPersonEntity, objectNode, errors);
        deserializeGenericPersonEntity(partPersonEntity, objectNode);
        // Deserialize postalAddress
        JsonNode currentNode = objectNode.get(POSTAL_ADDRESS);
        if (null != currentNode) {
            PostalAddress postalAddress = new PostalAddress();
            SimpleAddress simpleAddress = new SimpleAddress();
            deserializeSimpleAddressEntity(POSTAL_ADDRESS,
                    simpleAddress, currentNode.deepCopy(), errors);
            postalAddress.setSimpleAddress(simpleAddress);
            postalAddress.setPartPerson((PartPerson) partPersonEntity);
            partPersonEntity.setPostalAddress(postalAddress);
            objectNode.remove(N5ResourceMappings.POSTAL_ADDRESS);
        }
        // Deserialize residingAddress
        currentNode = objectNode.get(RESIDING_ADDRESS);
        if (null != currentNode) {
            ResidingAddress residingAddress =
                    new ResidingAddress();
            SimpleAddress simpleAddress = new SimpleAddress();
            deserializeSimpleAddressEntity(RESIDING_ADDRESS,
                    simpleAddress, currentNode.deepCopy(), errors);
            residingAddress.setSimpleAddress(simpleAddress);
            residingAddress.setPartPerson((PartPerson) partPersonEntity);
            partPersonEntity.setResidingAddress(residingAddress);
            objectNode.remove(RESIDING_ADDRESS);
        }
        // Deserialize kontaktinformasjon
        currentNode = objectNode.get(CONTACT_INFORMATION);
        if (null != currentNode) {
            ContactInformation contactInformation =
                    new ContactInformation();
            deserializeContactInformationEntity(
                    contactInformation, currentNode.deepCopy());
            partPersonEntity.
                    setContactInformation(contactInformation);
            objectNode.remove(CONTACT_INFORMATION);
        }
    }

    default void deserializePartUnitEntity(
            IPartUnitEntity partUnit,
            ObjectNode objectNode, StringBuilder errors) {
        deserializePartRole(partUnit, objectNode, errors);
        // Deserialize kontaktperson
        JsonNode currentNode = objectNode.get(CONTACT_PERSON);
        if (null != currentNode) {
            partUnit.setContactPerson(currentNode.textValue());
            objectNode.remove(CONTACT_PERSON);
        }
        // Deserialize navn
        currentNode = objectNode.get(NAME);
        if (null != currentNode) {
            partUnit.setName(currentNode.textValue());
            objectNode.remove(NAME);
        }
        // Deserialize organisasjonsnummer
        currentNode = objectNode.get(UNIT_IDENTIFIER);
        if (null != currentNode) {
            JsonNode node = currentNode.get(ORGANISATION_NUMBER);
            if (null != node) {
                partUnit.setUnitIdentifier(node.textValue());
                // This remove() call is placed inside block
                // to report error if no organisasjonsnummer
                // was found.
                objectNode.remove(UNIT_IDENTIFIER);
            }
        }
        // Deserialize kontaktperson
        currentNode = objectNode.get(CONTACT_PERSON);
        if (null != currentNode) {
            partUnit.setContactPerson(currentNode.textValue());
            objectNode.remove(CONTACT_PERSON);
        }
        // Deserialize postadresse
        currentNode = objectNode.get(POSTAL_ADDRESS);
        if (null != currentNode) {
            PostalAddress postalAddressEntity = new PostalAddress();
            SimpleAddress simpleAddress = new SimpleAddress();
            deserializeSimpleAddressEntity(POSTAL_ADDRESS,
                    simpleAddress, currentNode.deepCopy(), errors);
            postalAddressEntity.setSimpleAddress(simpleAddress);
            partUnit.setPostalAddress(postalAddressEntity);
            objectNode.remove(POSTAL_ADDRESS);
        }
        // Deserialize forretningsadresse
        currentNode = objectNode.get(BUSINESS_ADDRESS);
        if (null != currentNode) {
            BusinessAddress businessAddressEntity =
                    new BusinessAddress();
            SimpleAddress simpleAddress = new SimpleAddress();
            deserializeSimpleAddressEntity(BUSINESS_ADDRESS,
                    simpleAddress, currentNode.deepCopy(), errors);
            businessAddressEntity.setSimpleAddress(simpleAddress);
            partUnit.setBusinessAddress(
                    businessAddressEntity);
            objectNode.remove(BUSINESS_ADDRESS);
        }
        // Deserialize kontaktinformasjon
        currentNode = objectNode.get(CONTACT_INFORMATION);
        if (null != currentNode) {
            ContactInformation contactInformation =
                    new ContactInformation();
            deserializeContactInformationEntity(
                    contactInformation, currentNode.deepCopy());
            partUnit.setContactInformation(contactInformation);
            objectNode.remove(CONTACT_INFORMATION);
        }
    }

    default void deserializeGenericPersonEntity(
            IGenericPersonEntity person, ObjectNode objectNode) {
        JsonNode currentNode = objectNode.get(PERSON_IDENTIFIER);
        if (null != currentNode) {
            // Deserialize foedselsnummer
            JsonNode node = currentNode.get(SOCIAL_SECURITY_NUMBER);
            if (null != node) {
                person.setSocialSecurityNumber(node.textValue());
            }
            // Deserialize dnummer
            node = currentNode.get(D_NUMBER_FIELD);
            if (null != node) {
                person.setdNumber(node.textValue());
            }
            objectNode.remove(PERSON_IDENTIFIER);
        }
        // Deserialize navn
        currentNode = objectNode.get(NAME);
        if (null != currentNode) {
            person.setName(currentNode.textValue());
            objectNode.remove(NAME);
        }
    }


    default void deserializePartRole(
            IPartEntity part,
            ObjectNode objectNode, StringBuilder errors) {
        // Deserialize partrole
        PartRole entity = (PartRole)
                deserializeMetadataValue(objectNode,
                        PART_ROLE_FIELD,
                        new PartRole(),
                        errors, true);
        part.setPartRole(entity);
    }


    default void deserializeUserEntity(
            IUserEntity user, ObjectNode objectNode,
            StringBuilder errors) {
        // TODO implement
    }
}
