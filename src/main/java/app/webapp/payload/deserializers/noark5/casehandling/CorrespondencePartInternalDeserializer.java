package app.webapp.payload.deserializers.noark5.casehandling;

import app.domain.interfaces.entities.secondary.ICorrespondencePartInternalEntity;
import app.domain.noark5.admin.AdministrativeUnit;
import app.domain.noark5.admin.User;
import app.domain.noark5.casehandling.secondary.CorrespondencePart;
import app.domain.noark5.casehandling.secondary.CorrespondencePartInternal;
import app.webapp.payload.deserializers.noark5.secondary.CorrespondencePartDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

/**
 * Deserialize an incoming CorrespondencePart JSON object.
 */
public class CorrespondencePartInternalDeserializer<P extends CorrespondencePart>
        extends CorrespondencePartDeserializer<CorrespondencePart> {
    @Override
    public CorrespondencePartInternal deserialize(JsonParser jsonParser, DeserializationContext dc)
            throws IOException {
               CorrespondencePartInternal correspondencePartInternal =
                       new CorrespondencePartInternal();
        ObjectNode objectNode = mapper.readTree(jsonParser);
        deserializeNoarkSystemIdEntity(correspondencePartInternal, objectNode);
        deserializeCorrespondencePartInternalEntity(correspondencePartInternal, objectNode, errors);
        deserializeBSM(objectNode, correspondencePartInternal);
        check_payload_at_end(errors, objectNode);
        return correspondencePartInternal;
    }

    private void deserializeCorrespondencePartInternalEntity(
            ICorrespondencePartInternalEntity
                    correspondencePartInternal,
            ObjectNode objectNode, StringBuilder errors) {
        deserializeCorrespondencePartType(
                correspondencePartInternal, objectNode, errors);
        // Deserialize administrativEnhet
        JsonNode currentNode =
                objectNode.get(ADMINISTRATIVE_UNIT_FIELD);
        if (null != currentNode) {
            correspondencePartInternal.setAdministrativeUnit(
                    currentNode.textValue());
            objectNode.remove(ADMINISTRATIVE_UNIT_FIELD);
        }
        // Deserialize saksbehandler
        currentNode = objectNode.get(CASE_HANDLER);
        if (null != currentNode) {
            correspondencePartInternal.setCaseHandler(currentNode.textValue());
            objectNode.remove(CASE_HANDLER);
        }
        // Deserialize referanseAdministratitivEnhet
        currentNode = objectNode.get(REFERENCE_ADMINISTRATIVE_UNIT);
        if (null != currentNode) {
            AdministrativeUnit administrativeUnit = new AdministrativeUnit();
            deserializeAdministrativeUnitEntity(administrativeUnit, currentNode.deepCopy(), errors);
            objectNode.remove(REFERENCE_ADMINISTRATIVE_UNIT);
        }
        // Deserialize referanseSaksbehandler
        currentNode = objectNode.get(REFERENCE_CASE_HANDLER);
        if (null != currentNode) {
            User user = new User();
            deserializeUserEntity(user, currentNode.deepCopy(), errors);
            //correspondencePartInternal.setReferenceUser(user);
            objectNode.remove(REFERENCE_CASE_HANDLER);
        }
    }

    @Override
    protected String getType() {
        return CORRESPONDENCE_PART_INTERNAL;
    }
}