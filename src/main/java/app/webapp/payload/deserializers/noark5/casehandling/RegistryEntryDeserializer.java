package app.webapp.payload.deserializers.noark5.casehandling;

import app.domain.noark5.casehandling.RegistryEntry;
import app.domain.noark5.metadata.RegistryEntryStatus;
import app.domain.noark5.metadata.RegistryEntryType;
import app.webapp.payload.deserializers.SystemIdEntityDeserializer;
import app.webapp.payload.deserializers.noark5.interfaces.*;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

/**
 * Deserialize an incoming RegistryEntry JSON object.
 */
public class RegistryEntryDeserializer
        extends SystemIdEntityDeserializer<RegistryEntry>
        implements IClassifiedDeserializer, IDocumentMediumDeserializer, IDisposalDeserializer,
        IElectronicSignatureDeserializer, IScreeningDeserializer {
    @Override
    public RegistryEntry deserialize(JsonParser jsonParser, DeserializationContext dc)
            throws IOException {
               RegistryEntry registryEntry = new RegistryEntry();
        ObjectNode objectNode = mapper.readTree(jsonParser);
        // Deserialize general registryEntry properties
        deserializeSystemIdEntity(registryEntry, objectNode, errors);
        // Deserialize archivedBy
        JsonNode currentNode = objectNode.get(RECORD_ARCHIVED_BY);
        if (null != currentNode) {
            registryEntry.setArchivedBy(currentNode.textValue());
            objectNode.remove(RECORD_ARCHIVED_BY);
        }
        // Deserialize archivedDate
        registryEntry.setArchivedDate(deserializeDateTime(RECORD_ARCHIVED_DATE, objectNode, errors));
        // Deserialize general Record properties
        // Deserialize recordId
        currentNode = objectNode.get(RECORD_ID);
        if (null != currentNode) {
            registryEntry.setRecordId(currentNode.textValue());
            objectNode.remove(RECORD_ID);
        }
        // Deserialize title (not using nikita.utils to preserve order)
        currentNode = objectNode.get(TITLE);
        if (null != currentNode) {
            registryEntry.setTitle(currentNode.textValue());
            objectNode.remove(TITLE);
        }
        // Deserialize  publicTitle
        currentNode = objectNode.get(FILE_PUBLIC_TITLE);
        if (null != currentNode) {
            registryEntry.setPublicTitle(currentNode.textValue());
            objectNode.remove(FILE_PUBLIC_TITLE);
        }
        // Deserialize description
        currentNode = objectNode.get(DESCRIPTION);
        if (null != currentNode) {
            registryEntry.setDescription(currentNode.textValue());
            objectNode.remove(DESCRIPTION);
        }
        deserializeDocumentMedium(registryEntry, objectNode, errors);
        // Deserialize general registryEntry properties
        // Deserialize recordYear
        registryEntry.setRecordYear(deserializeInteger(REGISTRY_ENTRY_YEAR, objectNode, errors, false));
        // Deserialize recordSequenceNumber
        registryEntry.setRecordSequenceNumber
                (deserializeInteger(REGISTRY_ENTRY_SEQUENCE_NUMBER, objectNode, errors, false));
        // Deserialize registryEntryNumber
        registryEntry.setRegistryEntryNumber
                (deserializeInteger(REGISTRY_ENTRY_NUMBER,
                        objectNode, errors, false));
        // Deserialize registryEntryType
        RegistryEntryType registryEntryType = (RegistryEntryType)
                deserializeMetadataValue(objectNode, REGISTRY_ENTRY_TYPE, new RegistryEntryType(), errors, true);
        registryEntry.setRegistryEntryType(registryEntryType);
        // Deserialize RegistryEntryStatus
        RegistryEntryStatus registryEntryStatus = (RegistryEntryStatus)
                deserializeMetadataValue(objectNode, REGISTRY_ENTRY_STATUS, new RegistryEntryStatus(), errors, true);
        registryEntry.setRegistryEntryStatus(registryEntryStatus);
        // Deserialize recordDate
        registryEntry.setRecordDate(
                deserializeDateTime(REGISTRY_ENTRY_DATE, objectNode, errors));
        // Deserialize documentDate
        registryEntry.setDocumentDate(deserializeDateTime(REGISTRY_ENTRY_DOCUMENT_DATE, objectNode, errors));
        // Deserialize receivedDate
        registryEntry.setReceivedDate(deserializeDateTime(REGISTRY_ENTRY_RECEIVED_DATE, objectNode, errors));
        // Deserialize sentDate
        registryEntry.setSentDate(deserializeDateTime(REGISTRY_ENTRY_SENT_DATE, objectNode, errors));
        // Deserialize dueDate
        registryEntry.setDueDate(deserializeDateTime(REGISTRY_ENTRY_DUE_DATE, objectNode, errors));
        // Deserialize freedomAssessmentDate
        registryEntry.setFreedomAssessmentDate(deserializeDateTime(REGISTRY_ENTRY_RECORD_FREEDOM_ASSESSMENT_DATE,
                objectNode, errors));
        // Deserialize numberOfAttachments
        registryEntry.setNumberOfAttachments
                (deserializeInteger(REGISTRY_ENTRY_NUMBER_OF_ATTACHMENTS,
                        objectNode, errors, false));
        // Deserialize loanedDate
        registryEntry.setLoanedDate(
                deserializeDateTime(CASE_LOANED_DATE, objectNode, errors));
        // Deserialize loaneLinks
        currentNode = objectNode.get(CASE_LOANED_TO);
        if (null != currentNode) {
            registryEntry.setLoaneLinks(currentNode.textValue());
            objectNode.remove(CASE_LOANED_TO);
        }
        // Deserialize recordsManagementUnit
        currentNode = objectNode.get(CASE_RECORDS_MANAGEMENT_UNIT);
        if (null != currentNode) {
            registryEntry.setRecordsManagementUnit(currentNode.textValue());
            objectNode.remove(CASE_RECORDS_MANAGEMENT_UNIT);
        }
        registryEntry.setReferenceDisposal(deserializeDisposal(objectNode, errors));
        registryEntry.setReferenceScreening(deserializeScreening(objectNode, errors));
        registryEntry.setReferenceClassified(deserializeClassified(objectNode, errors));
        registryEntry.setReferenceElectronicSignature(deserializeElectronicSignature(objectNode, errors));
        check_payload_at_end(errors, objectNode);
        return registryEntry;
    }

    @Override
    protected String getType() {
        return REGISTRY_ENTRY;
    }
}