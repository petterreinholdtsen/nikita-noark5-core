package app.webapp.payload.deserializers.noark5.secondary;

import app.domain.noark5.metadata.CommentType;
import app.domain.noark5.secondary.Comment;
import app.webapp.payload.deserializers.SystemIdEntityDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

public class CommentDeserializer
        extends SystemIdEntityDeserializer<Comment> {
    @Override
    public Comment deserialize(JsonParser jsonParser, DeserializationContext dc) throws IOException {
               Comment comment = new Comment();
        ObjectNode objectNode = mapper.readTree(jsonParser);
        // Deserialize systemID
        deserializeNoarkSystemIdEntity(comment, objectNode);
        // Deserialize merknadstekst
        JsonNode currentNode = objectNode.get(COMMENT_TEXT);
        if (null != currentNode) {
            comment.setCommentText(currentNode.textValue());
            objectNode.remove(COMMENT_TEXT);
        } else {
            errors.append(COMMENT_TEXT + " is missing. ");
        }
        // Deserialize merknadstype
        CommentType commentType = (CommentType) deserializeMetadataValue(objectNode, COMMENT_TYPE, new CommentType(),
                errors, false);
        if (null != commentType.getCode()) {
            comment.setCommentType(commentType);
        }
        // Deserialize merknadsdato
        comment.setCommentDate(deserializeDateTime(COMMENT_DATE, objectNode, errors, false));
        // Deserialize merknadRegistrertAv
        currentNode = objectNode.get(COMMENT_REGISTERED_BY);
        if (null != currentNode) {
            comment.setCommentRegisteredBy(currentNode.textValue());
            objectNode.remove(COMMENT_REGISTERED_BY);
        }
        check_payload_at_end(errors, objectNode);
        return comment;
    }

    @Override
    protected String getType() {
        return COMMENT;
    }
}