package app.webapp.payload.deserializers.noark5.casehandling;

import app.domain.noark5.casehandling.RecordNote;
import app.webapp.payload.deserializers.SystemIdEntityDeserializer;
import app.webapp.payload.deserializers.noark5.interfaces.IClassifiedDeserializer;
import app.webapp.payload.deserializers.noark5.interfaces.IDisposalDeserializer;
import app.webapp.payload.deserializers.noark5.interfaces.IDocumentMediumDeserializer;
import app.webapp.payload.deserializers.noark5.interfaces.IScreeningDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

/**
 * Deserialize an incoming RecordNote JSON object.
 */
public class RecordNoteDeserializer
        extends SystemIdEntityDeserializer<RecordNote>
        implements IClassifiedDeserializer, IDocumentMediumDeserializer, IDisposalDeserializer, IScreeningDeserializer {
    @Override
    public RecordNote deserialize(JsonParser jsonParser, DeserializationContext dc) throws IOException {
               RecordNote recordNote = new RecordNote();
        ObjectNode objectNode = mapper.readTree(jsonParser);

        // Deserialize general recordNote properties
        deserializeSystemIdEntity(recordNote, objectNode, errors);

        // Deserialize archivedBy
        JsonNode currentNode = objectNode.get(RECORD_ARCHIVED_BY);
        if (null != currentNode) {
            recordNote.setArchivedBy(currentNode.textValue());
            objectNode.remove(RECORD_ARCHIVED_BY);
        }
        // Deserialize archivedDate
        recordNote.setArchivedDate(deserializeDateTime(RECORD_ARCHIVED_DATE, objectNode, errors));
        // Deserialize general Record properties
        // Deserialize recordId
        currentNode = objectNode.get(RECORD_ID);
        if (null != currentNode) {
            recordNote.setRecordId(currentNode.textValue());
            objectNode.remove(RECORD_ID);
        }
        // Deserialize title (not using nikita.utils to preserve order)
        currentNode = objectNode.get(TITLE);
        if (null != currentNode) {
            recordNote.setTitle(currentNode.textValue());
            objectNode.remove(TITLE);
        }
        // Deserialize  publicTitle
        currentNode = objectNode.get(FILE_PUBLIC_TITLE);
        if (null != currentNode) {
            recordNote.setPublicTitle(currentNode.textValue());
            objectNode.remove(FILE_PUBLIC_TITLE);
        }
        // Deserialize description
        currentNode = objectNode.get(DESCRIPTION);
        if (null != currentNode) {
            recordNote.setDescription(currentNode.textValue());
            objectNode.remove(DESCRIPTION);
        }
        deserializeDocumentMedium(recordNote, objectNode, errors);

        // Deserialize documentDate
        recordNote.setDocumentDate(deserializeDateTime(REGISTRY_ENTRY_DOCUMENT_DATE, objectNode, errors));
        // Deserialize receivedDate
        recordNote.setReceivedDate(deserializeDateTime(REGISTRY_ENTRY_RECEIVED_DATE, objectNode, errors));
        // Deserialize sentDate
        recordNote.setSentDate(deserializeDateTime(REGISTRY_ENTRY_SENT_DATE, objectNode, errors));
        // Deserialize dueDate
        recordNote.setDueDate(deserializeDateTime(REGISTRY_ENTRY_DUE_DATE, objectNode, errors));
        // Deserialize freedomAssessmentDate
        recordNote.setFreedomAssessmentDate(
                deserializeDateTime(REGISTRY_ENTRY_RECORD_FREEDOM_ASSESSMENT_DATE, objectNode, errors));
        // Deserialize numberOfAttachments
        recordNote.setNumberOfAttachments(
                deserializeInteger(REGISTRY_ENTRY_NUMBER_OF_ATTACHMENTS, objectNode, errors, false));
        // Deserialize loanedDate
        recordNote.setLoanedDate(deserializeDateTime(CASE_LOANED_DATE, objectNode, errors));
        // Deserialize loanedTo
        currentNode = objectNode.get(CASE_LOANED_TO);
        if (null != currentNode) {
            recordNote.setLoaneLinks(currentNode.textValue());
            objectNode.remove(CASE_LOANED_TO);
        }
        recordNote.setReferenceDisposal(deserializeDisposal(objectNode, errors));
        recordNote.setReferenceScreening(deserializeScreening(objectNode, errors));
        recordNote.setReferenceClassified(deserializeClassified(objectNode, errors));
        check_payload_at_end(errors, objectNode);
        return recordNote;
    }

    @Override
    protected String getType() {
        return RECORD_NOTE;
    }
}