package app.webapp.payload.deserializers.noark5.secondary;

import app.domain.noark5.metadata.SignOffMethod;
import app.domain.noark5.secondary.SignOff;
import app.webapp.payload.deserializers.SystemIdEntityDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.util.UUID;

import static app.utils.constants.N5ResourceMappings.*;

public class SignOffDeserializer
        extends SystemIdEntityDeserializer<SignOff> {
    @Override
    public SignOff deserialize(JsonParser jsonParser, DeserializationContext dc) throws IOException {
               SignOff signOff = new SignOff();
        ObjectNode objectNode = mapper.readTree(jsonParser);
        // Deserialize systemID
        deserializeNoarkSystemIdEntity(signOff, objectNode);
        deserializeNoarkCreateEntity(signOff, objectNode, errors);
        // Deserialize avskrivningsdato
        signOff.setSignOffDate(deserializeDateTime(SIGN_OFF_DATE, objectNode, errors));
        // Deserialize avskrevetAv
        JsonNode currentNode = objectNode.get(SIGN_OFF_BY);
        if (null != currentNode) {
            signOff.setSignOffBy(currentNode.textValue());
            objectNode.remove(SIGN_OFF_BY);
        }
        // Deserialize avskrivningsmetode
        SignOffMethod entity = (SignOffMethod)
                deserializeMetadataValue(objectNode,
                        SIGN_OFF_METHOD,
                        new SignOffMethod(),
                        errors, true);
        signOff.setSignOffMethod(entity);
        // TODO handle referanseAvskrevetAv
        // Deserialize referanseAvskrivesAvJournalpost
        UUID referenceSignedOffRecord =
                deserializeUUID(SIGN_OFF_REFERENCE_RECORD,
                        objectNode, errors, true);
        if (null != referenceSignedOffRecord) {
            signOff.setReferenceSignedOffRecordSystemID
                    (referenceSignedOffRecord);
        }
        // Deserialize referanseAvskrivesAvKorrespondansepart.
        UUID referenceSignedOffCorrespondencePart =
                deserializeUUID(SIGN_OFF_REFERENCE_CORRESPONDENCE_PART,
                        objectNode, errors, false);
        if (null != referenceSignedOffCorrespondencePart) {
            signOff.setReferenceSignedOffCorrespondencePartSystemID
                    (referenceSignedOffCorrespondencePart);
        }
        check_payload_at_end(errors, objectNode);
        return signOff;
    }

    @Override
    protected String getType() {
        return SIGN_OFF;
    }
}