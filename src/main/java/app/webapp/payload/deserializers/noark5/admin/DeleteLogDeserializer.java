package app.webapp.payload.deserializers.noark5.admin;

import app.domain.noark5.admin.DeleteLog;
import app.webapp.payload.deserializers.noark5.EventLogDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.DELETE_LOG;

public class DeleteLogDeserializer
        extends EventLogDeserializer<DeleteLog> {
    @Override
    public DeleteLog deserialize(JsonParser jsonParser, DeserializationContext dc)
            throws IOException {
        DeleteLog deleteLog = new DeleteLog();
        ObjectNode objectNode = mapper.readTree(jsonParser);
        deserializeEventLog(deleteLog, objectNode, errors);
        check_payload_at_end(errors, objectNode);
        return deleteLog;
    }

    @Override
    protected String getType() {
        return DELETE_LOG;
    }
}