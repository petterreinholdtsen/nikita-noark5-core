package app.service.interfaces;

import app.domain.noark5.EventLog;
import app.domain.noark5.SystemIdEntity;
import app.webapp.payload.links.EventLogLinks;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public interface IEventLogService {

    EventLogLinks generateDefaultEventLog(@NotNull final SystemIdEntity entity);

    EventLogLinks createNewEventLog(@NotNull final EventLog eventLog,
                                    @NotNull final SystemIdEntity entity);

    EventLogLinks findEventLogByOwner();

    EventLogLinks findSingleEventLog(@NotNull final UUID systemId);

    EventLogLinks handleUpdate(@NotNull final UUID systemId,
                               @NotNull final EventLog incomingEventLog);

    void deleteEntity(@NotNull final UUID systemId);
}
