package app.service.interfaces;

import app.domain.noark5.admin.Organisation;
import app.webapp.payload.links.admin.OrganisationLinks;
import jakarta.validation.constraints.NotNull;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

public interface IOrganisationService {
    @Transactional
    void save(Organisation organisation);

    OrganisationLinks findAll();

    OrganisationLinks findBySystemId(@NotNull UUID systemId);

    Organisation findOrganisationBySystemId(@NotNull UUID systemId);

    @Transactional
    OrganisationLinks update(UUID systemId, Organisation incomingOrganisation);

    @Transactional
    void deleteEntity(@NotNull UUID systemId);

    OrganisationLinks generateDefaultOrganisation();
}
