package app.service.interfaces.secondary;

import app.domain.noark5.metadata.Metadata;
import app.domain.noark5.secondary.Screening;
import app.webapp.payload.links.secondary.ScreeningMetadataLinks;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public interface IScreeningMetadataService {

    ScreeningMetadataLinks createScreeningMetadata(
            @NotNull final Screening screening,
            @NotNull final Metadata screeningMetadata);

    ScreeningMetadataLinks findAll();

    ScreeningMetadataLinks findBySystemId(@NotNull final UUID systemId);

    ScreeningMetadataLinks updateScreeningMetadataBySystemId(
            @NotNull final UUID systemId, @NotNull final Metadata screeningMetadata);

    void deleteScreeningMetadataBySystemId(@NotNull final UUID systemId);

    ScreeningMetadataLinks getDefaultScreeningMetadata(
            @NotNull final UUID systemId);
}
