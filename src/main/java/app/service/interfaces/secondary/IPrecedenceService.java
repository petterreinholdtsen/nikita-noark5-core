package app.service.interfaces.secondary;

import app.domain.noark5.secondary.Precedence;
import app.webapp.payload.links.secondary.PrecedenceLinks;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public interface IPrecedenceService {

    PrecedenceLinks updatePrecedenceBySystemId(
            @NotNull final UUID systemId,
            @NotNull final Precedence incomingPrecedence);

    PrecedenceLinks createNewPrecedence(
            @NotNull final Precedence entity);

    void deletePrecedenceBySystemId(@NotNull final UUID systemId);

    PrecedenceLinks findAll();

    PrecedenceLinks findBySystemId(
            @NotNull final UUID systemId);

    PrecedenceLinks generateDefaultPrecedence();

    boolean deletePrecedenceIfNotEmpty(@NotNull final Precedence precedence);
}
