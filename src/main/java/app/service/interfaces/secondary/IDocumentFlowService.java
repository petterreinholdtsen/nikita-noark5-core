package app.service.interfaces.secondary;

import app.domain.noark5.casehandling.RecordNote;
import app.domain.noark5.casehandling.RegistryEntry;
import app.domain.noark5.secondary.DocumentFlow;
import app.webapp.payload.links.secondary.DocumentFlowLinks;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public interface IDocumentFlowService {

    DocumentFlowLinks associateDocumentFlowWithRegistryEntry
            (DocumentFlow documentFlow, RegistryEntry registryEntry);

    DocumentFlowLinks associateDocumentFlowWithRecordNote
            (DocumentFlow documentFlow, RecordNote recordNote);

    DocumentFlowLinks updateDocumentFlowBySystemId
            (@NotNull final UUID systemId, DocumentFlow incomingDocumentFlow);

    void deleteDocumentFlowBySystemId(@NotNull final UUID systemId);

    DocumentFlowLinks findAll();

    DocumentFlowLinks findBySystemId(UUID systemId);

    DocumentFlowLinks generateDefaultDocumentFlow();

    void deleteDocumentFlow(@NotNull final DocumentFlow documentFlow);
}
