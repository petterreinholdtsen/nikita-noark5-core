package app.service.interfaces.odata;

import app.webapp.payload.links.LinksNoarkObject;

public interface IODataService {

    /**
     * Process an OData Query that corresponds to a GET request.
     *
     * @return a LinksNoarkObject with the correct type
     * @throws Exception is something goes wrong
     */
    LinksNoarkObject processODataQueryGet(String odataAppend);

    LinksNoarkObject processODataQueryGet();

    String processODataQueryDelete();

    LinksNoarkObject processODataSearchQuery(String search, int top, int skip);
}
