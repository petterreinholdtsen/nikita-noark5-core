package app.integration.mail.model;


import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

import static app.utils.constants.MailConstants.*;

public class EmailSerializer
        extends StdSerializer<Email> {

    public EmailSerializer() {
        super(Email.class);
    }

    @Override
    public void serialize(Email email, JsonGenerator jsonGenerator,
                          SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField(EMAIL_FROM, email.getFrom());
        jsonGenerator.writeFieldName(EMAIL_TO);
        jsonGenerator.writeStartArray();
        for (String recipient : email.getRecipients()) {
            jsonGenerator.writeString(recipient);
        }
        jsonGenerator.writeEndArray();
        jsonGenerator.writeFieldName(EMAIL_CC);
        jsonGenerator.writeStartArray();
        for (String carbonCopy : email.getCarbonCopies()) {
            jsonGenerator.writeString(carbonCopy);
        }
        jsonGenerator.writeEndArray();
        jsonGenerator.writeStringField(EMAIL_SUBJECT, email.getSubject());
        jsonGenerator.writeStringField(EMAIL_MESSAGE_TEXT, email.getMessageText());
        jsonGenerator.writeFieldName(EMAIL_ATTACHMENTS);
        jsonGenerator.writeStartArray();
        for (String attachment : email.getAttachments()) {
            jsonGenerator.writeString(attachment);
        }
        jsonGenerator.writeEndArray();
        jsonGenerator.writeEndObject();
    }
}
