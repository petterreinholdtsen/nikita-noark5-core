package app.integration.mail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static app.utils.constants.FileConstants.ERROR_STORAGE;
import static java.lang.String.format;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

@Profile("mailintegration")
@Service
public class MailFileService
        implements IMailFileService {

    private static final Logger logger = LoggerFactory.getLogger(MailFileService.class);

    /**
     * The directory where outgoing files to the mail queue are stored.
     */
    private final File storageDirOut;

    /**
     * Create the outgoing storage directory if they do not exist.
     * Note if it is not possible to create / write to the directory the program must
     * stop as access to this directory is required for correct functioning of the program.
     */
    public MailFileService(
            @Value("${nikita.download-dir.outgoing}") String pathnameDirOut,
            ApplicationContext applicationContext) {
        storageDirOut = new File(pathnameDirOut);
        // Check if directory exists, try to create it otherwise
        if (!storageDirOut.exists() && !storageDirOut.mkdir()) {
            logger.error(format(ERROR_STORAGE, "outgoing", storageDirOut.getAbsolutePath()));
            ((ConfigurableApplicationContext) applicationContext).close();
        }    // directory exists,check if it's writable at startup
        else if (!Files.isWritable(storageDirOut.toPath())) {
            logger.error(format("Incoming storage directory [%s] is not writable.", pathnameDirOut));
            logger.error("Application cannot start unless directory is writable");
            ((ConfigurableApplicationContext) applicationContext).close();
        }
    }

    public String writeFileToStorage(Path fromPath, String filename) {
        String storedFilename = storageDirOut.getAbsolutePath() + File.separator + filename;
        try {
            Files.copy(fromPath, Paths.get(storedFilename), REPLACE_EXISTING);
        } catch (IOException e) {
            logger.error(format("Error copying [%s] to outgoing storage. Reason given is [%s]",
                    filename, e.getMessage()));
        }
        return storedFilename;
    }
}
