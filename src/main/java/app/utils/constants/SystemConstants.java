package app.utils.constants;

public final class SystemConstants {

    public static final String VENDOR = "leverandoer";
    public static final String PRODUCT = "produkt";
    public static final String VERSION = "versjon";
    public static final String VERSION_DATE = "versjonsdato";
    public static final String PROTOCOL_VERSION = "protokollversjon";
}
