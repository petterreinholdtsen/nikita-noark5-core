package app.utils.constants;

import static app.utils.constants.Constants.*;

/**
 * Constants used for locking down controller endpoints
 */
public final class PATHPatterns {
    public static final String PATTERN_NEW_FONDS_STRUCTURE_ALL = SLASH + HREF_BASE_FONDS_STRUCTURE + SLASH + NEW + DASH + "*";
    public static final String PATTERN_METADATA_PATH = SLASH + HREF_BASE_METADATA + SLASH + "**";
}
