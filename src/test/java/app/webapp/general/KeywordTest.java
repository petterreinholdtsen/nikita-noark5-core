package app.webapp.general;

import org.junit.jupiter.api.BeforeEach;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static app.utils.KeywordCreator.createKeywordAsJSON;
import static app.utils.KeywordCreator.createUpdatedKeywordAsJSON;
import static app.utils.KeywordValidator.*;
import static app.utils.TestConstants.KEYWORD_TEST_UPDATED;
import static app.utils.constants.Constants.NOARK5_V5_CONTENT_TYPE_JSON;
import static app.utils.constants.Constants.REL_FONDS_STRUCTURE_KEYWORD;
import static app.utils.constants.HATEOASConstants.SELF;
import static app.utils.constants.N5ResourceMappings.*;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.BEFORE_TEST_METHOD;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Internal testing currently suspended in the codebase.
 * <p>
 * Note: In the upgrade to Spring Boot 3 we experienced problems with the domain model. There is no problem with the
 * domain model when running spring boot as an application, but there seems to be a problem with the domain model
 * is used in testing. Typically, the problem is that Hibernate is not able to deal with JOINED inheritance as it is
 * specified in nikita, and prints a Wrong entity retrieved exception stating that it expects a File when it was given
 * a Record (We tried to retrieve a Record). We have tried using a discriminator column, forcing the use of a
 * discriminator column but in test mode we are not able to get the Nikita domain model
 * <p>
 * We need to finish this Spring Boot 3 branch and move forward. All tests that will not run are currently commented
 * out until we have time to fix it.
 */
public class KeywordTest
        extends BaseTest {
    protected MockMvc mockMvc;


    @BeforeEach
    public void setUp(WebApplicationContext webApplicationContext,
                      RestDocumentationContextProvider restDocumentation) {
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .apply(documentationConfiguration(restDocumentation))
                .alwaysDo(document("{method-name}",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint())))
                .build();
    }

    /**
     * Check that it is possible to add a Keyword to an existing
     * File
     * <p>
     * 1. Check that the GET ny-noekkelord works
     * 2. POST ny-noekkelord and check value and self REL
     * 3. Check that the keyword object can be retrieved
     * 4. Check that the retrieved keyword object can be updated
     * 5. Check that OData query on mappe/noekkelord works
     * 6. Check that OData query on noekkelord works
     *
     * @throws Exception Serialising or validation exception
     */
    //////@Test
    @Sql(scripts = {"/db-tests/basic_structure.sql"}, executionPhase = BEFORE_TEST_METHOD)
    //@Sql({"/db-tests/basic_structure.sql", "/db-tests/keyword.sql"})
    //@WithMockCustomUser
    public void addKeywordWhenCreatingFile() throws Exception {
        // First get template to create / POST Keyword
        String urlNewKeyword = "/noark5v5/api/arkivstruktur/" +
                "mappe/48c81365-7193-4481-bc84-b025248fb310/" +
                NEW_KEYWORD;

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlNewKeyword)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));
        // We do not do anything else with the result. Just make sure the
        // call works
        resultActions.andExpect(status().isOk());
        printDocumentation(resultActions);

        // Create an Keyword object associated with the File
        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post(urlNewKeyword)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON)
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(createKeywordAsJSON()));
        resultActions.andExpect(status().isCreated());
        validateKeywordForFile(resultActions);
        printDocumentation(resultActions);

        // Retrieve an identified Keyword
        String urlKeyword = getHref(SELF, resultActions);
        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlKeyword)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));
        resultActions.andExpect(status().isOk());
        validateKeywordForFile(resultActions);
        printDocumentation(resultActions);

        // Update an identified Keyword
        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .put(urlKeyword)
                .contextPath(contextPath)
                .header("ETAG", "\"1\"")
                .accept(NOARK5_V5_CONTENT_TYPE_JSON)
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(createUpdatedKeywordAsJSON()));
        resultActions.andExpect(status().isOk());
        validateUpdatedKeyword(resultActions);
        printDocumentation(resultActions);

        // OData search for a File based on Keyword
        String odata = "?$filter=noekkelord/noekkelord eq '" +
                KEYWORD_TEST_UPDATED + "'&$top=1";
        String urlDocDescSearch = contextPath + "/api/arkivstruktur/" +
                FILE + odata;
        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlDocDescSearch)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));
        MockHttpServletResponse response = resultActions.andReturn()
                .getResponse();
        System.out.println(response.getContentAsString());
        resultActions.andExpect(status().isOk())
                .andExpect(jsonPath("$.results", hasSize(1)));
        printDocumentation(resultActions);
    }

    /**
     * Check that it is possible to add a Keyword to an existing
     * Class
     * <p>
     * 1. Check that the GET ny-noekkelord works
     * 2. POST ny-noekkelord and check value and self REL
     * 3. Check that the keyword object can be retrieved
     * 4. Check that the retrieved keyword object can be updated
     * 5. Check that OData query on mappe/noekkelord works
     * 6. Check that OData query on noekkelord works
     *
     * @throws Exception Serialising or validation exception
     */
    //////@Test
    @Sql(scripts = {"/db-tests/basic_structure.sql"}, executionPhase = BEFORE_TEST_METHOD)
    public void addKeywordWhenCreatingClass() throws Exception {
        // First get template to create / POST Keyword
        String urlNewKeyword = "/noark5v5/api/arkivstruktur/" +
                "klasse/596c85fb-a6c4-4381-86b4-81df05234028/" + NEW_KEYWORD;

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlNewKeyword)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));
        // We do not do anything else with the result. Just make sure the
        // call works
        resultActions.andExpect(status().isOk());
        printDocumentation(resultActions);

        // Create an Keyword object associated with the Class
        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post(urlNewKeyword)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON)
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(createKeywordAsJSON()));
        resultActions.andExpect(status().isCreated());
        validateKeywordForClass(resultActions);
        printDocumentation(resultActions);

        // Retrieve an identified Keyword
        String urlKeyword = getHref(SELF, resultActions);
        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlKeyword)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));
        resultActions.andExpect(status().isOk());
        validateKeywordForClass(resultActions);
        printDocumentation(resultActions);

        // Update an identified Keyword
        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .put(urlKeyword)
                .contextPath(contextPath)
                .header("ETAG", "\"1\"")
                .accept(NOARK5_V5_CONTENT_TYPE_JSON)
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(createUpdatedKeywordAsJSON()));
        resultActions.andExpect(status().isOk());
        validateUpdatedKeyword(resultActions);
        printDocumentation(resultActions);

        // OData search for a Class based on Keyword
        String odata = "?$filter=noekkelord/noekkelord eq '" +
                KEYWORD_TEST_UPDATED + "'";
        String urlDocDescSearch = contextPath + "/odata/api/arkivstruktur/" +
                CLASS + odata;
        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlDocDescSearch)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));
        resultActions.andExpect(status().isOk())
                .andExpect(jsonPath("$.results", hasSize(1)));
        printDocumentation(resultActions);
    }

    /**
     * Check that it is possible to add a Keyword to an existing
     * Record.
     * <p>
     * 1. Check that the GET ny-noekkelord works
     * 2. POST ny-noekkelord and check value and self REL
     * 3. Check that the keyword object can be retrieved
     * 4. Check that the retrieved keyword object can be updated
     * 5. Check that OData query on registrering/noekkelord works
     * 6. Check that OData query on noekkelord works
     *
     * @throws Exception Serialising or validation exception
     */
    //////@Test
    @Sql(scripts = {"/db-tests/basic_structure.sql"}, executionPhase = BEFORE_TEST_METHOD)
    public void addKeywordWhenCreatingRecord() throws Exception {
        // First get template to create / POST Keyword
        String urlNewKeyword = "/noark5v5/api/arkivstruktur/" +
                "registrering/99c2f1af-dd84-19e8-dd4f-cc21fe1578ff/" +
                NEW_KEYWORD;

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlNewKeyword)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));
        // We do not do anything else with the result. Just make sure the
        // call works
        resultActions.andExpect(status().isOk());
        printDocumentation(resultActions);

        // Create an Keyword object associated with the Record
        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post(urlNewKeyword)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON)
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(createKeywordAsJSON()));
        resultActions.andExpect(status().isCreated());
        validateKeywordForRecord(resultActions);
        printDocumentation(resultActions);

        // Retrieve an identified Keyword
        String urlKeyword = getHref(SELF, resultActions);
        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlKeyword)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));
        resultActions.andExpect(status().isOk());
        validateKeywordForRecord(resultActions);
        printDocumentation(resultActions);

        // Update an identified Keyword
        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .put(urlKeyword)
                .contextPath(contextPath)
                .header("ETAG", "\"1\"")
                .accept(NOARK5_V5_CONTENT_TYPE_JSON)
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(createUpdatedKeywordAsJSON()));
        resultActions.andExpect(status().isOk());
        validateUpdatedKeyword(resultActions);
        printDocumentation(resultActions);

        // OData search for a Record based on Keyword
        String odata = "?$filter=noekkelord/noekkelord eq '" +
                KEYWORD_TEST_UPDATED + "'";
        String urlDocDescSearch = contextPath + "/odata/api/arkivstruktur/" +
                RECORD + odata;
        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlDocDescSearch)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));
        resultActions.andExpect(status().isOk())
                .andExpect(jsonPath("$.results", hasSize(1)));
        printDocumentation(resultActions);
    }

    /**
     * Check that it is possible to delete a keyword by its systemID and that
     * the related file has no associated keyword afterwards
     * <p>
     * 1. Get associated File, make sure Keyword rel/href is in _links
     * 2. Delete Keyword using it's systemID
     * 3. Get previous associated File, make sure Keyword rel/href is
     * not in _links
     *
     * @throws Exception Serialising or validation exception
     */
    //////@Test
    @Sql(scripts = {"/db-tests/basic_structure.sql"}, executionPhase = BEFORE_TEST_METHOD)
    public void deleteKeywordObjectWithFile() throws Exception {
        // 1. Get associated File, make sure Keyword rel/href is in _links
        String urlFile = contextPath + "/api/arkivstruktur/" +
                "mappe/48c81365-7193-4481-bc84-b025248fb310";
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlFile)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));
        resultActions.andExpect(status().isOk())
                .andExpect(jsonPath(
                        "$._links.['" + REL_FONDS_STRUCTURE_KEYWORD + "']")
                        .exists());

        // 2. Delete Keyword using it's systemID
        String urlKeyword = contextPath + "/api/arkivstruktur/" +
                "noekkelord/81cea881-1203-4e3f-943c-c0294e81e528";
        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .delete(urlKeyword)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));
        MockHttpServletResponse response = resultActions.andReturn()
                .getResponse();
        System.out.println(response.getContentAsString());
        resultActions.andExpect(status().isNoContent());
        printDocumentation(resultActions);

        // 3. Get previous associated File, make sure Keyword rel/href
        // is not in _links
        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlFile)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        resultActions.andExpect(status().isOk())
                .andExpect(jsonPath(
                        "$._links.['" + REL_FONDS_STRUCTURE_KEYWORD + "']")
                        .doesNotExist());
    }

    /**
     * Check that it is possible to delete a Record that has a Keyword
     * associated with it. The test is that referential integrity should
     * not be an issue.
     *
     * @throws Exception Serialising or validation exception
     */
    //////@Test
    @Sql(scripts = {"/db-tests/basic_structure.sql"}, executionPhase = BEFORE_TEST_METHOD)
    public void deleteRecordWithKeyword() throws Exception {
        String urlRecord = "/noark5v5/api/arkivstruktur/" +
                "registrering/99c2f1af-dd84-19e8-dd4f-cc21fe1578ff";

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .delete(urlRecord)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));
        MockHttpServletResponse response = resultActions.andReturn()
                .getResponse();
        System.out.println(response.getContentAsString());
        resultActions.andExpect(status().isNoContent());
        printDocumentation(resultActions);
    }

    /**
     * Check that it is possible to delete a File that has a Keyword associated
     * with it File. The test is that referential integrity should not be an
     * issue.
     *
     * @throws Exception Serialising or validation exception
     */
    //////@Test
    @Sql(scripts = {"/db-tests/basic_structure.sql"}, executionPhase = BEFORE_TEST_METHOD)
    public void deleteFileWithKeyword() throws Exception {
        String urlFile = "/noark5v5/api/arkivstruktur/" +
                "mappe/48c81365-7193-4481-bc84-b025248fb310";
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .delete(urlFile)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));
        resultActions.andExpect(status().isNoContent());
        printDocumentation(resultActions);
    }

    /**
     * Check that it is possible to delete a Class that has a Keyword
     * associated with it. The test is that referential integrity should
     * not be an issue.
     *
     * @throws Exception Serialising or validation exception
     */
    //////@Test
    @Sql(scripts = {"/db-tests/basic_structure.sql"}, executionPhase = BEFORE_TEST_METHOD)
    public void deleteClassWithKeyword() throws Exception {
        String urlClass = "/noark5v5/api/arkivstruktur/" +
                "klasse/596c85fb-a6c4-4381-86b4-81df05234028";
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .delete(urlClass)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));
        resultActions.andExpect(status().isNoContent());
        printDocumentation(resultActions);
    }
}
