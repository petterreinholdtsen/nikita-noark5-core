package app.webapp.general;

import org.junit.jupiter.api.BeforeEach;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static app.utils.constants.Constants.NOARK5_V5_CONTENT_TYPE_JSON;
import static app.utils.constants.Constants.REL_CASE_HANDLING_PRECEDENCE;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Internal testing currently suspended in the codebase.
 * <p>
 * Note: In the upgrade to Spring Boot 3 we experienced problems with the domain model. There is no problem with the
 * domain model when running spring boot as an application, but there seems to be a problem with the domain model
 * is used in testing. Typically, the problem is that Hibernate is not able to deal with JOINED inheritance as it is
 * specified in nikita, and prints a Wrong entity retrieved exception stating that it expects a File when it was given
 * a Record (We tried to retrieve a Record). We have tried using a discriminator column, forcing the use of a
 * discriminator column but in test mode we are not able to get the Nikita domain model
 * <p>
 * We need to finish this Spring Boot 3 branch and move forward. All tests that will not run are currently commented
 * out until we have time to fix it.
 */
public class DeleteTest
        extends BaseTest {

    protected MockMvc mockMvc;


    @BeforeEach
    public void setUp(WebApplicationContext webApplicationContext,
                      RestDocumentationContextProvider restDocumentation) {
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .apply(documentationConfiguration(restDocumentation))
                .alwaysDo(document("{method-name}",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint())))
                .build();
    }

    /**
     * Check that it is possible to delete a precedence associated with a
     * RegistryEntry
     *
     * @throws Exception Serialising or validation exception
     */
    ////@Test
    @Sql({"/db-tests/basic_structure.sql", "/db-tests/precedence.sql"})
    public void deletePrecedenceWithRegistryEntry() throws Exception {
        String urlRegistryEntry = "/noark5v5/api/sakarkiv/" +
                "journalpost/16c2f0af-c684-49e8-bc4f-bc21de1578bb";

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .delete(urlRegistryEntry)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));
        resultActions.andExpect(status().isNoContent());
        printDocumentation(resultActions);
    }

    /**
     * Check that it is possible to delete a precedence associated with a
     * CaseFile. The point of the test is to make sure that the deletion of
     * the caseFile does not experience a referential integrity issue.
     *
     * @throws Exception Serialising or validation exception
     */
    ////@Test
    @Sql({"/db-tests/basic_structure.sql", "/db-tests/precedence.sql"})
    public void deletePrecedenceWithCaseFile() throws Exception {
        String urlCaseFile = "/noark5v5/api/sakarkiv/" +
                "saksmappe/ccefaca8-4eda-4164-84c8-4f2176312f29";

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .delete(urlCaseFile)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));
        resultActions.andExpect(status().isNoContent());
        printDocumentation(resultActions);
    }

    /**
     * Check that it is possible to delete a precedence by its systemID and that
     * the related caseFile has no associated precedence afterwards
     * <p>
     * 1. Get associated CaseFile, make sure Precedence rel/href is in _links
     * 2. Delete Precedence using it's systemID
     * 3. Get previous associated CaseFile, make sure Precedence rel/href is
     * not in _links
     *
     * @throws Exception Serialising or validation exception
     */
    ////@Test
    @Sql({"/db-tests/basic_structure.sql", "/db-tests/precedence.sql"})
    public void deletePrecedenceObjectWithCaseFile() throws Exception {

        // 1. Get associated CaseFile, make sure Precedence rel/href is in _links
        String urlCaseFile = contextPath + "/api/sakarkiv/" +
                "saksmappe/ccefaca8-4eda-4164-84c8-4f2176312f29";
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlCaseFile)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));
        resultActions.andExpect(status().isOk())
                .andExpect(jsonPath(
                        "$._links.['" + REL_CASE_HANDLING_PRECEDENCE + "']")
                        .exists());

        // 2. Delete Precedence using it's systemID
        String urlPrecedence = contextPath + "/api/sakarkiv/" +
                "presedens/e23cc0fe-d03f-4d6b-87e9-898deeb7d7da";
        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .delete(urlPrecedence)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));
        resultActions.andExpect(status().isNoContent());
        printDocumentation(resultActions);

        // 3. Get previous associated CaseFile, make sure Precedence rel/href
        // is not in _links
        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlCaseFile)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        resultActions.andExpect(status().isOk())
                .andExpect(jsonPath(
                        "$._links.['" + REL_CASE_HANDLING_PRECEDENCE + "']")
                        .doesNotExist());
    }

    /**
     * Check that it is possible to delete a precedence by its systemID and that
     * the related registryEntry has no associated precedence afterwards
     * <p>
     * 1. Get associated RegistryEntry, make sure Precedence rel/href is in _links
     * 2. Delete Precedence using it's systemID
     * 3. Get previous associated RegistryEntry, make sure Precedence rel/href is
     * not in _links
     *
     * @throws Exception Serialising or validation exception
     */
    ////@Test
    @Sql({"/db-tests/basic_structure.sql", "/db-tests/precedence.sql"})
    public void deletePrecedenceObjectWithRegistryEntry() throws Exception {

        // 1. Get associated RegistryEntry, make sure Precedence rel/href is in _links
        String urlRegistryEntry = contextPath + "/api/sakarkiv/" +
                "journalpost/16c2f0af-c684-49e8-bc4f-bc21de1578bb";
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlRegistryEntry)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));
        resultActions.andExpect(status().isOk())
                .andExpect(jsonPath(
                        "$._links.['" + REL_CASE_HANDLING_PRECEDENCE + "']")
                        .exists());

        // 2. Delete Precedence using it's systemID
        String urlPrecedence = contextPath + "/api/sakarkiv/" +
                "presedens/137dbc34-5669-4fdf-867e-985c3f1de60f";
        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .delete(urlPrecedence)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));
        resultActions.andExpect(status().isNoContent());
        printDocumentation(resultActions);

        // 3. Get previous associated RegistryEntry, make sure Precedence rel/href
        // is not in _links
        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlRegistryEntry)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        resultActions.andExpect(status().isOk())
                .andExpect(jsonPath(
                        "$._links.['" + REL_CASE_HANDLING_PRECEDENCE + "']")
                        .doesNotExist());
    }

    /**
     * Check that it is possible to delete a DocumentFlow associated with a
     * RegistryEntry
     *
     * @throws Exception Serialising or validation exception
     */
    ////@Test
    @Sql({"/db-tests/basic_structure.sql", "/db-tests/document_flow.sql"})
    public void deleteDocumentFlowWithRegistryEntry() throws Exception {
        String urlRegistryEntry = "/noark5v5/api/sakarkiv/" +
                "journalpost/8f6b084f-d727-4b46-bbe2-14bed2135fa9";
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .delete(urlRegistryEntry)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));
        resultActions.andExpect(status().isNoContent());
        printDocumentation(resultActions);
    }

    /**
     * Check that it is possible to delete a DocumentFlow associated with a
     * RecordNote
     *
     * @throws Exception Serialising or validation exception
     */
    ////@Test
    @Sql({"/db-tests/basic_structure.sql", "/db-tests/document_flow.sql"})
    public void deleteDocumentFlowWithRecordNote() throws Exception {
        String urlRecordNote = "/noark5v5/api/sakarkiv/" +
                "arkivnotat/11b32a9e-802d-43de-9bb5-c951e3bbe95b";
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .delete(urlRecordNote)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));
        resultActions.andExpect(status().isNoContent());
        printDocumentation(resultActions);
    }
}
