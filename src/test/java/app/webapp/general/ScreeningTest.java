package app.webapp.general;

import app.domain.noark5.Class;
import app.domain.noark5.*;
import app.domain.noark5.metadata.*;
import app.domain.noark5.secondary.Screening;
import app.domain.noark5.secondary.ScreeningMetadataLocal;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.*;
import app.webapp.payload.serializers.noark5.secondary.ScreeningMetadataSerializer;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.jayway.jsonpath.JsonPath;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.StringWriter;

import static app.utils.constants.Constants.*;
import static app.utils.constants.HATEOASConstants.HREF;
import static app.utils.constants.HATEOASConstants.SELF;
import static app.utils.constants.N5ResourceMappings.*;
import static java.time.OffsetDateTime.parse;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.AnyOf.anyOf;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Internal testing currently suspended in the codebase.
 * <p>
 * Note: In the upgrade to Spring Boot 3 we experienced problems with the domain model. There is no problem with the
 * domain model when running spring boot as an application, but there seems to be a problem with the domain model
 * is used in testing. Typically, the problem is that Hibernate is not able to deal with JOINED inheritance as it is
 * specified in nikita, and prints a Wrong entity retrieved exception stating that it expects a File when it was given
 * a Record (We tried to retrieve a Record). We have tried using a discriminator column, forcing the use of a
 * discriminator column but in test mode we are not able to get the Nikita domain model
 * <p>
 * We need to finish this Spring Boot 3 branch and move forward. All tests that will not run are currently commented
 * out until we have time to fix it.
 */
//@ExtendWith({RestDocumentationExtension.class, SpringExtension.class})
//@SpringBootTest(classes = nikita.N5CoreApp.class,
//        webEnvironment = RANDOM_PORT)
//@AutoConfigureRestDocs(outputDir = "target/snippets")
//@TestInstance(TestInstance.Lifecycle.PER_CLASS)
//@Transactional
public class ScreeningTest {

    private MockMvc mockMvc;

    @BeforeEach
    public void setUp(WebApplicationContext webApplicationContext,
                      RestDocumentationContextProvider restDocumentation) {
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .apply(documentationConfiguration(restDocumentation))
                .alwaysDo(document("{method-name}",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint())))
                .build();
    }

    /**
     * Check that it is possible to add a Screening to a File when creating
     * the File. Then check that it is possible to associate
     * ScreeningMetadata with file.
     * <p>
     * The following steps are taken:
     * 1. Get File template (GET ny-mappe)
     * 2. Create file (POST ny-mappe)
     * 3. Get created file (GET mappe) and check Screening attributes and
     * REL/HREF
     * 4. Create ScreeningMetadata (POST ny-skjermingmetadata)
     * 5. Create ScreeningMetadata (POST ny-skjermingmetadata)
     * 6. Update the first ScreeningMetadata (PUT skjermingmetadata)
     * 7. Delete the first ScreeningMetadata (DELETE skjermingmetadata)
     * The test creates a chain of requests that is expected to be applicable
     * for File/Screening/ScreeningMetadata
     *
     * @throws Exception Serialising or validation exception
     */
    @Sql("/db-tests/basic_structure.sql")
    @WithMockUser("test_user_admin@example.com")
    public void addScreeningToFile() throws Exception {
        // First get template to create / POST file
        String urlNewFile = "/noark5v5/api/arkivstruktur/arkivdel" +
                "/f1102ae8-6c4c-4d93-aaa5-7c6220e50c4d/ny-mappe";

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlNewFile)
                .contextPath("/noark5v5")
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath(
                        "$._links.['" +
                                REL_METADATA_SCREENING_METADATA + "'].['" +
                                HREF + "']").exists());

        // Next create a File object with an associated Screening
        // Note: We are not using the result of the GET ny-mappe, but want the
        // test to check tht it works

        File file = new File();
        file.setTitle("Title of file");

        // Create Metadata objects
        AccessRestriction accessRestriction =
                new AccessRestriction("P", "Personalsaker");
        ScreeningDocument screeningDocument = new ScreeningDocument(
                "H", "Skjerming av hele dokumentet");

        // Create a screening object and associate it with the File
        Screening screening = new Screening();
        screening.setAccessRestriction(accessRestriction);
        screening.setScreeningDocument(screeningDocument);
        screening.setScreeningAuthority("Unntatt etter Offentleglova");
        screening.setScreeningExpiresDate(parse("1942-07-25T00:00:00Z"));
        screening.setScreeningDuration(60);
        file.setReferenceScreening(screening);

        StringWriter jsonWriter = new StringWriter();
        JsonGenerator jgen = new JsonFactory().createGenerator(jsonWriter);
        var serializer = new FileSerializer();
        serializer.serializeNoarkEntity(file, new LinksNoarkObject(), jgen);
        jgen.close();

        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post(urlNewFile)
                .contextPath("/noark5v5")
                .accept(NOARK5_V5_CONTENT_TYPE_JSON)
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(jsonWriter.toString()));

        resultActions
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$." + SYSTEM_ID)
                        .exists())
                .andExpect(jsonPath("$." + TITLE)
                        .value("Title of file"));
        resultActions.andDo(document("home",
                preprocessRequest(prettyPrint()),
                preprocessResponse(prettyPrint())));

        MockHttpServletResponse response =
                resultActions.andReturn().getResponse();

        // Make sure we can retrieve the file and that the Screening
        // attributes are present
        String urlFile = "/noark5v5/api/arkivstruktur/mappe/" +
                JsonPath.read(response.getContentAsString(), "$." + SYSTEM_ID);

        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlFile)
                .contextPath("/noark5v5")
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        response = resultActions.andReturn().getResponse();

        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$." + SCREENING + "." +
                        ACCESS_RESTRICTION +
                        "." +
                        CODE).value("P"))
                .andExpect(jsonPath("$." + SCREENING + "." +
                        ACCESS_RESTRICTION + "." +
                        CODE_NAME).value("Personalsaker"))
                .andExpect(jsonPath("$." + SCREENING + "." +
                        SCREENING_SCREENING_DOCUMENT + "." +
                        CODE).value("H"))
                .andExpect(jsonPath("$." + SCREENING + "." +
                        SCREENING_SCREENING_DOCUMENT + "." +
                        CODE_NAME)
                        .value("Skjerming av hele dokumentet"))
                .andExpect(jsonPath("$." + SCREENING + "." +
                        SCREENING_AUTHORITY)
                        .value("Unntatt etter Offentleglova"))
                .andExpect(jsonPath("$." + SCREENING + "." +
                        SCREENING_DURATION)
                        .value(60))
                // Not picking them explicitly out as [0] [1] objects in the
                // array as the order might change later and then the test
                // will fail unnecessary
                .andExpect(jsonPath("$._links.['" +
                        REL_METADATA_SCREENING_METADATA + "'].href").exists())
                .andExpect(jsonPath("$." + SCREENING + "." +
                        SCREENING_EXPIRES_DATE).value(
                        anyOf(
                                is("1942-07-25T00:00:00Z"),
                                is("1942-07-25T00:00:00+00:00"))))
                .andExpect(jsonPath("$." + TITLE)
                        .value("Title of file"));
        resultActions.andDo(document("home",
                preprocessRequest(prettyPrint()),
                preprocessResponse(prettyPrint())));


        // Next, Create a ScreeningMetadata and POST it
        String urlNewScreeningMetadata = "/noark5v5/api/arkivstruktur/mappe/" +
                JsonPath.read(response.getContentAsString(),
                        "$." + SYSTEM_ID) + "/" + NEW_SCREENING_METADATA;
        // Make a note of the url for ScreeningMetadata associated with the
        // file. Will be used later.
        String urlScreeningMetadata = "/noark5v5/api/arkivstruktur/mappe/" +
                JsonPath.read(response.getContentAsString(),
                        "$." + SYSTEM_ID) + "/" + SCREENING_METADATA;

        ScreeningMetadataLocal screeningMetadata = new ScreeningMetadataLocal();
        screeningMetadata.setCode("NA");
        screeningMetadata.setCodeName("Skjerming navn avsender");

        jsonWriter = new StringWriter();
        jgen = new JsonFactory().createGenerator(jsonWriter);
        var serializerSM = new ScreeningMetadataSerializer();
        serializerSM.serializeNoarkEntity(screeningMetadata, new LinksNoarkObject(), jgen);
        jgen.close();

        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post(urlNewScreeningMetadata)
                .contextPath("/noark5v5")
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(jsonWriter.toString())
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));
        resultActions
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$." + CODE)
                        .exists())
                .andExpect(jsonPath("$." + CODE_NAME)
                        .exists())
                .andExpect(jsonPath("$._links.['" + SELF + "'].href")
                        .exists())
                .andExpect(jsonPath(
                        "$._links.['" +
                                REL_FONDS_STRUCTURE_SCREENING_METADATA +
                                "'].['" + HREF + "']").exists());

        // Create another ScreeningMetadata and POST it
        screeningMetadata.setCode("TKL");
        screeningMetadata.setCodeName("Skjerming tittel klasse");

        // Create a JSON object to POST
        jsonWriter = new StringWriter();
        jgen = new JsonFactory().createGenerator(jsonWriter);
        serializerSM = new ScreeningMetadataSerializer();
        serializerSM.serializeNoarkEntity(screeningMetadata, new LinksNoarkObject(), jgen);
        jgen.close();

        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post(urlNewScreeningMetadata)
                .contextPath("/noark5v5")
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(jsonWriter.toString())
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        resultActions
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$." + CODE)
                        .exists())
                .andExpect(jsonPath("$." + CODE_NAME)
                        .exists())
                .andExpect(jsonPath("$._links.['" + SELF + "'].href")
                        .exists())
                .andExpect(jsonPath(
                        "$._links.['" +
                                REL_FONDS_STRUCTURE_SCREENING_METADATA +
                                "'].['" + HREF + "']").exists());

        // Check that it is possible to retrieve the two ScreeningMetadata
        // objects
        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlScreeningMetadata)
                .contextPath("/noark5v5")
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        response = resultActions.andReturn().getResponse();

        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.results", hasSize(2)))
                .andExpect(jsonPath("$.results.[0]." + CODE)
                        .exists())
                .andExpect(jsonPath("$.results.[0]." + CODE_NAME)
                        .exists())
                .andExpect(jsonPath("$.results.[1]." + CODE)
                        .exists())
                .andExpect(jsonPath("$.results.[1]." + CODE_NAME)
                        .exists())
                .andExpect(jsonPath("$.results.[0]._links.['" +
                        SELF + "'].href")
                        .exists())
                .andExpect(jsonPath(
                        "$.results.[0]._links.['" +
                                REL_FONDS_STRUCTURE_SCREENING_METADATA + "'].['" +
                                HREF + "']").exists())
                .andExpect(jsonPath("$.results.[1]._links.['" +
                        SELF + "'].href")
                        .exists())
                .andExpect(jsonPath(
                        "$.results.[1]._links.['" +
                                REL_FONDS_STRUCTURE_SCREENING_METADATA + "'].['" +
                                HREF + "']").exists());

        resultActions.andDo(document("home",
                preprocessRequest(prettyPrint()),
                preprocessResponse(prettyPrint())));

        // Next, see that we can update the ScreeningMetadata
        String urlComplete = JsonPath.read(response.getContentAsString(),
                "$.results.[0]._links.self.href");
        urlScreeningMetadata = "/noark5v5/api/arkivstruktur" +
                "/skjermingmetadata" + urlComplete.split("/noark5v5/api" +
                "/arkivstruktur/skjermingmetadata")[1];

        // Create a ScreeningMetadata object different to the previous ones
        // that we can use to override the data in nikita
        ScreeningMetadata screeningMetadata2 = new ScreeningMetadata();
        screeningMetadata2.setCode("TM1");
        screeningMetadata2.setCodeName("Skjerming tittel mappe - unntatt første linje");

        // Create a JSON object to POST
        jsonWriter = new StringWriter();
        jgen = new JsonFactory().createGenerator(jsonWriter);
        serializerSM = new ScreeningMetadataSerializer();
        serializerSM.serializeNoarkEntity(screeningMetadata2, new LinksNoarkObject(), jgen);
        jgen.close();

        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .put(urlScreeningMetadata)
                .contextPath("/noark5v5")
                .header("ETAG", "\"1\"")
                .accept(NOARK5_V5_CONTENT_TYPE_JSON)
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(jsonWriter.toString()));

        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.['" + CODE + "']")
                        .value("TM1"))
                .andExpect(jsonPath("$.['" + CODE_NAME + "']")
                        .value("Skjerming tittel mappe - unntatt første linje"))
                .andExpect(jsonPath(
                        "$._links.['" +
                                REL_FONDS_STRUCTURE_SCREENING_METADATA + "'].['" +
                                HREF + "']").exists())
                .andExpect(jsonPath(
                        "$._links.['" + SELF + "'].['" + HREF + "']").exists());

        resultActions.andDo(document("home",
                preprocessRequest(prettyPrint()),
                preprocessResponse(prettyPrint())));

        // Next, check it is possible to delete the ScreeningMetadata
        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .delete(urlScreeningMetadata)
                .contextPath("/noark5v5")
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        resultActions.andExpect(status().isNoContent());
    }

    /**
     * Check that it is possible to add a Screening to a DocumentDescription when creating
     * the DocumentDescription. Then check that it is possible to associate
     * ScreeningMetadata with documentDescription.
     * <p>
     * The following steps are taken:
     * 1. Get DocumentDescription template (GET ny-dokumentbeskrivelse)
     * 2. Create documentDescription (POST ny-dokumentbeskrivelse)
     * 3. Get created documentDescription (GET dokumentbeskrivelse) and check Screening attributes and
     * REL/HREF
     * 4. Create ScreeningMetadata (POST ny-skjermingmetadata)
     * 5. Create ScreeningMetadata (POST ny-skjermingmetadata)
     * 6. Update the first ScreeningMetadata (PUT skjermingmetadata)
     * 7. Delete the first ScreeningMetadata (DELETE skjermingmetadata)
     * The test creates a chain of requests that is expected to be applicable
     * for DocumentDescription/Screening/ScreeningMetadata
     *
     * @throws Exception Serialising or validation exception
     */
    @Sql("/db-tests/basic_structure.sql")
    @WithMockUser("test_user_admin@example.com")
    public void addScreeningToDocumentDescription() throws Exception {
        // First get template to create / POST documentDescription
        String urlNewDocumentDescription = "/noark5v5/api/arkivstruktur" +
                "/registrering/dc600862-3298-4ec0-8541-3e51fb900054/" +
                "ny-dokumentbeskrivelse";

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlNewDocumentDescription)
                .contextPath("/noark5v5")
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath(
                        "$._links.['" +
                                REL_METADATA_SCREENING_METADATA + "'].['" +
                                HREF + "']").exists());

        // Next create a DocumentDescription object with an associated Screening
        // Note: We are not using the result of the GET ny-dokumentbeskrivelse, but want the
        // test to check tht it works

        DocumentDescription documentDescription = new DocumentDescription();
        documentDescription.setTitle("Title of documentDescription");

        // Create Metadata objects
        AccessRestriction accessRestriction =
                new AccessRestriction("P", "Personalsaker");
        ScreeningDocument screeningDocument = new ScreeningDocument(
                "H", "Skjerming av hele dokumentet");

        // Create a screening object and associate it with the DocumentDescription
        Screening screening = new Screening();
        screening.setAccessRestriction(accessRestriction);
        screening.setScreeningDocument(screeningDocument);
        screening.setScreeningAuthority("Unntatt etter Offentleglova");
        screening.setScreeningExpiresDate(parse("1942-07-25T00:00:00Z"));
        screening.setScreeningDuration(60);
        documentDescription.setReferenceScreening(screening);


        AssociatedWithRecordAs associatedWithRecordAs =
                new AssociatedWithRecordAs();
        associatedWithRecordAs.setCode("H");
        associatedWithRecordAs.setCodeName("Hoveddokument");
        documentDescription.setAssociatedWithRecordAs(associatedWithRecordAs);

        DocumentStatus documentStatus = new DocumentStatus();
        documentStatus.setCode("B");
        documentStatus.setCodeName("Dokumentet er under redigering");
        documentDescription.setDocumentStatus(documentStatus);

        DocumentType documentType = new DocumentType();
        documentType.setCode("B");
        documentType.setCodeName("Brev");
        documentDescription.setDocumentType(documentType);

        // Create a JSON object to POST
        StringWriter jsonWriter = new StringWriter();
        JsonGenerator jgen = new JsonFactory().createGenerator(jsonWriter);
        DocumentDescriptionSerializer serializer = new DocumentDescriptionSerializer();
        serializer.serializeNoarkEntity(documentDescription, new LinksNoarkObject(), jgen);
        jgen.close();

        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post(urlNewDocumentDescription)
                .contextPath("/noark5v5")
                .accept(NOARK5_V5_CONTENT_TYPE_JSON)
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(jsonWriter.toString()));

        resultActions
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$." + SYSTEM_ID)
                        .exists())
                .andExpect(jsonPath("$." + TITLE)
                        .value("Title of documentDescription"));
        resultActions.andDo(document("home",
                preprocessRequest(prettyPrint()),
                preprocessResponse(prettyPrint())));

        MockHttpServletResponse response =
                resultActions.andReturn().getResponse();

        // Make sure we can retrieve the documentDescription and that the Screening
        // attributes are present
        String urlDocumentDescription = "/noark5v5/api/arkivstruktur/dokumentbeskrivelse/" +
                JsonPath.read(response.getContentAsString(), "$." + SYSTEM_ID);

        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlDocumentDescription)
                .contextPath("/noark5v5")
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        response = resultActions.andReturn().getResponse();

        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$." + SCREENING + "." +
                        ACCESS_RESTRICTION +
                        "." +
                        CODE).value("P"))
                .andExpect(jsonPath("$." + SCREENING + "." +
                        ACCESS_RESTRICTION + "." +
                        CODE_NAME).value("Personalsaker"))
                .andExpect(jsonPath("$." + SCREENING + "." +
                        SCREENING_SCREENING_DOCUMENT + "." +
                        CODE).value("H"))
                .andExpect(jsonPath("$." + SCREENING + "." +
                        SCREENING_SCREENING_DOCUMENT + "." +
                        CODE_NAME)
                        .value("Skjerming av hele dokumentet"))
                .andExpect(jsonPath("$." + SCREENING + "." +
                        SCREENING_AUTHORITY)
                        .value("Unntatt etter Offentleglova"))
                .andExpect(jsonPath("$." + SCREENING + "." +
                        SCREENING_DURATION)
                        .value(60))
                // Not picking them explicitly out as [0] [1] objects in the
                // array as the order might change later and then the test
                // will fail unnecessary
                .andExpect(jsonPath("$._links.['" +
                        REL_METADATA_SCREENING_METADATA + "'].href").exists())
                .andExpect(jsonPath("$." + SCREENING + "." +
                        SCREENING_EXPIRES_DATE).value(
                        anyOf(
                                is("1942-07-25T00:00:00Z"),
                                is("1942-07-25T00:00:00+00:00"))))
                .andExpect(jsonPath("$." + TITLE)
                        .value("Title of documentDescription"));
        resultActions.andDo(document("home",
                preprocessRequest(prettyPrint()),
                preprocessResponse(prettyPrint())));


        // Next, Create a ScreeningMetadata and POST it
        String urlNewScreeningMetadata = "/noark5v5/api/arkivstruktur/dokumentbeskrivelse/" +
                JsonPath.read(response.getContentAsString(),
                        "$." + SYSTEM_ID) + "/" + NEW_SCREENING_METADATA;
        // Make a note of the url for ScreeningMetadata associated with the
        // documentDescription. Will be used later.
        String urlScreeningMetadata = "/noark5v5/api/arkivstruktur/dokumentbeskrivelse/" +
                JsonPath.read(response.getContentAsString(),
                        "$." + SYSTEM_ID) + "/" + SCREENING_METADATA;

        ScreeningMetadataLocal screeningMetadata = new ScreeningMetadataLocal();
        screeningMetadata.setCode("NA");
        screeningMetadata.setCodeName("Skjerming navn avsender");

        // Create a JSON object to POST
        jsonWriter = new StringWriter();
        jgen = new JsonFactory().createGenerator(jsonWriter);
        var serializerSM = new ScreeningMetadataSerializer();
        serializerSM.serializeNoarkEntity(screeningMetadata, new LinksNoarkObject(), jgen);
        jgen.close();

        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post(urlNewScreeningMetadata)
                .contextPath("/noark5v5")
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(jsonWriter.toString())
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        resultActions
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$." + CODE)
                        .exists())
                .andExpect(jsonPath("$." + CODE_NAME)
                        .exists())
                .andExpect(jsonPath("$._links.['" + SELF + "'].href")
                        .exists())
                .andExpect(jsonPath(
                        "$._links.['" +
                                REL_FONDS_STRUCTURE_SCREENING_METADATA +
                                "'].['" + HREF + "']").exists());

        // Create another ScreeningMetadata and POST it
        screeningMetadata.setCode("TKL");
        screeningMetadata.setCodeName("Skjerming tittel klasse");

        // Create a JSON object to POST
        jsonWriter = new StringWriter();
        jgen = new JsonFactory().createGenerator(jsonWriter);
        serializerSM = new ScreeningMetadataSerializer();
        serializerSM.serializeNoarkEntity(screeningMetadata, new LinksNoarkObject(), jgen);
        jgen.close();

        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post(urlNewScreeningMetadata)
                .contextPath("/noark5v5")
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(jsonWriter.toString())
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        resultActions
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$." + CODE)
                        .exists())
                .andExpect(jsonPath("$." + CODE_NAME)
                        .exists())
                .andExpect(jsonPath("$._links.['" + SELF + "'].href")
                        .exists())
                .andExpect(jsonPath(
                        "$._links.['" +
                                REL_FONDS_STRUCTURE_SCREENING_METADATA +
                                "'].['" + HREF + "']").exists());

        // Check that it is possible to retrieve the two ScreeningMetadata
        // objects
        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlScreeningMetadata)
                .contextPath("/noark5v5")
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        response = resultActions.andReturn().getResponse();

        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.results", hasSize(2)))
                .andExpect(jsonPath("$.results.[0]." + CODE)
                        .exists())
                .andExpect(jsonPath("$.results.[0]." + CODE_NAME)
                        .exists())
                .andExpect(jsonPath("$.results.[1]." + CODE)
                        .exists())
                .andExpect(jsonPath("$.results.[1]." + CODE_NAME)
                        .exists())
                .andExpect(jsonPath("$.results.[0]._links.['" +
                        SELF + "'].href")
                        .exists())
                .andExpect(jsonPath(
                        "$.results.[0]._links.['" +
                                REL_FONDS_STRUCTURE_SCREENING_METADATA + "'].['" +
                                HREF + "']").exists())
                .andExpect(jsonPath("$.results.[1]._links.['" +
                        SELF + "'].href")
                        .exists())
                .andExpect(jsonPath(
                        "$.results.[1]._links.['" +
                                REL_FONDS_STRUCTURE_SCREENING_METADATA + "'].['" +
                                HREF + "']").exists());

        resultActions.andDo(document("home",
                preprocessRequest(prettyPrint()),
                preprocessResponse(prettyPrint())));

        // Next, see that we can update the ScreeningMetadata
        String urlComplete = JsonPath.read(response.getContentAsString(),
                "$.results.[0]._links.self.href");
        urlScreeningMetadata = "/noark5v5/api/arkivstruktur" +
                "/skjermingmetadata" + urlComplete.split("/noark5v5/api" +
                "/arkivstruktur/skjermingmetadata")[1];

        // Create a ScreeningMetadata object different to the previous ones
        // that we can use to override the data in nikita
        ScreeningMetadata screeningMetadata2 = new ScreeningMetadata();
        screeningMetadata2.setCode("TM1");
        screeningMetadata2.setCodeName("Skjerming tittel mappe - unntatt første linje");

        // Create a JSON object to POST
        jsonWriter = new StringWriter();
        jgen = new JsonFactory().createGenerator(jsonWriter);
        serializerSM = new ScreeningMetadataSerializer();
        serializerSM.serializeNoarkEntity(screeningMetadata, new LinksNoarkObject(), jgen);
        jgen.close();

        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .put(urlScreeningMetadata)
                .contextPath("/noark5v5")
                .header("ETAG", "\"1\"")
                .accept(NOARK5_V5_CONTENT_TYPE_JSON)
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(jsonWriter.toString()));

        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.['" + CODE + "']")
                        .value("TM1"))
                .andExpect(jsonPath("$.['" + CODE_NAME + "']")
                        .value("Skjerming tittel mappe - unntatt første linje"))
                .andExpect(jsonPath(
                        "$._links.['" +
                                REL_FONDS_STRUCTURE_SCREENING_METADATA + "'].['" +
                                HREF + "']").exists())
                .andExpect(jsonPath(
                        "$._links.['" + SELF + "'].['" + HREF + "']").exists());

        resultActions.andDo(document("home",
                preprocessRequest(prettyPrint()),
                preprocessResponse(prettyPrint())));

        // Next, check it is possible to delete the ScreeningMetadata
        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .delete(urlScreeningMetadata)
                .contextPath("/noark5v5")
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        resultActions.andExpect(status().isNoContent());
    }

    /**
     * Check that it is possible to add a Screening to a Class when creating
     * the Class. Then check that it is possible to associate
     * ScreeningMetadata with class.
     * <p>
     * The following steps are taken:
     * 1. Get Class template (GET ny-klasse)
     * 2. Create class (POST ny-klasse)
     * 3. Get created class (GET klasse) and check Screening attributes and
     * REL/HREF
     * 4. Create ScreeningMetadata (POST ny-skjermingmetadata)
     * 5. Create ScreeningMetadata (POST ny-skjermingmetadata)
     * 6. Update the first ScreeningMetadata (PUT skjermingmetadata)
     * 7. Delete the first ScreeningMetadata (DELETE skjermingmetadata)
     * The test creates a chain of requests that is expected to be applicable
     * for Class/Screening/ScreeningMetadata
     *
     * @throws Exception Serialising or validation exception
     */
    @Sql("/db-tests/basic_structure.sql")
    @WithMockUser("test_user_admin@example.com")
    @Transactional
    public void addScreeningToClass() throws Exception {
        // First get template to create / POST class
        String urlNewClass = "/noark5v5/api/arkivstruktur" +
                "/" + CLASSIFICATION_SYSTEM +
                "/2d0b2dc1-f3bb-4239-bf04-582b1085581c/ny-klasse";

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlNewClass)
                .contextPath("/noark5v5")
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath(
                        "$._links.['" +
                                REL_METADATA_SCREENING_METADATA + "'].['" +
                                HREF + "']").exists());

        // Next create a Class object with an associated Screening
        // Note: We are not using the result of the GET ny-klasse, but want the
        // test to check tht it works

        Class klass = new Class();
        klass.setTitle("Title of class");
        klass.setClassId("CLASSID");
        // Create Metadata objects
        AccessRestriction accessRestriction =
                new AccessRestriction("P", "Personalsaker");
        ScreeningDocument screeningDocument = new ScreeningDocument(
                "H", "Skjerming av hele dokumentet");

        // Create a screening object and associate it with the Class
        Screening screening = new Screening();
        screening.setAccessRestriction(accessRestriction);
        screening.setScreeningDocument(screeningDocument);
        screening.setScreeningAuthority("Unntatt etter Offentleglova");
        screening.setScreeningExpiresDate(parse("1942-07-25T00:00:00Z"));
        screening.setScreeningDuration(60);
        klass.setReferenceScreening(screening);

        // Create a JSON object to POST
        StringWriter jsonWriter = new StringWriter();
        JsonGenerator jgen = new JsonFactory().createGenerator(jsonWriter);
        var serializer = new ClassSerializer();
        serializer.serializeNoarkEntity(klass, new LinksNoarkObject(), jgen);
        jgen.close();

        resultActions =
                mockMvc.perform(MockMvcRequestBuilders
                        .post(urlNewClass)
                        .contextPath("/noark5v5")
                        .accept(NOARK5_V5_CONTENT_TYPE_JSON)
                        .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                        .content(jsonWriter.toString()));

        resultActions
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$." + SYSTEM_ID)
                        .exists())
                .andExpect(jsonPath("$." + TITLE)
                        .value("Title of class"));
        resultActions.andDo(document("home",
                preprocessRequest(prettyPrint()),
                preprocessResponse(prettyPrint())));

        MockHttpServletResponse response =
                resultActions.andReturn().getResponse();

        // Make sure we can retrieve the class and that the Screening
        // attributes are present
        String urlClass = "/noark5v5/api/arkivstruktur/klasse/" +
                JsonPath.read(response.getContentAsString(), "$." + SYSTEM_ID);

        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlClass)
                .contextPath("/noark5v5")
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        response = resultActions.andReturn().getResponse();

        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$." + SCREENING + "." +
                        ACCESS_RESTRICTION +
                        "." +
                        CODE).value("P"))
                .andExpect(jsonPath("$." + SCREENING + "." +
                        ACCESS_RESTRICTION + "." +
                        CODE_NAME).value("Personalsaker"))
                .andExpect(jsonPath("$." + SCREENING + "." +
                        SCREENING_SCREENING_DOCUMENT + "." +
                        CODE).value("H"))
                .andExpect(jsonPath("$." + SCREENING + "." +
                        SCREENING_SCREENING_DOCUMENT + "." +
                        CODE_NAME)
                        .value("Skjerming av hele dokumentet"))
                .andExpect(jsonPath("$." + SCREENING + "." +
                        SCREENING_AUTHORITY)
                        .value("Unntatt etter Offentleglova"))
                .andExpect(jsonPath("$." + SCREENING + "." +
                        SCREENING_DURATION)
                        .value(60))
                // Not picking them explicitly out as [0] [1] objects in the
                // array as the order might change later and then the test
                // will fail unnecessary
                .andExpect(jsonPath("$._links.['" +
                        REL_METADATA_SCREENING_METADATA + "'].href").exists())
                .andExpect(jsonPath("$." + SCREENING + "." +
                        SCREENING_EXPIRES_DATE).value(
                        anyOf(
                                is("1942-07-25T00:00:00Z"),
                                is("1942-07-25T00:00:00+00:00"))))
                .andExpect(jsonPath("$." + TITLE)
                        .value("Title of class"));
        resultActions.andDo(document("home",
                preprocessRequest(prettyPrint()),
                preprocessResponse(prettyPrint())));


        // Next, Create a ScreeningMetadata and POST it
        String urlNewScreeningMetadata = "/noark5v5/api/arkivstruktur/klasse/" +
                JsonPath.read(response.getContentAsString(),
                        "$." + SYSTEM_ID) + "/" + NEW_SCREENING_METADATA;
        // Make a note of the url for ScreeningMetadata associated with the
        // class. Will be used later.
        String urlScreeningMetadata = "/noark5v5/api/arkivstruktur/klasse/" +
                JsonPath.read(response.getContentAsString(),
                        "$." + SYSTEM_ID) + "/" + SCREENING_METADATA;

        ScreeningMetadataLocal screeningMetadata = new ScreeningMetadataLocal();
        screeningMetadata.setCode("NA");
        screeningMetadata.setCodeName("Skjerming navn avsender");

        // Create a JSON object to POST
        jsonWriter = new StringWriter();
        jgen = new JsonFactory().createGenerator(jsonWriter);
        var serializerSM = new ScreeningMetadataSerializer();
        serializerSM.serializeNoarkEntity(screeningMetadata, new LinksNoarkObject(), jgen);
        jgen.close();

        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post(urlNewScreeningMetadata)
                .contextPath("/noark5v5")
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(jsonWriter.toString())
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        resultActions
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$." + CODE)
                        .exists())
                .andExpect(jsonPath("$." + CODE_NAME)
                        .exists())
                .andExpect(jsonPath("$._links.['" + SELF + "'].href")
                        .exists())
                .andExpect(jsonPath(
                        "$._links.['" +
                                REL_FONDS_STRUCTURE_SCREENING_METADATA +
                                "'].['" + HREF + "']").exists());

        // Create another ScreeningMetadata and POST it
        screeningMetadata.setCode("TKL");
        screeningMetadata.setCodeName("Skjerming tittel klasse");

        // Create a JSON object to POST
        jsonWriter = new StringWriter();
        jgen = new JsonFactory().createGenerator(jsonWriter);
        serializerSM = new ScreeningMetadataSerializer();
        serializerSM.serializeNoarkEntity(screeningMetadata, new LinksNoarkObject(), jgen);
        jgen.close();

        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post(urlNewScreeningMetadata)
                .contextPath("/noark5v5")
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(jsonWriter.toString())
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        resultActions
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$." + CODE)
                        .exists())
                .andExpect(jsonPath("$." + CODE_NAME)
                        .exists())
                .andExpect(jsonPath("$._links.['" + SELF + "'].href")
                        .exists())
                .andExpect(jsonPath(
                        "$._links.['" +
                                REL_FONDS_STRUCTURE_SCREENING_METADATA +
                                "'].['" + HREF + "']").exists());

        // Check that it is possible to retrieve the two ScreeningMetadata
        // objects
        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlScreeningMetadata)
                .contextPath("/noark5v5")
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        response = resultActions.andReturn().getResponse();

        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.results", hasSize(2)))
                .andExpect(jsonPath("$.results.[0]." + CODE)
                        .exists())
                .andExpect(jsonPath("$.results.[0]." + CODE_NAME)
                        .exists())
                .andExpect(jsonPath("$.results.[1]." + CODE)
                        .exists())
                .andExpect(jsonPath("$.results.[1]." + CODE_NAME)
                        .exists())
                .andExpect(jsonPath("$.results.[0]._links.['" +
                        SELF + "'].href")
                        .exists())
                .andExpect(jsonPath(
                        "$.results.[0]._links.['" +
                                REL_FONDS_STRUCTURE_SCREENING_METADATA + "'].['" +
                                HREF + "']").exists())
                .andExpect(jsonPath("$.results.[1]._links.['" +
                        SELF + "'].href")
                        .exists())
                .andExpect(jsonPath(
                        "$.results.[1]._links.['" +
                                REL_FONDS_STRUCTURE_SCREENING_METADATA + "'].['" +
                                HREF + "']").exists());

        resultActions.andDo(document("home",
                preprocessRequest(prettyPrint()),
                preprocessResponse(prettyPrint())));

        // Next, see that we can update the ScreeningMetadata
        String urlComplete = JsonPath.read(response.getContentAsString(),
                "$.results.[0]._links.self.href");
        urlScreeningMetadata = "/noark5v5/api/arkivstruktur" +
                "/skjermingmetadata" + urlComplete.split("/noark5v5/api" +
                "/arkivstruktur/skjermingmetadata")[1];

        // Create a ScreeningMetadata object different to the previous ones
        // that we can use to override the data in nikita
        ScreeningMetadata screeningMetadata2 = new ScreeningMetadata();
        screeningMetadata2.setCode("TM1");
        screeningMetadata2.setCodeName("Skjerming tittel mappe - unntatt første linje");

        // Create a JSON object to POST
        jsonWriter = new StringWriter();
        jgen = new JsonFactory().createGenerator(jsonWriter);
        serializerSM = new ScreeningMetadataSerializer();
        serializerSM.serializeNoarkEntity(screeningMetadata, new LinksNoarkObject(), jgen);
        jgen.close();

        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .put(urlScreeningMetadata)
                .contextPath("/noark5v5")
                .header("ETAG", "\"1\"")
                .accept(NOARK5_V5_CONTENT_TYPE_JSON)
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(jsonWriter.toString()));

        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.['" + CODE + "']")
                        .value("TM1"))
                .andExpect(jsonPath("$.['" + CODE_NAME + "']")
                        .value("Skjerming tittel mappe - unntatt første linje"))
                .andExpect(jsonPath(
                        "$._links.['" +
                                REL_FONDS_STRUCTURE_SCREENING_METADATA + "'].['" +
                                HREF + "']").exists())
                .andExpect(jsonPath(
                        "$._links.['" + SELF + "'].['" + HREF + "']").exists());

        resultActions.andDo(document("home",
                preprocessRequest(prettyPrint()),
                preprocessResponse(prettyPrint())));

        // Next, check it is possible to delete the ScreeningMetadata
        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .delete(urlScreeningMetadata)
                .contextPath("/noark5v5")
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        resultActions.andExpect(status().isNoContent());
    }


    /**
     * Check that it is possible to add a Screening to a Series when creating
     * the Series. Then check that it is possible to associate
     * ScreeningMetadata with series.
     * <p>
     * The following steps are taken:
     * 1. Get Series template (GET ny-arkivdel)
     * 2. Create series (POST ny-arkivdel)
     * 3. Get created series (GET arkivdel) and check Screening attributes and
     * REL/HREF
     * 4. Create ScreeningMetadata (POST ny-skjermingmetadata)
     * 5. Create ScreeningMetadata (POST ny-skjermingmetadata)
     * 6. Update the first ScreeningMetadata (PUT skjermingmetadata)
     * 7. Delete the first ScreeningMetadata (DELETE skjermingmetadata)
     * The test creates a chain of requests that is expected to be applicable
     * for Series/Screening/ScreeningMetadata
     *
     * @throws Exception Serialising or validation exception
     */
    @Sql("/db-tests/basic_structure.sql")
    @WithMockUser("test_user_admin@example.com")
    public void addScreeningToSeries() throws Exception {
        // First get template to create / POST series
        String urlNewSeries = "/noark5v5/api/arkivstruktur" +
                "/arkiv/3318a63f-11a7-4ec9-8bf1-4144b7f281cf/ny-arkivdel";

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlNewSeries)
                .contextPath("/noark5v5")
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath(
                        "$._links.['" +
                                REL_METADATA_SCREENING_METADATA + "'].['" +
                                HREF + "']").exists());

        // Next create a Series object with an associated Screening
        // Note: We are not using the result of the GET ny-arkivdel, but want the
        // test to check tht it works

        Series series = new Series();
        series.setTitle("Title of series");

        // Create Metadata objects
        AccessRestriction accessRestriction =
                new AccessRestriction("P", "Personalsaker");
        ScreeningDocument screeningDocument = new ScreeningDocument(
                "H", "Skjerming av hele dokumentet");

        // Create a screening object and associate it with the Series
        Screening screening = new Screening();
        screening.setAccessRestriction(accessRestriction);
        screening.setScreeningDocument(screeningDocument);
        screening.setScreeningAuthority("Unntatt etter Offentleglova");
        screening.setScreeningExpiresDate(parse("1942-07-25T00:00:00Z"));
        screening.setScreeningDuration(60);
        series.setReferenceScreening(screening);

        SeriesStatus seriesStatus = new SeriesStatus();
        seriesStatus.setCode("A");
        seriesStatus.setCodeName("Aktiv periode");
        series.setSeriesStatus(seriesStatus);

        // Create a JSON object to POST
        StringWriter jsonWriter = new StringWriter();
        JsonGenerator jgen = new JsonFactory().createGenerator(jsonWriter);
        var serializer = new SeriesSerializer();
        serializer.serializeNoarkEntity(series, new LinksNoarkObject(), jgen);
        jgen.close();

        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post(urlNewSeries)
                .contextPath("/noark5v5")
                .accept(NOARK5_V5_CONTENT_TYPE_JSON)
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(jsonWriter.toString()));

        resultActions
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$." + SYSTEM_ID)
                        .exists())
                .andExpect(jsonPath("$." + TITLE)
                        .value("Title of series"));
        resultActions.andDo(document("home",
                preprocessRequest(prettyPrint()),
                preprocessResponse(prettyPrint())));

        MockHttpServletResponse response =
                resultActions.andReturn().getResponse();

        // Make sure we can retrieve the series and that the Screening
        // attributes are present
        String urlSeries = "/noark5v5/api/arkivstruktur/arkivdel/" +
                JsonPath.read(response.getContentAsString(), "$." + SYSTEM_ID);

        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlSeries)
                .contextPath("/noark5v5")
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        response = resultActions.andReturn().getResponse();

        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$." + SCREENING + "." +
                        ACCESS_RESTRICTION +
                        "." +
                        CODE).value("P"))
                .andExpect(jsonPath("$." + SCREENING + "." +
                        ACCESS_RESTRICTION + "." +
                        CODE_NAME).value("Personalsaker"))
                .andExpect(jsonPath("$." + SCREENING + "." +
                        SCREENING_SCREENING_DOCUMENT + "." +
                        CODE).value("H"))
                .andExpect(jsonPath("$." + SCREENING + "." +
                        SCREENING_SCREENING_DOCUMENT + "." +
                        CODE_NAME)
                        .value("Skjerming av hele dokumentet"))
                .andExpect(jsonPath("$." + SCREENING + "." +
                        SCREENING_AUTHORITY)
                        .value("Unntatt etter Offentleglova"))
                .andExpect(jsonPath("$." + SCREENING + "." +
                        SCREENING_DURATION)
                        .value(60))
                // Not picking them explicitly out as [0] [1] objects in the
                // array as the order might change later and then the test
                // will fail unnecessary
                .andExpect(jsonPath("$._links.['" +
                        REL_METADATA_SCREENING_METADATA + "'].href").exists())
                .andExpect(jsonPath("$." + SCREENING + "." +
                        SCREENING_EXPIRES_DATE).value(
                        anyOf(
                                is("1942-07-25T00:00:00Z"),
                                is("1942-07-25T00:00:00+00:00"))))
                .andExpect(jsonPath("$." + TITLE)
                        .value("Title of series"));
        resultActions.andDo(document("home",
                preprocessRequest(prettyPrint()),
                preprocessResponse(prettyPrint())));


        // Next, Create a ScreeningMetadata and POST it
        String urlNewScreeningMetadata = "/noark5v5/api/arkivstruktur/arkivdel/" +
                JsonPath.read(response.getContentAsString(),
                        "$." + SYSTEM_ID) + "/" + NEW_SCREENING_METADATA;
        // Make a note of the url for ScreeningMetadata associated with the
        // series. Will be used later.
        String urlScreeningMetadata = "/noark5v5/api/arkivstruktur/arkivdel/" +
                JsonPath.read(response.getContentAsString(),
                        "$." + SYSTEM_ID) + "/" + SCREENING_METADATA;

        ScreeningMetadataLocal screeningMetadata = new ScreeningMetadataLocal();
        screeningMetadata.setCode("NA");
        screeningMetadata.setCodeName("Skjerming navn avsender");

        // Create a JSON object to POST
        jsonWriter = new StringWriter();
        jgen = new JsonFactory().createGenerator(jsonWriter);
        var serializerSM = new ScreeningMetadataSerializer();
        serializerSM.serializeNoarkEntity(screeningMetadata, new LinksNoarkObject(), jgen);
        jgen.close();

        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post(urlNewScreeningMetadata)
                .contextPath("/noark5v5")
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(jsonWriter.toString())
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        resultActions
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$." + CODE)
                        .exists())
                .andExpect(jsonPath("$." + CODE_NAME)
                        .exists())
                .andExpect(jsonPath("$._links.['" + SELF + "'].href")
                        .exists())
                .andExpect(jsonPath(
                        "$._links.['" +
                                REL_FONDS_STRUCTURE_SCREENING_METADATA +
                                "'].['" + HREF + "']").exists());

        // Create another ScreeningMetadata and POST it
        screeningMetadata.setCode("TKL");
        screeningMetadata.setCodeName("Skjerming tittel klasse");

        // Create a JSON object to POST
        jsonWriter = new StringWriter();
        jgen = new JsonFactory().createGenerator(jsonWriter);
        serializerSM = new ScreeningMetadataSerializer();
        serializerSM.serializeNoarkEntity(screeningMetadata, new LinksNoarkObject(), jgen);
        jgen.close();

        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post(urlNewScreeningMetadata)
                .contextPath("/noark5v5")
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(jsonWriter.toString())
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        resultActions
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$." + CODE)
                        .exists())
                .andExpect(jsonPath("$." + CODE_NAME)
                        .exists())
                .andExpect(jsonPath("$._links.['" + SELF + "'].href")
                        .exists())
                .andExpect(jsonPath(
                        "$._links.['" +
                                REL_FONDS_STRUCTURE_SCREENING_METADATA +
                                "'].['" + HREF + "']").exists());

        // Check that it is possible to retrieve the two ScreeningMetadata
        // objects
        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlScreeningMetadata)
                .contextPath("/noark5v5")
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        response = resultActions.andReturn().getResponse();

        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.results", hasSize(2)))
                .andExpect(jsonPath("$.results.[0]." + CODE)
                        .exists())
                .andExpect(jsonPath("$.results.[0]." + CODE_NAME)
                        .exists())
                .andExpect(jsonPath("$.results.[1]." + CODE)
                        .exists())
                .andExpect(jsonPath("$.results.[1]." + CODE_NAME)
                        .exists())
                .andExpect(jsonPath("$.results.[0]._links.['" +
                        SELF + "'].href")
                        .exists())
                .andExpect(jsonPath(
                        "$.results.[0]._links.['" +
                                REL_FONDS_STRUCTURE_SCREENING_METADATA + "'].['" +
                                HREF + "']").exists())
                .andExpect(jsonPath("$.results.[1]._links.['" +
                        SELF + "'].href")
                        .exists())
                .andExpect(jsonPath(
                        "$.results.[1]._links.['" +
                                REL_FONDS_STRUCTURE_SCREENING_METADATA + "'].['" +
                                HREF + "']").exists());

        resultActions.andDo(document("home",
                preprocessRequest(prettyPrint()),
                preprocessResponse(prettyPrint())));

        // Next, see that we can update the ScreeningMetadata
        String urlComplete = JsonPath.read(response.getContentAsString(),
                "$.results.[0]._links.self.href");
        urlScreeningMetadata = "/noark5v5/api/arkivstruktur" +
                "/skjermingmetadata" + urlComplete.split("/noark5v5/api" +
                "/arkivstruktur/skjermingmetadata")[1];

        // Create a ScreeningMetadata object different to the previous ones
        // that we can use to override the data in nikita
        ScreeningMetadata screeningMetadata2 = new ScreeningMetadata();
        screeningMetadata2.setCode("TM1");
        screeningMetadata2.setCodeName("Skjerming tittel mappe - unntatt første linje");

        // Create a JSON object to POST
        jsonWriter = new StringWriter();
        jgen = new JsonFactory().createGenerator(jsonWriter);
        serializerSM = new ScreeningMetadataSerializer();
        serializerSM.serializeNoarkEntity(screeningMetadata, new LinksNoarkObject(), jgen);
        jgen.close();

        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .put(urlScreeningMetadata)
                .contextPath("/noark5v5")
                .header("ETAG", "\"1\"")
                .accept(NOARK5_V5_CONTENT_TYPE_JSON)
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(jsonWriter.toString()));

        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.['" + CODE + "']")
                        .value("TM1"))
                .andExpect(jsonPath("$.['" + CODE_NAME + "']")
                        .value("Skjerming tittel mappe - unntatt første linje"))
                .andExpect(jsonPath(
                        "$._links.['" +
                                REL_FONDS_STRUCTURE_SCREENING_METADATA + "'].['" +
                                HREF + "']").exists())
                .andExpect(jsonPath(
                        "$._links.['" + SELF + "'].['" + HREF + "']").exists());

        resultActions.andDo(document("home",
                preprocessRequest(prettyPrint()),
                preprocessResponse(prettyPrint())));

        // Next, check it is possible to delete the ScreeningMetadata
        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .delete(urlScreeningMetadata)
                .contextPath("/noark5v5")
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        resultActions.andExpect(status().isNoContent());
    }

    /**
     * Check that it is possible to add a Screening to a Record when creating
     * the Record. Then check that it is possible to associate
     * ScreeningMetadata with record.
     * <p>
     * The following steps are taken:
     * 1. Get Record template (GET ny-registrering)
     * 2. Create record (POST ny-registrering)
     * 3. Get created record (GET registrering) and check Screening attributes and
     * REL/HREF
     * 4. Create ScreeningMetadata (POST ny-skjermingmetadata)
     * 5. Create ScreeningMetadata (POST ny-skjermingmetadata)
     * 6. Update the first ScreeningMetadata (PUT skjermingmetadata)
     * 7. Delete the first ScreeningMetadata (DELETE skjermingmetadata)
     * The test creates a chain of requests that is expected to be applicable
     * for Record/Screening/ScreeningMetadata
     *
     * @throws Exception Serialising or validation exception
     */
    @Sql("/db-tests/basic_structure.sql")
    @WithMockUser("test_user_admin@example.com")
    public void addScreeningToRecord() throws Exception {
        // First get template to create / POST record
        String urlNewRecord = "/noark5v5/api/arkivstruktur" +
                "/mappe/fed888c6-83e1-4ed0-922a-bd5770af3fad/ny-registrering";

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlNewRecord)
                .contextPath("/noark5v5")
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath(
                        "$._links.['" +
                                REL_METADATA_SCREENING_METADATA + "'].['" +
                                HREF + "']").exists());

        // Next create a Record object with an associated Screening
        // Note: We are not using the result of the GET ny-registrering, but want the
        // test to check tht it works

        RecordEntity record = new RecordEntity();
        record.setTitle("Title of record");

        // Create Metadata objects
        AccessRestriction accessRestriction =
                new AccessRestriction("P", "Personalsaker");
        ScreeningDocument screeningDocument = new ScreeningDocument(
                "H", "Skjerming av hele dokumentet");

        // Create a screening object and associate it with the Record
        Screening screening = new Screening();
        screening.setAccessRestriction(accessRestriction);
        screening.setScreeningDocument(screeningDocument);
        screening.setScreeningAuthority("Unntatt etter Offentleglova");
        screening.setScreeningExpiresDate(parse("1942-07-25T00:00:00Z"));
        screening.setScreeningDuration(60);
        record.setReferenceScreening(screening);

        // Create a JSON object to POST
        JsonFactory factory = new JsonFactory();
        StringWriter jsonWriter = new StringWriter();
        JsonGenerator jgen = new JsonFactory().createGenerator(jsonWriter);
        var serializer = new RecordSerializer();
        serializer.serializeNoarkEntity(record, new LinksNoarkObject(), jgen);
        jgen.close();

        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post(urlNewRecord)
                .contextPath("/noark5v5")
                .accept(NOARK5_V5_CONTENT_TYPE_JSON)
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(jsonWriter.toString()));

        resultActions
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$." + SYSTEM_ID)
                        .exists())
                .andExpect(jsonPath("$." + TITLE)
                        .value("Title of record"));
        resultActions.andDo(document("home",
                preprocessRequest(prettyPrint()),
                preprocessResponse(prettyPrint())));

        MockHttpServletResponse response =
                resultActions.andReturn().getResponse();

        // Make sure we can retrieve the record and that the Screening
        // attributes are present
        String urlRecord = "/noark5v5/api/arkivstruktur/registrering/" +
                JsonPath.read(response.getContentAsString(), "$." + SYSTEM_ID);

        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlRecord)
                .contextPath("/noark5v5")
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        response = resultActions.andReturn().getResponse();

        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$." + SCREENING + "." +
                        ACCESS_RESTRICTION +
                        "." +
                        CODE).value("P"))
                .andExpect(jsonPath("$." + SCREENING + "." +
                        ACCESS_RESTRICTION + "." +
                        CODE_NAME).value("Personalsaker"))
                .andExpect(jsonPath("$." + SCREENING + "." +
                        SCREENING_SCREENING_DOCUMENT + "." +
                        CODE).value("H"))
                .andExpect(jsonPath("$." + SCREENING + "." +
                        SCREENING_SCREENING_DOCUMENT + "." +
                        CODE_NAME)
                        .value("Skjerming av hele dokumentet"))
                .andExpect(jsonPath("$." + SCREENING + "." +
                        SCREENING_AUTHORITY)
                        .value("Unntatt etter Offentleglova"))
                .andExpect(jsonPath("$." + SCREENING + "." +
                        SCREENING_DURATION)
                        .value(60))
                // Not picking them explicitly out as [0] [1] objects in the
                // array as the order might change later and then the test
                // will fail unnecessary
                .andExpect(jsonPath("$._links.['" +
                        REL_METADATA_SCREENING_METADATA + "'].href").exists())
                .andExpect(jsonPath("$." + SCREENING + "." +
                        SCREENING_EXPIRES_DATE).value(
                        anyOf(
                                is("1942-07-25T00:00:00Z"),
                                is("1942-07-25T00:00:00+00:00"))))
                .andExpect(jsonPath("$." + TITLE)
                        .value("Title of record"));
        resultActions.andDo(document("home",
                preprocessRequest(prettyPrint()),
                preprocessResponse(prettyPrint())));


        // Next, Create a ScreeningMetadata and POST it
        String urlNewScreeningMetadata = "/noark5v5/api/arkivstruktur/registrering/" +
                JsonPath.read(response.getContentAsString(),
                        "$." + SYSTEM_ID) + "/" + NEW_SCREENING_METADATA;
        // Make a note of the url for ScreeningMetadata associated with the
        // record. Will be used later.
        String urlScreeningMetadata = "/noark5v5/api/arkivstruktur/registrering/" +
                JsonPath.read(response.getContentAsString(),
                        "$." + SYSTEM_ID) + "/" + SCREENING_METADATA;

        ScreeningMetadataLocal screeningMetadata = new ScreeningMetadataLocal();
        screeningMetadata.setCode("NA");
        screeningMetadata.setCodeName("Skjerming navn avsender");

        // Create a JSON object to POST
        jsonWriter = new StringWriter();
        jgen = new JsonFactory().createGenerator(jsonWriter);
        var serializerSM = new ScreeningMetadataSerializer();
        serializerSM.serializeNoarkEntity(screeningMetadata, new LinksNoarkObject(), jgen);
        jgen.close();

        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post(urlNewScreeningMetadata)
                .contextPath("/noark5v5")
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(jsonWriter.toString())
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        resultActions
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$." + CODE)
                        .exists())
                .andExpect(jsonPath("$." + CODE_NAME)
                        .exists())
                .andExpect(jsonPath("$._links.['" + SELF + "'].href")
                        .exists())
                .andExpect(jsonPath(
                        "$._links.['" +
                                REL_FONDS_STRUCTURE_SCREENING_METADATA +
                                "'].['" + HREF + "']").exists());

        // Create another ScreeningMetadata and POST it
        screeningMetadata.setCode("TKL");
        screeningMetadata.setCodeName("Skjerming tittel klasse");

        // Create a JSON object to POST
        jsonWriter = new StringWriter();
        jgen = new JsonFactory().createGenerator(jsonWriter);
        serializerSM = new ScreeningMetadataSerializer();
        serializerSM.serializeNoarkEntity(screeningMetadata, new LinksNoarkObject(), jgen);
        jgen.close();

        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post(urlNewScreeningMetadata)
                .contextPath("/noark5v5")
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(jsonWriter.toString())
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        resultActions
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$." + CODE)
                        .exists())
                .andExpect(jsonPath("$." + CODE_NAME)
                        .exists())
                .andExpect(jsonPath("$._links.['" + SELF + "'].href")
                        .exists())
                .andExpect(jsonPath(
                        "$._links.['" +
                                REL_FONDS_STRUCTURE_SCREENING_METADATA +
                                "'].['" + HREF + "']").exists());

        // Check that it is possible to retrieve the two ScreeningMetadata
        // objects
        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlScreeningMetadata)
                .contextPath("/noark5v5")
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        response = resultActions.andReturn().getResponse();

        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.results", hasSize(2)))
                .andExpect(jsonPath("$.results.[0]." + CODE)
                        .exists())
                .andExpect(jsonPath("$.results.[0]." + CODE_NAME)
                        .exists())
                .andExpect(jsonPath("$.results.[1]." + CODE)
                        .exists())
                .andExpect(jsonPath("$.results.[1]." + CODE_NAME)
                        .exists())
                .andExpect(jsonPath("$.results.[0]._links.['" +
                        SELF + "'].href")
                        .exists())
                .andExpect(jsonPath(
                        "$.results.[0]._links.['" +
                                REL_FONDS_STRUCTURE_SCREENING_METADATA + "'].['" +
                                HREF + "']").exists())
                .andExpect(jsonPath("$.results.[1]._links.['" +
                        SELF + "'].href")
                        .exists())
                .andExpect(jsonPath(
                        "$.results.[1]._links.['" +
                                REL_FONDS_STRUCTURE_SCREENING_METADATA + "'].['" +
                                HREF + "']").exists());

        resultActions.andDo(document("home",
                preprocessRequest(prettyPrint()),
                preprocessResponse(prettyPrint())));

        // Next, see that we can update the ScreeningMetadata
        String urlComplete = JsonPath.read(response.getContentAsString(),
                "$.results.[0]._links.self.href");
        urlScreeningMetadata = "/noark5v5/api/arkivstruktur" +
                "/skjermingmetadata" + urlComplete.split("/noark5v5/api" +
                "/arkivstruktur/skjermingmetadata")[1];

        // Create a ScreeningMetadata object different to the previous ones
        // that we can use to override the data in nikita
        ScreeningMetadata screeningMetadata2 = new ScreeningMetadata();
        screeningMetadata2.setCode("TM1");
        screeningMetadata2.setCodeName("Skjerming tittel mappe - unntatt første linje");

        // Create a JSON object to POST
        jsonWriter = new StringWriter();
        jgen = new JsonFactory().createGenerator(jsonWriter);
        serializerSM = new ScreeningMetadataSerializer();
        serializerSM.serializeNoarkEntity(screeningMetadata, new LinksNoarkObject(), jgen);
        jgen.close();

        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .put(urlScreeningMetadata)
                .contextPath("/noark5v5")
                .header("ETAG", "\"1\"")
                .accept(NOARK5_V5_CONTENT_TYPE_JSON)
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(jsonWriter.toString()));

        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.['" + CODE + "']")
                        .value("TM1"))
                .andExpect(jsonPath("$.['" + CODE_NAME + "']")
                        .value("Skjerming tittel mappe - unntatt første linje"))
                .andExpect(jsonPath(
                        "$._links.['" +
                                REL_FONDS_STRUCTURE_SCREENING_METADATA + "'].['" +
                                HREF + "']").exists())
                .andExpect(jsonPath(
                        "$._links.['" + SELF + "'].['" + HREF + "']").exists());

        resultActions.andDo(document("home",
                preprocessRequest(prettyPrint()),
                preprocessResponse(prettyPrint())));

        // Next, check it is possible to delete the ScreeningMetadata
        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .delete(urlScreeningMetadata)
                .contextPath("/noark5v5")
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        resultActions.andExpect(status().isNoContent());
    }
}
