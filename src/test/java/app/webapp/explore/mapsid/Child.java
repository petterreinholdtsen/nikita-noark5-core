package app.webapp.explore.mapsid;


import jakarta.persistence.*;
import org.hibernate.annotations.JdbcTypeCode;

import java.io.Serializable;
import java.util.UUID;

import static jakarta.persistence.FetchType.LAZY;
import static java.sql.Types.VARCHAR;

@Entity
public class Child
        extends EntityForMappedClass
        implements Serializable {

    @Id
    @JdbcTypeCode(VARCHAR)
    @Column(name = "code", insertable = false, updatable = false,
            nullable = false)
    protected UUID code;

    @OneToOne(fetch = LAZY)
    @MapsId
    @JoinColumn(name = "code")
    private Parent parent;

    public Parent getParent() {
        return parent;
    }

    public void setParent(Parent parent) {
        this.parent = parent;
    }

    public UUID getCode() {
        return code;
    }

    public void setCode(UUID code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "Child{" +
                "code='" + code + '\'' +
                '}';
    }
}
