package app.webapp.explore.mapsid;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import org.hibernate.annotations.JdbcTypeCode;
import org.springframework.data.annotation.CreatedBy;

import java.util.UUID;

import static java.sql.Types.VARCHAR;

@Entity
public class MappedClassForChild {

    @Id
    @JdbcTypeCode(VARCHAR)
    @Column(name = "code", insertable = false, updatable = false,
            nullable = false)
    protected UUID code;

    @CreatedBy
    @Column(name = "owned_by")
    private String ownedBy;

    public String getOwnedBy() {
        return ownedBy;
    }

    public void setOwnedBy(String ownedBy) {
        this.ownedBy = ownedBy;
    }
}
