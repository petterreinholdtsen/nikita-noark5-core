package app.webapp.explore.mapsid;

import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import org.springframework.data.annotation.CreatedBy;

@MappedSuperclass
public class EntityForMappedClass {

    @CreatedBy
    @Column(name = "owned_by")
    private String ownedBy;

    public String getOwnedBy() {
        return ownedBy;
    }

    public void setOwnedBy(String ownedBy) {
        this.ownedBy = ownedBy;
    }
}
