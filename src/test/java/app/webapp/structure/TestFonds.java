package app.webapp.structure;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static app.utils.constants.Constants.*;
import static app.utils.constants.HATEOASConstants.SELF;
import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.*;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
/**
 * Internal testing currently suspended in the codebase.
 * <p>
 * Note: In the upgrade to Spring Boot 3 we experienced problems with the domain model. There is no problem with the
 * domain model when running spring boot as an application, but there seems to be a problem with the domain model
 * is used in testing. Typically, the problem is that Hibernate is not able to deal with JOINED inheritance as it is
 * specified in nikita, and prints a Wrong entity retrieved exception stating that it expects a File when it was given
 * a Record (We tried to retrieve a Record). We have tried using a discriminator column, forcing the use of a
 * discriminator column but in test mode we are not able to get the Nikita domain model
 * <p>
 * We need to finish this Spring Boot 3 branch and move forward. All tests that will not run are currently commented
 * out until we have time to fix it.
 */
//@ExtendWith({RestDocumentationExtension.class, SpringExtension.class})
//@SpringBootTest(classes = nikita.N5CoreApp.class,
//        webEnvironment = RANDOM_PORT)
public class TestFonds {

    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @BeforeEach
    public void setUp(WebApplicationContext webApplicationContext,
                      RestDocumentationContextProvider restDocumentation) {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .apply(documentationConfiguration(restDocumentation))
                .apply(springSecurity())
                .alwaysDo(document("{method-name}",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint())))
                .build();
    }

    //@Test
    public void contextLoads() {
    }

    ////@Test
    //  @WithMockUser(roles=ROLE_ADMIN)
    public void applicationRootCheck() throws Exception {

        ResultActions actions =
                mockMvc.perform(get("/")
                                .accept(NOARK5_V5_CONTENT_TYPE_JSON))
                        .andExpect(status().isOk())
                        .andExpect(content().contentType
                                (NOARK5_V5_CONTENT_TYPE_JSON))
                        .andDo(document("home",
                                preprocessRequest(prettyPrint()),
                                preprocessResponse(prettyPrint()),
                                links(atomLinks(),
                                        linkWithRel(REL_LOGIN_OAUTH2).
                                                description("The login link")
                                )
                        ));

        MockHttpServletResponse response = actions.andReturn().getResponse();
        System.out.println(response.getContentAsString());
    }

    //@Test
    public void oidcLinkPublished() throws Exception {

        ResultActions actions =
                mockMvc.perform(get("/")
                        .accept(NOARK5_V5_CONTENT_TYPE_JSON))
                        .andExpect(status().isOk())
                        .andExpect(content().contentType
                                (NOARK5_V5_CONTENT_TYPE_JSON))
                        .andDo(document("home",
                                preprocessRequest(prettyPrint()),
                                preprocessResponse(prettyPrint()),
                                links(halLinks(),
                                        linkWithRel
                                                (REL_LOGIN_OIDC).
                                                description("Get login " +
                                                        "information"),
                                        linkWithRel(SELF).
                                                description("Self REL")
                                )
                        ));

        MockHttpServletResponse response = actions.andReturn().getResponse();
        System.out.println(response.getContentAsString());
    }
}
