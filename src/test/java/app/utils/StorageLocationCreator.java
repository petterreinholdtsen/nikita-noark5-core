package app.utils;

import app.domain.noark5.secondary.StorageLocation;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.secondary.StorageLocationSerializer;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;
import java.io.StringWriter;

import static app.utils.TestConstants.STORAGE_LOCATION_TEST;
import static app.utils.TestConstants.STORAGE_LOCATION_TEST_UPDATED;

public final class StorageLocationCreator {

    /**
     * Create a default StorageLocation for testing purposes
     *
     * @return a StorageLocation with all values set
     */
    public static StorageLocation createStorageLocation() {
        StorageLocation storageLocation = new StorageLocation();
        storageLocation.setStorageLocation(STORAGE_LOCATION_TEST);
        return storageLocation;
    }

    public static String createStorageLocationAsJSON() throws IOException {
        return createStorageLocationAsJSON(createStorageLocation());
    }

    public static String createUpdatedStorageLocationAsJSON() throws IOException {
        StorageLocation storageLocation = createStorageLocation();
        storageLocation.setStorageLocation(STORAGE_LOCATION_TEST_UPDATED);
        return createStorageLocationAsJSON(storageLocation);
    }

    public static String createStorageLocationAsJSON(StorageLocation storageLocation) throws IOException {
        StringWriter jsonWriter = new StringWriter();
        JsonGenerator jgen = new JsonFactory().createGenerator(jsonWriter);
        var serializer = new StorageLocationSerializer();
        serializer.serializeNoarkEntity(storageLocation, new LinksNoarkObject(), jgen);
        jgen.writeEndObject();
        jgen.close();
        return jsonWriter.toString();
    }
}
