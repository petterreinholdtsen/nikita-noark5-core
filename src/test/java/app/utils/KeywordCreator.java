package app.utils;

import app.domain.noark5.secondary.Keyword;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.secondary.KeywordSerializer;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;
import java.io.StringWriter;

import static app.utils.TestConstants.KEYWORD_TEST;
import static app.utils.TestConstants.KEYWORD_TEST_UPDATED;

public final class KeywordCreator {

    /**
     * Create a default Keyword for testing purposes
     *
     * @return a Keyword with all values set
     */
    public static Keyword createKeyword() {
        Keyword keyword = new Keyword();
        keyword.setKeyword(KEYWORD_TEST);
        return keyword;
    }

    public static String createKeywordAsJSON() throws IOException {
        return createKeywordAsJSON(createKeyword());
    }

    public static String createUpdatedKeywordAsJSON() throws IOException {
        Keyword keyword = createKeyword();
        keyword.setKeyword(KEYWORD_TEST_UPDATED);
        return createKeywordAsJSON(keyword);
    }

    public static String createKeywordAsJSON(Keyword keyword) throws IOException {
        StringWriter jsonWriter = new StringWriter();
        JsonGenerator jgen = new JsonFactory().createGenerator(jsonWriter);
        var serializer = new KeywordSerializer();
        serializer.serializeNoarkEntity(keyword, new LinksNoarkObject(), jgen);
        jgen.close();
        return jsonWriter.toString();
    }
}
