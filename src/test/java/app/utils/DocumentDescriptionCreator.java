package app.utils;

import app.domain.noark5.DocumentDescription;
import app.domain.noark5.metadata.AssociatedWithRecordAs;
import app.domain.noark5.metadata.DocumentStatus;
import app.domain.noark5.metadata.DocumentType;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.DocumentDescriptionSerializer;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;
import java.io.StringWriter;

import static app.utils.TestConstants.*;

public final class DocumentDescriptionCreator {

    /**
     * Create a default DocumentDescription for testing purposes
     *
     * @return a DocumentDescription with all values set
     */
    public static DocumentDescription createDocumentDescription() {
        DocumentDescription documentDescription = new DocumentDescription();
        documentDescription.setTitle(TITLE_TEST);
        AssociatedWithRecordAs associatedWithRecordAs =
                new AssociatedWithRecordAs();
        associatedWithRecordAs.setCode(DOCUMENT_ASS_REC_CODE_TEST);
        associatedWithRecordAs.setCodeName(DOCUMENT_ASS_REC_CODE_NAME_TEST);
        documentDescription.setAssociatedWithRecordAs(associatedWithRecordAs);
        DocumentType documentType = new DocumentType();
        documentType.setCode(DOCUMENT_TYPE_CODE_TEST);
        documentType.setCodeName(DOCUMENT_TYPE_CODE_NAME_TEST);
        documentDescription.setDocumentType(documentType);
        DocumentStatus documentStatus = new DocumentStatus();
        documentStatus.setCode(DOCUMENT_STATUS_CODE_TEST);
        documentStatus.setCodeName(DOCUMENT_STATUS_CODE_NAME_TEST);
        documentDescription.setDocumentStatus(documentStatus);
        documentDescription.setStorageLocation(STORAGE_LOCATION_TEST);
        return documentDescription;
    }

    public static String createDocumentDescriptionAsJSON() throws IOException {
        return createDocumentDescriptionAsJSON(createDocumentDescription());
    }

    public static String createDocumentDescriptionAsJSON(DocumentDescription documentDescription) throws IOException {
        StringWriter jsonWriter = new StringWriter();
        JsonGenerator jgen = new JsonFactory().createGenerator(jsonWriter);
        DocumentDescriptionSerializer serializer = new DocumentDescriptionSerializer();
        serializer.serializeNoarkEntity(documentDescription, new LinksNoarkObject(), jgen);
        jgen.close();
        return jsonWriter.toString();
    }
}
