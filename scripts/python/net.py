import logging
import urllib3
from urllib.error import HTTPError, URLError

http = urllib3.PoolManager()

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)


def do_post_request(url, body, headers):
    try:

        result = http.request('POST', url, headers=headers, body=body)
        return result
    except HTTPError as error:
        print(error.status, error.reason)
    except URLError as error:
        print(error.reason)
    except TimeoutError:
        print("Request timed out")


def do_get_request(url, headers):
    try:
        response = http.request('GET', url, headers=headers)
        return response
    except HTTPError as error:
        print(error.status, error.reason)
    except URLError as error:
        print(error.reason)
    except TimeoutError:
        print("Request timed out")
