import json
import logging
import urllib

from constants import *
from net import *

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)


# There are two separate approaches to login, keycloak admin and nikita user. They are
# practically the same code but I foresee later that they will be different.

def login_keycloak_admin(url_login, client_id, username, password):
    return do_login_user(url_login, client_id, username, password)


def login_nikita_user(url_login, client_id, username, password):
    return do_login_user(url_login, client_id, username, password)


def do_login_user(url_login, client_id, username, password):
    body = "client_id=" + client_id + "&username=" + username + "&password=" + \
           password + "&grant_type=password"
    headers = {'Content-type': 'application/x-www-form-urlencoded'}
    response = do_post_request(url_login, body, headers)
    if response.status != 200:
        logging.error(LOGIN_NOT_OK)
        raise Exception(LOGIN_NOT_OK)
    response_data = json.loads(response.data)
    logging.info(LOGIN_OK.format(response_data['expires_in'], response_data['access_token']))
    return response_data['access_token']


def create_keycloak_roles(keycloak_admin_header):
    for role in roles:
        role_payload = {
            'name': role,
            'description': 'Role used in Noark'
        }
        url_create_role = url_realm + realm_name + '/roles'
        response = do_post_request(url_create_role, json.dumps(role_payload),
                                   keycloak_admin_header)
        if response.status == 201:
            logging.info(ROLE_CREATE_OK.format(role, response.status))
        else:
            logging.error(ROLE_CREATE_NOT_OK.format(role, response.status))


#            raise Exception(ROLE_CREATE_NOT_OK.format(role, response.status))

def create_keycloak_nikita_admin_user(keycloak_admin_header, username, password):
    user_payload = {
        "username": username,
        "enabled": True,
        "emailVerified": True,
        "firstName": "Frank",
        "lastName": "Grimes",
        "email": username,
        "credentials": [
            {"type": "password",
             "value": password,
             "temporary": False}
        ],
        "realmRoles": roles,
        "clientRoles": {
            "account": roles + ["view-profile", "manage-account"]
        }
    }
    create_keycloak_user(keycloak_admin_header, user_payload)


def create_keycloak_general_user(keycloak_admin_header):
    for user_id in range(1, total_nikita_user_to_create):
        username = nikita_username_template.format(str(user_id))
        user_payload = {
            "username": username,
            "enabled": True,
            "emailVerified": True,
            "firstName": "Frank",
            "lastName": "Grimes",
            "email": nikita_username_template.format(str(user_id)),
            "credentials": [
                {"type": "password",
                 "value": nikita_password_template.format(str(user_id)),
                 "temporary": False}
            ],
            "realmRoles": roles,
            "clientRoles": {
                "account": roles + ["view-profile", "manage-account"]
            }
        }
        create_keycloak_user(keycloak_admin_header, user_payload)


#            raise Exception(USER_CREATE_NOT_OK.format(username, response.status))

def create_keycloak_user(keycloak_admin_header, user_payload):
    url_create_user = url_realm + realm_name + '/users'
    response = do_post_request(url_create_user, json.dumps(user_payload),
                               keycloak_admin_header)
    if response.status == 201:
        logging.info(USER_CREATE_OK.format(user_payload['username'], response.status))
    else:
        logging.error(USER_CREATE_NOT_OK.format(user_payload['username'], response.status))


def get_role_id(role, keycloak_admin_header_get):
    url_role = url_realm + realm_name + '/roles/' + role
    response = do_get_request(url_role, keycloak_admin_header_get)
    if response.status == 200:
        logging.info(GET_ROLE_OK.format(role, response.status))
        return json.loads(response.data)['id']
    else:
        logging.error(GET_ROLE_NOT_OK.format(role, response.status))
        raise Exception(GET_ROLE_NOT_OK.format(role, response.status))


def get_user_id(username, keycloak_admin_header_get):
    url_user_id = url_realm + realm_name + '/users?username=' + urllib.parse.quote_plus(
        username)
    response = do_get_request(url_user_id, keycloak_admin_header_get)
    if response.status == 200:
        logging.info(GET_USER_OK.format(username, response.status))
        user_results = json.loads(response.data)
        if len(user_results) == 1:
            return user_results[0]['id']
        else:
            logging.error(TOO_MANY_USERS.format(str(len(user_results)), username,
                                                response.status))
            raise Exception(TOO_MANY_USERS.format(str(len(user_results)), username,
                                                  response.status))
    else:
        logging.error(GET_USER_NOT_OK.format(username, response.status))
        raise Exception(GET_USER_NOT_OK.format(username, response.status))


def add_keycloak_users_to_role(keycloak_admin_header_get, keycloak_admin_header):
    for role in roles:
        role_id = get_role_id(role, keycloak_admin_header_get)
        # Create the general users
        for user_id_counter in range(1, total_nikita_user_to_create):
            user_id = get_user_id(nikita_username_template.format(str(user_id_counter)),
                                  keycloak_admin_header_get)
            do_add_keycloak_users_to_role(role, role_id, user_id, keycloak_admin_header)

        # Create the admin user
        user_id = get_user_id(nikita_admin_username, keycloak_admin_header_get)
        do_add_keycloak_users_to_role(role, role_id, user_id, keycloak_admin_header)


def do_add_keycloak_users_to_role(role, role_id, user_id, keycloak_admin_header):
    user_role_payload = [{
        'id': role_id,
        'name': role,
        'description': role,
        'composite': False,
        'clientRole': False,
        'containerId': realm_name.upper()
    }]
    # TODO: Remove upper??
    response = do_post_request(url_add_user_to_role.format(realm_name, user_id),
                               json.dumps(user_role_payload),
                               keycloak_admin_header)
    if response.status == 204:
        logging.info(ADD_USER_ROLE_OK.format(user_id, role, response.status))
    else:
        logging.error(ADD_USER_ROLE_NOT_OK.format(user_id, role, response.status))
        raise Exception(ADD_USER_ROLE_NOT_OK.format(user_id, role, response.status))


def create_keycloak_client(keycloak_admin_header):
    client_payload = {
        'id': nikita_client,
        'name': nikita_client,
        'description': 'General nikita client',
        'directAccessGrantsEnabled': True,
        'publicClient': True,
        'webOrigins': ['*']
    }

    url_create_client = url_realm + realm_name + '/clients'
    response = do_post_request(url_create_client, json.dumps(client_payload),
                               keycloak_admin_header)
    if response.status == 201:
        logging.info(CLIENT_CREATE_OK.format(nikita_client, response.status))
    else:
        logging.error(CLIENT_CREATE_NOT_OK.format(nikita_client, response.status))
        # raise Exception(CLIENT_CREATE_NOT_OK.format(nikita_client, response.status))


def create_keycloak_realm(keycloak_admin_header):
    realm_payload = {'id': realm_name,
                     'realm': realm_name,
                     'displayName': 'Recordkeeping Realm',
                     'accessTokenLifespan': 43200,
                     'enabled': True,
                     'sslRequired': 'external',
                     'registrationAllowed': False,
                     'loginWithEmailAllowed': True,
                     'duplicateEmailsAllowed': False,
                     'resetPasswordAllowed': False,
                     'editUsernameAllowed': False,
                     'bruteForceProtected': True
                     }

    response = do_post_request(url_realm, json.dumps(realm_payload), keycloak_admin_header)
    if response.status == 201:
        logging.info(REALM_CREATE_OK.format(realm_name, response.status))
    else:
        logging.error(REALM_CREATE_NOT_OK.format(realm_name, response.status))

#        raise Exception(REALM_CREATE_NOT_OK.format(realm_name, response.status))
