import json
import logging
from datetime import date

from constants import *
from net import *

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)


def create_nikita_general_users(nikita_admin_header):
    for user_id in range(1, total_nikita_user_to_create):
        user_payload = create_nikita_user_payload(
            nikita_username_template.format(str(user_id)),
            nikita_password_template.format(str(user_id)))
        create_nikita_user(nikita_admin_header, user_payload)


def create_nikita_admin_user(nikita_admin_header, username, password):
    user_payload = create_nikita_user_payload(username, password)
    create_nikita_user(nikita_admin_header, user_payload)


def create_nikita_user_payload(username, password):
    return {
        "brukerNavn": username,
        "passord": password,
        "fornavn": "Frank",
        "etternavn": "Grimes"
    }


def create_series_payload(title):
    return {
        'tittel': title,
        'arkivdelstatus': {
            'kode': 'A',
            'kodenavn': 'Aktiv periode'
        }
    }


def get_application_root(url, nikita_user_header):
    response = do_get_request(url, nikita_user_header)
    if response.status == 200:
        logging.info(NIKITA_APPLICATION_ROOT_OK.format(url, response.status))
        return json.loads(response.data)
    else:
        logging.error(NIKITA_APPLICATION_ROOT_NOT_OK.format(url, response.status))


def create_fonds(url, organsation_name, nikita_user_header):
    fonds_payload = {
        'tittel': organsation_name + ' arkiv ' + str(date.today())
    }
    response = do_post_request(url, json.dumps(fonds_payload), nikita_user_header)
    if response.status == 201:
        logging.info(NIKITA_CREATE_FONDS_OK.format(url, response.status))
        return json.loads(response.data)
    else:
        logging.error(NIKITA_CREATE_FONDS_NOT_OK.format(url, response.status))


def create_fonds_creator(fonds_url, fonds_creator_name, nikita_user_header):
    fonds_creator_payload = {
        'arkivskaperID': fonds_creator_name,
        'arkivskaperNavn': fonds_creator_name
    }
    response = do_post_request(fonds_url, json.dumps(fonds_creator_payload), nikita_user_header)
    if response.status == 201:
        logging.info(NIKITA_CREATE_FONDS_CREATOR_OK.format(fonds_url, response.status))
    else:
        logging.error(NIKITA_CREATE_FONDS_CREATOR_NOT_OK.format(fonds_url, response.status))


def create_email_series(create_series_url, organisation, nikita_user_header):
    email_series_payload = create_series_payload("Postmottakorganisasjon" + organisation)
    create_series(create_series_url, email_series_payload, nikita_user_header)


def create_regular_series(create_series_url, organisation, nikita_user_header):
    series_payload = create_series_payload('Saksbehandling for organisasjon ' + organisation)
    create_series(create_series_url, series_payload, nikita_user_header)


def create_series(create_series_url, series_payload, nikita_user_header):
    response = do_post_request(create_series_url, json.dumps(series_payload), nikita_user_header)
    if response.status == 201:
        logging.info(NIKITA_CREATE_SERIES_OK.format(create_series_url, response.status))
    else:
        logging.error(NIKITA_CREATE_SERIES_NOT_OK.format(create_series_url, response.status))
