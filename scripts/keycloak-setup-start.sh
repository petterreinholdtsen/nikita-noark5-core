#!/bin/sh

set -e

ver=22.0.1

if [ -z "${KEYCLOAK_ADMIN}" ] ; then
    export KEYCLOAK_ADMIN=admin
fi

if [ -z "${KEYCLOAK_ADMIN_PASSWORD}" ] ; then
    export KEYCLOAK_ADMIN_PASSWORD=admin
fi

cleanup() {
    echo exiting, killing keycloak
    if [ "$kcpid" ]; then
	kill $kcpid
	kcpid=""
    fi
}
trap cleanup QUIT EXIT INT

wget https://github.com/keycloak/keycloak/releases/download/${ver}/keycloak-${ver}.tar.gz
tar zxvf keycloak-${ver}.tar.gz

# Start keycloak
(cd keycloak-${ver} && bin/kc.sh start-dev) &
kcpid=$!

# Wait for keycloak to respont on the API
KEYCLOAK_URI=http://localhost:8080/
until $(curl --output /dev/null --silent --head --fail $KEYCLOAK_URI); do
   printf '.'
   sleep 5
done
curl --output /dev/null --silent --head --fail $KEYCLOAK_URI

scripts/populate_keycloak.sh

wait $kcpid
